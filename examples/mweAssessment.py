#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 15:37:35 2020

The latest cleanest mwe for the Assessment part. Get back to it when needed!

@author: Nicolas
"""

# basic 
import os
os.chdir('..')

import matplotlib.pyplot as plt

# used for loading data 
from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq
from source.modDictLoader import loadCostData, loadEmissionData

# used for sizing and analysis
from source.modAnalysisFState import getPowers
from source.modSizingTier0 import sizeAircraft

# used for assessment 
from source.modAssessmentTier0 import specificCostKWh, specificImpactKWh, specificFunctionalUnit, specificMissionFuel


#%%============================================================================
#    L O A D   D I C T I O N A R I E S
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 20.}, 
                        **{'ident': 'Sizing', 'cruiseRange': 250 * 1000,
                           'cruiseVelo': 250/3.6, 'hoverTime': 1.5 * 60.,
                           'nhover': 1, 'fUtil': 0.75})

# LOAD AIRCRAFT DESIGN
dDesign = loadAircraftDesign(**{'ident': 'VectoredThrust', 
                                'pl': 0.0225, 'ltod': 15.})


# LOAD ENERGY STORAGE TYPES
dBattery = loadFueltype('battery', **{'ident': 'liion',
                                      'esp': 260/1.2, 'eta': 0.8, 
                                      'eta_chg': 0.95, 'eta_grd': 0.97})

dFuelCell = loadFueltype('fuelcell')

dHybrid = loadFueltype('hybrid', **{'fuelcell': dFuelCell,
                                    'battery': dBattery,
                                    })
       
#[print(k,v) for k,v in dHybrid['fuelcell'].items()]

#%%============================================================================
#    P R E - P R O C :  L O A D  A N D  N O R M  A S S E S S M E N T - D A T A
#%=============================================================================

dArgs = {'fuelcell': dFuelCell, 'battery': dBattery}

costDict = loadCostData()
print('\nCosts, Raw Data')
[print('{:20s} {:7.3f} {:5s}'.format(k,v[0],v[1])) for k,v in costDict.items()]

costDict = specificCostKWh(costDict, dArgs)
print('\nCosts, Specific')
[print('{:20s} {:7.3f} € per kWh'.format(k,v)) for k,v in costDict.items()]
print()
print('#######################################################################')

impactDict = loadEmissionData()
#print('\nImpact, Raw Data')
#[print('{:20s} {:7.3f} {:5s}'.format(k,v[0],v[1])) for k,v in impactDict.items()]

impactDict = specificImpactKWh(impactDict, dArgs)
print('\nImpact, Specific')
[print('{:20s} {:7.3f} g per kWh'.format(k,v)) for k,v in impactDict.items()]
print()
print('#######################################################################')

#%%============================================================================
#    S T E P   B Y   S T E P 
#%=============================================================================


#WITH A BATTERY
# dFuel = dBattery
# dSizing = {'design': dDesign, 'mission': dMission, 'fuel': dBattery}
# dSizing = sizeAircraft(dSizing)
# dPowers = getPowers(dSizing)

# WITH A FUEL CELL
dFuel = dFuelCell
dSizing = {'design': dDesign, 'mission': dMission, 'fuel': dFuelCell}
dSizing = sizeAircraft(dSizing)
dPowers = getPowers(dSizing)

# WITH A HYBRID MODEL
# dFuel = dHybrid
# dSizing = {'design': dDesign, 'mission': dMission, 'fuel': dHybrid}
# dSizing = sizeAircraft(dSizing)
# dPowers = getPowers(dSizing)


specCost = specificMissionFuel(costDict, dFuel, dMission, dPowers)
dCost = specificFunctionalUnit('VKT', specCost, dMission)
#dCost = specificFunctionalUnit('PKT', specCost, dMission)
#dCost = specificFunctionalUnit('LKT', specCost, dMission)

specImpact = specificMissionFuel(impactDict, dFuel, dMission, dPowers)
dImpc = specificFunctionalUnit('VKT', specImpact, dMission)
dCost = specificFunctionalUnit('PKT', specImpact, dMission)
#dCost = specificFunctionalUnit('LKT', specImpact, dMission)

import sys
sys.exit()

#%%============================================================================
#    I N T E G R A T E D  S I Z I N G  A N D   A S S E S S M E N T
#%=============================================================================


#%%============================================================================
#    P E R F O R M  S I Z I N G
#%=============================================================================



