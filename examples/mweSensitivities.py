#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:48:13 2020

@author: Nicolas
"""


import sys

import os
os.chdir('..')

import numpy as np
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq
#from devs.devVerifyMassModel import getAircraftMass
#from source.modSizingTier0 import getAircraftMass
from source.modInterface import getAircraftMass

from source.util import loadTUMcolors



# used for assessment
# from source.modSizingTier0 import getAircraftMass

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dRequiredMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 10.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 161 * 1000,
                                      'cruiseVelo': 60.0, 'hoverTime': 2 * 60.})

# LOAD AIRCRAFT DESIGN
dDesignMTR = loadAircraftDesign(**{'ident': 'Turboshaft Helicopter', 
                                        'pl': 0.0465, 'ltod': 3.1,'fStruct': 0.735})
dDesignTLT = loadAircraftDesign(**{'ident': 'Turboshaft Tiltrotor', 
                                        'pl': 0.0375, 'ltod': 5.3,'fStruct': 0.809})
dDesignEDP = loadAircraftDesign(**{'ident': 'Electric Lift and Cruise', 
                                        'pl': 0.0405, 'ltod': 11.,'fStruct': 0.47})

# LOAD ENERGY STORAGE TYPES
dBatteryLiIon = loadFueltype('battery', **{'ident': 'liion',
                                           'esp': 250/1.2, 'eta': 0.68})

dJetAMTR = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.365})

dJetATLT = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.4150})

def sensitivityAnalysisOAT(fun, dCase, dSample):
    # Framework for One-at-a-Time sampling sensitivity analysis
    dResult = {}
    for k0, v0 in dSample.items():
        dResult[k0] = {}
        #print('# Variation of', k0, 'parameters')
        for k1, v1, in v0.items():
            #print('## Variation of', k1)
            listRes = []
            for i, value in enumerate(v1):
                baseline = dCase[k0][k1]
                dCase[k0][k1] = value
                listRes.append(fun(dCase))
                dCase[k0][k1] = baseline
                #if dCase['fuel']['fueltype'] == 'battery': print(listRes[-1])
            dResult[k0][k1] = listRes                        
    return dResult

# def plotter(dCase, dResult):
#     fig = plt.figure(figsize=(4,4))

#     plt.title(dCase['design']['ident'])
#     if dCase['design']['ident'] == 'Turboshaft Helicopter': plt.ylabel('Design gross mass (kg)')

#     for k0, v0 in dResult.items():
#         for k1, v1, in v0.items():
#             if k1 == 'fStruct': 
#                 clr = tumClr['TUMdarker100']
#                 lbl = 'EW/DGW'
#             elif k1 == 'ltod': 
#                 clr = tumClr['TUMpptOrange']
#                 lbl = 'L/D'
#             elif k1 == 'pl':
#                 clr = tumClr['TUMpptGreen']
#                 lbl = 'PL'
#             else:
#                 clr = tumClr['TUMpptGray']
#                 if dCase['fuel']['fueltype'] == 'battery':
#                     lbl = 'Espec'
#                 elif dCase['fuel']['fueltype'] == 'kerosene':
#                     lbl = 'SFC'
#             plt.plot(range(len(v1)), v1, label=lbl, color=clr)
#     plt.grid()
#     plt.legend()
#     plt.xlim(0.,10.)
#     plt.xticks([0.,2.5,5.0,7.5,10.], ['-10%','-5%','0%','+5%','+10%'])
#     plt.xlabel('Variation from baseline')
#     plt.ylim(1000.,5000.)
#     plt.show()
   
def plotSimple(i, axis, interval, dCase, dResult):
    
    plt.title(dCase['design']['ident'])
    axis.tick_params(length=0.0)
    axis.set_yticklabels([])

    for k0, v0 in dResult.items():
        for k1, v1, in v0.items():
            if not k0 == 'mission':
                axis.plot(interval, v1, color='gray', label=k1, linewidth=0.8)
            else:
                axis.plot(interval, v1, label=k1, linewidth=0.8)

    if i == 0: 
        axis.set_ylabel('Design gross mass (tons)')
        axis.set_yticklabels([r'$1.0$',r'$2.0$',r'$3.0$',r'$4.0$',r'$5.0$'])
        axis.legend(fontsize='x-small', loc='upper left')
    elif i == 1: 
        axis.set_xlabel('Variation from baseline '+r'$(\%)$')
        axis.legend(fontsize='x-small', loc='upper left')
    elif i == 2:
        axis.legend(fontsize='x-small', loc='lower left')
    axis.grid()
    axis.set_xlim(-0.11,0.11)
    axis.set_xticks([-0.1,-0.05,0.,0.05,0.1])
    #axis.set_xticklabels([r'$-10\%$',r'$-5\%$',r'$0\%$',r'$+5\%$',r'$+10\%$'], ha='center')
    axis.set_xticklabels([r'$-10.$',r'$-5.$',r'$0.$',r'$+5.$',r'$+10.$'])
    axis.set_ylim(1000.,5000.)    

def plotAxis(i, axis, interval, dCase, dResult):
    
    plt.title(dCase['design']['ident'])
    axis.tick_params(length=0.0)
    axis.set_yticklabels([])



    for k0, v0 in dResult.items():
        for k1, v1, in v0.items():
            if k1 == 'fStruct': 
                clr = tumClr['TUMdarker100']
                lbl = 'EW/DGW'
            elif k1 == 'ltod': 
                clr = tumClr['TUMpptOrange']
                lbl = 'L/D'
            elif k1 == 'pl':
                clr = tumClr['TUMpptGreen']
                lbl = 'PL'
            else:
                clr = tumClr['TUMpptGray']
                if dCase['fuel']['fueltype'] == 'battery':
                    lbl = 'Espec'
                elif dCase['fuel']['fueltype'] == 'kerosene':
                    lbl = 'SFC'
            print(clr)
            axis.plot(interval, v1, label=lbl, color=clr, linewidth=0.8)

    if i == 0: 
        axis.set_ylabel('Design gross mass (tons)')
        axis.set_yticklabels([r'$1.0$',r'$2.0$',r'$3.0$',r'$4.0$',r'$5.0$'])
        axis.legend(fontsize='x-small', loc='upper left')
    elif i == 1: 
        axis.set_xlabel('Variation from baseline '+r'$(\%)$')
        axis.legend(fontsize='x-small', loc='upper left')
    elif i == 2:
        axis.legend(fontsize='x-small', loc='lower left')
        
    axis.grid()
    axis.set_xlim(-0.11,0.11)
    axis.set_xticks([-0.1,-0.05,0.,0.05,0.1])
    #axis.set_xticklabels([r'$-10\%$',r'$-5\%$',r'$0\%$',r'$+5\%$',r'$+10\%$'], ha='center')
    axis.set_xticklabels([r'$-10.$',r'$-5.$',r'$0.$',r'$+5.$',r'$+10.$'])
    axis.set_ylim(1000.,5000.)

    
XSMALL = 6
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
       
iSamples = 25

lower = -0.12
bLow = 1.0+lower
upper = 0.12
bUp = 1.0+upper

interval = np.linspace(lower,upper,iSamples)

dCaseMTR = {'design': dDesignMTR, 'mission': dRequiredMission, 'fuel': dJetAMTR}
dVariMTR = {'design': {'ltod': np.linspace(dCaseMTR['design']['ltod'] * bLow, dCaseMTR['design']['ltod'] * bUp, iSamples), 
                      'pl': np.linspace(dCaseMTR['design']['pl'] * bLow, dCaseMTR['design']['pl'] * bUp, iSamples), 
                      'fStruct': np.linspace(dCaseMTR['design']['fStruct'] * bLow, dCaseMTR['design']['fStruct'] * bUp, iSamples)},
           'fuel': {'sfc': np.linspace(dCaseMTR['fuel']['sfc'] * bLow, dCaseMTR['fuel']['sfc'] * bUp, iSamples)},
           'mission': {'cruiseVelo': np.linspace(dCaseMTR['mission']['cruiseVelo'] * bLow, dCaseMTR['mission']['cruiseVelo'] * bUp, iSamples),
                       'cruiseRange': np.linspace(dCaseMTR['mission']['cruiseRange'] * bLow, dCaseMTR['mission']['cruiseRange'] * bUp, iSamples),
                       'hoverTime': np.linspace(dCaseMTR['mission']['hoverTime'] * bLow, dCaseMTR['mission']['hoverTime'] * bUp, iSamples)}}

dCaseTLT = {'design': dDesignTLT, 'mission': dRequiredMission, 'fuel': dJetATLT}
dVariTLT = {'design': {'ltod': np.linspace(dCaseTLT['design']['ltod'] * bLow, dCaseTLT['design']['ltod'] * bUp, iSamples), 
                      'pl': np.linspace(dCaseTLT['design']['pl'] * bLow, dCaseTLT['design']['pl'] * bUp, iSamples), 
                      'fStruct': np.linspace(dCaseTLT['design']['fStruct'] * bLow, dCaseTLT['design']['fStruct'] * bUp, iSamples)},
           'fuel': {'sfc': np.linspace(dCaseTLT['fuel']['sfc'] * bLow, dCaseTLT['fuel']['sfc'] * bUp, iSamples)},
           'mission': {'cruiseVelo': np.linspace(dCaseMTR['mission']['cruiseVelo'] * bLow, dCaseMTR['mission']['cruiseVelo'] * bUp, iSamples),
                       'cruiseRange': np.linspace(dCaseMTR['mission']['cruiseRange'] * bLow, dCaseMTR['mission']['cruiseRange'] * bUp, iSamples),
                       'hoverTime': np.linspace(dCaseMTR['mission']['hoverTime'] * bLow, dCaseMTR['mission']['hoverTime'] * bUp, iSamples)}}

dCaseEDP = {'design': dDesignEDP, 'mission': dRequiredMission, 'fuel': dBatteryLiIon}
dVariEDP = {'design': {'ltod': np.linspace(dCaseEDP['design']['ltod'] * bLow, dCaseEDP['design']['ltod'] * bUp, iSamples), 
                       'pl': np.linspace(dCaseEDP['design']['pl'] * bLow, dCaseEDP['design']['pl'] * bUp, iSamples), 
                       'fStruct': np.linspace(dCaseEDP['design']['fStruct'] * bLow, dCaseEDP['design']['fStruct'] * bUp, iSamples)},
           'fuel': {'esp': np.linspace(dCaseEDP['fuel']['esp'] * bLow, dCaseEDP['fuel']['esp'] * bUp, iSamples)},
           'mission': {'cruiseVelo': np.linspace(dCaseMTR['mission']['cruiseVelo'] * bLow, dCaseMTR['mission']['cruiseVelo'] * bUp, iSamples),
                       'cruiseRange': np.linspace(dCaseMTR['mission']['cruiseRange'] * bLow, dCaseMTR['mission']['cruiseRange'] * bUp, iSamples),
                       'hoverTime': np.linspace(dCaseMTR['mission']['hoverTime'] * bLow, dCaseMTR['mission']['hoverTime'] * bUp, iSamples)}}
         
# dmassOAT = sensitivityAnalysisOAT(getAircraftMass, dBaseline, dVari)
# plotter(dmassOAT)

dsensAnsysMTR = sensitivityAnalysisOAT(getAircraftMass, dCaseMTR, dVariMTR)
dsensAnsysTLT = sensitivityAnalysisOAT(getAircraftMass, dCaseTLT, dVariTLT)
dsensAnsysEDP = sensitivityAnalysisOAT(getAircraftMass, dCaseEDP, dVariEDP)

tumClr = loadTUMcolors()

plt.rc('text', usetex=True)
plt.rc('font', family='serif')   

figure = plt.figure(figsize=(5.809,1.7))
grid = gs.GridSpec(1, 3, bottom=0.05, right=0.95, left=0.1, top=0.95, wspace=0.1)

plotSimple(0, plt.subplot(grid[0]), interval, dCaseMTR, dsensAnsysMTR)
plotSimple(1, plt.subplot(grid[1]), interval, dCaseTLT, dsensAnsysTLT)
plotSimple(2, plt.subplot(grid[2]), interval, dCaseEDP, dsensAnsysEDP)

#plotAxis(0, plt.subplot(grid[0]), interval, dCaseMTR, dsensAnsysMTR)
#plotAxis(1, plt.subplot(grid[1]), interval, dCaseTLT, dsensAnsysTLT)
#plotAxis(2, plt.subplot(grid[2]), interval, dCaseEDP, dsensAnsysEDP)
#figure.savefig('test.pdf', bbox_inches='tight', dpi=250.)
plt.show()

# plotter(dCaseMTR, dsensAnsysMTR)
# plotter(dCaseTLT, dsensAnsysTLT)
# plotter(dCaseEDP, dsensAnsysEDP)


#sample_ltod = sample('uniform', [dBaseline['design']['ltod'] * 0.9, dBaseline['design']['ltod'] * 1.1], size=iSamples)
# sample_sw = np.linspace(dBaseline['design']['fStruct'] * 0.9, dBaseline['design']['fStruct'] * 1.1, iSamples)
# sample_ltod = np.linspace(dBaseline['design']['ltod'] * 0.9, dBaseline['design']['ltod'] * 1.1, iSamples)
# sample_pl = np.linspace(dBaseline['design']['pl'] * 0.9, dBaseline['design']['pl'] * 1.1, iSamples)
# #sample_eta = np.linspace(dBaseline['fuel']['eta'] * 0.9, dBaseline['fuel']['eta'] * 1.1, iSamples)
# sample_esp = np.linspace(dBaseline['fuel']['esp'] * 0.9, dBaseline['fuel']['esp'] * 1.1, iSamples)
# sample_sfc = np.linspace(dBaseline['fuel']['sfc'] * 0.9, dBaseline['fuel']['sfc'] * 1.1, iSamples)

# sample_rng = np.linspace(dBaseline['mission']['cruiseRange'] * 0.1, dBaseline['mission']['cruiseRange'] * 1.5, iSamples)
# sample_hov = np.linspace(dBaseline['mission']['hoverTime'] * 0.1, dBaseline['mission']['hoverTime'] * 1.5, iSamples)

# plt.plot(range(iSamples), dmassOAT['design']['fStruct'], label='NA: f_S', color='k', linestyle='-')
# plt.plot(range(iSamples), dmassOAT['mission']['cruiseRange'], label='NA: f_S', color='b', linestyle='-')
# plt.plot(range(iSamples), dmassOAT['mission']['hoverTime'], label='NA: f_S', color='g', linestyle='-')
# plt.plot(range(iSamples), dassWirth['design']['fStruct'], label='DW: f_S', color='k', linestyle='--')
# plt.plot(range(iSamples), dassWirth['mission']['cruiseRange'], label='DW: f_S', color='b', linestyle='--')
# plt.plot(range(iSamples), dassWirth['mission']['hoverTime'], label='DW: f_S', color='g', linestyle='--')
# plt.grid()
# plt.legend()
# plt.ylim(1000.,5000.)
# plt.show()

# ---- sizing 
#dgmAircraft = getAircraftMass(dSizingCase) # (deterministic)

