#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 16:31:14 2019

@author: gu95pug
"""

#%%============================================================================
#    D E V E L O P M E N T
#%============================================================================= 

# basic 
import os
os.chdir('..')

import numpy as np

import sys

from source.util import loadTUMcolors

from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq, loadEmissionData
from source.modSizingTier0 import sizeAircraft, getAircraftMassAndPower, sizeAndReturnPAvg

from source.modAssessmentTier0 import specificImpactKWh

#from source.modInterface import getAircraftComponents
#from source.modInterface import getTotalMaterialImpact, calcMaterialImpact

from source.modMonteCarlo import scalarFunMonteCarlo

from source.modStatistics import getSample
from source.modSensitivityAnalysis import sensitivityAnalysisOAT, sensitivityBarplotOAT
from source.modSensitivityAnalysis import getComponentMaterialSamples


from source.modInterface import calcFullSizingAndAssessment, getFullImpactVKT, sumFullImpactVKT
from source.modInterface import getOtherMaterialImpact, getStructMaterialImpact, calcComponentsMaterialImpact, getTotalComponentsMaterialImpact

#%%============================================================================
#    L O A D   D I C T I O N A R I E S
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 20.}, 
                        **{'ident': 'Sizing', 'cruiseRange': 150 * 1000,
                           'cruiseVelo': 250/3.6, 'hoverTime': 1.5 * 60.,
                           'nhover': 1, 'fUtil': 0.75, 'opHours': 15000})

# LOAD AIRCRAFT DESIGN
dDesignVec = loadAircraftDesign(**{'ident': 'Vectored Thrust', 
                                'pl': 0.025, 'ltod': 15.})

# LOAD AIRCRAFT DESIGN
dDesignMul = loadAircraftDesign(**{'ident': 'Multicopter', 
                                'pl': 0.055, 'ltod': 3.5})

# LOAD AIRCRAFT DESIGN
dDesignTil = loadAircraftDesign(**{'ident': 'Tiltrotor', 
                                'pl': 0.035, 'ltod': 8.5})

# LOAD ENERGY STORAGE TYPES
dBattery = loadFueltype('battery', **{'ident': 'liion',
                                      'esp': 260/1.2, 'eta': 0.8, 
                                      'eta_chg': 0.95, 'eta_grd': 0.97})

dFuelCell = loadFueltype('fuelcell')

dHybrid = loadFueltype('hybrid', **{'fuelcell': dFuelCell,
                                    'battery': dBattery,
                                    })

dKerosene = loadFueltype('kerosene')


dSizing = {'design': dDesignVec, 'mission': dMission, 'fuel': dBattery}


# LOAD MATERIAL FRACTIONS AND IMPACTS FOR EACH



###############################################################################





dComponents = {'struct': {'aluminium': 0.25, 'steel': 0.15, 'cfrp': 0.60},
               'other': {'aluminium': 0.00, 'steel': 0.20, 'cfrp': 0.80}}

dMatImpacts = {'aluminium': 13.70,
               'steel':      3.27,
               'cfrp':      32.73} # kg CO2e per kg Material

dDeviations = {'struct': {'aluminium': 0.1, 'steel': 0.1, 'cfrp': 0.1},
               'other': {'aluminium': 0.1, 'steel': 0.1, 'cfrp': 0.1}}

print()
print('DETERMINISTIC')
dData = {'components': dComponents, 'matImpact': dMatImpacts, 'matStDev': dDeviations}
dRes = calcComponentsMaterialImpact(dData)
dTot = getTotalComponentsMaterialImpact(dData)
[print(k0,v0) for k0,v0 in dRes.items()]
print(dTot)
#detRes = getTotalComponentsMaterialImpact(dData)
#print('Total', detRes)

print()
print('PROBABILISTIC')

iS = 10
# for i in range(0,iS):
#     dOneSample = singleComponentMaterialSample(dData)
#     [print(k0,v0) for k0,v0 in dOneSample.items()]
#     print()

# dSamp = getComponentMaterialSamples(dData,iS)

# [print(k0,k1,v1)  for k0,v0 in dSamp.items() for k1,v1 in v0.items()]

print()
print('INCLUDE INTO MONTE CARLO FORMULATION')

#dSample = {'components': getComponentMaterialSamples(dData, iS)}
#dSample = {'matImpact': {'aluminium': getSample('uniform', [ 7.29, 27.69], iS),
#                        'steel':     getSample('uniform', [ 1.92, 5.87], iS)}}

dSample = {'matImpact': {'aluminium': getSample('uniform', [ 7.29, 27.69], iS),
                          'steel':     getSample('uniform', [ 1.92, 5.87], iS)},
           'components': getComponentMaterialSamples(dData, iS)}

# print('###')
# [print(k0,k1,v1,'\n')  for k0,v0 in dSample.items() for k1,v1 in v0.items()]

# dStructMatImpact = sensitivityAnalysisOAT(getStructMaterialImpact, dData, dSample)
# # print()
# print('###')
# [print(k0,k1,v1)  for k0,v0 in dStructMatImpact.items() for k1,v1 in v0.items()]

print()
dMC, dFF = scalarFunMonteCarlo(getStructMaterialImpact, dData, dSample, iS)
# print('Factorial Designs')
# [print(exp) for exp in dFF]
print('Monte Carlo Results')
print(dMC)
print('Median:', np.median(dMC), 'Mean:', np.mean(dMC))
# 
# dOtherMatImpact = sensitivityAnalysisOAT(getOtherMaterialImpact, dData, dSample)

# print('###')
# [print(k0,k1,v1)  for k0,v0 in dOtherMatImpact.items() for k1,v1 in v0.items()]

#%%============================================================================
#    N O W   F U L L   A S S E S S M E N T    A N D   S A M P L I N G
#%=============================================================================

print('\n####################################################################\n')
print('DETERMINISTIC')

dFuelImpact = loadEmissionData()

dCase = {'design': dDesignVec, 
         'mission': dMission, 
         'fuel': dHybrid,
         ###
         'fuelImpact': dFuelImpact,
         ###
         'components': dComponents,
         'matImpact': dMatImpacts,
         'matStDev': dDeviations}

dOutput = getFullImpactVKT(dCase)

print(dOutput)

print('\n####################################################################\n')
print('PROBABILISTIC')

iS = 10
dSample = {'matImpact': {'aluminium': getSample('uniform', [ 7.29, 27.69], iS),
                          'steel':     getSample('uniform', [ 1.92, 5.87], iS)},
           'components': getComponentMaterialSamples(dData, iS)}

dMC, dFF = scalarFunMonteCarlo(sumFullImpactVKT, dCase, dSample, iS)
print('Monte Carlo Results')
print(dMC)
print('Median:', np.median(dMC), 'Mean:', np.mean(dMC))

sys.exit()



# TASK HERE IS TO ETEND THE BARPLOOAT FUNCTION SO THAT THRID LEVEL IS POSSIBLE -> HYBRID CONCEPT
# AND THEN TO ALLOW THIS TO BE SHOWN IN PLOT
var_alum_imp  = [7.29, 13.70, 27.69]
var_steel_imp = [1.92,  3.27,  5.87]

print()
print()


dVariation = {#----------------------------------
             'matImpact'    : {'aluminium'   :  var_alum_imp,
                               'steel'  :       var_steel_imp}}

lstKeys = ['min','base','max']
dRes, dVar, sVar = sensitivityBarplotOAT(getStructMaterialImpact, dData, dVariation, lstKeys)

#print()
#print(dResultSA)

import matplotlib.pyplot as plt

def quickBarplotAxis(dResult, sVar, dVar, size, annotate, **kwargs):

    # DATA PREPARATION   s
    y = np.arange(len(sVar))    

    outMin = np.zeros(( len(sVar) ))
    outMax = np.zeros(( len(sVar) ))
    
    for i, base in enumerate(dResult['base']):
        outMin[i] = dResult['min'][i] - base
        outMax[i] = dResult['max'][i] - base
    
    # THE FIGURE
    fig = plt.figure(figsize=size)
    
    axes = plt.subplot()
    
    axes.invert_yaxis()
    axes.set_yticks(y)
    axes.set_yticklabels(sVar)
    axes.set_xlabel('gCO2e / PKT')
    axes.xaxis.grid(zorder=0, which='both')
    axes.tick_params(axis='y', which='both', length=0)
    
    ###########################################################################
    axes.barh(y, outMin, left=dResult['base'], **kwargs)
    axes.barh(y, outMax, left=dResult['base'], **kwargs)
        
    if annotate:
        for key, val in dVar.items():
            for i, v in enumerate(val):
                axes.text(dResult['base'][i]+0.015*dResult['base'][i], i+0.02, str(dVar['base'][i][2]), color='k', fontsize=10, va='center')

    axes.spines['right'].set_visible(False)
    axes.spines['left'].set_visible(False)
    
    return fig, axes


figSize = (5.809,3.5)
tumci = loadTUMcolors()
dColors = {'darkest': tumci['TUMdarkest80'],
           'darker': tumci['TUMdarker80'],
           'lighter': tumci['TUMdarker50'],
           'lightest': tumci['TUMlighter50']}    

kwargs = {'color': dColors['darker'],
          'edgecolor': 'w',
          'height': 0.5,
          'linewidth': 1.}
fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
plt.show()
#dMC, lFF = scalarFunMonteCarlo(getTotalMaterialImpact, dData, dSample, iS)

sys.exit()

#%%============================================================================
#    A L L   G O O D   F R O M   H E R E 
#%=============================================================================

dMaterials = {'aluminium': 0.25,
              'steel':     0.15,
              'cfrp':      0.60} # %/100 of material in aircraft

dMatImpacts = {'aluminium': 13.70,
            'steel':      3.27,
            'cfrp':      32.73} # kg CO2e per kg Material

dDeviations = {'aluminium': 0.1,
               'steel':     0.1,
               'cfrp':      0.1}

#%%============================================================================
#    E X E C U T E
#%=============================================================================

print()
print('DETERMINISTIC')
dData = {'materials': dMaterials, 'matImpact': dMatImpacts, 'matStDev': dDeviations}
detRes = getTotalMaterialImpact(dData)
print('Total', detRes)

print()
print('PROBABILISTIC')

iS = 3 # --> number of sample points!!!

dSample = {'matImpact': {'aluminium': getSample('uniform', [ 7.29, 27.69], iS),
                         'steel':     getSample('uniform', [ 1.92, 5.87], iS)},
           'materials': getMaterialSamples(dData, iS)} 
#[print(k0,k1,s1) for k0, s0 in dSample.items() for k1,s1 in s0.items()]
dMC, lFF = scalarFunMonteCarlo(getTotalMaterialImpact, dData, dSample, iS)

print(dMC)
print('Median:', np.median(dMC), 'Mean:', np.mean(dMC))
#print()
#[print(ff) for ff in lFF] 
#print()

from source.modInterface import calcFullSizingAndAssessment, getFullImpactVKT

#%%============================================================================
#    N O W   F U L L   A S S E S S M E N T    A N D   S A M P L I N G
#%=============================================================================

print('\n####################################################################\n')
print('DETERMINISTIC')

dFuelImpact = loadEmissionData()

dCase = {'design': dDesignVec, 
         'mission': dMission, 
         'fuel': dHybrid,
         ###
         'fuelImpact': dFuelImpact,
         ###
         'materials': dMaterials,
         'matImpact': dMatImpacts,
         'matStDev': dDeviations}

dOutput = getFullImpactVKT(dCase)
[print('{:20s} {:5.2f} gCO2e / VKT'.format(key,out)) for key,out in dOutput.items()]
print('--------------------------------------')
print('{:20s} {:5.2f} gCO2e / VKT'.format('total', sum(dOutput.values())))
print()



sys.exit()


