#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 15:09:22 2020

Is deprecated to my current knowledge. Review at later stage with clear head....

@author: Nicolas
"""

# basic 
import os
os.chdir('..')

import matplotlib.pyplot as plt

# used for sizing
from source.modSizingTier0 import sizeAircraft
from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq
from source.modAssessmentTier0 import getAircraftPowerConsumption,lossesPerConsumedEnergy, impactPerPrimaryEnergy,impactPerStoredEnergy

from source.modSensitivityAnalysis import sensitivityBarplotOAT
from source.util import loadTUMcolors, getTime

def printEnergyLevels(dFuel, dLevels, dOutflows):
    print('{:14s} {:>15s} |  {:>29s}'.format(dFuel['fueltype'], 'Energy Levels', 'Outflows'))
    for (kL,vL), (kO,vO) in zip(dLevels.items(), dOutflows.items()):
        print('{:18s} {:7.3f} kWh | {:18s} {:7.3f} kWh'.format(kL, vL, kO, vO))
    print('{:18s} {:7.3f} kWh | {:18s}\n'.format('', sum(dOutflows.values()), 'sumLosses'))

def printImpacts(dFuel, dImpact, dImpactSource):
    print('{:18s} {:>15s}\n {:>33s}'.format(dFuel['fueltype'], 'Impacts by', dImpactSource['impacttype']))
    for k, v in dImpact.items():
        print('{:18s} {:7.3f} {:>7s}'.format(k, v, dImpactSource['unit']))
    print()

# European Comission / EUROSTAT
# https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Electricity_price_statistics
electricityEU28_2019_S2_avg_exTax = 0.0794   # € / kWh # exlcuding taxes and levies (but including energy generation, supply and network)
electricityEU28_2019_S2_avg_exVAT = 0.0836   # € / kWh # excluding VAT and recoverable taxes and levies
electricityEU28_2019_S2_avg_incTax = 0.1473  # € / kWh # including all taxes and levies

# exTax minus the wholesale price because grid fees. better way would be to have good values for the grid fees
electricityEU28_2019_S2_exTax_avg = 0.0839
electricityEU28_2019_S2_exTax_min = 0.0617
electricityEU28_2019_S2_exTax_max = 0.1479
#electricityEU28_2019_S2_exTax_p10 = 0.06522
#electricityEU28_2019_S2_exTax_p90 = 0.09842

electricityEU28_2015_gridFee_min = 0.005    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
#electricityEU28_2015_gridFee_p10 = 0.016    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
electricityEU28_2015_gridFee_avg = 0.022    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
#electricityEU28_2015_gridFee_p90 = 0.026    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
electricityEU28_2015_gridFee_max = 0.037    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf

electricityEU28_2019_taxes_avg = electricityEU28_2019_S2_avg_incTax - electricityEU28_2019_S2_avg_exTax
electricityEU28_2019_taxes_min = 0.0083
electricityEU28_2019_taxes_max = 0.1707
#electricityEU28_2019_taxes_p10 = 0.0162
#electricityEU28_2019_taxes_p90 = 0.0797


def energyPerFlightHour(dArgs):
    # return energy per flighthour in kWh based on sizing
    dMission = dArgs['mission']
    dDesign = dArgs['design']
    dBattery = dArgs['fuel']
        
    dSizing = sizeAircraft({'mission': dMission, 'design': dDesign, 'fuel': dBattery})
    dAnalysis = getAircraftPowerConsumption(dSizing)
        
    return 1.0 * dAnalysis['analysis']['pAvg'] / 1000.   

def impactAircraftBattery(dArgs):
    dBattery = dArgs['fuel']
    dCause = dArgs['cause']
    dAircraft = dArgs['aircraft']
    
    energy = energyPerFlightHour(dArgs) # in kWh
    impact = energy * dCause['batteryProd'] * 1000 / dBattery['fDoD'] / dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr']
    #impact = impactPerStoredEnergy(energy, dBattery, dCause)['batteryProduction'] * 1000

    return impact / dAircraft['nPAX'] / dAircraft['fUtil'] / dAircraft['avgKmh']

def costAircraftBattery(dArgs):
    dBattery = dArgs['fuel']
    dCause = dArgs['cause']
    dAircraft = dArgs['aircraft']
        
    energy = energyPerFlightHour(dArgs) # in kWh
    cost = energy * dCause['batteryProd'] / dBattery['fDoD'] /  dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr']
    #print()
    #print('Cost of Battery Pack Production: {:.4f} € / kWh'.format(dCause['batteryProd']))
    #print('Cost of Battery Pack Usage: {:.4f} € / kWh'.format(cost/energy))
    #print('... per Flighthour: {:.4f} € / FH'.format(cost))
    #print('... per VKT: {:.4f} € / VKT'.format(cost / dAircraft['avgKmh']))
    #print('... per PKT: {:.4f} € / PKT'.format(cost / dAircraft['nPAX'] / dAircraft['fUtil'] / dAircraft['avgKmh']))
    #print()
    return cost / dAircraft['nPAX'] / dAircraft['fUtil'] / dAircraft['avgKmh']

    #costElectricity = (lcoe + fee + tax) / etaChg / etaDis / etaMtr 

def impactAircraftOperation(dArgs):
    dBattery = dArgs['fuel']
    dCause = dArgs['cause']
    dAircraft = dArgs['aircraft']
        
    energy = energyPerFlightHour(dArgs) # in kWh
    impact = impactPerPrimaryEnergy(energy, dBattery, dCause)['sumImpact'] * 1000
    return impact / dAircraft['nPAX'] / dAircraft['fUtil'] / dAircraft['avgKmh']

def costAircraftOperation(dArgs):
    dBattery = dArgs['fuel']
    dCause = dArgs['cause']
    dAircraft = dArgs['aircraft']
        
    energy = energyPerFlightHour(dArgs) # in kWh
    costOfCharge = (dCause['elecWsp'] + dCause['elecGrid'] + dCause['elecTaxes']) 
    #print()
    #print('Cost of Charge: {:.4f} € / kWh'.format(costOfCharge))
    #print('... with ineff: {:.4f} € / kWh'.format(costOfCharge / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr']))
    cost = energy * costOfCharge / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #print('Per Flighthour: {:.4f} € / FH'.format(cost))
    #print('Per VKT: {:.4f} € / VKT'.format(cost / dAircraft['avgKmh']))
    #print('Per PKT: {:.4f} € / PKT'.format(cost / dAircraft['nPAX'] / dAircraft['fUtil'] / dAircraft['avgKmh']))
    #print()
    return cost / dAircraft['nPAX'] / dAircraft['fUtil'] / dAircraft['avgKmh']

def impactLCAeVTOL(dArgs):
    impOps = impactAircraftOperation(dArgs)
    impBat = impactAircraftBattery(dArgs)
    return impOps + impBat

def costLCAeVTOL(dArgs):
    cstOps = costAircraftOperation(dArgs)
    cstBat = costAircraftBattery(dArgs)
    return cstOps + cstBat

# LOAD MISSION REQUIREMENTS
dMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 20.}, 
                        **{'ident': 'Sizing', 'cruiseRange': 250 * 1000,
                           'cruiseVelo': 250/3.6, 'hoverTime': 1.5 * 60.})

# LOAD AIRCRAFT DESIGN
dDesign = loadAircraftDesign(**{'ident': 'VectoredThrust', 
                                'pl': 0.0225, 'ltod': 15.})


# LOAD ENERGY STORAGE TYPES
dBattery = loadFueltype('battery', **{'ident': 'liion',
                                      'esp': 260/1.2, 'eta': 0.8, 
                                      'eta_mtr': 0.97, 'eta_dis': 0.9, 
                                      'eta_chg': 0.95, 'eta_grd': 0.97})


#dSizingCase = {'mission': dMission, 'design': dDesign, 'fuel': dBattery}


dImpact = {'primary'    : 0.023,   # impact of primary energy per kWh charged energy
          'batteryProd' : 62.0,     # impact of battery production per kWh capacity
          'flighthours' : 1.}       # stupid - change later

cProd = electricityEU28_2019_S2_exTax_avg - electricityEU28_2015_gridFee_avg
cGrid = electricityEU28_2015_gridFee_avg
cTax = electricityEU28_2019_taxes_avg

dCost = {'elecWsp'     : cProd,
         'elecGrid'    : cGrid,
         'elecTaxes'   : cTax,
         'batteryProd'  : 135.,     # money (cost) per battery pack capacity
         'flighthours'  : 1.}       # stupid - change later

dAircraft = {'nPAX'     : 4,
             'fUtil'    : 0.8,
             'avgKmh'   : 200.}

dArgs = {'mission': dMission, 
         'design': dDesign, 
         'fuel': dBattery, 
         'cause': dImpact,
         'aircraft': dAircraft}

dEconomics = {'mission': dMission, 
              'design': dDesign, 
              'fuel': dBattery, 
              'cause': dCost,
              'aircraft': dAircraft}


# impact
var_primary     = [0.023, 0.023, 0.155]
var_batteryProd = [dImpact['batteryProd']*0.8,dImpact['batteryProd'],dImpact['batteryProd']*1.2]
# cost
var_elecWSP     = [electricityEU28_2019_S2_exTax_min - electricityEU28_2015_gridFee_avg,
                   electricityEU28_2019_S2_exTax_avg - electricityEU28_2015_gridFee_avg,
                   electricityEU28_2019_S2_exTax_max - electricityEU28_2015_gridFee_avg]
var_elecGridfee = [electricityEU28_2015_gridFee_min,
                   electricityEU28_2015_gridFee_avg,
                   electricityEU28_2015_gridFee_max]
var_elecTaxes  =  [electricityEU28_2019_taxes_min,
                   electricityEU28_2019_taxes_avg,
                   electricityEU28_2019_taxes_max]
var_batteryCost = [100.,135.,150.]
# operations: aircraft
var_fUtil       = [0.7, 0.8, 0.9]
var_avgKmh      = [175., 200., 225.]
# sizing: design
var_pl          = [0.02, 0.0225, 0.025]
var_ltod        = [14., 15., 16.]
# sizing: fuel
var_nCycles     = [1000.,1000.,2500]
var_fDoD        = [0.75,0.80,0.85]
# sizing: mission
var_cruiseRange = [200.*1000,250.*1000,250.*1000]
var_hoverTime   = [1.*60,1.5*60,2.*60]


#energyPerFH = 200. # manual entry of average energy consumed 
#energyPerFH = 1. * dAnalysis['analysis']['pAvg'] / 1000.        # 1 FH (in h) x average Power (in kW)
#dOutflowBattery, dLevelsBattery = lossesPerConsumedEnergy(energyPerFH, dBatteryLiIon)
dOutflowBattery, dLevelsBattery = lossesPerConsumedEnergy(1., dBattery)
print('\n===============================================')
print('Get to know the function lossesPerConsumedEnergy:')
printEnergyLevels(dBattery, dLevelsBattery, dOutflowBattery)


print('\n============================================')
print('# COST ANALYSIS #')
print('============================================')
print('Battery Production           : {:6.3f} €/PKT'.format(costAircraftBattery(dEconomics)))
print('VTOL Operation               : {:6.3f} €/PKT'.format(costAircraftOperation(dEconomics)))
print('============================================')
print('Life Cycle Cost (not really) : {:6.3f} €/PKT'.format(costLCAeVTOL(dEconomics)))
print('============================================')

print('\n===============================================')
print('# ENVIRONMENTAL ANALYSIS #')
print('===============================================')
print('Battery Production          : {:6.3f} gCO2e/PKT'.format(impactAircraftBattery(dArgs)))
print('VTOL Operations             : {:6.3f} gCO2e/PKT'.format(impactAircraftOperation(dArgs)))
print('VTOL Structure Porudciotn   : {:6.3f} gCO2e/PKT'.format(0.0))
print('Infrastructure Construction : {:6.3f} gCO2e/PKT'.format(0.0))
print('Infrastructure Operation    : {:6.3f} gCO2e/PKT'.format(0.0))
print('===============================================')
print('Life Cycle Assessment       : {:6.3f} gCO2e/PKT'.format(impactLCAeVTOL(dArgs)))
print('===============================================')

# import sys
# sys.exit()

#%%============================================================================
#    S E N S I T I V I T I E S
#%============================================================================= 

lstKeys = ['min','base','max']

from source.modPlotRoutines import quickBarplotAxis

plt.rc('text', usetex=True)
plt.rc('font', family='serif')  

figSize = (5.809,2.3)
figSize = (5.809,3.5)
tumci = loadTUMcolors()
dColors = {'darkest': tumci['TUMdarkest80'],
           'darker': tumci['TUMdarker80'],
           'lighter': tumci['TUMdarker50'],
           'lightest': tumci['TUMlighter50']}    

kwargs = {'color': dColors['darker'],
          'edgecolor': 'w',
          'height': 0.5,
          'linewidth': 1.}

#%%============================================================================
# Impact of VTOL Flight Energy Consumption

dVariation = {#----------------------------------
             'cause'    : {'primary'   :  var_primary},
             'aircraft' : {'fUtil'     :  var_fUtil,
                           'avgKmh'    :  var_avgKmh},
             'design'   : {'pl'        :  var_pl,
                           'ltod'      :  var_ltod},
             'mission'  : {'cruiseRange' : var_cruiseRange,
                           'hoverTime'   : var_hoverTime}
             }

dRes, dVar, sVar = sensitivityBarplotOAT(impactAircraftOperation, dArgs, dVariation, lstKeys)

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Impact from eVTOL Flight')

plt.show()

#%%============================================================================
# Impact of VTOL Battery Production

dVariation = {#----------------------------------
             'cause'    : {'batteryProd'  :  var_batteryProd},
             'aircraft' : {'fUtil'        :  var_fUtil,
                           'avgKmh'       :  var_avgKmh},
             'design'   : {'pl'           :  var_pl,
                           'ltod'         :  var_ltod},
             'fuel'     : {'nCycles'      :  var_nCycles,
                           'fDoD'         :  var_fDoD},
             'mission'  : {'cruiseRange' : var_cruiseRange,
                           'hoverTime'   : var_hoverTime}
             }

dRes, dVar, sVar = sensitivityBarplotOAT(impactAircraftBattery, dArgs, dVariation, lstKeys)

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Impact from eVTOL Battery Production')

plt.show()

#%%============================================================================
# Impact of VTOL Battery Production and VTOL FLight

dVariation = {#----------------------------------
             'cause'    : {'primary'   :  var_primary,
                           'batteryProd'  :  var_batteryProd},
             'aircraft' : {'fUtil'        :  var_fUtil,
                           'avgKmh'       :  var_avgKmh},
             'design'   : {'pl'           :  var_pl,
                           'ltod'         :  var_ltod},
             'fuel'     : {'nCycles'      :  var_nCycles,
                           'fDoD'         :  var_fDoD}
             }

dRes, dVar, sVar = sensitivityBarplotOAT(impactLCAeVTOL, dArgs, dVariation, lstKeys)

kwargs.update(**{'color': tumci['TUMdarker50']})

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Life Cycle Assessment of eVTOL')

plt.show()

#%%============================================================================
# Impact of VTOL Battery Production and VTOL FLight on COST

dVariation = {#----------------------------------
             'cause'    : {'elecWsp'       :  var_elecWSP,
                           'elecGrid'      :  var_elecGridfee,
                           'elecTaxes'     :  var_elecTaxes,
                           'batteryProd'       :  var_batteryCost},
             'aircraft' : {'fUtil'             :  var_fUtil,
                           'avgKmh'            :  var_avgKmh},
             'design'   : {'pl'                :  var_pl,
                           'ltod'              :  var_ltod},
             'fuel'     : {'nCycles'           :  var_nCycles,
                           'fDoD'              :  var_fDoD}
             }     

dRes, dVar, sVar = sensitivityBarplotOAT(costLCAeVTOL, dEconomics, dVariation, lstKeys)

kwargs.update(**{'color': tumci['TUMdarker20']})

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Life Cycle Cost of eVTOL')
strTime = getTime()

#SAVETO = 'output/devCostVTOL_' + strTime + '.pdf'
#fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)
plt.show()
