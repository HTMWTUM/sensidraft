#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 15 19:16:11 2020

@author: Nicolas
"""

# basic 
import os
os.chdir('..')

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

# used for sizing
from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq
from source.modOptimizeTier0 import optAircraftMass, optAircraftImpact

from source.util import loadTUMcolors

#%%============================================================================
#    S P E C I F I C   F U N C T I O N S
#%=============================================================================

def optimumOverRange(optFunction, x0, optArgs, optBounds, xRange, xKeys):
    
    optRangeX = np.zeros([len(xRange)])
    optRangeY = np.zeros([len(xRange)])
    
    for i, xValue in enumerate(xRange):
        optArgs[xKeys[0]].update({xKeys[1]: xValue})
        optResult = optimize.minimize(optFunction, x0=0.025, args=optArgs, bounds=optBounds)
        optRangeX[i] = optResult['x'][0]
        optRangeY[i] = optResult['fun']
    
    return optRangeX, optRangeY

def plotOptimumOverRange(xAxis, lCurves, lColors, lLabels, lLines, sTitle, sAxis, sVals, bAxis, bVals, SAVEME):
    fig = plt.figure()
    ax1 = fig.add_subplot()
    ax1.grid(True)
    ax1.set_title(sTitle)
    ax1.set_xlim(bAxis[0],bAxis[1])
    ax1.set_ylim(bVals[0],bVals[1])
    ax1.set_xlabel(sAxis)
    ax1.set_ylabel(sVals)
    for curve, clr, lbl, lst in zip(lCurves, lColors, lLabels, lLines):
        ax1.plot(xAxis, curve, color=clr, linewidth=1.2, linestyle=lst, label=lbl)
    ax1.legend()
    if SAVEME:
        fig.savefig(sTitle + '.png')
    plt.show()

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dRequiredMission = loadMissionReq({'nPAX': 3, 'nCrew': 1, 'minutesResCrs': 20.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 200 * 1000,
                                      'cruiseVelo': 95.0, 'hoverTime': 0.5 * 60.})

# LOAD AIRCRAFT DESIGN
dDesignAircraft = loadAircraftDesign(**{'ident': 'VectoredThrust', 
                                        'pl': 0.025, 'ltod': 16.})

# LOAD ENERGY STORAGE TYPES
dBatteryLiIon = loadFueltype('battery', **{'ident': 'liion',
                                           'esp': 260/1.2, 'eta': 0.8})
dBatteryLiSulfur = loadFueltype('battery', **{'ident': 'lisulfur', 
                                              'esp': 450/1.2, 'eta': 0.8, 'nCycles': 500.})
dFuelCellH2 = loadFueltype('fuelcell', **{'ident': 'h2fc', 
                                          'eta_fc': 0.55, 'eta_mtr': 0.95, 'opHours': 5000.})
dJetA = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.365})
dSynthetic = loadFueltype('synthetic', **{'ident': 'power2liquid', 
                                          'sfc': 0.365})

# LOAD GWP CAUSATION
dAssessGwpBatteries = {'batteryProd': 100,
                       'primary': 0.03}

dAssessGwpFuelCells = {#'hydrogenProd': 1.5, 
                       #'hydrogenTrans': 2.3, 
                       'fuelCellProd': 116/2,
                       #'electricityProd': 0.05,
                       'primary': 0.03}

dAssessGwpJetA = {'keroseneCombustion': 3.16,
                  'keroseneRefinery': 0.3,
                  'keroseneTransport': 0.5}
    
dAssessGwpSynthetic = {'electricityProd': 0.03,
                       'primary': 0.03}

#%%============================================================================
#    O P T I M I Z A T I O N
#%=============================================================================

#
### Change according to 

strObj = 'Mass'            # 'Mass'          | 'Impact'          | 'Cost'
optFunc = optAircraftMass  # optAircraftMass | optAircraftImpact | 
strVals = 'DGM (kg)'       # 'DGM (kg)'      | 'GWP (kgCO2eq)'   | 
bndVals = [0.0,2000.]      # [0.0,2000.]     | [0.0,0.05]        |

# strObj = 'Impact'          
# optFunc = optAircraftImpact 
# strVals = 'GWP (kgCO2eq)'
# bndVals = [0.0,0.05]

sTitle = strObj + 'Optima over Range of Variables'

dOptBatteryLiIon = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon, 'assessment': dAssessGwpBatteries}
dOptBatteryLiSulfur = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiSulfur, 'assessment': dAssessGwpBatteries}
dOptFuelCellH2 = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dFuelCellH2, 'assessment': dAssessGwpFuelCells}
dOptJetA = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dJetA, 'assessment': dAssessGwpJetA}
dOptSynthetic = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dSynthetic, 'assessment': dAssessGwpSynthetic}


x0 = 0.025
optBounds = [(0.01, 0.05)]
cruiseRanges = np.arange(1000,501000,10000)
sTitle = strObj + 'Optima over Range of Variables'

xLiIon, yLiIon = optimumOverRange(optFunc, x0, dOptBatteryLiIon, optBounds, cruiseRanges, ['mission', 'cruiseRange'])
xLiSulfur, yLiSulfur = optimumOverRange(optFunc, x0, dOptBatteryLiSulfur, optBounds, cruiseRanges, ['mission', 'cruiseRange'])
xFuelCellH2, yFuelCellH2 = optimumOverRange(optFunc, x0, dOptFuelCellH2, optBounds, cruiseRanges, ['mission', 'cruiseRange'])
xJetA, yJetA = optimumOverRange(optFunc, x0, dOptJetA, optBounds, cruiseRanges, ['mission', 'cruiseRange'])
xSynthetic, ySynthetic = optimumOverRange(optFunc, x0, dOptSynthetic, optBounds, cruiseRanges, ['mission', 'cruiseRange'])

tumClr = loadTUMcolors()

lColors = [tumClr['TUMdarker100'], tumClr['TUMlighter100'], tumClr['TUMpptGreen'], tumClr['TUMpptOrange'], tumClr['TUMpptGray'], tumClr['TUMpptGray']]
lLabels = ['Li-Ion', 'Li-Sulfur', 'Hydrogen Fuel Cell', 'JetA Turboshaft', 'Synthetic']
lLines = ['-', '-', '-', '-', '-']

lCurves = [yLiIon, yLiSulfur, yFuelCellH2, yJetA, ySynthetic]

plotOptimumOverRange(cruiseRanges, lCurves, lColors, lLabels, lLines, sTitle, 'Cruise Range (m)', strVals, [0.,500000.], bndVals, False)