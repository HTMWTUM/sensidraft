#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 14:20:19 2019

@author: gu95pug
"""

# basic 
import os
os.chdir('..')

import numpy as np
import matplotlib.pyplot as plt

# used for sizing
from source.modSizingTier0 import getAircraftMass
from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq

from source.util import loadTUMcolors


def dependentDesignLinear(varInp, keyInp, keyDep, slope, offset, fun, dCase):
    
    varOut = np.zeros((len(varInp)))
    varDep = np.zeros((len(varInp)))
    
    for i, x in enumerate(varInp):
        dCase['design'][keyInp] = x      
        varDep[i] = slope * x + offset
        dCase['design'][keyDep] = varDep[i]

        varOut[i] = fun(dCase)
        
    return varOut, varDep

def plotCurves(xAxis, lCurves, lColors, lLabels, lLines, sTitle, sVals, bAxis, bVals):
    fig = plt.figure()
    ax1 = fig.add_subplot()
    ax1.grid(True)
    ax1.set_title(sTitle)
    ax1.set_xlim(bAxis[0],bAxis[1])
    ax1.set_ylim(bVals[0],bVals[1])
    ax1.set_xlabel('Power Loading (kN/kW)')
    ax1.set_ylabel(sVals)
    ax2 = ax1.twiny()
    #ax2.plot(LtoD, dgmAircraft)
    ax2.set_xlim(16.,4.)
    ax2.set_xlabel('Lift over Drag ratio(-)')
    for curve, clr, lbl, lst in zip(lCurves, lColors, lLabels, lLines):
        ax1.plot(xAxis, curve, color=clr, linewidth=1.2, linestyle=lst, label=lbl)
    ax1.legend()
    plt.show()
     

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

from source.modAssessmentTier0 import getAircraftImpactTotal

# LOAD MISSION REQUIREMENTS
dRequiredMission = loadMissionReq({'nPAX': 3, 'nCrew': 1, 'minutesResCrs': 20.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 200 * 1000,
                                      'cruiseVelo': 95.0, 'hoverTime': 2. * 60.})

# LOAD AIRCRAFT DESIGN
dDesignAircraft = loadAircraftDesign(**{'ident': 'VectoredThrust', 
                                        'pl': 0.025, 'ltod': 16.})

# LOAD ENERGY STORAGE TYPES
dBatteryLiIon = loadFueltype('battery', **{'ident': 'liion',
                                           'esp': 260/1.2, 'eta': 0.8})
dBatteryLiSulfur = loadFueltype('battery', **{'ident': 'lisulfur', 
                                              'esp': 450/1.2, 'eta': 0.8, 'nCycles': 500.})
dFuelCellH2 = loadFueltype('fuelcell', **{'ident': 'h2fc', 
                                          'eta_fc': 0.55, 'eta_mtr': 0.95, 'opHours': 5000.})
dJetA = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.365})
dSynthetic = loadFueltype('synthetic', **{'ident': 'power2liquid', 
                                          'sfc': 0.365})


#%%============================================================================
#    L / D  and  P L  Ratio  
#%=============================================================================
    
LtoD_min = 4.         # y2
LtoD_max = 16.0      # y1

PL_min = 0.015         # x1
PL_max = 0.050         # x2

# y2 - y1 / x2 - x1
slope = (LtoD_min - LtoD_max) / (PL_max - PL_min)
offset = (LtoD_max * PL_max - LtoD_min * PL_min) / (PL_max - PL_min)

PL = np.arange(0.001,0.056,0.001)


#%%============================================================================
#    S I Z I N G
#%=============================================================================

##### Battery LiIon 
dSizingLiIon = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon}
dgmAircraftLiIon, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftMass, dSizingLiIon)
# ------------------
# dEmissionLiIon = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon, 'assessment': dBatteryLiIon}
# gwpAircraftLiIon, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dEmissionLiIon)
# ------------------
# dCostLiIon = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon, 'assessment': dCostBatteries}
# costAircraftLiIon, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dCostLiIon)


###### Battery LiSulfur
dSizingLiSulfur = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiSulfur}
dgmAircraftLiSulfur, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftMass, dSizingLiSulfur)
# ------------------
# dEmissionLiSulfur = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiSulfur, 'assessment': dGWPBatteries}
# gwpAircraftLiSulfur, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dEmissionLiSulfur)
# ------------------
# dCostLiSulfur = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiSulfur, 'assessment': dCostBatteries}
# costAircraftLiSulfur, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dCostLiSulfur)


###### Hydrogen Fuel Cell
dSizingFuelCell = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dFuelCellH2}
dgmAircraftFuelCell, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftMass, dSizingFuelCell)
# ------------------
# dEmissionFuelCell = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dFuelCellH2, 'assessment': dGWPFuelCells}
# gwpAircraftFuelCell, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dEmissionFuelCell)
# ------------------
# dCostFuelCell = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dFuelCellH2, 'assessment': dEconFuelCell}
# costAircraftFuelCell, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dCostFuelCell)


###### Kerosene Turboshaft
dSizingJetA = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dJetA}
dgmAircraftJetA, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftMass, dSizingJetA)
# ------------------
# dEmissionJetA = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dJetA, 'assessment': dGWPJetAs}
# gwpAircraftJetA, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dEmissionJetA)
# ------------------
# dCostFuelCell = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dFuelCell, 'assessment': dEconFuelCell}
# costAircraftFuelCell, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dCostFuelCell)

###### Synthetic Turboshaft
dSizingSynthetic = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dSynthetic}
dgmAircraftSynthetic, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftMass, dSizingSynthetic)
# ------------------
# dEmissionSynthetic = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dSynthetic, 'assessment': dGWPSynthetic}
# gwpAircraftSynthetic, LtoD = dependentDesignLinear(PL, 'pl', 'ltod', slope, offset, getAircraftImpactTotal, dEmissionSynthetic)


tumClr = loadTUMcolors()

lColors = [tumClr['TUMdarker100'], tumClr['TUMlighter100'], tumClr['TUMpptGreen'], tumClr['TUMpptOrange'], tumClr['TUMpptGray'], tumClr['TUMpptGray']]
lLabels = ['Li-Ion', 'Li-Sulfur', 'Hydrogen Fuel Cell', 'Synthetic', 'JetA Turboshaft']
lLines = ['-', '-', '-', '-', '--']

lCurves = [dgmAircraftLiIon, dgmAircraftLiSulfur, dgmAircraftFuelCell, dgmAircraftSynthetic, dgmAircraftJetA]
plotCurves(PL, lCurves, lColors, lLabels, lLines, 'Design Gross Weight', 'DGM (kg)', [0.015,0.05], [0.,3000.])

# lCurves = [gwpAircraftLiIon, gwpAircraftLiSulfur, gwpAircraftFuelCell, gwpAircraftFuelCell2, gwpAircraftJetA, gwpAircraftSynthetic]
# plotCurves(PL, lCurves, lColors, lLabels, lLines, 'Greenhouse Gas Emissions', 'GWP (kg CO2eq)', [0.015,0.05], [0.,0.3])

#%%============================================================================
#    O P T I M I Z A T I O N
#%=============================================================================      

from scipy import optimize
from source.modOptimizeTier0 import optAircraftMass, optAircraftImpact

def plotCurvesAndOptima(xAxis, lOptima, lCurves, lColors, lLabels, lLines, sTitle, sVals, bAxis, bVals):
    fig = plt.figure()
    ax1 = fig.add_subplot()
    ax1.grid(True)
    ax1.set_title(sTitle)
    ax1.set_xlim(bAxis[0],bAxis[1])
    ax1.set_ylim(bVals[0],bVals[1])
    ax1.set_xlabel('Power Loading (kN/kW)')
    ax1.set_ylabel(sVals)
    ax2 = ax1.twiny()
    #ax2.plot(LtoD, dgmAircraft)
    ax2.set_xlim(16.,4.)
    ax2.set_xlabel('Lift over Drag ratio(-)')
    for optimum, curve, clr, lbl, lst in zip(lOptima, lCurves, lColors, lLabels, lLines):
        ax1.plot(xAxis, curve, color=clr, linewidth=1.2, linestyle=lst, label=lbl)
        #ax1.axhline(optimum['fun'], color=clr, linewidth=1.2, linestyle=lst)
        ax1.axvline(optimum['x'][0], color=clr, linewidth=1.2, linestyle=lst)
    ax1.legend()
    plt.show()

dOptBatteryLiIon = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon}
optBatteryLiIon = optimize.minimize(optAircraftMass, x0=0.025, args=dOptBatteryLiIon, bounds=[(0.005, 0.05)])

dOptBatteryLiSulfur = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiSulfur}
optBatteryLiSulfur = optimize.minimize(optAircraftMass, x0=0.025, args=dOptBatteryLiSulfur, bounds=[(0.005, 0.05)])

dOptFuelCellH2 = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dFuelCellH2}
optFuelCellH2 = optimize.minimize(optAircraftMass, x0=0.025, args=dOptFuelCellH2, bounds=[(0.005, 0.05)])

dOptJetA = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dJetA}
optJetA = optimize.minimize(optAircraftMass, x0=0.025, args=dOptJetA, bounds=[(0.005, 0.05)])

dOptSynthetic = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dSynthetic}
optSynthetic = optimize.minimize(optAircraftMass, x0=0.025, args=dOptSynthetic, bounds=[(0.005, 0.05)])

lOptima = [optBatteryLiIon, optBatteryLiSulfur, optFuelCellH2, optJetA, optSynthetic]

plotCurvesAndOptima(PL, lOptima, lCurves, lColors, lLabels, lLines, 'Design Gross Weight', 'DGM (kg)', [0.015,0.05], [0.,3000.])

# dOptimizeImpact = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon, 'assessment': dGWPdata, 'optFunction': getTotalImpact}
# optI = optimize.minimize(optAircraftImpact, x0=0.025, args=dOptimizeImpact, bounds=[(0.01, 0.05)])

# dOptimizeCost = {'design': dDesignAircraft, 'mission': dRequiredMission, 'fuel': dBatteryLiIon, 'assessment': dCost, 'optFunction': getTotalImpact}
# optC = optimize.minimize(optAircraftImpact, x0=0.025, args=dOptimizeCost, bounds=[(0.01, 0.05)])




