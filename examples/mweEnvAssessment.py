#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:39:17 2021

@author: Nicolas André
"""

import os
os.chdir('..')

import copy as cp

from data.inputVTOL import loadCase, loadTech, loadFueltype
from data.inputImpact import loadOpsImpact

from source.modSizingTier0 import getAircraftMass
from source.modAssessmentTier0 import getTotalSpecEnergy
from source.modAssessmentTier0 import getTotalOperationsImpact

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

dFuel = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lmb', 'baseline'), 
                                         'highPowSys': loadTech('lpo', 'baseline')}) 

dCase = loadCase('joby', 'baseline', dFuel)

# Set INFO Dict to tell subordinate functions what to do
dCase['info'] = {}
dCase['info']['objective'] = 'impact'    # tells on top level if you should evaluate dgm, energy consumption, or impact
dCase['info']['funcUnit'] = 'PKT'        # the functional unit the energy or impact is referred to

# Set IMPACT Data domain, i.e. the underlying grid carbon electricity and so forth
#case['opsImpact'] = loadOpsImpact(dCase['fuel'], '2030base')
dCase['opsImpact'] = loadOpsImpact(dCase['fuel'], '2030green')

# Set the OPERATIONS domain, i.e. the off-design mission on which the evaluation of impact / energy is to be carried out
dCase['operations'] = cp.deepcopy(dCase['mission']) # important to do deepcopy to not change sizing mission

dCase['operations']['fUtil'] = 0.75
dCase['operations']['fCirc'] = 1.00 # only use this if comparison with car or other ground based modes is to be performed
                                    # fCirs = 1.2 resembles as 20% longer distance with a car as compared to vtol 
                                    # the factor is applied on the vtol result value and thus needs to be treated super carefully

#%%============================================================================
#    A N A L Y S I S
#%=============================================================================

if dCase['info']['objective'] == 'dgm':
    val = getAircraftMass(dCase)

elif dCase['info']['objective'] == 'energy':
    val = getTotalSpecEnergy(dCase)

elif dCase['info']['objective'] == 'impact':
    val = getTotalOperationsImpact(dCase)

for k0, v0 in dCase.items():
    print('\n===', k0, '=========')
    for k1, v1 in v0.items():
        if type(v1) == dict:
            print('\n >> ', k1)
            for k2, v2 in v1.items():
                print('  # {:15s} {}'.format(k2, v2))
        else:
            print('  # {:15s} {}'.format(k1, v1))
            
