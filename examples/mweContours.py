#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 07:57:55 2020

@author: Nicolas
"""

import os
os.chdir('..')

import matplotlib.pyplot as plt

from data.inputVTOL import loadMission, loadDesign, loadTech, loadFueltype

from source.modSizingTier0 import getAircraftMass
from source.modStatistics import getSample
from source.modSensitivityAnalysis import contourAnalysis2D

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

# Mind the following three functions and parameters
# It is possible to build on reference scenarios for each dictionary
# as pre-defined in the data folder
# In that, baseline, min, and max resemble the different data scenarios that can be chosen from

# LOAD MISSION REQUIREMENTS
dMission = loadMission('joby', 'baseline')

# LOAD AIRCRAFT DESIGN
dDesign = loadDesign('joby', 'max')

# LOAD ENERGY STORAGE TYPES
dFuel = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lmb', 'min'), 
                                         'highPowSys': loadTech('lpo', 'baseline')}) 
    
#%%============================================================================
#    C A S E  A N D  C O N T O U R  D A T A
#%=============================================================================

dCase = {'design': dDesign, 'mission': dMission, 'fuel': dFuel}
#print('Baseline Mass MTR:', getAircraftMass(dSizing))

iSamples = 6*5
#ly = np.linspace(1.0*60, 3.5*60, iSamples)
#lx = np.linspace(50000, 175000, iSamples)

# Set tuples along with the contour is to be evaluated
tupX = ('mission', 'cruiseRange', getSample('linspace', [50000, 175000], iSamples)) 
tupY = ('mission', 'hoverTime', getSample('linspace', [1.0*60, 3.5*60], iSamples)) 

X, Y, Z = contourAnalysis2D(getAircraftMass, dCase, tupX, tupY)


#%%============================================================================
#    P L O T  
#%============================================================================= 

#tumClr = loadTUMcolors()

clrs = []
for s in range(iSamples):
    clrs.append((0/255, 82/255, 147/255, (255-25*s)/255))

levels = [1.01*60, 1.5*60, 2.0*60, 2.5*60, 3.0*60, 3.49*60]

figure = plt.figure()
plt.contour(X, Z, Y, levels, linewidths=0.8, colors=clrs)
plt.title('Contour Plot along levels of' + tupY[1])
plt.ylabel('Design gross mass')
plt.xlabel(tupX[1])
plt.show()
