#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 13:39:17 2021

@author: Nicolas André
"""

import os
os.chdir('..')

from data.inputVTOL import loadCase, loadTech, loadFueltype
from source.modSizingTier0 import getAircraftMass

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

dFuel = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lmb', 'baseline'), 
                                         'highPowSys': loadTech('lpo', 'baseline')}) 

dCase = loadCase('joby', 'baseline', dFuel)

for k0, v0 in dCase.items():
    print('\n===', k0, '=========')
    for k1, v1 in v0.items():
        if type(v1) == dict:
            print('\n >> ', k1)
            for k2, v2 in v1.items():
                print('  # {:15s} {}'.format(k2, v2))
        else:
            print('  # {:15s} {}'.format(k1, v1))
            
print('\n==> Sizing. DGM = {:.1f}'.format(getAircraftMass(dCase)))
