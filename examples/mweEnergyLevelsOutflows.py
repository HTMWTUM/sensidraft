#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 18:31:52 2020

@author: Nicolas
"""
import sys

import os
os.chdir('..')
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

import numpy as np
import matplotlib.pyplot as plt

from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq

# used for assessment
from source.modAssessmentTier0 import getAircraftImpactDict


#%%============================================================================
#    S U P P O R T I N G   F U N C T I O N S
#%=============================================================================

def printEnergyLevels(dFuel, dLevels, dOutflows):
    print('{:14s} {:>15s} |  {:>29s}'.format(dFuel['fueltype'], 'Energy Levels', 'Outflows'))
    for (kL,vL), (kO,vO) in zip(dLevels.items(), dOutflows.items()):
        print('{:18s} {:7.3f} kWh | {:18s} {:7.3f} kWh'.format(kL, vL, kO, vO))
    print('{:18s} {:7.3f} kWh | {:18s}\n'.format('', sum(dOutflows.values()), 'sumLosses'))

def printImpacts(dFuel, dImpact, dImpactSource):
    print('{:18s} {:>15s}\n {:>33s}'.format(dFuel['fueltype'], 'Impacts by', dImpactSource['impacttype']))
    for k, v in dImpact.items():
        print('{:18s} {:7.3f} {:>7s}'.format(k, v, dImpactSource['unit']))
    print()

#%%============================================================================
#    L O A D   D A T A
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dRequiredMission = loadMissionReq({'nPAX': 3, 'nCrew': 1, 'minutesResCrs': 20.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 200 * 1000,
                                      'cruiseVelo': 95.0, 'hoverTime': 1.5 * 60.})

# LOAD AIRCRAFT DESIGN
dDesignAircraft = loadAircraftDesign(**{'ident': 'VectoredThrust', 
                                        'pl': 0.025, 'ltod': 16.})

# LOAD ENERGY STORAGE TYPES
dBatteryLiIon = loadFueltype('battery', **{'ident': 'liion',
                                           'esp': 260/1.2, 'eta': 0.8,
                                           'eta_mtr': 0.97, 'eta_dis': 0.9, 
                                           'eta_chg': 0.95, 'eta_grd': 0.97})

# dBatteryLiSulfur = loadFueltype('battery', **{'ident': 'lisulfur', 
                                              # 'esp': 450/1.2, 'eta': 0.8, 'nCycles': 500.})

dFuelCellH2 = loadFueltype('fuelcell', **{'ident': 'h2fc', 
                                          'eta_fc': 0.55, 'eta_mtr': 0.97, 'opHours': 5000.,
                                          'eta_trans': 0.99, 'eta_grd': 0.99})

# dJetA = loadFueltype('kerosene', **{'ident': 'conventional', 
#                                     'sfc': 0.365})

dSynthetic = loadFueltype('synthetic', **{'ident': 'power2liquid', 
                                          'sfc': 0.365,
                                          'eta_gear': 0.98,
                                          'eta_trans': 0.9,
                                          'eta_fts': 0.8,
                                          'eta_elysis': 0.65, 'eta_grd': 0.99})


#from source.modAssessmentTier0 import impactPerConsumedEnergy, impactPerSegment
from source.modAssessmentTier0 import lossesPerConsumedEnergy, impactPerPrimaryEnergy

#%%============================================================================
#    E V A L :  F L O W  A N D  L E V E L  O F  E N E R G I E S
#%=============================================================================

# for 1.0 kWh
dOutflowBattery, dLevelsBattery = lossesPerConsumedEnergy(1.0, dBatteryLiIon)
dOutflowFuelCell, dLevelsFuelCell = lossesPerConsumedEnergy(1.0, dFuelCellH2)
dOutflowSynthetic, dLevelsSynthetic = lossesPerConsumedEnergy(1.0, dSynthetic)

printEnergyLevels(dBatteryLiIon, dLevelsBattery, dOutflowBattery)
printEnergyLevels(dFuelCellH2, dLevelsFuelCell, dOutflowFuelCell)
printEnergyLevels(dSynthetic, dLevelsSynthetic, dOutflowSynthetic)


#%%============================================================================
#    E V A L :  I M P A C T  O F  E N E R G I E S
#%=============================================================================

#Mögliche einfachere Schnittstelle: die Energiemenge als Quant zur Source hinzufügen!
#Achtung: die unit muss zur unit der consumed energy dazu passen
dImpactPrimaryEnergy = {'primary': 0.05,
                        'impacttype': 'primary energy',
                        'unit': 'gCO2e'} # per kWh
# for 1.0 kWh
dImpactBattery = impactPerPrimaryEnergy(1.0, dBatteryLiIon, dImpactPrimaryEnergy)
dImpactFuelCell = impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)
dImpactSynthetic = impactPerPrimaryEnergy(1.0, dSynthetic, dImpactPrimaryEnergy)

printImpacts(dBatteryLiIon, dImpactBattery, dImpactPrimaryEnergy)
printImpacts(dFuelCellH2, dImpactFuelCell, dImpactPrimaryEnergy)
printImpacts(dSynthetic, dImpactSynthetic, dImpactPrimaryEnergy)


#%%============================================================================
#    E V A L :  L E V E L I Z E D  C O S T  O F  E N E R G I E S
#%=============================================================================

dCostPrimaryEnergy = {'primary': 7.5,
                      'impacttype': 'primary energy',
                      'unit': '€ ct'} # per kWh

dCostPrimary = impactPerPrimaryEnergy(1.0, dBatteryLiIon, dCostPrimaryEnergy)
printImpacts(dBatteryLiIon, dCostPrimary, dCostPrimaryEnergy)

prices = np.arange(5.0,12.0,0.5)
lcoeProd = []
lcoeUse = []

for pric in prices:
    dCostPrimaryEnergy.update({'primary': pric+6.5+1.5})
    lcoeProd.append(impactPerPrimaryEnergy(1.0, dBatteryLiIon, dCostPrimaryEnergy)['consumed'])
    lcoeUse.append(impactPerPrimaryEnergy(1.0, dBatteryLiIon, dCostPrimaryEnergy)['sumImpact'])

# 10 €/kg
# 33.322 kWh/kg usable

lcoeGERIndu2019 = 18.44 / dBatteryLiIon['eta_mtr'] / dBatteryLiIon['eta_chg'] / dBatteryLiIon['eta_dis'] # ct / kWh https://www.bdew.de/service/daten-und-grafiken/strompreis-fuer-die-industrie/

lcoeHyd2020 = 10 / 33.322 * 100 / dFuelCellH2['eta_fc'] / dFuelCellH2['eta_mtr']
lcoeHyd2030 = 5 / 33.322 * 100 / dFuelCellH2['eta_fc'] / dFuelCellH2['eta_mtr']
#lcoeHyd2020 = 10 / 33.322 / dFuelCellH2['eta_fc'] / dFuelCellH2['eta_mtr']

costStack = 100 * 35.0 / dFuelCellH2['opHours']    # € / kW  / h

# € / kWh 
costBat = 100. * 135. / dBatteryLiIon['fDoD'] / dBatteryLiIon['nCycles']
costBatAct = 100. * dLevelsBattery['storedBattery'] * 135. / dBatteryLiIon['fDoD'] / dBatteryLiIon['nCycles'] / dBatteryLiIon['eta_mtr'] / dBatteryLiIon['eta_dis']


plt.plot(prices, lcoeProd, color='k', label='prod inc EEG & Tax')
plt.plot(prices, lcoeUse, color='gray', label='use inc EEG & Tax')
plt.axhline(lcoeGERIndu2019, color='k', linestyle='--', label='Industry Price 2019')
plt.axhline(costBat, color='red', linestyle='-', label='Batt Price')
plt.axhline(costBatAct, color='gold', linestyle='-.', label='Batt Price Act')
plt.axhline(lcoeHyd2020, color='blue', label='LCOE Hydrogen 2020')
plt.axhline(lcoeHyd2030, color='green', label='LCOE Hydrogen 2030.')
plt.axhline(costBatAct+lcoeGERIndu2019, color='orange', label='Batt + LCOE + Tax + ..')
plt.axvline(9.5 , color='k', linestyle='-.', label='GER 2019')
#plt.axvline(0.0295 , color='k', linestyle='--', label='Guezuraga et al. (WC)')
plt.xlabel('lcoe primary energy')
plt.ylabel('prices in € ct')
plt.legend(fontsize='small')
plt.grid()
plt.show()

# $ 0,058 
# $ 0,053 
# $ 0,054 
# $ 0,050 
# $ 0,044 
# $ 0,040 
# $ 0,067 
# $ 0,076 
# $ 0,064 
# $ 0,064 
# $ 0,066 
# $ 0,051 
# $ 0,041 
# $ 0,062 
# $ 0,065 
# $ 0,058 
# $ 0,085 
# $ 0,074 
# $ 0,090 

#%%============================================================================
#    P L O T :  M O D E L  I N E F F I C I E N C I E S
#%=============================================================================

import numpy as np

energies = np.arange(0.005,0.055,0.005)

conImpact = []
sumImpact = []
dispImpact = []

for prim in energies:
    dImpactPrimaryEnergy.update({'primary': prim})
    conImpact.append(impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['consumed'])
    sumImpact.append(impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['sumImpact'])
    dispImpact.append(impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['consumed'] + 
                      impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['gridInfrastr'] +
                      impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['hydrElectrolysis'] +
                      impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['hydrCompression'] +
                      impactPerPrimaryEnergy(1.0, dFuelCellH2, dImpactPrimaryEnergy)['hydrTransport'])

plt.plot(energies, conImpact, color='k', label='consumed')
plt.plot(energies, sumImpact, color='gray', label='total')
plt.plot(energies, dispImpact, color='gray', linestyle='--', label='dispensed')
plt.axhline(0.09, color='red', label='Bareiß et al.')
plt.axhline(0.05759 , color='green', label='Burkhardt et al.')
plt.axhline(0.0291 , color='gold', label='Cetinkaya et al.')
plt.axvline(0.0078 , color='k', linestyle='-.', label='Bonou et al. (BC)')
plt.axvline(0.0295 , color='k', linestyle='--', label='Guezuraga et al. (WC)')
plt.legend()
plt.grid()
plt.show()

# 1kWh of consumed energy leads to i.e. 1.145 kWh stored in the battery
# so the energy level is relevant
   
# dImpactBatteryProduction = {'batteryProd': 150}




# print(1.0 * dImpactBatteryProduction['batteryProd'] / dBatteryLiIon['fDoD'] / dBatteryLiIon['nCycles'])
# print(dLevelsBattery['storedBattery'] * dImpactBatteryProduction['batteryProd'] / dBatteryLiIon['fDoD'] / dBatteryLiIon['nCycles'])




#%%============================================================================
#    R E M A I N D E R S
#%=============================================================================

#                     'purchasePrice': 3.60 * 3.7854}    # $ / gallon * gallon / kg


# EU28 (https://ec.europa.eu) 0.2159 EUR per kWh first half 2019
# (https://about.bnef.com) 176 $ per kWh battery in 2018, down from 1.160 $ in 2010
# hydrogen sold for 3.23 $ per kg in GER
# # hydrogen california: 13.99 $ (https://cafcp.org/content/cost-refill)
# dCost = {'electricityProd': 0.2159, 'batteryProd': 0.9*176,
#          'hydrogenProd': 0.9*13.99, #'hydrogenTrans': 2.5, 
#          'fuelCellProd': 0.9*55.}
#                     'purchasePrice': 3.60 * 3.7854}    # $ / gallon * gallon / kg

# dEconFuelCell

