#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 11:17:33 2020

@author: Nicolas
"""


def lengthRecursiveDict(dic):
    lenDic = 0
    for v0 in dic.values():
        lenDic += len(v0)   
    return lenDic

import numpy as np

import matplotlib.pyplot as plt


output = {'base': [1.,1.,1.],
          'min': [0.9,0.8,0.95],
          'max': [1.05,1.55,2.4]}
   
dVar = {'mision': {'cruiseRange': [10,20],
                   'hoverTime': [1,2]},
        'design': {'ltod': [10,15]}}

strVar = ['rCruise', 
          'tHover', 
          'LtoD']

lenDicVar = lengthRecursiveDict(dVar)
print(lenDicVar)
y = np.arange(lenDicVar)
print(y)

color = 'green'

outMin = np.zeros(( lenDicVar ))
outMinus = np.zeros(( lenDicVar ))
outPlus = np.zeros(( lenDicVar ))
outMax = np.zeros(( lenDicVar ))
for i, base in enumerate(output['base']):
    outMin[i] = output['min'][i] - base
    #outMinus[i] = output['minus'][i] - base
    #outPlus[i] = output['plus'][i] - base
    outMax[i] = output['max'][i] - base
    
print(outMin)
print(outMax)
    
fig = plt.figure()
ax = plt.subplot()

ax.invert_yaxis()
ax.xaxis.grid(zorder=0, which='both')
barw = 0.7

###########################################################################
ax.barh(y, outMin, left=output['base'], height=barw, color='W', zorder=3)
ax.barh(y, outMin, left=output['base'], height=barw, color=color, alpha=0.5, edgecolor='w', linewidth=1., zorder=3)
#ax.barh(y, outMinus, left=output['base'], height=barw, color=color, alpha=0.9, edgecolor='w', linewidth=1., zorder=3)
ax.barh(y, outMax, left=output['base'], height=barw, color='W', zorder=3)
ax.barh(y, outMax, left=output['base'], height=barw, color=color, alpha=0.5, edgecolor='w', hatch='/////', linewidth=1., zorder=3)
#ax.barh(y, outPlus, left=output['base'], height=barw, color=color, alpha=0.9, edgecolor='w', hatch='/////', linewidth=1., zorder=3)

###########################################################################    
ax.axvline(output['base'][0],linewidth=1., color=color, zorder=5)

ax.set_yticks(y)
ax.set_yticklabels(strVar)


plt.show()