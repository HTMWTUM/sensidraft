#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 10:49:12 2020

@author: Nicolas
"""

import os
os.chdir('..')

from source.modSizingTier0 import getAircraftMass
from source.modMonteCarlo import scalarFunMonteCarlo
from source.modStatistics import getSample
from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 10.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 161 * 1000,
                                      'cruiseVelo': 60.0, 'hoverTime': 2 * 60.})

# LOAD AIRCRAFT DESIGN
dDesign = loadAircraftDesign(**{'ident': 'Electric Lift and Cruise', 
                                'pl': 0.0405, 'ltod': 11.,'fStruct': 0.35})

# LOAD ENERGY STORAGE TYPES
dBattery = loadFueltype('battery', **{'ident': 'liion',
                                      'esp': 250/1.2, 'eta': 0.68})

#%%============================================================================
#    D E T E R M I N I S T I C
#%=============================================================================

dSizing = {'design': dDesign, 'mission': dMission, 'fuel': dBattery}
       
# ---- sizing 
mass = getAircraftMass(dSizing) # (deterministic)
        
print('The Aircraft Design Gross Weight at given \nRequirements is DGW = {:6.1f} kg'.format(mass))

#%%============================================================================
#    P R O B A B I L I S T I C 
#%=============================================================================

iS = 1000


#### first round: uniform distribution
sampLD = getSample('uniform', [11, 15], iS)

# important: dict needs the same keys, otherwise it will not work
dDesignSamples = {'ltod': sampLD}

#dSamples = {'design': dDesign, 'mission': dMission, 'fuel': dBattery}
dSamples = {'design': dDesignSamples}

# reinitialize dSizing dictionary
dSizing = {'design': dDesign, 'mission': dMission, 'fuel': dBattery}

# perform the mcAnalysis
mcMasses_uni, mcDesigns_uni = scalarFunMonteCarlo(getAircraftMass, dSizing, dSamples, iS)


# for simplistic debugging
# print()
# for m, dff in zip(mcMasses, mcDesigns):
#     print('for', dff, 'we get', m)

#### second round: normal distribution
sampLD = getSample('normal', [13, 1], iS)
dDesignSamples = {'ltod': sampLD}
dSamples = {'design': dDesignSamples}
dSizing = {'design': dDesign, 'mission': dMission, 'fuel': dBattery}
mcMasses_norm, mcDesigns_norm = scalarFunMonteCarlo(getAircraftMass, dSizing, dSamples, iS)


#%%============================================================================
#    O U T P U T
#%=============================================================================

import numpy as np
from scipy.stats import scoreatpercentile  

print('Comparison:    {:>10s} {:>10s}'.format('Uniform', 'Normal'))
print('Mean masses:   {:10.0f} {:10.0f}'.format(np.mean(sorted(mcMasses_uni)), np.mean(sorted(mcMasses_norm))))
print('Median masses: {:10.0f} {:10.0f}'.format(scoreatpercentile(mcMasses_uni, 50), scoreatpercentile(mcMasses_norm, 50)))





