#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 14:20:19 2019

@author: gu95pug
"""

# basic 
import os
os.chdir('..')

# used for sizing
#from source.modSizingTier0 import getAircraftMass
from source.modInterface import getAircraftMass

# used for carpet and plotting
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from source.carpet import calcCarpet
from source.util import loadTUMcolors

from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

#nPax = 4
#nCrew = 1
#minutesReserve = 20.
#mPayload = 88 * nPax              # repr. for m/f ratio of 70/30 (94kg/75kg)
#mCrew = 92 * nCrew                # repr. for men
#tReserve = 60 * minutesReserve    # regulation: 20mins in fwd flight

# LOAD MISSION REQUIREMENTS
dRequiredMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 10.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 161 * 1000,
                                      'cruiseVelo': 60.0, 'hoverTime': 2 * 60.})

# LOAD AIRCRAFT DESIGN
dDesignMTR = loadAircraftDesign(**{'ident': 'Turboshaft Helicopter', 
                                        'pl': 0.0465, 'ltod': 3.1,'fStruct': 0.735})
dDesignTLT = loadAircraftDesign(**{'ident': 'Turboshaft Tiltrotor', 
                                        'pl': 0.0375, 'ltod': 5.3,'fStruct': 0.809})
dDesignEDP = loadAircraftDesign(**{'ident': 'Electric Lift and Cruise', 
                                        'pl': 0.0405, 'ltod': 11.,'fStruct': 0.47})

# LOAD ENERGY STORAGE TYPES
dBatteryLiIon = loadFueltype('battery', **{'ident': 'liion',
                                           'esp': 250/1.2, 'eta': 0.68})

dJetAMTR = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.365})

dJetATLT = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.4150})

#dRequiredMission = {'ident':         'Sizing',     
#                    'cruiseRange':     300000.,     # m
#                    'cruiseVelo':         69.4,     # m/s - 250km/h (Web 300km/h)
#                    'hoverTime':      1.5 * 60,     # s
#                    'massPayload':    mPayload,     # kg
#                    'massCrew':          mCrew,     # kg
#                    'nPax':               nPax,     # -
#                    'timeReserve':    tReserve}     # s
#
#dDesignAircraft = {'ident':  'vectored',
#                   'pl':           0.025,         # N/W
#                   'ltod':           16.,         # -
#                   'fStruct':       0.24,         # - (mStruc = fStruc * DGW)
#                   'fOth':           0.6}        # - (mOth = fOth * Use)
#
#
#dBatteryLiIon = {'fueltype':      'battery',
#                 'nCycles':            1000,               # -
#                 'esp':             260/1.2,               # Wh/kg (pack specific = cell specific / 1.2) 
#                 'eta':                0.85,               # - (motor efficiency)
#                 'voltage':            550.,               # V 425 - 650 but doesnt have a direct influence on weight if C-rate arbitrary
#                 'dcp':                1.03,               # - (battery discharge parameter - see L.Traub )
#                 'motorError':            1,               # - (factor multiplied on the electric motor weight)
#                 'cRateMax':             5.,
#                 'fDoD':                0.8}

#%%============================================================================
#    S I Z I N G
#%=============================================================================

dSizingCase = {'design': dDesignEDP, 'mission': dRequiredMission, 'fuel': dBatteryLiIon}
       
# ---- sizing 
dgmAircraft = getAircraftMass(dSizingCase) # (deterministic)
        
print('The Aircraft Design Gross Weight at given \nRequirements is DGW = {:6.1f} kg'.format(dgmAircraft))


#%%============================================================================
#    C A R P E T
#%=============================================================================

tumClr = loadTUMcolors()
cColor = [tumClr['TUMdarker100'], tumClr['TUMpptOrange']]
cLines = ['--', '-.']

cKeys = ['cruiseRange', 'hoverTime']
dCarpet = {'mission': {cKeys[0]: [250000., 200000., 150000., 100000., 50000.],
                             cKeys[1]: [0.5*60, 1.0*60, 1.5*60, 2.0*60, 2.5*60]}}

# dCarpetLilium = {'mission': {'cruiseRange': [150000., 100000., 50000.]},
#            'design': {'ltod': [15., 16., 16.]}}

X, Y = calcCarpet(getAircraftMass, dSizingCase, dCarpet)

figCarpet = plt.figure(figsize=(6,4))
plt.title('Carpet plot. Variation of ' + str(cKeys[0]) + ' and ' + str(cKeys[1]) , fontsize=12)
plt.tick_params(axis='x',which='both',bottom=False,labelbottom=False)
 
plt.plot(X, Y.T, linestyle='--', color=cColor[0], linewidth=1)
plt.plot(X.T, Y, linestyle='-.', color=cColor[1], linewidth=1)
                
custom_lines = [Line2D([0], [0], color=cColor[0], lw=1.5),
                Line2D([0], [0], color=cColor[1], lw=1.5)]
plt.legend(custom_lines, ['Var: ' + str(cKeys[0]), 'Var: ' + str(cKeys[1])])

plt.show()
