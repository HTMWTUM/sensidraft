#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 09:18:41 2020

@author: nicolas
"""

import os
os.chdir('..')

import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

#import matplotlib.gridspec as gs
#from matplotlib.colors import ListedColormap

from source.util import loadTUMcolors, getTime, printCase, loadPlotFonts

# used for loading data 
#from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq, loadPredefCase
#from source.modDictLoader import loadCostData, loadEmissionData

#from data.inputVTOL import loadCase, loadTech, loadFueltype


import copy

## used for sizing and analysis
#from source.modAnalysisFState import getPowers
#from source.modSizingTier0 import sizeAircraft
#getAircraftMassAndPower, sizeAndReturnPAvg, sizeAircraftForPayload
##from source.modSizingTier0 import sizeAircraft, getAircraftMass, getAircraftMassAndPower, sizeAndReturnPAvg
#
## used for assessment 
from source.modStatistics import getSample
#from source.modSensitivityAnalysis import sensitivityAnalysisOAT, contourAnalysis2D
from source.modMonteCarlo import scalarFunMonteCarlo

#from source.modAssessmentTier0 import specificCostKWh, specificImpactKWh, specificFunctionalUnit, specificMissionFuel

from source.modSizingTier0 import getAircraftComponents, getAircraftMass
# from source.modgetEnergyVKT, getEnergyPKT, calcActualRange, sumEnergyPKT
# from source.modInterface import getFuelImpactVKT, sumFuelImpactVKT
# from source.modInterface import getFuelImpactPKT, sumFuelImpactPKT
# from source.modInterface import getFuelCostVKT, sumFuelCostVKT


# dBattery = {'fueltype':      'battery',
#           'ident':     'Lithium Ion',
#           'esp':                400.,          # Wh/kg (pack specific = cell specific / 1.2) 
#           'eta_mtr':            0.85,
#           'eta_dis':            0.90,
#           'dcp':                1.03,          # - (battery discharge parameter - see L.Traub )
#           'cRate':               3.0,
#           'fDoD':                0.8,}

from data.inputVTOL import loadTech, loadFueltype

dBattery = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lmb', 'baseline'), 
                                              'highPowSys': loadTech('lpo', 'baseline')})

dBattery['highEnerSys']['esp'] = 400. * 0.8
dBattery['highEnerSys']['eta_mtr'] = 0.85
dBattery['highEnerSys']['eta_dis'] = 0.9
# dBattery['highPowSys']['esp'] = 400. * 0.8

dMission = {'ident':     'Brown_MIT_Sizing',     
           'cruiseRange':        93 * 1000,     # m
           'cruiseVelo':                67.,     # m/s
           'hoverTime':             2 * 60.,     # s
           'massPayload':           3 * 91.,     # kg
           'massCrew':                  86.,     # kg
           'timeReserve':         20. * 60.}

def calcPL(DL, FM, RHO):
    import math 
    return math.sqrt(2*RHO) / math.sqrt(DL) * FM

print(calcPL(718, 0.78, 1.225))
print(calcPL(215, 0.76, 1.225))

# DL = 718 N/m^2
# PL = ( 0.035 @ FM=0.6 
#        0.041 @ FM=0.7 
#        0.044 @ FM=0.7 
#        0.047 @ FM=0.8 )
dLiftCruise = {'ident': 'liftcruise',
               'fStruct':       0.26,
               'fOth':          0.8,
               'ltod':          10.0,
               'pl':           0.046}

# DL = 215 N/m^2
# PL = ( 0.064 @ FM=0.6 
#        0.075 @ FM=0.7 
#        0.085 @ FM=0.8 )
dCompound = {'ident':  'compound',
             'fStruct':      0.26,
             'fOth':         0.8,
             'ltod':          9.0,
             'pl':          0.081}
             # 'pl':          0.075}

# DL = 718 N/m^2
# PL = ( 0.035 @ FM=0.6 
#        0.041 @ FM=0.7 
#        0.047 @ FM=0.8 )
dTiltWing = {'ident': 'tiltwing',
             'fStruct':     0.26,
             'fOth':        0.8,
             'ltod':        12.0,
             'pl':         0.046}
             # 'pl':         0.053}

# DL = 718 N/m^2
# PL = ( 0.035 @ FM=0.6 
#        0.041 @ FM=0.7 
#        0.047 @ FM=0.8 
#        0.049 @ FM=0.85)
dTiltRotor = {'ident': 'tiltrotor',
              'fStruct':      0.26,
              'fOth':         0.8,
              'ltod':         14.0,
              'pl':          0.046}
              # 'pl':          0.053}

caseLiftCruise = {'mission':dMission, 'fuel':dBattery, 'design':dLiftCruise}
caseCompound = {'mission':dMission, 'fuel':dBattery, 'design':dCompound}
caseTiltWing = {'mission':dMission, 'fuel':dBattery, 'design':dTiltWing}
caseTiltRotor = {'mission':dMission, 'fuel':dBattery, 'design':dTiltRotor}

cases = [caseLiftCruise, caseCompound, caseTiltWing, caseTiltRotor]

compA = getAircraftComponents(caseLiftCruise)



dataBrown = {'lc': {'mass': {'dgm' :   1504.,
                             'frame':   795.,
                             'battery': 350.,
                             'useful':  359.},
                    'energy': {'reserve': 112.-74.,
                               'hover':    74.-50.,
                               'cruise':       50.},
                    'power': {'cruise':129,
                              'hover':361,
                              'reserve':114}
                    },

             'co': {'mass': {'dgm' :   1472.,
                             'frame':   736.,
                             'battery': 377.,
                             'useful':  359.},
                    'energy': {'reserve': 123.-76.,
                               'hover':    76.-61.,
                               'cruise':       61.},
                    'power': {'cruise':157,
                              'hover':232,
                              'reserve':136}
                    },
                    
             'tw': {'mass': {'dgm' :   1440.,
                             'frame':   790.,
                             'battery': 291.,
                             'useful':  359.},
                    'energy': {'reserve':  93.-63.,
                               'hover':    63.-40.,
                               'cruise':       40.},
                    'power': {'cruise':102,
                              'hover':344,
                              'reserve':90}
                    },
             
             'tr': {'mass': {'dgm' :   1322.,
                             'frame':   727.,
                             'battery': 236.,
                             'useful':  359.},
                    'energy': {'reserve': 77.-53.,
                               'hover':    53.-31.,
                               'cruise':       31.},
                    'power': {'cruise':81,
                              'hover':317,
                              'reserve':71}
                    }
                    }

#%=============================================================================

dgm = []
frame = []
battery = []
useful = []
battuse = []
framebatt = []
for typ, typDic in dataBrown.items():
    dgm.append(typDic['mass']['dgm'])
    frame.append(typDic['mass']['frame'])
    battery.append(typDic['mass']['battery'])
    useful.append(typDic['mass']['useful'])
    battuse.append(battery[-1]+useful[-1])
    framebatt.append(frame[-1]+battery[-1])

#%=============================================================================

eHover = []
eCruise = []
eReserve = []
eHovCrs = []    

for typ, typDic in dataBrown.items():
    eHover.append(typDic['energy']['hover'])
    eCruise.append(typDic['energy']['cruise'])
    eReserve.append(typDic['energy']['reserve'])
    eHovCrs.append(eHover[-1]+eCruise[-1])

#%=============================================================================
    
pHover = []
pCruise = []
pReserve = []

for typ, typDic in dataBrown.items():
    pHover.append(typDic['power']['hover'])
    pCruise.append(typDic['power']['cruise'])
    pReserve.append(typDic['power']['reserve'])

#%=============================================================================

from scipy.stats import scoreatpercentile  


from source.modSizingTier0 import getAirframeMass, getFuelMass, getUsefulLoad
from source.modSizingTier0 import getCruiseEnergy, getReserveEnergy, getHoverEnergy
from source.modSizingTier0 import getCruisePower, getHoverPower

iS = 1000

masses = []
mAirframe = []
mFuel = []
mUseful = []
mFrameFuel = []

medians = {'mass': {'total': np.zeros((len(cases))),
                    'frame': np.zeros((len(cases))),
                    'fuel': np.zeros((len(cases))),
                    'useful': np.zeros((len(cases)))},
           'power': {'hover': np.zeros((len(cases))),
                     'cruise': np.zeros((len(cases)))}}
totalConfLow = {'mass': {'total': np.zeros((len(cases))),
                         'frame': np.zeros((len(cases))),
                         'fuel': np.zeros((len(cases))),
                         'useful': np.zeros((len(cases)))},
                'power': {'hover': np.zeros((len(cases))),
                          'cruise': np.zeros((len(cases)))}}
totalConfHigh = {'mass': {'total': np.zeros((len(cases))),
                          'frame': np.zeros((len(cases))),
                          'fuel': np.zeros((len(cases))),
                          'useful': np.zeros((len(cases)))},
                 'power': {'hover': np.zeros((len(cases))),
                           'cruise': np.zeros((len(cases)))}}

cruise = []
reserve = []
hover = []
eSum = []

powerCruise = []
powerHover = []

functs = {'mass': {'total': getAircraftMass,
                   'frame': getAirframeMass,
                   'fuel': getFuelMass,
                   'useful': getUsefulLoad},
          'power': {'hover': getHoverPower,
                    'cruise': getCruisePower}}

for i, case in enumerate(cases):

    masses.append(getAircraftMass(case))
    mAirframe.append(getAirframeMass(case))
    mFuel.append(getFuelMass(case))
    mUseful.append(getUsefulLoad(case))
    mFrameFuel.append(mAirframe[-1] + mFuel[-1])

    cruise.append(getCruiseEnergy(case))
    reserve.append(getReserveEnergy(case))
    hover.append(getHoverEnergy(case))
    eSum.append(cruise[-1] + reserve[-1] + hover[-1])

    powerCruise.append(getCruisePower(case))
    powerHover.append(getHoverPower(case))

    sampleFS = getSample('normal', [case['design']['fStruct'], case['design']['fStruct']*0.1], iS)
    samplesPL = getSample('normal', [case['design']['pl'], case['design']['pl']*0.1], iS)
    dSamples = {'design': {'fStruct': sampleFS, 'pl': samplesPL}}

    for key0, dic0 in functs.items():
        for key1, funct in dic0.items():
            #print(key0, key1, funct)
            mc, dff = scalarFunMonteCarlo(funct, case, dSamples, iS)
            medians[key0][key1][i] = scoreatpercentile(mc, 50)
            totalConfLow[key0][key1][i] = medians[key0][key1][i] - scoreatpercentile(mc, 5)
            totalConfHigh[key0][key1][i]= scoreatpercentile(mc, 95) - medians[key0][key1][i]
    
    #massMC, dff = scalarFunMonteCarlo(getAircraftMass, case, dSamples, iS)
    #medians['mass']['total'][i] = scoreatpercentile(massMC, 50)
    #totalConfLow['mass']['total'][i] = medians['mass']['total'][i] - scoreatpercentile(massMC, 5)
    #totalConfHigh['mass']['total'][i]= scoreatpercentile(massMC, 95) - medians['mass']['total'][i]
    
    # powerMC, dff = scalarFunMonteCarlo(getCruisePower, case, dSamples, iS)
    # medians['mass']['total'][i] = scoreatpercentile(massMC, 50)
    # totalConfLow['mass']['total'][i] = medians[-1] - scoreatpercentile(massMC, 5)
    # totalConfHigh['mass']['total'][i]= scoreatpercentile(massMC, 95) - medians[-1]
    
    
#%=============================================================================

# print(totalConfLow['mass']['total'])

tumClr = loadTUMcolors()

import matplotlib.gridspec as gs

figure = plt.figure(figsize=(5.809,2.0))
grid = gs.GridSpec(1, 2, bottom=0.02, right=0.95, left=0.1, top=0.98, wspace=0.3)


xPos = range(0, len(dataBrown)) 
barw = 0.65   

xMin = [x-barw/3 for x in xPos]
xMax = [x+barw/3 for x in xPos]

#plt.bar(xPos, frame, width=barw, color=tumClr['TUMpptGray'],alpha=0.2)
#plt.bar(xPos, battery, bottom=frame, width=barw, color=tumClr['TUMpptGray'],alpha=0.6)
#plt.bar(xPos, useful, bottom=framebatt, width=barw, color=tumClr['TUMpptGray'],alpha=0.4)

axis1 = plt.subplot(grid[0])

l1 = axis1.bar(xPos, dgm, width=barw, color=tumClr['TUMpptGray'],alpha=0.1,zorder=1, label='DGM (Ref.)')
l2 = axis1.bar(xMin, frame, width=barw/3, color=tumClr['TUMpptGray'],alpha=0.7,zorder=1, label='Airframe (Ref.)')
l3 = axis1.bar(xPos, battery, width=barw/3, color=tumClr['TUMpptGray'],alpha=0.5,zorder=1, label='Battery (Ref.)')
l4 = axis1.bar(xMax, useful, width=barw/3, color=tumClr['TUMpptGray'],alpha=0.3,zorder=1, label='Useful (Ref.)')

e1 = axis1.errorbar(xPos, masses, yerr=[totalConfLow['mass']['total'], totalConfHigh['mass']['total']], 
               xerr=None, fmt='o',capsize=3. ,color=tumClr['TUMpptGray'],linewidth=1.0,
               marker='^', markersize=5, markeredgewidth=0.8, markeredgecolor='k', markerfacecolor='w', label='DGM')
e2 = axis1.errorbar(xMin, medians['mass']['frame'], yerr=[totalConfLow['mass']['frame'], totalConfHigh['mass']['frame']], 
               xerr=None, fmt='o',capsize=3., color=tumClr['TUMpptGray'],linewidth=1.0, 
               marker='o', markersize=5, markeredgewidth=0.8,markeredgecolor='k', markerfacecolor='w', label='Airframe')
e3 = axis1.errorbar(xPos, medians['mass']['fuel'], yerr=[totalConfLow['mass']['fuel'], totalConfHigh['mass']['fuel']], 
               xerr=None, fmt='o',capsize=3., color=tumClr['TUMpptGray'],linewidth=1.0,
               marker='s', markersize=4, markeredgewidth=0.8,markeredgecolor='k', markerfacecolor='w', label='Battery')
e4 = axis1.errorbar(xMax, medians['mass']['useful'], yerr=[totalConfLow['mass']['useful'], totalConfHigh['mass']['useful']], 
               xerr=None, fmt='o',capsize=3., color=tumClr['TUMpptGray'],linewidth=1.0,
               marker='d', markersize=5, markeredgewidth=0.8,markeredgecolor='k', markerfacecolor='w', label='Useful')



# axis1.plot(xPos, masses, linewidth=0., marker='^', markersize=5, markeredgewidth=0.8, markeredgecolor='k', markerfacecolor='w',zorder=3)
# axis1.plot(xPos, mFuel, linewidth=0., marker='o', markersize=5, markeredgewidth=0.8,markeredgecolor='k', markerfacecolor='w',zorder=3)
# axis1.plot(xMin, mAirframe, linewidth=0., marker='s', markersize=4, markeredgewidth=0.8,markeredgecolor='k', markerfacecolor='w',zorder=3)
# axis1.plot(xMax, mUseful, linewidth=0., marker='d', markersize=5, markeredgewidth=0.8,markeredgecolor='k', markerfacecolor='w',zorder=3)

xLabels = ['Lift + Cruise', 'Compound', 'Tilt Wing', 'Tilt Rotor']

legend = [l1,l2,l3,l4,e1,e2,e3,e4]
axis1.legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2, fontsize=7.5,
          loc='lower center', bbox_to_anchor= (0.5, 1.01), ncol=2, borderaxespad=0, frameon=False) 

axis1.set_ylim(0.,2000.)
axis1.set_yticks(np.linspace(0.,2000.,6))
axis1.set_yticklabels(['0','400','800','1200','1600','2000'], fontdict={'fontsize':7})   
axis1.grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
axis1.set_ylabel('System Masses (kg)', fontsize=8)
axis1.set_xticks(xPos)
axis1.set_xticklabels(xLabels, rotation=45, fontsize=7)
axis1.set_xlim(min(xPos)-barw,max(xPos)+barw)
#axis1.set_xticks(xPos)

#%=============================================================================

# plt.bar(xPos, eCruise, width=barw, color=tumClr['TUMpptGray'],alpha=0.8)
# plt.bar(xPos, eHover, bottom=eCruise, width=barw, color=tumClr['TUMpptGray'],alpha=0.6)
# plt.bar(xPos, eReserve, bottom=eHovCrs, width=barw, color=tumClr['TUMpptGray'],alpha=0.4)
# for i, x in enumerate(xPos):
#     plt.plot(x, reserve[i], 'ko')
#     plt.plot(x, reserve[i]+hover[i], 'ko')
#     plt.plot(x, reserve[i]+hover[i]+cruise[i], 'ko')

# plt.plot(xPos, eSum, 'ko')

# plt.ylim(0.,200.)
# plt.xlim(min(xPos)-barw,max(xPos)+barw)
# plt.xticks(xPos)
# plt.show()

#%=============================================================================

axis2 = plt.subplot(grid[1])
# print(totalConfLow['power']['hover'])

xMin = [x-barw/4 for x in xPos]
xMax = [x+barw/4 for x in xPos]

l5 = axis2.bar(xMin, pCruise, width=barw/2, color=tumClr['TUMpptGray'],alpha=0.8, label='Cruise (Ref.)')
l6 = axis2.bar(xMax, pHover, width=barw/2, color=tumClr['TUMpptGray'],alpha=0.4, label='Hover (Ref.)')

e5 = axis2.errorbar(xMax, medians['power']['hover'], yerr=[totalConfLow['power']['hover'], totalConfHigh['power']['hover']], 
               xerr=None, fmt='o',capsize=3., markersize=5, marker='s', markeredgecolor='k', markerfacecolor='w',
               color=tumClr['TUMpptGray'],linewidth=1.0, label='Cruise')
e6 = axis2.errorbar(xMin, medians['power']['cruise'], yerr=[totalConfLow['power']['cruise'], totalConfHigh['power']['cruise']], 
               xerr=None, fmt='o',capsize=3., markersize=5, marker='d', markeredgecolor='k', markerfacecolor='w',
               color=tumClr['TUMpptGray'],linewidth=1.0, label='Hover')

# axis2.plot(xMin, powerCruise, linewidth=0., marker='s', markeredgecolor='k', markerfacecolor='w',zorder=3)
# axis2.plot(xMax, powerHover, linewidth=0., marker='d', markeredgecolor='k', markerfacecolor='w',zorder=3)

legend2 = [l5,l6,e5,e6]
axis2.legend(handles=legend2, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2, fontsize=8,
          loc='lower center', bbox_to_anchor= (0.5, 1.01), ncol=2, borderaxespad=0, frameon=False) 
axis2.set_ylim(0.,500.)
axis2.set_yticks(np.linspace(0.,500.,6))
axis2.set_yticklabels(['0','100','200','300','400','500'], fontdict={'fontsize':7})   
axis2.grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
axis2.set_ylabel('Battery Power Draw (kW)', fontsize=8)
axis2.set_xlim(min(xPos)-barw,max(xPos)+barw)
axis2.set_xticks(xPos)
axis2.set_xticklabels(xLabels, rotation=45, fontsize=7)

strTime = getTime()
SAVETO = 'output/dissVerificationBrown_' + strTime + '.pdf'
figure.savefig(SAVETO, bbox_inches='tight', dpi=250.)

plt.show()

