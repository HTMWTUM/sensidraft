#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 11:04:34 2020

@author: nicolas
"""

import os
os.chdir('..')

import sys
import numpy as np
import copy as cp
import matplotlib.pyplot as plt
from source.util import loadTUMcolors, getTime, printCase, loadPlotFonts

from data.inputVTOL import loadCase, loadTech, loadFueltype

from source.modSizingTier0 import getAircraftMass, getPowertrainComponents, getAircraftComponents

from matplotlib.lines import Line2D


# If the values of the ideal battery and the two hybrid battery packs are equal,
# the result of the sizing is equal. Proves that the flightstate-modelling and
# analysis of energies for each flight state, together with the energy supply through
# individual battery types, is valid and working!

# Advice if working with ideal battery: Get C-Rate via the equivalent P-Spec, i.e.:
# p-Spec LiPo = 25. (1/h) * 200 (Wh/kg) / 1.2 = 4167 W/kg
# equiv. C-Rate IdealBatt = 4167 (W/kg) * 1.2 / 300 (Wh/kg) = 16.7

# dIdealBattery = {'fueltype':      'battery',
#                   'ident':     'Lithium Ion',
#                   'esp':            300/1.20,          # Wh/kg (pack specific = cell specific / 1.2) 
#                   'eta_mtr':            0.97,
#                   'eta_dis':            0.95,
#                   'dcp':                1.03,          # - (battery discharge parameter - see L.Traub )
#                   #'motorError':            1,          # - (factor multiplied on the electric motor weight)
#                   'cRate':              3.5,
#                   'fDoD':               0.80,}

dLiMetal = loadTech('lmb', 'baseline')
# dLiMetal = {'fueltype':      'battery',
#             'ident':     'Lithium Metal',
#             'esp':            300/1.20,          # Wh/kg (pack specific = cell specific / 1.2) 
#             'eta_mtr':            0.97,
#             'eta_dis':            0.95,
#             'dcp':                1.03,          # - (battery discharge parameter - see L.Traub )
#             #'motorError':            1,          # - (factor multiplied on the electric motor weight)
#             'cRate':               3.5,
#             'fDoD':               0.80,}

dLiPo = loadTech('lpo', 'baseline')
# dLiPo = {'fueltype':       'battery',
#           'ident': 'Lithium Polymer',
#           'esp':             170/1.2,          # Wh/kg (pack specific = cell specific / 1.2) 
#           'eta_mtr':            0.97,
#           'eta_dis':            0.95,
#           'dcp':                1.03,          # - (battery discharge parameter - see L.Traub )
#           #'motorError':            1,          # - (factor multiplied on the electric motor weight)
#           'cRate':               25.,
#           'fDoD':                0.8,}

dFuelCellLiq = loadTech('h2gas', 'baseline')

# dFuelCellLiq = {'fueltype':        'fuelcell',
#                 'ident':            'Liquid H2',
#                 'fractionH2T':            0.20,
#                 'pSpec':                   800,         # kW / kg
#                 'lhv':                  33.322,         # kWh / kg
#                 'eta_fc':                 0.55,         # -
#                 'eta_mtr':                 1.0,}         # -
   

dHybridLMB = loadFueltype('hybridBattery', **{'highEnerSys': dLiMetal, 'highPowSys': dLiPo})
dHybridH2FC = loadFueltype('hybrid', **{'highEnerSys': dFuelCellLiq, 'highPowSys': dLiPo})

import sys
# sys.exit()
#vtol = 'city'

#hybridLMB = loadPredefCase('city', dHybridLMB)

#hybridLMB['fuel']['calcOptHybr'] = False
#simpleLMB = loadPredefCase(vtol, dIdealBattery)
#print(getPowertrainComponents(hybridLMB))

fDOH = np.arange(0.0,1.01,0.01)

import matplotlib.gridspec as gs

fig = plt.figure(figsize=(5.809,1.809))

# set grid object
grid = gs.GridSpec(1, 3, bottom=0.05, right=0.95, left=0.1, top=0.95, wspace=0.05)

caseA = loadCase('city', 'baseline', dHybridLMB)
caseA['mission']['cruiseRange'] = 50000.
caseA['mission']['hoverTime'] = 90.

caseB = loadCase('lilium', 'baseline', dHybridLMB)
caseB['mission']['cruiseRange'] = 200000.
# caseB['mission']['hoverTime'] = 90.
# caseB['mission']['hoverTime'] = 90.

caseC = loadCase('skai', 'baseline', dHybridH2FC)
caseC['mission']['cruiseRange'] = 300000.

print(caseC['fuel']['highEnerSys'])
sys.exit()
lstCases = [caseA,
            caseB,
            caseC,
            ]

#print(lstCases)


dPtr = getPowertrainComponents(caseA)

# print(dPtr)
# sys.exit()

# iterate 

greys = plt.get_cmap('Greys')
blues = plt.get_cmap('Blues')
greens = plt.get_cmap('Greens')
oranges = plt.get_cmap('Oranges')


for i, case in enumerate(lstCases):
    
    massPowerBattery = []
    massEnergyBattery = []
    massTotalBattery = []
    
    massStackWeight = []
    massFuelAndTank = []
    massTotalPowertrain = []
    
    for f in fDOH:
    
        fOpt = 1. - case['mission']['cruiseVelo'] * case['design']['pl'] / case['design']['ltod']
        print(fOpt)
        case['fuel']['fHybrid'] = f
        case['fuel']['calcOptHybr'] = False
        dPtr = getPowertrainComponents(case)
        
        if case['fuel']['fueltype'] == 'hybridBattery':
            massPowerBattery.append(dPtr['powerBatt'])
            massEnergyBattery.append(dPtr['energyBatt'])
            massTotalBattery.append(massPowerBattery[-1] + massEnergyBattery[-1])
        elif case['fuel']['fueltype'] == 'hybrid':
            massPowerBattery.append(dPtr['powerBatt'])
            massStackWeight.append(dPtr['stackCell'])
            massFuelAndTank.append(dPtr['hydrogen'] + dPtr['tank'])
            massTotalBattery.append(massPowerBattery[-1] + massStackWeight[-1] + massFuelAndTank[-1])
            #print(dPtr)
            
    axis = plt.subplot(grid[i])
    
    if case['fuel']['fueltype'] == 'hybridBattery':

        hes_batt, = axis.plot(fDOH, massEnergyBattery, color=greens(0.6), linewidth=0.8, linestyle='-',
                  marker='s', markerfacecolor=greens(0.6), markeredgewidth=0.8, markersize=4, markevery=7,
                  label='Li-Metal Battery (HES)')
        # axis.plot(fDOH, massTotalBattery, color=greens(1.0), linewidth=0.8, linestyle='-',
        #           marker='.', markerfacecolor=greens(1.0), markeredgewidth=0.8, markersize=4, markevery=7)
        # # axis.axvline(fOpt, color=oranges(0.8), linestyle='--', linewidth=1.0)
    elif case['fuel']['fueltype'] == 'hybrid':
        # axis.plot(fDOH, massPowerBattery, color=greens(0.6), linewidth=0.8, linestyle='-',
        #           marker='o', markerfacecolor='w', markeredgewidth=0.8, markersize=4, markevery=7,
        #           label = 'Li-Polymer Battery (HPS)')
        hes_fcell, = axis.plot(fDOH, massStackWeight, color=blues(0.6), linewidth=0.8, linestyle='-',
                  marker='^', markerfacecolor=blues(0.6), markeredgewidth=0.8, markersize=4, markevery=7,
                  label='Fuel Cell Stack (HES)')
        hes_hydr, = axis.plot(fDOH, massFuelAndTank, color=blues(0.6), linewidth=0.8, linestyle='-',
                  marker='P', markerfacecolor='w', markeredgewidth=0.8, markersize=4, markevery=7,
                  label = 'Hydrogen & Tank (HES)') 
    total, = axis.plot(fDOH, massTotalBattery, color=greys(1.0), linewidth=0.8, linestyle='-',
              marker='.', markerfacecolor=greys(1.0), markeredgewidth=0.8, markersize=4, markevery=7,
              label='Total Weight (HES + HPS)')
    optimum = axis.axvline(fOpt, color=oranges(0.8), linestyle='--', linewidth=1.0, label='Optimum DoH (calc.)')
    
    hps, = axis.plot(fDOH, massPowerBattery, color=greens(0.6), linewidth=0.8, linestyle='-',
              marker='o', markerfacecolor='w', markeredgewidth=0.8, markersize=4, markevery=7,
              label = 'Li-Polymer Battery (HPS)')
    if i == 0:
        axis.set_xlim(0.0,1.0)
        axis.set_xticks(np.arange(0.1,1.0,0.2))
        axis.set_title('Wingless w. LMB + LPO\n(Design Range = 50km)', fontsize=7)
        axis.set_ylabel('Mass (kg)', fontsize=7)
    elif i ==1:
        axis.set_xlim(0.4,1.0)
        axis.set_xticks(np.arange(0.5,1.0,0.2))
        axis.set_yticklabels([])
        axis.set_title('Vectored w. LMB + LPO\n(Design Range = 200km)', fontsize=7)
    else:
        axis.set_xlim(0.0,0.6)
        axis.set_xticks(np.arange(0.1,0.6,0.2))
        axis.set_yticklabels([])
        axis.set_title('Wingless w. GH2 + LPO\n(Design Range = 300km)', fontsize=7)
        #w. 
    axis.set_xlabel('Degree of Hybridization (-)', fontsize=7)
    axis.set_ylim(0.,1400.)
    axis.set_yticks(np.linspace(0.0,1400.,8))
    axis.grid(which='both', axis='both', linestyle='-', linewidth=0.1, color='gray')  
   

# legend = [Line2D([], [], color=greens(0.8), linestyle='-', label='Li-Polymer Battery (HPS)'),
#           Line2D([], [], color=greens(0.8), linestyle='--', label='Li-Metal Battery (HES)'),
#           Line2D([], [], color=blues(0.8), linestyle=':', label='Fuel Cell Stack (HES)'),
#           Line2D([], [], color=blues(0.8), linestyle='--', label='Hydrogen & Tank (HES)'),
#           Line2D([], [], color=greys(1.0), linestyle='-', label='Total Powertrain Weight'),
#           Line2D([], [], color=greys(1.0), linestyle='-', label='Optimum DoH (calc.)')]


axis = plt.subplot(grid[1])

legend = [hps, hes_batt, hes_fcell, hes_hydr, total, optimum]
axis.legend(handles=legend, columnspacing = 1.0, handletextpad = 0.4, handlelength=1.5,
          loc='lower center', bbox_to_anchor= (0.5, 1.2), ncol=3, borderaxespad=0, frameon=False) 
# plt.tight_layout()

strTime = getTime()
SAVETO = 'output/dissHybridization_' + strTime + '.pdf'
fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)

plt.show()
