#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 14:55:52 2020

@author: Nicolas
"""

import sys
import os
os.chdir('..')

import copy

from data.inputVTOL import loadCase, loadTech, loadFueltype
from data.inputImpact import gmid, gcid, loadOpsImpact

from diss.outSensitivityAnalyses import dissSensitivitiesMasses
from diss.outSensitivityAnalyses import dissSensitivityImpactVehicle

from diss.outSizingAndAnalysis import dissSizingAndAnalyisPlot
from diss.outCaseAnalysis import dissCaseStudyLCA
from diss.outComponentImpacts import dissComponentImpactPlot

#%%============================================================================
#    M A T P L O T L I B   S E T T I N G S

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams['hatch.linewidth'] = 0.6

XSMALL = 6
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)         # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=XSMALL)       # fontsize of the tick labels
plt.rc('ytick', labelsize=XSMALL)       # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE) # fontsize of the figure title

#%%============================================================================
#    C A S E   D E F I N I T I O N

dHybridLMB = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lmb', 'baseline'), 
                                              'highPowSys': loadTech('lpo', 'baseline')})


caseVolo   =  loadCase('volo', 'baseline', dHybridLMB)
caseCora   =  loadCase('cora', 'baseline', dHybridLMB)
caseCity   =  loadCase('city', 'baseline', dHybridLMB)
caseVahana =  loadCase('vahana', 'baseline', dHybridLMB)
caseJoby   =  loadCase('joby', 'baseline', dHybridLMB)
caseLilium =  loadCase('lilium', 'baseline', dHybridLMB)


dHybridFuelCell = loadFueltype('hybrid', **{'highEnerSys': loadTech('h2liq', 'baseline'), 
                                            'highPowSys': loadTech('lpo', 'baseline')})

caseSkai =  loadCase('skai', 'baseline', dHybridFuelCell)


listCases = [copy.deepcopy(caseVolo), 
             copy.deepcopy(caseCity), 
             copy.deepcopy(caseVahana), 
             copy.deepcopy(caseCora), 
             copy.deepcopy(caseJoby), 
             copy.deepcopy(caseLilium), 
             copy.deepcopy(caseSkai)]

dCaseNames = {'volo':   'Volocopter',
              'city':   'City\nAirbus',
              'vahana': 'Vahana',
              'cora':   'Cora',
              'lilium': 'Lilium',
              'joby':   'Joby',
              'skai':   'Skai',
              }

mSource = {'volo': 900.,
          'city': 2200.,
          'vahana': 815.,
          'cora': None,
          'lilium': None,
          'joby': 2177,
          'skai': None,
          'hylevio': None,
          }


#%%============================================================================

# prepare the cases with the according sub dictonaries
for case in listCases:
    ID = case['design']['ident']
    # SET WHETHER DEBUGGING PRINTOUTS SHALL APPEAR
    case['info'] = {'debug': False, 'funcUnit': 'PKT'}
    # SET NAME FOR PRINTING THE CASE IN PLOTS
    case['name'] = dCaseNames[ID]
    # SET ORIGINAL WEIGHT COMMUNICATED BY COMPANIE - NOT USED FOR CALCULATIONS EXCEPT CASE PREP IN modPrepVTOLData.py
    case['mtom_official'] = mSource[ID]
    
    # SET ADDITIONAL PARAMETERS OR CHANGE EXISTING PARAS OF THE CASE DICTIONARIES
    # MISSION REQUIREMENTS
    case['mission']['fUtil'] = 1.0
    # OPERATIONS
    case['operations'] = copy.deepcopy(case['mission'])
    case['operations']['fUtil'] = 0.75
    case['operations']['fCirc'] = 1.20
    # COMPONENT DATA FOR MATERIAL IMPACT CALCULATION
    case['components'] = gcid()
    case['matImpact'] = gmid('baseline')
    # ECOSYSTEM, I.E. THE BACKGROUND DEFINITIONS    
    case['ecosys'] = {'vehicleLife': 8000.}
    
    # LOAD DATA FOR OPERATION IMPACT VALUES
    case['opsImpact'] = loadOpsImpact(case['fuel'], '2030base')



#%%============================================================================

dissSizingAndAnalyisPlot(listCases, True)
# sys.exit()

dissSensitivitiesMasses(listCases, True)
# sys.exit()

dissComponentImpactPlot(listCases, True)
# sys.exit()

dissSensitivityImpactVehicle(listCases, True)
#sys.exit()

dissCaseStudyLCA(listCases, True)
# sys.exit()

#dissSensitivityImpactLifeCycle(propulsionCases, False)

