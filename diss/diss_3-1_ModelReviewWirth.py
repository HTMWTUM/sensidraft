#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:48:13 2020

@author: Nicolas
"""


import sys

import os
os.chdir('..')

import numpy as np
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

from source.modDictLoader import loadAircraftDesign, loadFueltype, loadMissionReq
# from source.modStatistics import getSample
from source.modSizingTier0 import getAircraftMass

from source.util import loadTUMcolors, getTime

from source.modSensitivityAnalysis import sensitivityAnalysisOAT

greys = plt.get_cmap('Greys')
blues = plt.get_cmap('Blues')
greens = plt.get_cmap('Greens')
oranges = plt.get_cmap('Oranges')


def plotAxis(i, axis, interval, dCase, dResult):
    
    plt.title(dCase['design']['ident'])
    axis.tick_params(length=0.0)
    axis.set_yticklabels([])



    for k0, v0 in dResult.items():
        for k1, v1, in v0.items():
            if k1 == 'fStruct': 
                #clr = tumClr['TUMdarker100']
                clr=greys(0.8)
                mfc=greys(0.3)
                lsty='--'
                mrk='s'
                lbl = 'EW/DGW'
            elif k1 == 'ltod': 
                clr=greens(0.8)
                mfc=greens(0.3)
                lsty='-.-'
                mrk='o'
                lbl = 'L/D'
            elif k1 == 'pl':
                clr=oranges(0.8)
                mfc=oranges(0.3)
                mrk='^'
                lsty='-.'
                lbl = 'PL'
            else:
                mrk='v'
                lsty='-'
                lbl = 'sfc / $e_{spec}$'
                clr=blues(0.8)
                mfc=blues(0.3)
                # if dCase['fuel']['fueltype'] == 'battery':
                    # lbl = 'Espec'
                # elif dCase['fuel']['fueltype'] == 'kerosene':
                    # lbl = 'SFC'
            axis.plot(interval, v1, label=lbl, color=clr, 
                      marker=mrk, markersize=4, markeredgewidth=0.8, markerfacecolor=mfc,
                      linewidth=0.8)

    if i == 0: 
        axis.set_ylabel('Design gross mass (tons)')
        axis.set_yticklabels([r'$1.0$',r'$2.0$',r'$3.0$',r'$4.0$',r'$5.0$'])
        axis.legend(fontsize='x-small', loc='upper left')
    elif i == 1: 
        axis.set_xlabel('Variation from baseline '+r'$(\%)$')
        #axis.legend(fontsize='x-small', loc='upper left')
    elif i == 2:
        pass
        #axis.legend(fontsize='x-small', loc='lower left')
        
    axis.grid()
    axis.set_xlim(-0.125,0.125)
    axis.set_xticks([-0.1,-0.05,0.,0.05,0.1])
    
    axis.set_xticklabels(['-10','-5','0','5','10'])
    # axis.set_xticklabels([r'$-10.$',r'$-5.$',r'$0.$',r'$+5.$',r'$+10.$'])
    axis.set_ylim(1500.,4500.)

def plotContour(i, axis, dCase, X, Y, Z, clrs, levels):
    
    plt.title(dCase['design']['ident'])
    axis.tick_params(length=0.0)
    axis.set_yticklabels([])    
    axis.contour(X, Y, Z, levels, linewidths=0.8, colors=clrs)

    if i == 0: 
        axis.set_ylabel('Design gross mass (tons)')
        axis.set_yticklabels([r'$1.0$',r'$2.0$',r'$3.0$',r'$4.0$',r'$5.0$'])
        axis.plot(150000, 2199.56, marker='x', markersize=6, color=tumClr['TUMpptOrange'])

        #axis.legend(fontsize='x-small', loc='upper left')
    elif i == 1: 
        axis.set_xlabel('Design mission range (km)')
        axis.plot(150000, 3017.23, marker='x', markersize=6, color=tumClr['TUMpptOrange'])
#        #axis.legend(fontsize='x-small', loc='upper left')
    elif i == 2:
        axis.annotate('', xy=(92000, 3250), xytext=(123000, 1750), 
                      arrowprops=dict(arrowstyle='->',color=clrs[0]))
        axis.text(92000, 3200,r'$210sec$', ha="center", va="bottom", color=clrs[0])
        axis.text(121000, 1750,r'$60sec$', ha="left", va="top", color=clrs[0])
        axis.plot(150000, 3860.04, marker='x', markersize=6, color=tumClr['TUMpptOrange'])
        #        #axis.legend(fontsize='x-small', loc='lower left')
        
    axis.grid()
    axis.set_xticks([50000,75000,100000,125000,150000,175000])
    axis.set_xticklabels([r'$50$',r'$75$',r'$100$',r'$125$',r'$150$',r'$175$'])
    axis.set_ylim(1000.,5000.)

    axis.xaxis.get_majorticklabels()[0].set_horizontalalignment('left')

#%%============================================================================
#    P A R A M E T E R S
#%=============================================================================

# LOAD MISSION REQUIREMENTS
dRequiredMission = loadMissionReq({'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 20.}, 
                                   **{'ident': 'Sizing', 'cruiseRange': 150 * 1000,
                                      'cruiseVelo': 50.0, 'hoverTime': 2.0 * 60.})

# LOAD AIRCRAFT DESIGN
dDesignMTR = loadAircraftDesign(**{'ident': 'Turboshaft Helicopter', 
                                        'pl': 0.0465, 'ltod': 3.1,'fStruct': 0.735, 'fOth':0.0})
dDesignTLT = loadAircraftDesign(**{'ident': 'Turboshaft Tiltrotor', 
                                        'pl': 0.0375, 'ltod': 5.3,'fStruct': 0.809, 'fOth':0.0})
dDesignEDP = loadAircraftDesign(**{'ident': 'Electric Lift and Cruise', 
                                        #'pl': 0.0405, 'ltod': 11.,'fStruct': 0.47, 'fOth':0.0})
                                        'pl': 0.0405, 'ltod': 11.,'fStruct': 0.47, 'fOth':0.0})

# LOAD ENERGY STORAGE TYPES
dBatteryLiIon = loadFueltype('battery', **{'ident': 'liion',
                                           'esp': 260/1.2, 
                                           'eta_dis': 0.68, 
                                           'eta_mtr': 1.0,
                                           'cRate':  25.0,
                                           'fDoD':    1.0,
                                           'dcp':    1.03},)

dJetAMTR = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.365})

dJetATLT = loadFueltype('kerosene', **{'ident': 'conventional', 
                                    'sfc': 0.4150})

    
    
XSMALL = 6
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
       
iSamples = 25

# lower = -0.12
# bLow = 1.0+lower
# upper = 0.12
# bUp = 1.0+upper

bLow = 0.9
bUp = 1.1


interval = np.linspace(-0.3,0.3,iSamples)

dCaseMTR = {'design': dDesignMTR, 'mission': dRequiredMission, 'fuel': dJetAMTR}
print(dCaseMTR)
print()
dVariMTR = {'design': {'ltod': np.linspace(dCaseMTR['design']['ltod'] * bLow, dCaseMTR['design']['ltod'] * bUp, iSamples), 
                      'pl': np.linspace(dCaseMTR['design']['pl'] * bLow, dCaseMTR['design']['pl'] * bUp, iSamples), 
                      'fStruct': np.linspace(dCaseMTR['design']['fStruct'] * bLow, dCaseMTR['design']['fStruct'] * bUp, iSamples)},
           'fuel': {'sfc': np.linspace(dCaseMTR['fuel']['sfc'] * bLow, dCaseMTR['fuel']['sfc'] * bUp, iSamples)}}

dCaseTLT = {'design': dDesignTLT, 'mission': dRequiredMission, 'fuel': dJetATLT}
print(dCaseTLT)
print()
dVariTLT = {'design': {'ltod': np.linspace(dCaseTLT['design']['ltod'] * bLow, dCaseTLT['design']['ltod'] * bUp, iSamples), 
                      'pl': np.linspace(dCaseTLT['design']['pl'] * bLow, dCaseTLT['design']['pl'] * bUp, iSamples), 
                      'fStruct': np.linspace(dCaseTLT['design']['fStruct'] * bLow, dCaseTLT['design']['fStruct'] * bUp, iSamples)},
           'fuel': {'sfc': np.linspace(dCaseTLT['fuel']['sfc'] * bLow, dCaseTLT['fuel']['sfc'] * bUp, iSamples)}}

dCaseEDP = {'design': dDesignEDP, 'mission': dRequiredMission, 'fuel': dBatteryLiIon}
print(dCaseEDP)
print()
dVariEDP = {'design': {'ltod': np.linspace(dCaseEDP['design']['ltod'] * bLow, dCaseEDP['design']['ltod'] * bUp, iSamples), 
                       'pl': np.linspace(dCaseEDP['design']['pl'] * bLow, dCaseEDP['design']['pl'] * bUp, iSamples), 
                       'fStruct': np.linspace(dCaseEDP['design']['fStruct'] * bLow, dCaseEDP['design']['fStruct'] * bUp, iSamples)},
            'fuel': {'esp': np.linspace(dCaseEDP['fuel']['esp'] * bLow, dCaseEDP['fuel']['esp'] * bUp, iSamples)}}
             
# dmassOAT = sensitivityAnalysisOAT(getAircraftMass, dBaseline, dVari)
# plotter(dmassOAT)

print('Baseline Mass MTR:', getAircraftMass(dCaseMTR))
print('Baseline Mass TLT:', getAircraftMass(dCaseTLT))
print('Baseline Mass DEP:', getAircraftMass(dCaseEDP))
    
dsensAnsysMTR = sensitivityAnalysisOAT(getAircraftMass, dCaseMTR, dVariMTR)
dsensAnsysTLT = sensitivityAnalysisOAT(getAircraftMass, dCaseTLT, dVariTLT)
dsensAnsysEDP = sensitivityAnalysisOAT(getAircraftMass, dCaseEDP, dVariEDP)

tumClr = loadTUMcolors()

# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')   

figure = plt.figure(figsize=(5.809,1.7))
grid = gs.GridSpec(1, 3, bottom=0.05, right=0.95, left=0.1, top=0.95, wspace=0.1)
plotAxis(0, plt.subplot(grid[0]), interval, dCaseMTR, dsensAnsysMTR)
plotAxis(1, plt.subplot(grid[1]), interval, dCaseTLT, dsensAnsysTLT)
plotAxis(2, plt.subplot(grid[2]), interval, dCaseEDP, dsensAnsysEDP)
#figure.savefig('test.pdf', bbox_inches='tight', dpi=250.)
strTime = getTime()


SAVETO = 'output/dissSensitivitiesWirth_' + strTime + '.pdf'
figure.savefig(SAVETO, bbox_inches='tight', dpi=250.)

plt.show()

# sys.exit()

#%%============================================================================
#    C A S E  A N D  C O N T O U R  D A T A
#%=============================================================================

dCaseMTR = {'design': dDesignMTR, 'mission': dRequiredMission, 'fuel': dJetAMTR}
dCaseTLT = {'design': dDesignTLT, 'mission': dRequiredMission, 'fuel': dJetATLT}
dCaseEDP = {'design': dDesignEDP, 'mission': dRequiredMission, 'fuel': dBatteryLiIon}


iSamples = 6*5
ly = np.linspace(1.0*60, 3.5*60, iSamples)
lx = np.linspace(50000, 175000, iSamples)

tumClr = loadTUMcolors()

clrs = []
for s in range(iSamples):
    clrs.append((0/255, 82/255, 147/255, (255-25*s)/255))

X, Y = np.meshgrid(lx, ly)

print('Baseline Mass MTR:', getAircraftMass(dCaseMTR))
print('Baseline Mass TLT:', getAircraftMass(dCaseTLT))
print('Baseline Mass DEP:', getAircraftMass(dCaseEDP))

zMTR = np.zeros([len(lx), len(ly)])
zTLT = np.zeros([len(lx), len(ly)])
zEDP = np.zeros([len(lx), len(ly)])

for i, (lx, ly) in enumerate(zip(X, Y)):
    for j, (x, y) in enumerate(zip(lx, ly)):
        dCaseMTR['mission']['cruiseRange'] = x
        dCaseMTR['mission']['hoverTime'] = y
        zMTR[i][j] = getAircraftMass(dCaseMTR)
        zTLT[i][j] = getAircraftMass(dCaseTLT)
        zEDP[i][j] = getAircraftMass(dCaseEDP)
        #print(i, j, x, y, z)

#%%============================================================================
#    P L O T  
#%=============================================================================

# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')   

levels = [1.01*60, 1.5*60, 2.0*60, 2.5*60, 3.0*60, 3.49*60]

figure = plt.figure(figsize=(5.809,1.7))
grid = gs.GridSpec(1, 3, bottom=0.05, right=0.95, left=0.1, top=0.95, wspace=0.1)
plotContour(0, plt.subplot(grid[0]), dCaseMTR, X, zMTR, Y, clrs, levels)
plotContour(1, plt.subplot(grid[1]), dCaseTLT, X, zTLT, Y, clrs, levels)
plotContour(2, plt.subplot(grid[2]), dCaseEDP, X, zEDP, Y, clrs, levels)

strTime = getTime()
SAVETO = 'output/dissContourWirth_' + strTime + '.pdf'
figure.savefig(SAVETO, bbox_inches='tight', dpi=250.)
plt.show()

#
