#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 15:54:50 2020

@author: nicolas
"""

import sys

#import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.lines import Line2D
#from matplotlib.patches import Patch

from source.modStatistics import getSample
from source.modMonteCarlo import scalarFunMonteCarlo

# from source.util import loadTUMcolors, loadPlotFonts, getTime
from source.util import loadTUMcolors, getTime

from source.modAssessmentTier0 import calcProductionImpacts, calcLifeCycleImpact
from source.modAssessmentTier0 import getTotalProductionImpact, getTotalLifeCycleImpact
from data.inputImpact import gmiv, goiv
from data.inputVTOL import gtv, gdv, gmv

greens = plt.get_cmap('Greens')
greys = plt.get_cmap('Greys')

def normDict(dic):
    normDic = {}
    for k,v in dic.items():
        normDic[k] = float(v) / sum(dic.values()) * 100
    return normDic

def setColorVehicle(key):
    clr = {}
    if key == 'struct': 
        clr[key] = greys(0.8)
    elif key == 'other':
        clr[key] = greys(0.65)
    elif key == 'battery':
        clr[key] = greys(0.45)
    elif key == 'ptrain':
        clr[key] = greys(0.4)   
    elif key == 'stack':
        clr[key] = greys(0.3)   
    elif key == 'tank':
        clr[key] = greys(0.1)  
    else:
        print('Dunno this key')
    return clr

# def setAlphaVehicle(key):
#     alpha = {}
#     if key == 'struct': 
#         alpha[key] = 0.8
#     elif key == 'other':
#         alpha[key] = 0.65
#     elif key == 'battery':
#         alpha[key] = 0.45
#     elif key == 'ptrain':
#         alpha[key] = 0.2        
#     elif key == 'stack':
#         alpha[key] = 0.2    
#     elif key == 'tank':
#         alpha[key] = 0.2    
#     else:
#         print('Dunno this key')
#     return alpha

def setHatchColorVehicle(key):
    clr = {}
    if key == 'struct': 
        clr[key] = 'w'
    elif key == 'other':
        clr[key] = 'w'
    elif key == 'battery':
        clr[key] = 'w'
    elif key == 'ptrain':
        clr[key] = 'k'       
    elif key == 'stack':
        clr[key] = 'k'   
    elif key == 'tank':
        clr[key] = 'k'   
    else:
        print('Dunno this key')
    return clr

def setHatchehicle(key):
    hatch = {}
    if key == 'struct': 
        hatch[key] = ''
    elif key == 'other':
        hatch[key] = ''
    elif key == 'battery':
        hatch[key] = '///'
    elif key == 'ptrain':
        hatch[key] = ''        
    elif key == 'stack':
        hatch[key] = ''   
    elif key == 'tank':
        hatch[key] = '////'
    else:
        print('Dunno this key')
    return hatch


def setAlphaPkt(key):
    alpha = {}
    if key == 'struct': 
        alpha[key] = 0.8
    elif key == 'other':
        alpha[key] = 0.65
    elif key == 'ptrain':
        alpha[key] = 0.2  
    elif key == 'hydrProdImpact':
        alpha[key] = 1.0 
    elif key == 'hydrTankEquipImpact':
        alpha[key] = 1.0  
    elif key == 'fuelcellProdImpact':
        alpha[key] = 1.0 
    elif 'battery' in key:
        alpha[key] = 1.0
    elif 'elec' in key:
        alpha[key] = 1.0         
    else:
        print('Dunno this key')
    return alpha
 
 
 
def setHatchPkt(key):
    hatch = {}
    if key == 'struct': 
        hatch[key] = ''
    elif key == 'other':
        hatch[key] = ''
    elif key == 'ptrain':
        hatch[key] = ''  
    elif key == 'hydrProdImpact':
        # hatch[key] = '\\\\\\' 
        hatch[key] = '\\\\\\' 
    elif key == 'hydrTankEquipImpact':
        # hatch[key] = '\\\\\\'  
        hatch[key] = '\\\\\\'  
    elif key == 'fuelcellProdImpact':
        hatch[key] = ''  
    elif 'battery' in key:
        hatch[key] = '\\\\'
    elif 'elec' in key:
        hatch[key] = ''         
    else:
        print('Dunno this key')
    return hatch
   
def setColorPkt(key):
    tumClr = loadTUMcolors()
    clr = {}
    if key == 'struct': 
        clr[key] = 'k'
    elif key == 'other':
        clr[key] = 'k'
    elif key == 'ptrain':
        clr[key] = 'k'
    elif key == 'hydrProdImpact':
        clr[key] = greens(0.4)
    elif key == 'hydrTankEquipImpact':
        clr[key] = greens(0.9)
    elif key == 'fuelcellProdImpact':
        clr[key] = greens(0.2)
    elif 'battery' in key:
        clr[key] = greens(0.7)
    elif 'elec' in key:
        clr[key] = greens(0.4)
    else:
        print('Dunno this key')
    return clr    



def setHatchColorPkt(key):
    tumClr = loadTUMcolors()
    clr = {}
    if key == 'struct': 
        clr[key] = 'w'
    elif key == 'other':
        clr[key] = 'w'
    elif key == 'ptrain':
        clr[key] = 'w'
    elif key == 'hydrProdImpact':
        clr[key] = greens(1.0)
    elif key == 'hydrTankEquipImpact':
        clr[key] = greens(1.0)
    elif key == 'fuelcellProdImpact':
        clr[key] = greens(1.0)
    elif 'battery' in key:
        clr[key] = 'w'
    elif 'elec' in key:
        clr[key] = greens(0.9)
    else:
        print('Dunno this key')
    return clr    


#%%
def dissComponentImpactPlot(sortedCases, boolPrint):

    dCompNames = {'struct': 'Structure',
                  'other': 'Other',
                  'battery': 'Battery',
                  'tank': 'Tank',
                  'stack': 'Stack',
                  'ptrain': 'Motor',
                  }
    labels = []



    tumClr = loadTUMcolors()
    # loadPlotFonts()

    impactsVeh = []
    dImpactsPKT = []

    #dCompMaterials = gcid()
    #dMatImpacts = gmid('baseline')
    

    impactPerVehicle = {}
    normPerVehicle = {}
    sumPerVehicle = {}
    
    impactPerPKT = {}
    normPerPKT = {}
    sumPerPKT = {}
    
    impLowerD = []
    impUpperD = []
    impMeanD = []

    impLowerI = []
    impUpperI = []
    impMeanI = []    

    pktLowerD = []
    pktUpperD = []
    pktMeanD = []   

    pktLowerI = []
    pktUpperI = []
    pktMeanI = []  
    
    iS = 10000
    from scipy.stats import scoreatpercentile  
    
    for case in sortedCases:
        ID = case['design']['ident']       
        labels.append(case['name'])
        
        ### DETERMINISTIC ASSESSMENT
        # COMPONENTS - PER VEHICLE IMPACT
        dImpactsProdVeh = calcProductionImpacts(case)        
        impactPerVehicle[ID] = dImpactsProdVeh
        normPerVehicle[ID] = normDict(dImpactsProdVeh)
        sumPerVehicle[ID] = sum(dImpactsProdVeh.values())

        # LIFE-CYCLE IMPACT ASSESSMENT
        dImpactsPKT = calcLifeCycleImpact(case)       
        impactPerPKT[ID] = dImpactsPKT
        normPerPKT[ID] = normDict(dImpactsPKT)
        #sumPerPKT[ID] = sum(dImpactsPKT.values())
        sumPerPKT[ID] = getTotalLifeCycleImpact(case)


        fuelID = case['fuel']['highEnerSys']['ident']

        ### PROBABILISTIC

        sampleMat = {'aluminium': getSample('triang', [gmiv('cleanFuture','aluminium'), gmiv('baseline','aluminium'), gmiv('worstCase','aluminium')], iS),
                    'steel':     getSample('triang', [gmiv('cleanFuture','steel'), gmiv('baseline','steel'), gmiv('worstCase','steel')], iS),
                    'titan':     getSample('triang', [gmiv('cleanFuture','titan'), gmiv('baseline','titan'), gmiv('worstCase','titan')], iS),
                    'copp':      getSample('triang', [gmiv('cleanFuture','copp'), gmiv('baseline','copp'), gmiv('worstCase','copp')], iS),
                    'gfrp':      getSample('triang', [gmiv('cleanFuture','gfrp'), gmiv('baseline','gfrp'), gmiv('worstCase','gfrp')], iS),
                    'cfrp':      getSample('triang', [gmiv('cleanFuture','cfrp'), gmiv('baseline','cfrp'), gmiv('worstCase','cfrp')], iS),
                    'plastic':   getSample('triang', [gmiv('cleanFuture','plastic'), gmiv('baseline','plastic'), gmiv('worstCase','plastic')], iS),
                    'battery':   getSample('triang', [gmiv('cleanFuture','battery'), gmiv('baseline','battery'), gmiv('worstCase','battery')], iS),
                    }
                     

        if case['fuel']['fueltype'] == 'hybridBattery':
            sampleHES = {'esp':      getSample('triang',[gtv(fuelID,'min','esp'),gtv(fuelID,'baseline','esp'),gtv(fuelID,'max','esp')], iS),
                         'fDoD':     getSample('triang',[gtv(fuelID,'min','fDoD'),gtv(fuelID,'baseline','fDoD'),gtv(fuelID,'max','fDoD')], iS),
                         'eta_mtr':  getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
                         }            
        elif case['fuel']['fueltype'] == 'hybrid':
            sampleHES = {'pSpec':       getSample('triang',[gtv(fuelID,'min','pSpec'),gtv(fuelID,'baseline','pSpec'),gtv(fuelID,'max','pSpec')], iS),
                         'fractionH2T': getSample('triang',[gtv(fuelID,'min','fractionH2T'),gtv(fuelID,'baseline','fractionH2T'),gtv(fuelID,'max','fractionH2T')], iS),
                         'eta_mtr':     getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
                         'eta_fc':      getSample('triang',[gtv(fuelID,'min','eta_fc'),gtv(fuelID,'baseline','eta_fc'),gtv(fuelID,'max','eta_fc')], iS),
                        }
            
        sampleD = {'design':     {'ltod':    getSample('triang', [gdv(ID,'min','ltod'), gdv(ID,'baseline','ltod'), gdv(ID,'max','ltod')], iS),
                                  'pl'  :    getSample('triang', [gdv(ID,'min','pl'), gdv(ID,'baseline','pl'), gdv(ID,'max','pl')], iS),
                                  'fStruct': getSample('triang', [gdv(ID,'min','fStruct'), gdv(ID,'baseline','fStruct'), gdv(ID,'max','fStruct')], iS),
                                  },
                  'mission':    {'hoverTime': getSample('triang', [gmv(ID,'min','hoverTime'), gmv(ID,'baseline','hoverTime'), gmv(ID,'max','hoverTime')], iS)},
                  'fuel':       {'highEnerSys': sampleHES}
                  }

        # import copy
        # sampleI = copy.deepcopy(sampleD)
        # sampleI.update(**{'matImpact': sampleMat})
        sampleI = {'matImpact': sampleMat}
    
        mcResultD, mcDesignsD = scalarFunMonteCarlo(getTotalProductionImpact, case, sampleD, iS)
        impMeanD.append(scoreatpercentile(mcResultD, 50.0))
        impLowerD.append(sumPerVehicle[case['design']['ident']] - scoreatpercentile(mcResultD, 2.5))
        impUpperD.append(scoreatpercentile(mcResultD, 97.5) - sumPerVehicle[case['design']['ident']])
 
       
        mcResultI, mcDesignsI = scalarFunMonteCarlo(getTotalProductionImpact, case, sampleI, iS)
        impMeanI.append(scoreatpercentile(mcResultI, 50.0))
        impLowerI.append(sumPerVehicle[case['design']['ident']] - scoreatpercentile(mcResultI, 2.5))
        impUpperI.append(scoreatpercentile(mcResultI, 97.5) - sumPerVehicle[case['design']['ident']])
  
        # ### PROBABILISTIC
        # if case['fuel']['fueltype'] == 'hybridBattery':
        #     #baseline = goiv(case['fuel'], '2030base', 'elecImpactHES')
        #     #dVarImp = {'elecImpactHES': getSample('triang', [baseline*0.9, baseline, baseline*1.1], iS)}
        #     dVarImp = {'elecImpactHES': getSample('triang', [goiv(case['fuel'], '2030green', 'elecImpactHES'), 
        #                                                      goiv(case['fuel'], '2030base', 'elecImpactHES'),
        #                                                      goiv(case['fuel'], 'baseline', 'elecImpactHES')], iS),
        #                'batteryProdImpactHES': getSample('triang', [goiv(case['fuel'], '2030green', 'batteryProdImpactHES'), 
        #                                                             goiv(case['fuel'], '2030base', 'batteryProdImpactHES'),
        #                                                             goiv(case['fuel'], 'baseline', 'batteryProdImpactHES')], iS),                       }
        #     dVarTech = {'highEnerSys': {'nCycles': getSample('triang', [750., 1000., 1250.], iS)}}
        # elif case['fuel']['fueltype'] == 'hybrid':
        #     #baseline = goiv(case['fuel'], '2030base', 'hydrProdImpact')
        #     #dVarImp = {'hydrProdImpact': getSample('triang', [baseline*0.9, baseline, baseline*1.1], iS)}
        #     dVarImp = {'hydrProdImpact': getSample('triang', [goiv(case['fuel'], '2030green', 'hydrProdImpact'), 
        #                                                       goiv(case['fuel'], '2030base', 'hydrProdImpact'),
        #                                                       goiv(case['fuel'], 'baseline', 'hydrProdImpact')], iS),
        #                'fuelcellProdImpact': getSample('triang', [goiv(case['fuel'], '2030green', 'fuelcellProdImpact'), 
        #                                                           goiv(case['fuel'], '2030base', 'fuelcellProdImpact'),
        #                                                           goiv(case['fuel'], 'baseline', 'fuelcellProdImpact')], iS),
        #                }
        #     dVarTech = {'highEnerSys': {'opHours': getSample('triang', [3500., 4500., 5500.], iS)}}
        
        # samplePKT = {'opsImpact': dVarImp,
        #               'fuel': dVarTech,
        #               } 

        pktResultD, pktDesignsD = scalarFunMonteCarlo(getTotalLifeCycleImpact, case, sampleD, iS)
        
        pktMeanD.append(scoreatpercentile(pktResultD, 50.0))
        #pktMean.append(np.mean(pktResult))
        pktLowerD.append(pktMeanD[-1] - scoreatpercentile(pktResultD, 2.5))
        pktUpperD.append(scoreatpercentile(pktResultD, 97.5) - pktMeanD[-1])

        pktResultI, pktDesignsI = scalarFunMonteCarlo(getTotalLifeCycleImpact, case, sampleI, iS)
        
        pktMeanI.append(scoreatpercentile(pktResultI, 50.0))
        #pktMean.append(np.mean(pktResult))
        pktLowerI.append(pktMeanI[-1] - scoreatpercentile(pktResultI, 2.5))
        pktUpperI.append(scoreatpercentile(pktResultI, 97.5) - pktMeanI[-1])        

    xRange = range(len(sortedCases))

 
    from matplotlib.lines import Line2D
    from matplotlib.patches import Patch
    

    textwidth = 5.809    
    textheight = 8.275

    # fig, ax = plt.subplots(figsize=(textwidth,0.65*textheight),
    #                         nrows=2, ncols=1, sharex=True, sharey=False,
    #                         gridspec_kw={'hspace': 0.1, 
    #                                     'height_ratios': [2, 1.5]})
        
    fig1, ax1 = plt.subplots(figsize=(textwidth,0.4*textheight))
    par1 = ax1.twinx()

    fig2, ax2 = plt.subplots(figsize=(textwidth,0.3*textheight))

    axes = [ax1, ax2]
    for ax in axes:
        ax.set_xlim(min(xRange)-0.5, max(xRange)+0.5)
        ax.xaxis.set_ticks([])

    ax1.get_xaxis().set_ticks([])

    barwidth=0.4
    
        
    xVeh, xPkt = [], []
    yVeh, yPkt = [], []
    xVehD, xVehI = [], []
    xPktD, xPktI = [], []
    
    legend2 = []
    for i, case in enumerate(sortedCases):
          
        ident = case['design']['ident']
        print(i, case['design']['ident'])
        
        #--------------------------------
        #dImpVeh = impactPerVehicle[ident]
        dNormVeh = normPerVehicle[ident]
        yVeh.append(sumPerVehicle[ident])
        #yVeh.append(impMean)
        xVeh.append(i-barwidth/2)
        xVehD.append(i-barwidth*(3/4))
        xVehI.append(i-barwidth*(1/4))

        bVeh = 0.
        for k, (key, norm) in enumerate(dNormVeh.items()):
            # alpha = setAlphaVehicle(key)   
            hatch = setHatchehicle(key)
            htclr = setHatchColorVehicle(key)
            colors = setColorVehicle(key)
            ax2.bar(xVeh[-1], norm, bottom=bVeh, 
                    color=colors[key], hatch=hatch[key],
                    # color='k', alpha=alpha[key], hatch=hatch[key],
                    #color='k', alpha=(k+1)/len(dNormVeh.items()),
                    edgecolor=htclr[key], width=barwidth, linewidth=0.0)
            bVeh = bVeh + norm
        
        #--------------------------------
        #dImpPkt = impactPerPKT[ident]
        dNormPkt = normPerPKT[ident]
        yPkt.append(sumPerPKT[ident])
        xPkt.append(i+barwidth/2)
        xPktD.append(i+barwidth*(1/4))
        xPktI.append(i+barwidth*(3/4))
        
        # print(xPkt)

        bPkt = 0.
        for k, (key, norm) in enumerate(dNormPkt.items()):

            alpha = setAlphaPkt(key)  
            clr = setColorPkt(key)
            hatch = setHatchPkt(key)
            htclr = setHatchColorPkt(key)


            ax2.bar(xPkt[-1], norm, bottom=bPkt, 
                    color=clr[key], alpha=alpha[key], hatch=hatch[key],
                    #color='green', alpha=k/len(dNormVeh.items()),
                    edgecolor=htclr[key], width=barwidth, linewidth=0.0)
            bPkt = bPkt + norm            


    ax1.bar(xVeh, yVeh, color='k', alpha=0.5, width=barwidth)            
    #ax1.bar(xVeh, impMeanI, color='k', alpha=0.4, width=barwidth)            

    par1.bar(xPkt, yPkt, color=greens(0.7), alpha=0.6, width=barwidth)    
    #par1.bar(xPkt, pktMeanI, color=greens(0.7), alpha=0.4, width=barwidth)    

    ax1.errorbar(xVehD, impMeanD, yerr=(impLowerD,impUpperD),
                color='k', linewidth=0.0, linestyle='-',
                marker='o', markersize = 5., markerfacecolor='w',
                elinewidth =0.8, capsize=3., 
                zorder=1.)

    ebA = ax1.errorbar(xVehI, impMeanI, yerr=(impLowerI,impUpperI),
                color='k', linewidth=0.0, linestyle='--',
                marker='^', markersize = 5., markerfacecolor='k',
                elinewidth =0.8, capsize=3., 
                zorder=1.)
    ebA[-1][0].set_linestyle('--')


    # ax1.plot(xVeh, yVeh,
    #           color='k', linewidth=0.0, linestyle='--',
    #           marker='o', markersize = 3., markerfacecolor='k') 
    
    par1.errorbar(xPktD, pktMeanD, yerr=(pktLowerD,pktUpperD),
                color=greens(0.9), linewidth=0.0, linestyle='-',
                marker='o', markersize = 5., markerfacecolor='w',
                elinewidth =0.8, capsize=3., 
                zorder=1.)   

    ebP = par1.errorbar(xPktI, pktMeanI, yerr=(pktLowerI,pktUpperI),
                color=greens(0.9), linewidth=0.0, linestyle='--',
                marker='^', markersize = 5., markerfacecolor=greens(0.9),
                elinewidth =0.8, capsize=3., 
                zorder=1.)       
    ebP[-1][0].set_linestyle('--')

    y1max = 80000.
    y2max = 300.

    ax1.set_ylim(0.,y1max)
    ax1.set_yticks([0.,20000.,40000.,60000.,80000.])
    import numpy as np
    ax1.set_yticks(np.linspace(0.0,80000.,9))
    ax1.set_yticklabels(['0','10','20','30','40','50','60','70','80'])
    
    par1.set_ylim(0.,y2max)
    par1.set_yticks([0.,50.,100.,150.,200.,250.,300.])
    par1.set_yticklabels(['0','50','100','150','200','250','300'])

    ax1.set_xticks(range(len(sortedCases)))
    ax1.set_xticklabels(labels, fontsize=8)

    for tick in ax1.get_xticklabels():
        tick.set_verticalalignment("center")

    ax1.tick_params(axis='x', pad=15., length=0.0)    
 
    ax2.set_xticks(range(len(sortedCases)))
    ax2.set_xticklabels(labels)

    for tick in ax2.get_xticklabels():
        tick.set_verticalalignment("center")

    ax2.tick_params(axis='x', pad=15., length=0.0)  

    ax1.grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
    ax1.set_ylabel('Production Carbon Emission (t CO2e/VTOL)')
    par1.set_ylabel('Life-Cycle Carbon Emission (gCO2e/PKT)')

    ax2.grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
    ax2.set_ylim(0.,100.)
    ax2.set_ylabel('Breakdown of Impacts (%)', fontsize=8)
   

    legend = [Patch(facecolor='k', alpha=0.5, label='Production'),
              Patch(facecolor=greens(0.7), alpha=0.7, label='Life-Cycle'),
               ######
               Line2D([], [], color='k', marker='o', markersize = 5.,markerfacecolor='w', 
                      label='Variation of Design, Tech, Req',linewidth=0.8),
               Line2D([], [], color=greens(0.9), marker='o', markersize = 5.,markerfacecolor='w', 
                      label='Variation of Design, Tech, Req',linewidth=0.8),
               ######
               Line2D([], [], color='k', marker='^', markersize = 5.,markerfacecolor='k', 
                      label='Variation of Material Impact',linewidth=0.8, linestyle='--'),
               Line2D([], [], color=greens(0.9), marker='^', markersize = 5.,markerfacecolor=greens(0.9), 
                      label='Variation of Material Impact',linewidth=0.8, linestyle='--'),              
               ]

    # legendVeh = [Patch(facecolor='k', alpha=0.80, hatch='', label='Structure'),
    #              Patch(facecolor='k', alpha=0.65, hatch='', label='Others'),
    #              Patch(facecolor='k', alpha=0.45, hatch='//', label='Battery'),
    #              Patch(facecolor='k', alpha=0.2, hatch='', label='Powertrain'),
    #              Patch(facecolor='k', alpha=0.2, hatch='', label='Fuel Stack'),
    #              Patch(facecolor='k', alpha=0.2, hatch='\\\\\\', label='Fuel Tank'),               
    #             ]
    
    legendVeh = [Patch(facecolor=greys(0.8), hatch='', label='Structure'),
                  Patch(facecolor=greys(0.65), hatch='', label='Others'),
                  Patch(facecolor=greys(0.45), edgecolor='w', linewidth=0.0, hatch='///', label='Battery'),
                  Patch(facecolor=greys(0.2),  hatch='', label='Powertrain'),
                  Patch(facecolor=greys(0.2),  hatch='', label='Stack'),
                  Patch(facecolor=greys(0.2),  edgecolor='k', linewidth=0.0, hatch='/////', label='Tank'),       
                  # ----
                  Patch(facecolor=greens(0.7), edgecolor='w', linewidth=0.0, hatch='\\\\\\', label='Battery'),
                  Patch(facecolor=greens(0.4), hatch='', label='Electricity'),
                  Patch(facecolor=greens(0.2), hatch='', label='Stack'),               
                  Patch(facecolor=greens(0.9), edgecolor=greens(1.0), linewidth=0.0, hatch='\\\\\\\\', label='Tank'),               
                  Patch(facecolor=greens(0.4), edgecolor=greens(1.0), linewidth=0.0, hatch='\\\\\\\\', label='Hydrogen'),
                  ]

    # legendPkt = [Patch(facecolor='k', alpha=0.80, hatch='', label='Structure'),
    #              Patch(facecolor='k', alpha=0.65, hatch='', label='Others'),
    #              Patch(facecolor='k', alpha=0.2, hatch='', label='Powertrain'),
    #              ###
               
    #             ]  
    
    ax1.legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.5, fontsize=8,
              loc='lower center', bbox_to_anchor= (0.5, 1.02), ncol=3, borderaxespad=0, frameon=False) 

    ax2.legend(handles=legendVeh, columnspacing = 0.6, handletextpad = 0.4, handlelength=2.0, 
              loc='lower center', bbox_to_anchor= (0.5, 1.02), ncol=6, borderaxespad=0, frameon=False) 

    # par2 = ax2.twinx()
    # par2.set_yticks([])
    # par2.legend(handles=legendPkt, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.5,
    #           loc='center right', bbox_to_anchor= (0.9, 1.02), ncol=2, borderaxespad=0, frameon=True) 
 

    ax2.set_xticks(range(len(sortedCases)))
    ax2.set_xticklabels(labels, fontsize=8)


    plt.tight_layout()  
    if boolPrint:

        strTime = getTime()
        SAVETO1 = 'output/dissAssessmentVTOL_' + strTime + '.pdf'
        fig1.savefig(SAVETO1, bbox_inches='tight', dpi=250.)
        SAVETO2 = 'output/dissAssessmentBreakdown_' + strTime + '.pdf'
        fig2.savefig(SAVETO2, bbox_inches='tight', dpi=250.)
        
    plt.show()

#     # import sys
#     # sys.exit()
    
