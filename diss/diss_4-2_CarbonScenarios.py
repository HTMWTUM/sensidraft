#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 13:58:22 2021

@author: nicolas
"""
import os
os.chdir('..')

import matplotlib.pyplot as plt
from source.util import getTime

gridCarbInt = {'WRLD' : [],
               'CN' : [],
               'IN' : [],
               'SEA' : [],
               'US' : [],
               'EU' : []}

str2010 = [525.36,748.81,804.01,586.32,526.98,344.12]
str2011 = [532.79,763.38,775.29,573.68,506.79,347.09]
str2012 = [531.79,739.52,852.17,551.89,483.58,347.35]
str2013 = [527.64,724.37,811.75,551.47,485.60,328.65]
str2014 = [517.62,680.28,833.94,571.64,481.79,313.49]
str2015 = [501.80,651.29,771.56,577.88,451.91,307.12]
str2016 = [486.35,627.73,722.56,564.37,429.57,289.66]
str2017 = [481.71,622.60,717.24,562.23,417.58,284.50]
str2018 = [475.60,612.57,708.78,570.71,405.47,269.01]
str2025 = [345.61,478.20,503.03,486.31,263.01,137.34]
str2030 = [237.22,340.11,349.77,326.48,162.78,72.16]
str2035 = [147.83,207.91,214.76,196.09,93.46,51.64]
str2040 = [80.83,100.53,107.78,117.80,45.20,32.25]

years = [2010,2011,2012,
         2013,2014,2015,
         2016,2017,2018,
         2025,2030,2035,2040]

allYrs = [str2010,str2011,str2012,
          str2013,str2014,str2015,
          str2016,str2017,str2018,
          str2025,str2030,str2035,str2040]


for y, yr in enumerate(allYrs):
    for key, value in zip(gridCarbInt.keys(),yr):
        gridCarbInt[key].append(value)
        print(y, key, value)


#%% 

textwidth = 5.809    
textheight = 8.275

fig, axLeft = plt.subplots(figsize=(textwidth,0.37*textheight))
                       
labels = {'WRLD' : 'World',
        'CN' : 'China',
        'IN' : 'India',
        'SEA' : 'South East Asia',
        'US' : 'United States',
        'EU' : 'Europe'}

vir = plt.get_cmap('viridis')

colors = {'WRLD' : 'k',
        'CN' :  vir(0.9),
        'IN' :  vir(0.7),
        'SEA' : vir(0.5),
        'US' :  vir(0.3),
        'EU' :  vir(0.1)}

marks = {'WRLD' : 'o',
        'CN' :  's',
        'IN' :  'p',
        'SEA' : 'X',
        'US' :  '+',
        'EU' :  'd'}

from matplotlib.lines import Line2D


legend = []
#axRight = axLeft.twinx()
for region, impacts in gridCarbInt.items():
    legend.append(Line2D([], [], color=colors[region], marker=marks[region], markersize = 7., markerfacecolor='w', 
                      label=labels[region],linewidth=0.8))
    axLeft.plot(years, impacts, color=colors[region], marker=marks[region], markersize = 7., markerfacecolor='w', )

# axLeft.axvline(2018, 'k', linestyle='--')

axLeft.set_ylabel('Grid carbon intensity (gCO2e / kWh)')
axLeft.set_ylim(0.,900.)
axLeft.set_xlim(2010.,2040.)

axLeft.grid(axis='y', linestyle='-', linewidth=0.75, color='lightgray')  


#axRight.set_ylabel('Grid carbon intensity (gCO2e / kWh)')

axLeft.legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.5,
              loc='upper right', ncol=1, frameon=True) 

plt.tight_layout()  
boolPrint = True
if boolPrint:
    strTime = getTime()
    SAVETO = 'output/dissCarbonScenarios_' + strTime + '.pdf'
    fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)



#axLeft.set_xlim(0.,5.)

plt.show()