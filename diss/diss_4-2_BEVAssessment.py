#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 10:51:12 2020

@author: Nicolas
"""

import os
os.chdir('..')

from source.modSensitivityAnalysis import sensitivityBarplotOAT
from source.util import loadTUMcolors, getTime

# import matplotlib as mpl
# mpl.rcParams.update(mpl.rcParamsDefault)

import matplotlib.pyplot as plt 


#%%============================================================================
#    F U N C T I O N S
#%=============================================================================

def impactRoadConstruction(dArgs):  
    dCause = dArgs['cause']
    dRoad = dArgs['road']
    dCar = dArgs['car']
    impact = 1000 * dCause['roadConstruction'] * (1 - dCause['fRoadConstr']) / dRoad['nVehicles'] / dRoad['avgKm'] * dRoad['fAlloc'] 
    return impact / dCar['pax'] / dCar['fUtil'] * dRoad['fCirc']

def impactRoadOperation(dArgs):  
    dCause = dArgs['cause']
    dRoad = dArgs['road']
    dCar = dArgs['car']
    impact = 1000 * dCause['roadOperation'] * (1 - dCause['fRoadOps']) / dRoad['nVehicles'] / dRoad['avgKm'] * dRoad['fAlloc']  
    return impact / dCar['pax'] / dCar['fUtil'] * dRoad['fCirc']

def impactCarOperation(dArgs):
    dCause = dArgs['cause']
    dCar = dArgs['car']
    dRoad = dArgs['road']
    impact = dCause['gridIntensity'] * dCar['fuelEconOEM'] * dCar['fCorr'] / dCar['eta_tot'] 
    return impact / dCar['pax'] / dCar['fUtil'] * dRoad['fCirc']

def impactBatteryProductionMileage(dArgs):
    dCause = dArgs['cause']
    dBattery = dArgs['battery']
    dRoad = dArgs['road']
    dCar = dArgs['car']
    impact = 1000 * dCause['battProduction'] * dCause['fBattRecyc'] * dBattery['capacity'] / dBattery['batMileage']
    return impact / dCar['pax'] / dCar['fUtil'] * dRoad['fCirc']

def impactBatteryProductionCycles(dArgs):
    dCause = dArgs['cause']
    dBattery = dArgs['battery']
    dRoad = dArgs['road']
    dCar = dArgs['car']
    impact = 1000 * dCause['battProduction'] / dBattery['fDoD'] / dBattery['cycles'] * dCause['fBattRecyc']
    return impact / dCar['pax'] / dCar['fUtil'] * dRoad['fCirc']

def impactCarBody(dArgs):
    dCause = dArgs['cause']
    dCar = dArgs['car']
    dRoad = dArgs['road']    
    impact = 1000 * dCause['vehProduction'] * dCause['fVehProduction'] / dCar['vehMileage'] 
    return impact / dCar['pax'] / dCar['fUtil'] * dRoad['fCirc']

def impactCarProductionBEV(dArgs):    
    impBattery = impactBatteryProductionMileage(dArgs)
    impCarBody = impactCarBody(dArgs)    
    impact = impBattery + impCarBody    
    return impact

# def lifeCycleImpactBEV(dArgs):
#     roadCon = impactRoadConstruction(dArgs)
#     roadOps = impactRoadOperation(dArgs)
#     carOps = impactCarOperation(dArgs)
#     carProd = impactCarProductionBEV(dArgs)
#     return roadCon + roadOps + carOps + carProd

def lifeCycleImpactBEV(dArgs):
    carOps = impactCarOperation(dArgs)
    carProd = impactCarProductionBEV(dArgs)
    return carOps + carProd


# 1. Implement Bottom Up from Lilium Numbers

#%%============================================================================
#    D A T A
#%=============================================================================

#%%============================================================================
# R O A D  C O N S T R U C T I O N   A N D   O P E R A T I O N 
infra_roadConstruction_Impact_GER = 15.4e9       # kg CO         # UBA Study: https://www.umweltbundesamt.de/sites/default/files/medien/376/publikationen/texte_96_2013_treibhausgasemissionen_durch_infrastruktur_und_fahrzeuge_2015_01_07.pdf
infra_roadOperation_Impact_GER = 2.76e9          # kg CO         # UBA Study: https://www.umweltbundesamt.de/sites/default/files/medien/376/publikationen/texte_96_2013_treibhausgasemissionen_durch_infrastruktur_und_fahrzeuge_2015_01_07.pdf
infra_streetKilometers_GER = 644.48e3            # km            # https://www.laenderdaten.de/verkehr/strassennetz.aspx
infra_noVehicles_GER = 43.85123e6                # -             # https://www.markt.de/ratgeber/fahrzeuge/wie-viele-autos-gibt-es-in-deutschland/
infra_avgVehicleKilometers_GER = 13.727e3        # km            # UBA Study: https://www.umweltbundesamt.de/sites/default/files/medien/376/publikationen/texte_96_2013_treibhausgasemissionen_durch_infrastruktur_und_fahrzeuge_2015_01_07.pdf
infra_fAlloc_GER = 0.45                          # - (%)         # How many vehicles in service are actually used for the use case
infra_fCircuity_GER = 1.20                       # - (%)         # https://www.researchgate.net/publication/23526493_Selected_country_circuity_factors_for_road_travel_distance_estimation           
infra_fLearning = 0.0                            # - (%)         # Learning factor regarding the reduction of emissions

#%%============================================================================
# B E V   O P E R A T I O N
#ops_fuelEconomyBEV = 0.16            # kWh / km          # according to Tesla
ops_fuelEconomyBEV = 0.128           # kWh / km          # according to Tesla
ops_fCorrection = 1.3                # - (%)             # actual fuel economy compared to OEM value
ops_gridCarbonIntensity = 72.0       # gCO2e / kWh       # ...
ops_etaBEV = 1.0                     # - (%)             # Q: does this apply on top pf 30% correction?
ops_paxPerVehicle = 5.
ops_vehicleUtilization = 0.45 

#%%============================================================================
# B E V   V E H I C L E  &  B A T T E R Y   P R O D U C I T O N
bev_batteryCapacity = 60             # kWh
bev_batteryProduction = 62.0         # kg CO2e / kWh
bev_batteryLifetime_cycles = 6000    # - (charges)
bev_batteryLifetime_km = 300e3       # km
bev_battery_fDoD = 0.8               # 
bev_battery_fRecycling = 1.0         # - (%) impact adjusted for recycling

bev_vehicleProduction = 5.7e3        # kg
bev_vehicleLifetime_km = 300e3       # km
bev_vehicle_fProduction = 0.45       # km

#%%============================================================================
# V A R I A T I O N S  O N  A L L  T H E  D A T A

# cause
var_fRoadConstr    = [0.0,0.0,0.5]
var_vehProduction  = [bev_vehicleProduction*0.8, bev_vehicleProduction, bev_vehicleProduction*1.2]
var_fuelEcon       = [ops_fuelEconomyBEV*0.8, ops_fuelEconomyBEV, ops_fuelEconomyBEV*1.2]
var_fBattRecyc     = [0.0,0.0,0.5]
var_fVehProduction = [0.3,0.45,0.6]
var_gridIntensity  = [20., 20., 72.]
# road
var_fAlloc         = [0.4,0.45,0.5]
var_fCirc          = [1.00,1.20,1.46]
# vehicle
var_fUtil          = [0.25,0.45,0.65]
# battery
var_batMileage     = [200e3, 300e3, 400e3] 


#%%============================================================================
#    S E T   U P   D I C T S
#%=============================================================================

dImpact = {'battProduction'   : bev_batteryProduction,
           'vehProduction'    : bev_vehicleProduction,
           'roadConstruction' : infra_roadConstruction_Impact_GER,
           'roadOperation'    : infra_roadOperation_Impact_GER,
           'gridIntensity'    : ops_gridCarbonIntensity,
           'fBattRecyc'       : bev_battery_fRecycling,
           'fVehProduction'   : bev_vehicle_fProduction,
           'fRoadConstr'      : infra_fLearning,
           'fRoadOps'         : 0.0,
           }

dBattery = {'capacity'   : bev_batteryCapacity,           
            'cycles'     : bev_batteryLifetime_cycles,      
            'batMileage' : bev_batteryLifetime_km,      
            'fDoD'       : bev_battery_fDoD,                  
            }

dInfraRoad = {'streetKm' : infra_streetKilometers_GER,            
              'nVehicles': infra_noVehicles_GER,                  
              'avgKm'    : infra_avgVehicleKilometers_GER,            
              'fAlloc'   : infra_fAlloc_GER,                        
              'fCirc'    : infra_fCircuity_GER,                     
              }                       

dCarBEV = {'vehMileage'  : bev_vehicleLifetime_km,       
           'fuelEconOEM' : ops_fuelEconomyBEV,         
           'fCorr'       : ops_fCorrection,         
           'eta_tot'     : ops_etaBEV,               
           'pax'         : ops_paxPerVehicle,      
           'fUtil'       : ops_vehicleUtilization, 
            }

dCase = {'cause'   : dImpact,
         'car'     : dCarBEV, 
         'battery' : dBattery,
         'road'    : dInfraRoad}

# print('===============================================')
# print('Infrastructure Construction  : {:6.3f} gCO2e/PKT'.format(impactRoadConstruction(dCase)))
# print('Infrastructure Operation     : {:6.3f} gCO2e/PKT'.format(impactRoadOperation(dCase)))
# print('Electric Vehicle Operation   : {:6.3f} gCO2e/PKT'.format(impactCarOperation(dCase)))
# print('Battery Production (Cycles)  : {:6.3f} gCO2e/PKT'.format(impactBatteryProductionMileage(dCase)))
# print('Battery Production (Mileage) : {:6.3f} gCO2e/PKT'.format(impactBatteryProductionCycles(dCase)))
# print('Car & Battery Production     : {:6.3f} gCO2e/PKT'.format(impactCarProductionBEV(dCase)))
# print('===============================================')
# print('Life Cycle Assessment        : {:6.3f} gCO2e/PKT'.format(lifeCycleImpactBEV(dCase)))
# print('===============================================')


#%%============================================================================
#    S E N S I T I V I T I E S
#%============================================================================= 

lstKeys = ['min','base','max']

from source.modPlotRoutines import quickBarplotAxis

# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')  

figSize = (5.809,2.3)
tumci = loadTUMcolors()
dColors = {'darkest': tumci['TUMdarkest80'],
           'darker': tumci['TUMdarker80'],
           'lighter': tumci['TUMdarker50'],
           'lightest': tumci['TUMlighter50']}    

kwargs = {'color': dColors['darker'],
          'edgecolor': 'w',
          'height': 0.5,
          'linewidth': 1.}

#%%============================================================================
# Impact of Road Construction

dVariation = {#----------------------------------
             'cause': {'fRoadConstr' :  var_fRoadConstr},
             'road' : {'fAlloc'      :  var_fAlloc,
                       'fCirc'       :  var_fCirc},
             'car'  : {'fUtil'       :  var_fUtil}
             }

dRes, dVar, sVar = sensitivityBarplotOAT(impactRoadConstruction, dCase, dVariation, lstKeys)

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Impact from Road Construction')

plt.show()

#%%============================================================================
# Impact of Road Operation

dVariation = {#----------------------------------
             'road' : {'fAlloc'      :  var_fAlloc,
                       'fCirc'       :  var_fCirc},
             'car'  : {'fUtil'       :  var_fUtil}
             }

dRes, dVar, sVar = sensitivityBarplotOAT(impactRoadOperation, dCase, dVariation, lstKeys)

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Impact from Road Operation')
plt.show()

#%%============================================================================
# Impact of BEV Vehicle Operation
dVariation = {#----------------------------------
              'cause': {'gridIntensity'  :  var_gridIntensity},
              'road' : {'fCirc'          :  var_fCirc},
              'car'  : {'fuelEconOEM'    :  var_fuelEcon,
                        'fUtil'          :  var_fUtil},
              }

dRes, dVar, sVar = sensitivityBarplotOAT(impactCarOperation, dCase, dVariation, lstKeys)

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Impact from BEV Operation')
plt.show()

#%%============================================================================
# Impact of BEV Vehicle and Battery Production

dVariation = {#----------------------------------
              'cause'   : {'fVehProduction' :  var_fVehProduction,
                           'fBattRecyc'     :  var_fBattRecyc},
              'road'    : {'fCirc'          :  var_fCirc},
              'car'     : {'fUtil'          :  var_fUtil},
              'battery' : {'batMileage'     :  var_batMileage}
              }

dRes, dVar, sVar  = sensitivityBarplotOAT(impactCarProductionBEV, dCase, dVariation, lstKeys)

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 
ax.set_title('Influencial Factors in Impact from BEV and Battery Production')
plt.show()


#%%============================================================================
# Impact on full BEV Life Cycle Assessment

dVariation = {#----------------------------------
              'cause'  : {#'fRoadConstr'    : var_fRoadConstr,
                          'vehProduction'  : var_vehProduction,
                          #'fBattRecyc'     : var_fBattRecyc,
                          'fVehProduction' : var_fVehProduction,
                          'gridIntensity'  : var_gridIntensity},
              'car'    : {'fUtil'          : var_fUtil,
                          'fuelEconOEM'    : var_fuelEcon},
              'battery': {'batMileage'     : var_batMileage},
              'road'   : {'fCirc'          : var_fCirc,
                          'fAlloc'         : var_fAlloc}
              }

dRes, dVar, sVar = sensitivityBarplotOAT(lifeCycleImpactBEV, dCase, dVariation, lstKeys)

from source.modMonteCarlo import scalarFunMonteCarlo
from source.modStatistics import getSample

size = 100



dSamples = {'cause'  : {#'fRoadConstr'    : var_fRoadConstr,
                        'vehProduction'  : getSample('triang', var_vehProduction, size),
                        #'fBattRecyc'     : getSample('triang', [0.5,0.7,1.0], size),
                        'fVehProduction' : getSample('triang', var_fVehProduction, size),
                        'gridIntensity'  : getSample('triang', [20., 20., 72.], size)},
            'car'    : {#'fUtil'          : getSample('triang', [0.45,0.45,0.45], size),
                        'fuelEconOEM'    : getSample('triang', [ops_fuelEconomyBEV*0.8, ops_fuelEconomyBEV, ops_fuelEconomyBEV*1.2], size)},
            'battery': {'batMileage'     : getSample('triang', [100e3, 300e3, 400e3], size)},
            #'road'   : {'fCirc'          : getSample('triang', [1.00,1.20,1.46], size),
            #            'fAlloc'         : getSample('triang', [0.4,0.45,0.5], size)}
            }

scenarios = {'2030base':  {'batteryProduction': 112., 'elecProduction': 72.},
             '2030green': {'batteryProduction':  62., 'elecProduction': 20.}}

import numpy as np
from scipy.stats import scoreatpercentile

for key, scen in scenarios.items():
    print()
    print(key, scen['elecProduction'], scen['batteryProduction'] )
    dCase['cause']['gridIntensity'] = scen['elecProduction']
    dCase['cause']['battProduction'] = scen['batteryProduction']

    mcResults, mcFactDesign = scalarFunMonteCarlo(lifeCycleImpactBEV, dCase, dSamples, size)

    print(np.median(mcResults))
    print(scoreatpercentile(mcResults, 50.0))
    print('Lower:', scoreatpercentile(mcResults, 2.5), 'at', scoreatpercentile(mcResults, 50.0) - scoreatpercentile(mcResults, 2.5))
    print('Upper:', scoreatpercentile(mcResults, 97.5), 'at',  scoreatpercentile(mcResults, 97.5) - scoreatpercentile(mcResults, 50.0))
import sys
sys.exit()



kwargs.update(**{'color': tumci['TUMdarker50']})

fig, ax = quickBarplotAxis(dRes, sVar, dVar, figSize, True, **kwargs)
ax.set_title('Influencial Factors in BEV Life Cycle Assessment')
ax.axvline(dRes['base'][0],linewidth=1.5, color=tumci['TUMpptOrange'], zorder=5) 

strTime = getTime()
# SAVETO = 'output/dissDevsLCA_BEV_' + strTime + '.pdf'
# fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)

plt.show()

# 2. Think: How can this be done more sophisticated
# 3. Get to a baseline formuluation and apply sensitivity analyses
# 4. Research more data if necessary
