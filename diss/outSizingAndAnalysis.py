#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 15:54:50 2020

@author: nicolas
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
#from matplotlib.patches import Patch

from source.modStatistics import getSample
from source.modMonteCarlo import scalarFunMonteCarlo

from source.util import loadTUMcolors, getTime

from source.modSizingTier0 import getAircraftComponents, getAircraftMass
from source.modAssessmentTier0 import getSpecEnergies, getTotalSpecEnergy

#from source.modInterface import sumEnergyFH, getEnergyFH
    
from scipy.stats import scoreatpercentile  
     
from data.inputVTOL import gdv, gmv, gtv

def normDict(dic):
    normDic = {}
    for k,v in dic.items():
        normDic[k] = float(v) / sum(dic.values()) * 100
    return normDic

def setAlphaComponents(key):
    alpha = {}
    if key == 'struct': 
        alpha[key] = 0.7
    elif key == 'ptrain':
        alpha[key] = 0.1
    elif key == 'useful':
        alpha[key] = 0.9
    elif key == 'other':
        alpha[key] = 0.5        
    elif key == 'fuel':
        alpha[key] = 0.3    
    else:
        print('Dunno this key')
    return alpha  

def setAlphaEnerg(key):
    alpha = {}
    if key == 'hover': 
        alpha[key] = 0.9
    elif key == 'cruise':
        alpha[key] = 0.5  
    else:
        print('Dunno this key')
    return alpha             


#%%
def dissSizingAndAnalyisPlot(sortedCases, boolPrint):

    tumClr = loadTUMcolors()
    # loadPlotFonts()

    labels = []

    ranges = []    

    dictComp = {}
    normComp = {}
    sumComp = {}
    
    dictEnerg = {}
    normEnerg = {}
    sumEnerg = {}

    massLower = []
    massUpper = []
    massMedian = []   

    energLower = []
    energUpper = []
    energMedian = []   
    
    iS = 1000

    for i, case in enumerate(sortedCases):
        print('>> Start with case', case['design']['ident'])
        
        ID = case['design']['ident']
        
        ranges.append(case['mission']['cruiseRange']/1000.)
        labels.append(case['name'])
           
        ### DETERMINISTIC SIZING + ANALYSIS
        # COMPONENTS - DESIGN GROSS MASS
        dComponents = getAircraftComponents(case)        
        dictComp[ID] = dComponents
        normComp[ID] = normDict(dComponents)
        sumComp[ID] = (sum(dComponents.values()))
        
        # SPECIFIC ENERGY CONSUMPTIONS PER PKT
        dEnergies = getSpecEnergies(case)
        dictEnerg[ID] = dEnergies
        normEnerg[ID] = normDict(dEnergies)
        sumEnerg[ID] = sum(dEnergies.values())

        fuelID = case['fuel']['highEnerSys']['ident']
        
        if ID == 'lilium':
            print(case['flightstates']['hover']['pReq'] / 1000.)

        ### PROBABILISTIC SIZING + ANALYSIS
        # COMPONENTS - DESIGN GROSS MASS
        
        if case['fuel']['fueltype'] == 'hybridBattery':
            sampleHES = {'esp':      getSample('triang',[gtv(fuelID,'min','esp'),gtv(fuelID,'baseline','esp'),gtv(fuelID,'max','esp')], iS),
                         'fDoD':     getSample('triang',[gtv(fuelID,'min','fDoD'),gtv(fuelID,'baseline','fDoD'),gtv(fuelID,'max','fDoD')], iS),
                         'eta_mtr':  getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
                         }            
        elif case['fuel']['fueltype'] == 'hybrid':
            sampleHES = {'pSpec':       getSample('triang',[gtv(fuelID,'min','pSpec'),gtv(fuelID,'baseline','pSpec'),gtv(fuelID,'max','pSpec')], iS),
                         'fractionH2T': getSample('triang',[gtv(fuelID,'min','fractionH2T'),gtv(fuelID,'baseline','fractionH2T'),gtv(fuelID,'max','fractionH2T')], iS),
                         'eta_mtr':     getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
                         'eta_fc':      getSample('triang',[gtv(fuelID,'min','eta_fc'),gtv(fuelID,'baseline','eta_fc'),gtv(fuelID,'max','eta_fc')], iS),
                        }
            
        sampleMASS = {'design':     {'ltod':    getSample('triang', [gdv(ID,'min','ltod'), gdv(ID,'baseline','ltod'), gdv(ID,'max','ltod')], iS),
                                     'pl'  :    getSample('triang', [gdv(ID,'min','pl'), gdv(ID,'baseline','pl'), gdv(ID,'max','pl')], iS),
                                     'fStruct': getSample('triang', [gdv(ID,'min','fStruct'), gdv(ID,'baseline','fStruct'), gdv(ID,'max','fStruct')], iS),
                                     },
                      'mission':    {'hoverTime': getSample('triang', [gmv(ID,'min','hoverTime'), gmv(ID,'baseline','hoverTime'), gmv(ID,'max','hoverTime')], iS)},
                      'fuel':       {'highEnerSys': sampleHES}
                  }

        # massRes, mcDesigns = scalarFunMonteCarlo(getAircraftMass, case, sampleMASS, iS)
        # massMedian.append(scoreatpercentile(massRes, 50.0))
        # massLower.append(sumComp[ID] - scoreatpercentile(massRes, 2.5))
        # massUpper.append(scoreatpercentile(massRes, 97.5) - sumComp[ID])

        massRes, mcDesigns = scalarFunMonteCarlo(getAircraftMass, case, sampleMASS, iS)
        massMedian.append(scoreatpercentile(massRes, 50.0))
        massLower.append(massMedian[-1] - scoreatpercentile(massRes, 2.5))
        massUpper.append(scoreatpercentile(massRes, 97.5) - massMedian[-1])

        # SPECIFIC ENERGY CONSUMPTIONS PER PKT
        # sampleENERG = {#
        #                #'design': {'ltod':    getSample('triang', [gdv(ID,'min','ltod'), gdv(ID,'baseline','ltod'), gdv(ID,'max','ltod')], iS),
        #                #           'pl'  :    getSample('triang', [gdv(ID,'min','pl'), gdv(ID,'baseline','pl'), gdv(ID,'max','pl')], iS),
        #                #           'fStruct': getSample('triang', [gdv(ID,'min','fStruct'), gdv(ID,'baseline','fStruct'), gdv(ID,'max','fStruct')], iS),
        #                #         },
        #                'fuel': {'highEnerSys': sampleHES}
        #                }
    
        energRes, mcDesigns = scalarFunMonteCarlo(getTotalSpecEnergy, case, sampleMASS, iS)
        energMedian.append(scoreatpercentile(energRes, 50.0))
        energLower.append(energMedian[-1] - scoreatpercentile(energRes, 2.5))
        energUpper.append(scoreatpercentile(energRes, 97.5) - energMedian[-1])    
    
        # import sys
        # sys.exit()

    #%%===========================================================================
    
    mpl.rcParams['axes.linewidth'] = 0.5 #set the value globally
    mpl.rcParams['ytick.major.width'] = 0.5 #set the value globally
    mpl.rcParams['xtick.major.width'] = 0.5 #set the value globally
    
    barwidth = 0.4
    y1max = 3500.
    y2max = 1.2

    #%%===========================================================================
    from matplotlib.patches import Patch

    textwidth = 5.809    
    textheight = 8.275

    # fig, ax = plt.subplots(figsize=(textwidth,0.75*textheight),
    #                        nrows=2, ncols=1, sharex=True, sharey=False,
    #                        gridspec_kw={'hspace': 0.075, 
    #                                     'height_ratios': [1, 1]})

    fig1, ax1 = plt.subplots(figsize=(textwidth,0.4*textheight))
                           
    #ax1 = ax[0]
    par1 = ax1.twinx()
    par2 = ax1.twinx()
    #ax1.set_title('Design Gross Mass and Energy Consumption relative to Maximum Range')
    
    upper = [ax1, par1, par2]
 
    fig2, ax2 = plt.subplots(figsize=(textwidth,0.3*textheight))


    #ax2 = ax[1]
    ax2.set_ylabel('Breakdown of Component Mass\nand Consumed Specific Energy  (\%)')
   

    axes = [ax1, ax2]
    for ax in axes:
        ax.xaxis.set_ticks([])

    ax1.get_xaxis().set_ticks([])
    
    blues = plt.get_cmap('Blues')
    oranges = plt.get_cmap('Oranges')

    xPos = []
    xComp, xEnerg = [], []
    yComp, yEnerg = [], []       
    legendComp, legendEnerg = [], []


    for i, case in enumerate(sortedCases):

        ID = case['design']['ident']
        xPos.append(i)

        #par2.bar(xPos[-1], ranges[i], width=2.5*barwidth, edgecolor=oranges(1.0), linewidth=1.0, color='w', alpha=1.0, zorder=.)

        dNormComp = normComp[ID]
        yComp.append(sumComp[ID])
        xComp.append(i-barwidth/2)
        
        bComp = 0.
        for key, norm in dNormComp.items():
            alpha = setAlphaComponents(key)         
            if i == 0: legendComp.append(Patch(facecolor='k', alpha=alpha[key], label=key))
            ax2.bar(xComp[-1], norm, bottom=bComp, 
                    color='k', alpha=alpha[key],
                    edgecolor='k', width=barwidth, linewidth=0.0)
            bComp = bComp + norm

        dNormEnerg = normEnerg[ID]
        yEnerg.append(sumEnerg[ID])
        xEnerg.append(i+barwidth/2)


        bEnerg = 0.
        for key, norm in dNormEnerg.items():
            alpha = setAlphaEnerg(key)  
            if i == 0: legendComp.append(Patch(facecolor=blues(0.7), alpha=alpha[key], hatch='\\\\\\', edgecolor='w', label=key))
            ax2.bar(xEnerg[-1], norm, bottom=bEnerg, hatch='\\\\', edgecolor='w',
                    #color=tumClr['TUMdarker100'], alpha=alpha[key],
                    color=blues(0.7), alpha=alpha[key],
                    width=barwidth, linewidth=0.0)
            bEnerg = bEnerg + norm    
       
        mtom = case['mtom_official']
        if not mtom == None: ax1.plot(xComp[i], mtom, color='k', 
                                      marker='o', markerfacecolor='w',
                                      markersize = 4., zorder=5.)

    # ax1.plot(xComp, yComp,
    #          color='k', linewidth=0.0, linestyle='--',
    #          marker='o', markersize = 3., markerfacecolor='k') 

    # par1.plot(xEnerg, yEnerg,
    #           color=tumClr['TUMdarker100'], linewidth=0.0, linestyle='--',
    #           marker='s', markersize = 3., markerfacecolor=tumClr['TUMdarker100'])


    #ax1.bar(xComp, massMedian, width=barwidth, color='k', alpha=0.6,  zorder=1)
    ax1.bar(xComp, sumComp.values(), width=barwidth, color='k', alpha=0.6,  zorder=1)

    err1 = ax1.errorbar(xComp, massMedian, yerr=(massLower,massUpper),
                color='k', linewidth=0.0, linestyle='-',
                marker='o', markersize = 4., alpha=0.9,
                elinewidth =0.8, capsize=5., 
                zorder=1., label='Stochastic DGM')

    #par1.bar(xEnerg, energMedian, width=barwidth,color=blues(0.7), alpha=0.7) 
    par1.bar(xEnerg, sumEnerg.values(), width=barwidth,color=blues(0.7), alpha=0.7) 
             #color=tumClr['TUMdarker100'], alpha=0.6)
             
    
    err2 = par1.errorbar(xEnerg, energMedian, yerr=(energLower,energUpper),
                #color=tumClr['TUMdarker100'], linewidth=0.0, linestyle='-',
                color=blues(0.9), linewidth=0.0, linestyle='-',
                marker='s', markersize = 4., alpha=0.9,
                elinewidth =0.8, capsize=5., 
                zorder=5., label='Stochastic CSE')
    
    par2.bar(xPos, ranges, width=2.5*barwidth, fill=False, edgecolor=oranges(0.5), linewidth=1.0,  zorder=0.)


    # par2.plot(xPos, ranges, 
    #           color=tumClr['TUMpptOrange'], linewidth=0.5, 
    #           marker='d', markerfacecolor='w', markersize=3., 
    #           )

    par1.set_ylim(0.,y2max)
    #par1.tick_params(colors=tumClr['TUMdarker100'])
    #par1.yaxis.label.set_color(tumClr['TUMdarker100'])

    ax1.set_ylim(0.,y1max)
    ax1.grid(axis='y', linestyle='-', linewidth=0.1, color='gray')

    ax2.set_ylim(0.,100.)
    ax2.set_yticklabels(['0\%','20\%','40\%','60\%','80\%','100\%'], fontdict={'fontsize':8})   
    ax2.grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
    ax2.set_axisbelow(True)
   
    ax1.set_ylabel('Design Gross Mass (kg)')
    par1.set_ylabel('Consumption Specific Energy (kWh/PKT)')
    par2.set_ylabel('Maximum Design Range (km)')
    
    par2.spines['right'].set_position(('outward', 37.5))    # right, left, top, bottom   
    par2.set_ylim(0.,700)    # right, left, top, bottom   
    #par2.tick_params(colors=oranges(0.5))
    #par2.yaxis.label.set_color(oranges(0.5))
    
    for axis in upper:
        axis.tick_params(axis='x', length=0.0)  # no x-ticks                 

    ax1.set_xticks(range(len(sortedCases)))
    ax1.set_xticklabels(labels)
    for tick in ax1.get_xticklabels():
        tick.set_verticalalignment("center")

    ax1.tick_params(axis='x', pad=12., length=0.0)      

    ax2.set_xticks(range(len(sortedCases)))
    ax2.set_xticklabels(labels)
    for tick in ax2.get_xticklabels():
        tick.set_verticalalignment("center")

    ax2.tick_params(axis='x', pad=12., length=0.0)     
   
    # LEGENDS
    #legend_m = []
    legend = []

    # h,l = ax1.get_legend_handles_labels()
    # print(h,l)
    
    legend.append(Patch(facecolor='k', alpha=0.7, label='Deterministic DGM'))
    legend.append(err1)

    #legend.append(Line2D([], [], color=tumClr['TUMdarker100'], marker='s', markersize = 5.,  
    #                       markerfacecolor='w', label='Energy',linewidth=0.0))
    legend.append(Patch(facecolor=blues(0.7), alpha=0.7, label='Deterministic CSE'))    
    legend.append(err2)

    #Patch(facecolor=blues(), edgecolor=oranges(0.5), label='Range')
    legend.append(Line2D([], [], color='k', alpha=0.9, marker='o', markersize = 5., 
                           markerfacecolor='w', label='Official DGM',linewidth=0.0))    
    legend.append(Patch(facecolor='w', edgecolor=oranges(0.5), label='Maximum Design Range'))
                
    
    ax1.legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
              loc='upper center', bbox_to_anchor= (0.5, 1.15), ncol=3, borderaxespad=0, frameon=False) 
    ax2.legend(handles=legendComp, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
              loc='lower left', bbox_to_anchor= (1.01, 0.00), ncol=1, borderaxespad=0, frameon=False) 
   

    plt.tight_layout()  
    if boolPrint:
        strTime = getTime()
        SAVETO1 = 'output/dissVTOLsizing_' + strTime + '.pdf'
        fig1.savefig(SAVETO1, bbox_inches='tight', dpi=250.)
        SAVETO2 = 'output/dissVTOLcomponents_' + strTime + '.pdf'
        fig2.savefig(SAVETO2, bbox_inches='tight', dpi=250.)

    plt.show()
