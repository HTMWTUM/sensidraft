#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 14:35:56 2020

@author: nicolas
"""

import matplotlib.pyplot as plt
import matplotlib as mpl
import copy as cp

#from matplotlib import gridspec
from matplotlib.patches import Patch
from matplotlib.lines import Line2D

from source.util import loadTUMcolors, getTime

from source.modSizingTier0 import getAircraftMass
from source.modAssessmentTier0 import getTotalProductionImpact, getTotalOperationsImpact
from source.modAssessmentTier0 import getTotalSpecEnergy

from source.modSensitivityAnalysis import sensitivityBarplotOAT

from data.inputImpact import gmid, gcid, gmiv
from data.inputVTOL import gdv, gmv, gtv



def convert(lst): 
    return [ -i for i in lst ] 

def normalize(lst, base):
    return [ i/b for i,b in zip(lst, base) ]

def getTechVariations(case):
    fuelID = case['fuel']['highEnerSys']['ident']
    if case['fuel']['fueltype'] == 'hybridBattery':
        dVarTech = {'highEnerSys': {'esp':      [gtv(fuelID,'min','esp'),gtv(fuelID,'baseline','esp'),gtv(fuelID,'max','esp')],
                                    'fDoD':     [gtv(fuelID,'min','fDoD'),gtv(fuelID,'baseline','fDoD'),gtv(fuelID,'max','fDoD')],
                                    'eta_mtr':  [gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')]}}

    elif case['fuel']['fueltype'] == 'hybrid':
        dVarTech = {'highEnerSys': {'pSpec':        [gtv(fuelID,'min','pSpec'),gtv(fuelID,'baseline','pSpec'),gtv(fuelID,'max','pSpec')],
                                    'fractionH2T':  [gtv(fuelID,'min','fractionH2T'),gtv(fuelID,'baseline','fractionH2T'),gtv(fuelID,'max','fractionH2T')],
                                    'eta_mtr':      [gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')]}}
    else:
        print('Other Fuel type Variation not yet defined. ERROR')
        dVarTech = None
        
    return dVarTech

def dissSensitivityImpactLifeCycle(sortedCases, boolPrint):

    saImpPkt = {}
    saVars = {}

    for case in sortedCases:
        ID = case['fuel']['ident']
        
        
        workCase = cp.deepcopy(case)

        dOpImpact = {'elecProduction':     [0.020, 0.072, 0.237],  # pure wind; ref; world 2030 
                     'batteryProduction':  [62.,112.,212],
                     'hydrProdTotal':      [970, 2412, 11893.0],
                    }
                
        if case['fuel']['fueltype'] == 'hybridBattery':
            dVarTech = {'highEnerSys': {'nCycles': [750., 1000., 1250.]}}
        elif case['fuel']['fueltype'] == 'hybrid':
            dVarTech = {'highEnerSys': {'opHours': [3500., 4500., 5500.]}}
        
        dVariations = {#------------
                       'opImpact':      dOpImpact,
                       'fuel':          dVarTech,
                       # 'operations': dVarOps,
                      }

        #dVariations.update(**{'operations': dVarOps})
        
        dImpPkt, dVar, sVar = sensitivityBarplotOAT(getTotalOperationsImpact, workCase, dVariations, ['min', 'base',  'max'])
        saImpPkt[ID] = dImpPkt    
        saVars[ID] = sVar

    tumClr = loadTUMcolors()
    textwidth = 5.809    
    textheight = 8.275

    figPkt, axes = plt.subplots(figsize=(3/7*textwidth,0.45*textheight),
                            nrows=len(sVar), ncols=len(sortedCases), sharex=True, sharey=False,
                            gridspec_kw={'hspace': 0.15 , 'wspace': 0.15})
    
    # INDICES FOR LAST ROW AND LAST COLUMN
    #iMax = len(sortedCases) - 1
    jMax = len(sVar) - 1
    
    for i, case in enumerate(sortedCases):
        ID = case['fuel']['ident']
        
        for j, var in enumerate(saVars[ID]):
            # RESET FORMATTING
            print('I am here')
            axes[j][i].xaxis.set_ticks([])
            axes[j][i].yaxis.set_ticks([])
            
            #axes[j][i].text(0.5,0.5,str(ID) + '\n' +  str(var), ha='center', va='center')
            
            xVal = [0., 1., 2.]
            
            yEnr = [saImpPkt[ID]['min'][j]/saImpPkt[ID]['base'][j], 1.0, saImpPkt[ID]['max'][j]/saImpPkt[ID]['base'][j]]
            
            axes[j][i].plot(xVal, yEnr, color=tumClr['TUMpptGreen'], linewidth=0.8, linestyle='-')
            
            axes[j][i].set_xlim(0.0,2.0)
            axes[j][i].set_ylim(0.65,1.35)
            
            #axes[j][i].text(2.,1.25,str(saVars[ID][j]),ha='right',va='top',fontsize='small')
        
        
        #xes[jMax][i] = text(0.5,-0.2,str(dCaseNames[ID]),ha='center',va='top',fontsize='small')
        #dCaseNames[ID])
            

        axes[jMax][i].set_xlabel(str(case['name']))

    # # axes[0][0].set_ylabel(str(sVar[0]))
    # # axes[0][0].set_yticks([0.5,1.0,1.5])
    # # axes[1][0].set_ylabel(str(sVar[1]))
    # # axes[1][0].set_yticks([0.5,1.0,1.5])
    # # axes[2][0].set_ylabel(str(sVar[2]))
    # # axes[2][0].set_yticks([0.5,1.0,1.5])

    axes[0][0].set_ylabel('Electricity \nProduction')
    axes[1][0].set_ylabel('Battery \n Production')
    axes[2][0].set_ylabel('Hydrogen \n Production')
    axes[3][0].set_ylabel('Battery \& Fuel \n Cell Life')
    # # axes[5][0].set_ylabel(r'$f_{DoD},\;P_{sp}$')
    # # axes[6][0].set_ylabel(r'$f_{Util}$')
    # # axes[7][0].set_ylabel(r'$n_{Seg}$')
    
    # legend = [Line2D([], [], label='Impact Per Vehicle', color=tumClr['TUMpptGray'], linewidth=1.0),
    #           Line2D([], [], label='Life Cycle Impact', color=tumClr['TUMpptGreen'], linewidth=1.0, linestyle='--')]
    
    # axes[jMax][3].legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
    #           loc='upper center', bbox_to_anchor= (0.5, -0.8), ncol=2, borderaxespad=0, frameon=True) 

    plt.tight_layout()  
    if boolPrint:
        strTime = getTime()
        SAVETO = 'output/dissSAfueltypes_' + strTime + '.pdf'
        figPkt.savefig(SAVETO, bbox_inches='tight', dpi=250.)


    import sys
    sys.exit()



def dissSensitivityImpactVehicle(sortedCases, boolPrint):
    print('\n## This is function << dissSensitivityImpactVehicle >>')
  
    saImpVeh = {}
    saVars = {}
    
    for case in sortedCases:
        print('>> Start with case', case['design']['ident'])
       
        ID = case['design']['ident']
        
        workCase = cp.deepcopy(case)
        
        dVarMatImpact = {'aluminium': [gmiv('cleanFuture','aluminium'), gmiv('baseline','aluminium'),   gmiv('worstCase','aluminium')],
                         #'steel':     [gmiv('cleanFuture','steel'),     gmiv('baseline','steel'),       gmiv('worstCase','steel')],  
                         'titan':     [gmiv('cleanFuture','titan'),     gmiv('baseline','titan'),       gmiv('worstCase','titan')],  
                         #'copp':      [gmiv('cleanFuture','copp'),      gmiv('baseline','copp'),        gmiv('worstCase','copp')],  
                         #'gfrp':      [gmiv('cleanFuture','gfrp'),      gmiv('baseline','gfrp'),        gmiv('worstCase','gfrp')],  
                         'cfrp':      [gmiv('cleanFuture','cfrp'),      gmiv('baseline','cfrp'),        gmiv('worstCase','cfrp')],  
                         #'plastic':   [gmiv('cleanFuture','plastic'),   gmiv('baseline','plastic'),     gmiv('worstCase','plastic')],  
                         'battery':   [gmiv('cleanFuture','battery'),   gmiv('baseline','battery'),     gmiv('worstCase','battery')],  
                         }
        
        variationsVeh = {'matImpact': dVarMatImpact}

        
        dImpVeh, dVar, sVar = sensitivityBarplotOAT(getTotalProductionImpact, workCase, variationsVeh, ['min', 'base',  'max'])
        saImpVeh[ID] = dImpVeh
        saVars[ID] = sVar
        

#%%
    tumClr = loadTUMcolors()
    textwidth = 5.809    
    textheight = 8.275

    figVeh, axes = plt.subplots(figsize=(textwidth,0.1*textheight),
                               nrows=1, ncols=len(sortedCases), sharex=True, sharey=False,
                               gridspec_kw={'hspace': 0.15 , 'wspace': 0.15})

    greys = plt.get_cmap('Greys')
    blues = plt.get_cmap('Blues')
    greens = plt.get_cmap('Greens')
    oranges = plt.get_cmap('Oranges')
    

    styles = {'aluminium': ':',
              'cfrp': '--',
              'battery': '-.',
              #'steel': '-',
              # 'copp': '-',
              'titan': '-',
              # 'gfrp': '-',
              # 'plastic': '-',
              }


    markers = {'aluminium': 's',
              'cfrp': '^',
              'battery': 'o',
              #'steel': '-',
              # 'copp': '-',
              'titan': 'd',
              # 'gfrp': '-',
              # 'plastic': '-',
              }
    
    colors = {'aluminium': blues(0.7),
              'cfrp': greens(0.7),
              'battery': oranges(0.7),
              #'steel': 'r',
              # 'copp': 'g',
              'titan': greys(0.8),
              # 'gfrp': 'y',
              # 'plastic': 'gray',
              }
    labels = {'aluminium': 'Aluminium',
              'cfrp': 'Carbon Fiber',
              'battery': 'Battery',
              'titan': 'Titan'}
    legend = []

    for i, case in enumerate(sortedCases):
        ID = case['design']['ident']
        for j, var in enumerate(saVars[ID]):
            # RESET FORMATTING
            axes[i].xaxis.set_ticks([])
            axes[i].yaxis.set_ticks([])

            xVal = [0., 1., 2.]
            yVal = [saImpVeh[ID]['min'][j]/saImpVeh[ID]['base'][j], 1.0, saImpVeh[ID]['max'][j]/saImpVeh[ID]['base'][j]]
            
            if not var == 'battery':
                axes[i].plot(xVal, yVal, linewidth=0.7, marker=markers[var], color=colors[var], markersize=3, mew=0.7,
                             markerfacecolor='w', linestyle=styles[var], zorder=5, clip_on=False)
            else:
                axes[i].plot(xVal, yVal, linewidth=0.7, marker=markers[var], color=colors[var], markersize=3, mew=0.7,
                             markerfacecolor='w', linestyle=styles[var], zorder=10, clip_on=True)
                
            
            axes[i].set_xlim(0.0,2.0)
            axes[i].set_ylim(0.9,1.1)

        axes[i].set_yticks([0.70,0.85,1.0,1.15,1.3])
        if i != 0:
            axes[i].yaxis.set_tick_params(which='both',labelleft=False, length=0.0)
        if i == 0:
            axes[i].text(-0.5,1.43,'$\%$',va='center',fontsize=6)
            axes[i].yaxis.set_tick_params(which='both', length=0.0)
            axes[i].set_yticklabels(['-30','-15','0','15','30'], fontsize=6)
    
        axes[i].set_xticks([0.0,1.0,2.0])
        axes[i].set_xticklabels(['min.', 'mode', 'max.'], rotation = 45, va='top')
        # set the alignment for outer ticklabels
        xticks = axes[i].get_xticklabels()
        xticks[0].set_ha("left")
        xticks[-1].set_ha("right")
        
        axes[i].text(1.0,0.3,str(case['name']), ha='center',va='center', fontsize=7)

        # axes[i].set_xlabel(str(case['name']))    
        axes[i].xaxis.set_tick_params(which='both', labelbottom=True, length=0.0, pad=2)
  
        # labels = [item.get_text() for item in xticks]
        # labels[0] = xticklabels[ID][k][0]
        # labels[-1] = xticklabels[ID][k][-1]

        # trax[j][i].set_xticklabels(labels, fontsize=6)
        
        axes[i].grid(which='both', axis='both', linestyle='-', linewidth=0.1, color='gray')  
        
  
    for j, var in enumerate(saVars[ID]):
        legend.append(Line2D([], [], label=labels[var], linewidth=0.7, marker=markers[var], color=colors[var], markersize=4, mew=0.7,
                             markerfacecolor='w', linestyle=styles[var]))

    axes[3].legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=2.2, fontsize=7,
              loc='lower center', bbox_to_anchor= (0.5, 1.05), ncol=4, borderaxespad=0, frameon=False) 

    plt.tight_layout()  
    if boolPrint:
        strTime = getTime()
        SAVETO = 'output/dissSAVehicleProd_' + strTime + '.pdf'
        figVeh.savefig(SAVETO, bbox_inches='tight', dpi=250.)

    plt.show()



#%%

def dissSensitivitiesMasses(sortedCases, boolPrint):
    print('\n## This is function << dissSensitivitiesMasses >>')
    
    saMasses = {}
    saEnergies = {}
    saVars = {}
    dVars = {}
    
    for case in sortedCases:
        print('>> Start with case', case['design']['ident'])
        
        ID = case['design']['ident']
        
        workCase = cp.deepcopy(case)
        
        dVarDesign = {'ltod':    [ gdv(ID, 'min', 'ltod'),    gdv(ID, 'baseline', 'ltod'),    gdv(ID, 'max', 'ltod')],
                      'pl':      [ gdv(ID, 'min', 'pl'),      gdv(ID, 'baseline', 'pl'),      gdv(ID, 'max', 'pl')],
                      'fStruct': [ gdv(ID, 'min', 'fStruct'), gdv(ID, 'baseline', 'fStruct'), gdv(ID, 'max', 'fStruct')]}
        
        dVarReqs = {#'cruiseRange': [gmv(ID, 'min', 'cruiseRange'),gmv(ID, 'baseline', 'cruiseRange'),gmv(ID, 'max', 'cruiseRange')],
                    'hoverTime':   [gmv(ID, 'min', 'hoverTime'),gmv(ID, 'baseline', 'hoverTime'),gmv(ID, 'max', 'hoverTime')]}
        
        fuelID = case['fuel']['highEnerSys']['ident']
        if case['fuel']['fueltype'] == 'hybridBattery':
            dVarTech = {'highEnerSys': {'esp':      [gtv(fuelID,'min','esp'),gtv(fuelID,'baseline','esp'),gtv(fuelID,'max','esp')],}}
                                        #'fDoD':     [gtv(fuelID,'min','fDoD'),gtv(fuelID,'baseline','fDoD'),gtv(fuelID,'max','fDoD')],
                                        #'eta_mtr':  [gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')]}}

        elif case['fuel']['fueltype'] == 'hybrid':
            dVarTech = {'highEnerSys': {'pSpec':        [gtv(fuelID,'min','pSpec'),gtv(fuelID,'baseline','pSpec'),gtv(fuelID,'max','pSpec')],}}
                                        #'fractionH2T':  [gtv(fuelID,'min','fractionH2T'),gtv(fuelID,'baseline','fractionH2T'),gtv(fuelID,'max','fractionH2T')],
                                        #'eta_mtr':      [gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')]}}
        
        
        dVarOps = {#'fUtil': [0.7,0.75,0.8],
                   'cruiseRange': [workCase['mission']['cruiseRange']*0.5,workCase['mission']['cruiseRange']*0.75,workCase['mission']['cruiseRange']],   
                   'hoverTime': [workCase['mission']['hoverTime'],workCase['mission']['hoverTime']*1.5,workCase['mission']['hoverTime']*2]}   
        
        dVariations = {#------------
                       'mission':  dVarReqs,
                       'design':   dVarDesign,
                       'fuel':     dVarTech,
                       'operations': dVarOps,
                      }
        
        dMasses, dVar, sVar = sensitivityBarplotOAT(getAircraftMass, workCase, dVariations, ['min', 'base',  'max'])
        saMasses[ID] = dMasses
        saVars[ID] = sVar
        dVars[ID] = dVariations      
        
        dEnergies, dVar, sVar = sensitivityBarplotOAT(getTotalSpecEnergy, workCase, dVariations, ['min', 'base',  'max'])
        saEnergies[ID] = dEnergies


    
    

    sVolo =   [[60, 90, 120],[2.5, 3.0, 3.5],   [68, 100, 115],[0.24, 0.26, 0.28],[250, 271, 292],[18, 27, 36],[90, 135, 180]]
    sCity =   [[60, 90, 120],[2.5, 3.0, 3.5],   [52, 56, 60],[0.24, 0.26, 0.28],[250, 271, 292],[15, 27.5, 50],[90, 135, 180]]
    sVahana = [[60, 90, 120],[5.5, 6.5, 7.5],   [49, 56, 63],[0.25, 0.27, 0.29],[250, 271, 292],[50, 75, 100],[90, 135, 180]]
    sCora =   [[60, 90, 120],[4.5, 5.5, 6.5],   [32, 40, 49],[0.25, 0.27, 0.29],[250, 271, 292],[50, 75, 100],[90, 135, 180]]
    sJoby =   [[60, 90, 120],[12.5, 13, 14.5],[32, 36, 41],[0.24, 0.27, 0.30],[250, 271, 292],[120, 180, 240],[90, 135, 180]]
    sLilium = [[60, 90, 120],[16, 17, 18],[17, 19, 20],[0.24, 0.27, 0.30],[250, 271, 292],[150, 225, 300],[90, 135, 180]]
    sSkai =   [[60, 90, 120],[2.5, 3.0, 3.5],   [33, 43, 54],[0.24, 0.26, 0.28],[0.75, 1.0, 2.0],[322, 483, 644],[90, 135, 180]]
        
    # xTik = [[60.0, 90.0, 120.0],
    #         ]
    
    xTik = {'volo':sVolo,
            'city':sCity,
            'vahana':sVahana,
            'cora':sCora,
            'joby':sJoby,
            'lilium':sLilium,
            'skai':sSkai}

    import math
    myVariations = cp.deepcopy(dVars)
    myTicks = {}
    
    for i, case in enumerate(sortedCases):
        ID = case['design']['ident']
        myTicks[ID] = {}
        
        for key0, val0 in myVariations[ID].items():
            
            myTicks[ID][key0] = {}
            for key1, val1 in val0.items():
            
                if key1 == 'hoverTime':
                    myTicks[ID][key0][key1] = list(map((lambda x: str(math.trunc(x))), val1))
                elif key1 == 'ltod':
                    myTicks[ID][key0][key1] = list(map((lambda x: str(x)), val1))
                elif key1 == 'pl':
                    # val1 = list(map((lambda x: x * 1000), val1))
                    myTicks[ID][key0][key1] = list(map((lambda x: str(math.trunc(x * 1000))), val1))
                elif key1 == 'fStruct':
                    myTicks[ID][key0][key1] = list(map((lambda x: str(x)), val1))                    
                elif key1 == 'highEnerSys':
                    for key2, val2 in val1.items():
                        if key2 == 'esp':
                            myTicks[ID][key0][key2] = list(map((lambda x: str(math.trunc(x * 1.2))), val2))
                        elif key2 == 'pSpec':
                            myTicks[ID][key0][key2] = list(map((lambda x: str(x / 1000.)), val2))
                elif key1 == 'cruiseRange':
                    myTicks[ID][key0][key1] = list(map((lambda x: str(math.trunc(x / 1000))), val1)) 
                #print(ID, key0, key1, val1)
    
    # for i, case in enumerate(sortedCases):
    #     ID = case['design']['ident']
    #     for key0, val0 in myTicks[ID].items():
    #         for key1, val1 in val0.items():
    #             print(ID, key0, key1, myTicks[ID][key0][key1])
        

    tumClr = loadTUMcolors()
    textwidth = 5.809    
    textheight = 8.275
    jMax = len(sVar)

    #print(len(saVars['volo']),len(range(jMax)))
    
    saType = ['mass',
              'both',
              'both',
              'mass',
              'mass',
              'enrg',
              'enrg']
    
    yLabels = [r'$t_{hov}$ [s]',
               r'$L/D$ [-]',
               r'$PL$ [N/kW]',
               r'$f_{str}$ [-]',
               r'$e_{sp}$ [Wh/kg]',
               r'$r_{crs}$ [km]',
               r'$t_{hov}$ [s]']
    
    fig0, trax = plt.subplots(figsize=(textwidth,0.37*textheight),
                           nrows=3, ncols=len(sortedCases), sharex=False, sharey=False,
                           gridspec_kw={'hspace': 0.57 , 'wspace': 0.17})
    
    
    fig, axes = plt.subplots(figsize=(textwidth,0.88*textheight),
                           nrows=len(sVar), ncols=len(sortedCases), sharex=False, sharey=False,
                           gridspec_kw={'hspace': 0.53 , 'wspace': 0.17})

    fig2, viax = plt.subplots(figsize=(0.9*textwidth,0.25*textheight),
                           nrows=1, ncols=2, sharex=False, sharey=False,
                           gridspec_kw={'wspace': 0.275})

    # INDICES FOR LAST ROW AND LAST COLUMN
    #iMax = len(sortedCases) - 1
    blues = plt.get_cmap('Blues')
    oranges = plt.get_cmap('Oranges')
    # mpl.rcParams['hatch.linewidth'] = '0.8'
    #print()
    xlims = {'volo':    {'ltod' : [1.5,5.5],
                         'pl': [0.090, 0.130],
                         'fStruct': [0.22,0.32]},
             'city':    {'ltod' : [1.5,5.5],
                         'pl': [0.035, 0.075],
                         'fStruct': [0.22,0.32]},
             'vahana':  {'ltod' : [5.5,9.5],
                         'pl': [0.035, 0.075],
                         'fStruct': [0.22,0.32]},
             'cora':    {'ltod' : [7.5,11.5],
                         'pl': [0.025, 0.065],
                         'fStruct': [0.22,0.32]},
             'lilium':  {'ltod' : [15.5,19.5],
                         'pl': [0.00, 0.040],
                         'fStruct': [0.22,0.32]},
             'joby':    {'ltod' : [13.0,17.0],
                         'pl': [0.025, 0.065],
                         'fStruct': [0.22,0.32]},
             'skai':    {'ltod' : [1.5,5.5],
                         'pl': [0.025, 0.065],
                         'fStruct': [0.22,0.32]}
             }
    
    xticklabels =   {'volo':    {'ltod' : [1.5,5.5],
                                 'pl': [90, 130],
                                 'fStruct': [0.22,0.32]},
                     'city':    {'ltod' : [1.5,5.5],
                                 'pl': [35, 75],
                                 'fStruct': [0.22,0.32]},
                     'vahana':  {'ltod' : [5.5,9.5],
                                 'pl': [35, 75],
                                 'fStruct': [0.22,0.32]},
                     'cora':    {'ltod' : [7.5,11.5],
                                 'pl': [25, 65],
                                 'fStruct': [0.22,0.32]},
                     'lilium':  {'ltod' : [15.5,19.5],
                                 'pl': [0, 40],
                                 'fStruct': [0.22,0.32]},
                     'joby':    {'ltod' : [13.0,17.0],
                                 'pl': [25, 65],
                                 'fStruct': [0.22,0.32]},
                     'skai':    {'ltod' : [1.5,5.5],
                                 'pl': [25, 65],
                                 'fStruct': [0.22,0.32]}
                     }

    yLbl = [r'$L/D$ [-]',
            r'$PL$ [N/kW]',
            r'$f_{str}$ [-]']
    
    varKeys = ['mission',
               'design',
               'design',
               'design',
               'fuel',
               'operations',
               'operations']
    
    

    for i, case in enumerate(sortedCases):
        
        
        ID = case['design']['ident']
        #print(ID)

    

        #print(dVars[ID])
        for j, dVar, sVar, sTyp, yLab in zip(range(jMax), varKeys, saVars[ID], saType, yLabels):
 
            #%%           
            # print(dVar, sVar)
            # print(j, test[j])
            axes[j][i].yaxis.set_ticks([])
            axes[j][i].xaxis.set_ticks([])
            
            axes[j][i].set_yticks([0.70,0.85,1.0,1.15,1.3])
            if i != 0:
                axes[j][i].yaxis.set_tick_params(which='both',labelleft=False, length=0.0)
            if i == 0:
                axes[j][i].yaxis.set_tick_params(which='both', length=0.0)
                axes[j][i].set_yticklabels(['-30','-15','0','15','30'], fontsize=6)
            
            axes[j][i].grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
            
            axes[j][i].xaxis.set_tick_params(which='both', labelbottom=True, length=0.0, pad=2)
            axes[j][i].set_xticks([0.25,1.0,1.75])
            #axes[j][i].set_xticklabels(xTik[ID][j])
            axes[j][i].set_xticklabels(myTicks[ID][dVar][sVar], fontsize=6)

            # xticks = trax[j][i].get_xticklabels()
            # labels = [item.get_text() for item in xticks]

            # labels[j] = xticklabels[ID][k][0]
            # labels[-1] = xticklabels[ID][k][-1]

            # trax[j][i].set_xticklabels(labels)
            
            #myTicks[ID][key0][key1]
            
            xVal = [0.3, 1., 1.7]
            xMas = [0.15,0.85,1.55]
            xEnr = [0.45,1.15,1.85]
                      
            # xTick = ['1','2','3']
            yMas = [saMasses[ID]['min'][j]/saMasses[ID]['base'][j], 1.0, saMasses[ID]['max'][j]/saMasses[ID]['base'][j]]
            yEnr = [saEnergies[ID]['min'][j]/saEnergies[ID]['base'][j], 1.0, saEnergies[ID]['max'][j]/saEnergies[ID]['base'][j]]     
            
            if sTyp == 'mass':
                axes[j][i].bar(xVal, yMas, color='k', alpha=0.6, width=0.6, hatch='//', linewidth=0.0, edgecolor='w', linestyle='--')
            elif sTyp == 'enrg':
                if j == 5:
                    yEnr = [saEnergies[ID]['min'][j]/saEnergies[ID]['max'][j], saEnergies[ID]['base'][j]/saEnergies[ID]['max'][j], 1.0]
                elif j == 6:
                    yEnr = [1.0, saEnergies[ID]['base'][j]/saEnergies[ID]['min'][j], saEnergies[ID]['max'][j]/saEnergies[ID]['min'][j]]
                axes[j][i].bar(xVal, yEnr, color=blues(0.7), alpha=0.7, width=0.6, hatch='\\\\', linewidth=0.0, edgecolor='w', linestyle='--')
            elif sTyp == 'both':
                axes[j][i].bar(xMas, yMas, color='k', alpha=0.6, width=0.3, hatch='//', linewidth=0.0, edgecolor='w', linestyle='--')
                axes[j][i].bar(xEnr, yEnr, color=blues(0.7), alpha=0.7, width=0.3, hatch='\\\\', linewidth=0.0, edgecolor='w',linestyle='--')
            
            # axes[j][i].set_xticks(xVal,['1','2','3'])
            axes[j][i].set_xlim(0.0,2.0)
            
            #axes[j][0].set_ylabel(yLab)
    
            #axes[j][i].set_xlabel(yLab, fontsize=6)
            if ID == 'volo':
                axes[j][i].text(-0.5,1.45,'$\%$',va='center',fontsize=6)
            
            if ID == 'skai' and j == 4:
                axes[j][i].text(1.0,0.315,r'$p_{sp}$ [kW/kg]',ha='center',fontsize=6)
            else:
                axes[j][i].text(1.0,0.315,yLab,ha='center',fontsize=6)
            axes[j][i].set_ylim(0.6,1.4)
            #axes[j][i].grid(which='both', axis='y', color='gray', linestyle='-', linewidth=0.3)

            if ID == 'lilium':
                if sVar == 'pl':
                    yVisMas = yMas
                    yVisEnr = yEnr
            #%%

        import numpy as np
        from source.modStatistics import getSample, getPDF, getCDF
        
        size = 10000
        
        keys = ['ltod', 'pl', 'fStruct']
        
        for j, (k, lbl) in enumerate(zip(keys, yLbl)):
            
            par1 = trax[j][i].twinx()

            trax[j][i].yaxis.set_ticks([])
            trax[j][i].xaxis.set_ticks([])
            par1.yaxis.set_ticks([])
            par1.xaxis.set_ticks([])            
            
            values = dVars[ID]['design'][k]
            
            samp = getSample('triang', values, size)
            pdf = getPDF('triang', values, size)
            cdf = getCDF('triang', values, size)

            # print(ID, k, samp)

            #trax[j][i].plot(np.sort(samp), pdf)
            trax[j][i].plot(np.sort(samp), cdf, color=oranges(0.75), linewidth=0.8, linestyle='--')
            par1.plot(np.sort(samp), pdf, color='k', linewidth=0.8)
            
            #if k == 'fStruct':
            xmid = (max(xlims[ID][k]) + min(xlims[ID][k]))/2
            #trax[j][i].set_xlim([min(xlims[ID][k])-0.05*xmid,max(xlims[ID][k])+0.05*xmid])
            trax[j][i].xaxis.set_tick_params(which='both', labelbottom=True, length=0.5, pad=2)
            trax[j][i].set_xlim(xlims[ID][k])
            trax[j][i].set_xticks(np.linspace(min(xlims[ID][k]),max(xlims[ID][k]),5))
            #trax[j][i].set_xticklabels(xticklabels[ID][k])
            trax[j][i].set_ylim(0.,1.0)
            trax[j][i].text(xmid,-0.35,lbl,ha='center',fontsize=6)
            
            trax[j][i].yaxis.set_tick_params(which='both', labelleft=False, length=0.0)
            trax[j][i].set_yticks([0.0,0.5,1.0])
            
            xticks = trax[j][i].get_xticklabels()
            labels = [item.get_text() for item in xticks]
            labels[0] = xticklabels[ID][k][0]
            labels[-1] = xticklabels[ID][k][-1]

            trax[j][i].set_xticklabels(labels, fontsize=6)
            
            # set the alignment for outer ticklabels
            xticks[0].set_ha("left")
            xticks[-1].set_ha("right")
            
            trax[j][i].grid(which='both', axis='both', linestyle='-', linewidth=0.1, color='gray')

            if k == 'pl':
                visSmp = samp
                visPDF = pdf
                visCDF = cdf
                xMidVis = xmid
        #%%

        if ID == 'lilium':
            k = 'pl'
            par2 = viax[0].twinx()
            par2.yaxis.set_ticks([])
            
            viax[0].plot(np.sort(visSmp), visCDF, color=oranges(0.75), linewidth=0.8, linestyle='--', zorder=1)
            par2.plot(np.sort(visSmp), visPDF, color='k', linewidth=0.8, zorder=1)

            #print(max(visCDF), max(visPDF))
            viax[0].set_ylim(0.,1.0)
            viax[0].yaxis.set_tick_params(which='both', labelleft=False, length=0.0)
            viax[0].set_yticks([0.0,0.5,1.0])
            viax[0].set_ylabel('Probability [-]')
            
            viax[0].xaxis.set_tick_params(which='both', labelbottom=True, length=2, pad=2)
            viax[0].set_xlabel(r'$PL$ [N/kW]')
            #viax[0].text(xMidVis,-0.2,r'$PL$ [N/kW]',ha='center',fontsize=8)
            viax[0].set_xlim([0.0075,0.025])
            viax[0].set_xticks([0.010,0.015,0.02])
            viax[0].set_xticklabels(['10','15','20'])
            
            viax[0].plot(0.013, 0.06, color='k', markersize=5, markeredgewidth=0.8, marker='v', markerfacecolor='k')
            viax[0].plot(0.016, 0.95, color='k', markersize=5, markeredgewidth=0.8, marker='o', markerfacecolor='k')
            viax[0].plot(0.021, 0.06, color='k', markersize=5, markeredgewidth=0.8, marker='s', markerfacecolor='k')
            
            # par2.annotate('a polar annotation', xy=(0.015, 0.0), xytext=(-50, 30),
            #             textcoords='offset points', arrowprops=dict(arrowstyle="->"),
            #             ha='right',va='bottom')
            

            #viax[0].set_xlim([min(xlims[ID][k])-0.1*min(xlims[ID][k]),max(xlims[ID][k])+0.1*max(xlims[ID][k])])
            # viax[0].set_xticks(np.linspace(min(xlims[ID][k]),max(xlims[ID][k]),5))
            # xticks = viax[0].get_xticklabels()
            # labels = [item.get_text() for item in xticks]
            # labels[0] = xticklabels[ID][k][0]
            # labels[-1] = xticklabels[ID][k][-1]
            # viax[0].set_xticklabels(labels)

            # set the alignment for outer ticklabels
            xticks[0].set_ha("left")
            xticks[-1].set_ha("right")
            viax[0].grid(which='both', axis='both', linestyle='-', linewidth=0.1, color='gray')
            
            viax[1].bar(xMas, yVisMas, color='k', alpha=0.6, width=0.3, hatch='//', linewidth=0.0, edgecolor='w', linestyle='--')
            viax[1].bar(xEnr, yVisEnr, color=blues(0.7), alpha=0.7, width=0.3, hatch='\\\\', linewidth=0.0, edgecolor='w',linestyle='--')
            
            viax[1].set_yticks([0.70,0.85,1.0,1.15,1.3])
            viax[1].set_ylim(0.6,1.4)
            viax[1].set_ylabel('Sensitivitiy [$\%$]')
            viax[1].set_yticklabels(['-30','-15','0','15','30'])
            viax[1].yaxis.set_tick_params(which='both', labelleft=True, length=2, pad=2)
            
            # viax[1].text(-0.3,1.4,'%',va='center',fontsize=8)
            #viax[1].text(0.5,0.55,r'$PL$ [N/kW]',ha='center',fontsize=8)
            viax[1].set_xlabel(r'$PL$ [N/kW]')
            viax[1].grid(axis='y', linestyle='-', linewidth=0.1, color='gray')
  
            
            viax[1].xaxis.set_tick_params(which='both', labelbottom=True, length=0.0, pad=5)
            viax[1].set_xticks([0.25,1.0,1.75])
            viax[1].set_xticklabels(myTicks[ID]['design'][k])

            viax[1].plot(0.25, 0.6, color='k', markersize=5, markeredgewidth=0.8, marker='v', markerfacecolor='k', clip_on=False)
            viax[1].plot(1.0, 0.6, color='k', markersize=5, markeredgewidth=0.8, marker='o', markerfacecolor='k', clip_on=False)
            viax[1].plot(1.75, 0.6, color='k', markersize=5, markeredgewidth=0.8, marker='s', markerfacecolor='k', clip_on=False)            
            
    #%%
        #axes[jMax-1][i].set_xlabel(str(case['name']), va='center', labelpad=12)
        axes[jMax-1][i].text(1.0,0.05,str(case['name']), ha='center',va='center')
        trax[2][i].text(xmid,-0.7,str(case['name']), ha='center',va='center')
 


    legend = [Patch(facecolor='k', alpha=0.6, hatch='///', edgecolor='w', linewidth=0.0, label='DGM Sensitivity'),
              Patch(facecolor=blues(0.7), alpha=0.7, hatch='\\\\\\', edgecolor='w', linewidth=0.0, label='CSE Sensitivity')]
    
    axes[0][3].legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
              loc='upper center', bbox_to_anchor= (0.5, 1.45), ncol=2, borderaxespad=0, frameon=False) 

    legendTriang = [Line2D([], [], color='k', linewidth=0.8, label='Probability Density Function (normalized)'),
                    Line2D([], [], color=oranges(0.75), linewidth=0.8, linestyle='--', label='Cumulative Density Function ')]
    
    trax[0][3].legend(handles=legendTriang, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
              loc='upper center', bbox_to_anchor= (0.5, 1.45), ncol=2, borderaxespad=0, frameon=False) 

    viaxLegend0 = [Line2D([], [], color='k', linewidth=0.8, label='Probability Density'),
                   Line2D([], [], color=oranges(0.75), linewidth=0.8, linestyle='--', label='Cumulative Density')]

    viaxLegend1 = [Patch(facecolor='k', alpha=0.6, hatch='///', edgecolor='w', linewidth=0.0, label='Design Gross Mass'),
                   Patch(facecolor=blues(0.7), alpha=0.7, hatch='\\\\\\', edgecolor='w', linewidth=0.0, label='Consumed Specific Energy')]

    viax[0].legend(handles=viaxLegend0, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
                   loc='lower center', bbox_to_anchor= (0.5, 1.00), ncol=1, borderaxespad=0, frameon=False) 

    viax[1].legend(handles=viaxLegend1, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.2,
                   loc='lower center', bbox_to_anchor= (0.5, 1.00), ncol=1, borderaxespad=0, frameon=False)

    plt.tight_layout()  
    boolPrint = True
    if boolPrint:
        strTime = getTime()
        
        SAVETO = 'output/dissSAmasses_' + strTime + '.pdf'
        fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)
        
        SAVETRIANG = 'output/dissTriang_' + strTime + '.pdf'
        fig0.savefig(SAVETRIANG, bbox_inches='tight', dpi=250.)
 
        SAVEVISUAL = 'output/dissDistVisual_' + strTime + '.pdf'
        fig2.savefig(SAVEVISUAL, bbox_inches='tight', dpi=250.)
    plt.show()

#%%

#     figMission, axMis = plt.subplots(figsize=(textwidth,0.85*textheight),
#                            nrows=len(1), ncols=len(sortedCases), sharex=False, sharey=False,
#                            gridspec_kw={'hspace': 0.35 , 'wspace': 0.17})

#     for i, case in enumerate(sortedCases):
#             ID = case['design']['ident']
    
#             for j, sVar, sTyp, yLab in zip(range(jMax), saVars[ID], saType, yLabels):
     
#                 axMis[j][i].yaxis.set_ticks([])
#                 axMis[j][i].xaxis.set_ticks([])
                
#                 axMis[j][i].set_yticks([0.8,1.0,1.2])
#                 if i != 0:
#                     axMis[j][i].yaxis.set_tick_params(which='both',labelleft=False, length=0.0)
#                 if i == 0:
#                     axMis[j][i].yaxis.set_tick_params(which='both', length=0.0)
#                     axMis[j][i].set_yticklabels(['0.8','1.0','1.2'])
                
#                 axMis[j][i].grid(which='both', axis='y', color='gray', linestyle='-', linewidth=0.5)
                
#                 axMis[j][i].xaxis.set_tick_params(which='both', labelbottom=True, length=0.0)
#                 axMis[j][i].set_xticks([0.3,1.0,1.8])
#                 #axes[j][i].set_xticklabels(['0.5','1.0','1.5'])
#                 axMis[j][i].set_xticklabels(xTik[ID][j])

#     if boolPrint:
#         strTime = getTime()
#         SAVETO = 'output/dissSAmassesMission_' + strTime + '.pdf'
#         fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)

# #%%

#     figDesign, axDes = plt.subplots(figsize=(textwidth,0.85*textheight),
#                            nrows=len(4), ncols=len(sortedCases), sharex=False, sharey=False,
#                            gridspec_kw={'hspace': 0.35 , 'wspace': 0.17})

#     if boolPrint:
#         strTime = getTime()
#         SAVETO = 'output/dissSAmassesDesign_' + strTime + '.pdf'
#         fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)

# #%%

#     figOps, axOps = plt.subplots(figsize=(textwidth,0.85*textheight),
#                            nrows=len(4), ncols=len(sortedCases), sharex=False, sharey=False,
#                            gridspec_kw={'hspace': 0.35 , 'wspace': 0.17})

#     if boolPrint:
#         strTime = getTime()
#         SAVETO = 'output/dissSAmassesOps_' + strTime + '.pdf'
#         fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)
        
    # import sys
    # sys.exit()