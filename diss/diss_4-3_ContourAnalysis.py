#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 07:57:55 2020

@author: Nicolas
"""

import os
os.chdir('..')
import copy
import sys 

import numpy as np
import matplotlib.pyplot as plt

from data.inputVTOL import loadCase, gdv, gmv, loadFueltype, loadTech

from source.util import getTime

import matplotlib as mpl

# BE CAREFUL WHICH MODEL IS TAKEN INTO CONSIDERATION!!!
from source.modSizingTier0 import getAircraftMass
from source.modStatistics import getSample
from source.modSensitivityAnalysis import contourAnalysis2D
from source.modAssessmentTier0 import getTotalSpecEnergy
from source.modAssessmentTier0 import getTotalOperationsImpact

from data.inputImpact import loadOpsImpact

from scipy.optimize import curve_fit
from scipy.stats import linregress


# def calc_Vcruise(ltod):

#     y1 = 35.
#     y2 = 90.
#     x1 = 3.0
#     x2 = 16.0
    
#     slope = (y2 - y1) / (x2 - x1)
#     offset = y1 - slope * x1    
    
#     return slope * ltod + offset    

# def calc_fStruct(ltod):
    
#     y1 = 0.235
#     y2 = 0.305
#     x1 = 3.0
#     x2 = 17.0
    
#     slope = (y2 - y1) / (x2 - x1)
#     offset = y1 - slope * x1    
    
#     return slope * ltod + offset

# def calc_pl(ltod, TYPE):
    
#     if TYPE == 'base':
#         pl = func(ltod, *plRegrBase)
#     elif TYPE == 'min':
#         pl = func(ltod, *plRegrMin)
#     elif TYPE == 'max':
#         pl = func(ltod, *plRegrMax)    
#     else:
#         print('NAN')
    
#     return pl

#%%============================================================================
#   FORMATTING SETTINGS
#  ============================================================================

XSMALL = 6
SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=XSMALL)    # fontsize of the tick labels
plt.rc('ytick', labelsize=XSMALL)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

mpl.rcParams['hatch.linewidth'] = 0.6

greys = plt.get_cmap('Greys')
blues = plt.get_cmap('Blues')
greens = plt.get_cmap('Greens')
oranges = plt.get_cmap('Oranges')

#%%============================================================================
#   GLOBAL VARIABLES
#  ============================================================================

textwidth = 5.809    
textheight = 8.275

boolPrintRegression = True
doStateOfTheArtPlot = False

doFourDimsPlot = True
doFourSensitivityPlot = True

#%%============================================================================
#   FUNCTIONS
#  ============================================================================

def func(x, a, b, c):
    return a * np.exp(-b * x) + c

def regrPowerloading(ld, at, popt):
    return func(ld, *popt['pl'][at])

def regrCruiseVelo(ld, at, slope, intercept):
    return intercept['cruiseVelo'][at] + slope['cruiseVelo'][at] * ld

def regrStructRatio(ld, at, slope, intercept):  
    return intercept['fStruct'][at] + slope['fStruct'][at] * ld

def printRegressionGraph(X, Y):
    

    # fig, ax = plt.subplots(figsize=(textwidth,0.4*textheight))
    
    figure, ax = plt.subplots(figsize=(1.0*textwidth,0.22*textheight),
                              nrows=1, ncols=3, sharex=False, sharey=False,
                              gridspec_kw={'wspace': 0.3})
    
    
    #ylabels = ['Power Loading (N/kW)', 'Structural Mass Fraction (-)', 'Cruise Velocity (m/s)']
    markers = ['<', 'o', 'D', '^', '>', 'H']
    # labels = 
    ax[0].set_title('Power Loading (N/kW)', size=7)
    # print(model)
    # ax[0].plot(ltodReg, model['baseline'](ltodReg))
    
    ax[0].errorbar(X['baseline'], Y['pl']['baseline'],xerr=[xlow['pl'],xup['pl']], yerr=[ylow['pl'], yup['pl']],
                    color='k', marker=None, linewidth=0., markersize=0., markeredgewidth=0.8, elinewidth=0.8, capsize=3.0)
    ax[0].plot(ltodReg, regressions['pl']['baseline'],'k-', linewidth=0.8)
    ax[0].fill_between(ltodReg, regressions['pl']['min'], regressions['pl']['max'], color='k', alpha=0.15, zorder=0., edgecolor=None)
    
    ax[1].set_title('Structural Mass Fraction (-)', size=7)
    ax[1].errorbar(X['baseline'], Y['fStruct']['baseline'],
                    xerr=[xlow['fStruct'],xup['fStruct']], yerr=[ylow['fStruct'], yup['fStruct']],
                    color='k', marker=None, linewidth=0.0, markersize=0., markeredgewidth=0.8, elinewidth=0.8, capsize=3.0)
    ax[1].plot(ltodReg, regressions['fStruct']['baseline'],'k-', linewidth=0.8)
    ax[1].fill_between(ltodReg, regressions['fStruct']['min'], regressions['fStruct']['max'], color='k', alpha=0.15, zorder=0., edgecolor=None)
    
    ax[2].set_title('Cruise Velocity (m/s)', size=7)
    ax[2].errorbar(X['baseline'], Y['cruiseVelo']['baseline'],
                    xerr=[xlow['cruiseVelo'],xup['cruiseVelo']],
                    color='k', marker=None, linewidth=0.0, markersize=0., markeredgewidth=0.8, elinewidth=0.8, capsize=3.0)
    ax[2].plot(ltodReg, regressions['cruiseVelo']['baseline'],'k-', linewidth=0.8)
    ax[2].fill_between(ltodReg, regressions['cruiseVelo']['min'], regressions['cruiseVelo']['max'], color='k', alpha=0.15, zorder=0., edgecolor=None)
    
    for i, case in enumerate(listCases):
        ax[0].plot(X['baseline'][i], Y['pl']['baseline'][i], color='k', marker=markers[i], markersize=4., markeredgewidth=0.8, markerfacecolor='w',linewidth=0.8,label=case['name'])
        ax[1].plot(X['baseline'][i], Y['fStruct']['baseline'][i], color='k', marker=markers[i], markersize=4., markeredgewidth=0.8, markerfacecolor='w',linewidth=0.0)
        ax[2].plot(X['baseline'][i], Y['cruiseVelo']['baseline'][i], color='k', marker=markers[i], markersize=4., markeredgewidth=0.8, markerfacecolor='w',linewidth=0.0)
    
    
    for i, axis in enumerate(ax):
        axis.set_xlim(1.0,19.0)
        axis.set_xticks(np.linspace(1.0,19.0,10))
        axis.set_xlabel('Lift-to-Drag ratio (-)', fontsize=7)
        #axis.set_ylabel(ylabels[i], fontsize=7)
        axis.grid(axis='both', linestyle='-', linewidth=0.1, color='gray')
        axis.yaxis.set_tick_params(which='both', labelleft=True, length=2.5, pad=2)
        axis.xaxis.set_tick_params(which='both', labelbottom=True, length=2.5, pad=2)
    
    ax[0].set_ylim(0.01,0.13)
    ax[0].set_yticks(np.linspace(0.01,0.13,7))
    ax[1].set_ylim(0.23,0.31)
    ax[2].set_ylim(20.,100.)
    
    ax[0].legend(fontsize=6, handlelength=2.0)
    
    plt.tight_layout()  
    boolPrint = True
    if boolPrint:
        strTime = getTime()
        
        SAVETO = 'output/dissRegressions_' + strTime + '.pdf'
        figure.savefig(SAVETO, bbox_inches='tight', dpi=250.)
        
    plt.show()

    return None


def purgeArtifacts(key, X, Y, Z):
    
    if key == 'dgm':
        for r, row in enumerate(X):
            for c, col in enumerate(Y):
                # if X[r][c] > 350000. and Y[r][c] < 8.:
                #     Z[r][c] = 0.999
                # elif X[r][c] > 250000. and Y[r][c] < 6.5:
                #     Z[r][c] = 0.999
                # elif X[r][c] > 380000. and Y[r][c] < 10.:
                #     Z[r][c] = 0.999
                if X[r][c] > 50000. and Y[r][c] < 1.5:
                    Z[r][c] = 9999.                
                elif X[r][c] > 70000. and Y[r][c] < 2.4:
                    Z[r][c] = 9999.
                elif X[r][c] > 90000. and Y[r][c] < 2.7:
                    Z[r][c] = 9999.
                elif X[r][c] > 120000. and Y[r][c] < 4.0:
                    Z[r][c] = 9999.
                elif X[r][c] > 150000. and Y[r][c] < 4.5:
                    Z[r][c] = 9999.
                elif X[r][c] > 200000. and Y[r][c] < 6.:
                    Z[r][c] = 9999.
                elif X[r][c] > 250000. and Y[r][c] < 12:
                    Z[r][c] = 9999. 
                    
    elif key == 'lmb':
        for r, row in enumerate(X):
            for c, col in enumerate(Y):
                # if X[r][c] > 350000. and Y[r][c] < 8.:
                #     Z[r][c] = 0.999
                # elif X[r][c] > 250000. and Y[r][c] < 6.5:
                #     Z[r][c] = 0.999
                if X[r][c] > 65000. and Y[r][c] < 1.5:
                    Z[r][c] = 9999.     
                elif X[r][c] > 75000. and Y[r][c] < 2.:
                    Z[r][c] = 9999.
                elif X[r][c] > 120000. and Y[r][c] < 3.:
                    Z[r][c] = 9999.
                elif X[r][c] > 150000. and Y[r][c] < 4.:
                    Z[r][c] = 9999.
                elif X[r][c] > 200000. and Y[r][c] < 5.:
                    Z[r][c] = 9999.
                elif X[r][c] > 250000. and Y[r][c] < 8:
                    Z[r][c] = 9999.
                elif X[r][c] > 400000. and Y[r][c] < 12.:
                    Z[r][c] = 9999.
 
    elif key == 'lab':
        for r, row in enumerate(X):
            for c, col in enumerate(Y):
                # if X[r][c] > 350000. and Y[r][c] < 8.:
                #     Z[r][c] = 0.999
                # elif X[r][c] > 250000. and Y[r][c] < 6.5:
                #     Z[r][c] = 0.999
                # elif X[r][c] > 380000. and Y[r][c] < 10.:
                #     Z[r][c] = 0.999
                if X[r][c] > 150000. and Y[r][c] < 1.5:
                    Z[r][c] = 9999.
                elif X[r][c] > 175000. and Y[r][c] < 1.8:
                    Z[r][c] = 9999.
                elif X[r][c] > 200000. and Y[r][c] < 2.25:
                    Z[r][c] = 9999.
                elif X[r][c] > 300000. and Y[r][c] < 3.:
                    Z[r][c] = 9999.
                elif X[r][c] > 350000. and Y[r][c] < 4.:
                    Z[r][c] = 9999.

    else:
        pass

    return Z

def wrapRegressions(case):

    ld = case['design']['ltod']
    popt = case['info']['popt']
    intercept = case['info']['intercept']
    slope = case['info']['slope']
    at = case['info']['at']

    case['design']['pl'] = regrPowerloading(ld, 'baseline', popt)
    case['design']['fStruct'] = regrStructRatio(ld, 'baseline', slope, intercept)
    case['mission']['cruiseVelo'] = regrCruiseVelo(ld, 'baseline', slope, intercept)
    case['operations']['cruiseVelo'] = regrCruiseVelo(ld, 'baseline', slope, intercept)
    case['operations']['cruiseRange'] = case['mission']['cruiseRange']

    
    if case['info']['wrapKey'] == 'dgm':
        val = getAircraftMass(case)

    elif case['info']['wrapKey'] == 'energy':
        val = getTotalSpecEnergy(case)

    elif case['info']['wrapKey'] == 'impact':
        val = getTotalOperationsImpact(case)

    return val

def getContourMin(case, tupX, tupY, samples):
        
    import copy as cp 
    from math import isnan
    copyCase = cp.deepcopy(case)
    
    minimumX = []
    minimumY = []
    
    xMin = min(tupX[2])
    xMax = max(tupX[2])
    yMin = min(tupY[2])
    yMax = max(tupY[2])
    
    for x, rng in enumerate(np.linspace(xMin,xMax,samples)):
    #for x, rng in enumerate(tupX[2]):
        out = []
        minOut = 99999.
        for y, ld in enumerate(np.linspace(yMin,yMax,samples)):
        #for y, ld in enumerate(tupY[2]):
            copyCase['mission']['cruiseRange'] = rng
            copyCase['design']['ltod'] = ld
            out.append(wrapRegressions(copyCase))
            if not isnan(out[-1]):
                if out[-1] < minOut:
                    minOut = out[-1]
                    minLD = ld
                    minR = rng
        #     print('x {} | y {}: {:8.0f} {:5.1f} {:6.1f} kg {}'.format(x,y,rng,ld,out[-1], isnan(out[-1])))
        # print('Minimum is:', minOut, minR, minLD)
        minimumY.append(minR)
        minimumX.append(minLD)    

    return minimumX, minimumY    

def setUpperTicks(axis, regressions, at):

    axPL = axis.twiny()
    axPL.set_xlim(regressions['pl'][at][0],regressions['pl'][at][-1])
    axPL.set_xticks(np.linspace(0.01,0.13,7))
    axPL.set_xlabel('Power Loading (N/kW)', fontsize=6)
    labelPL = axPL.get_xticklabels()        
    labelPL[0].set_ha("right")
    labelPL[-1].set_ha("left")
        
    axFS = axis.twiny()
    axFS.set_xlim(regressions['fStruct'][at][0],regressions['fStruct'][at][-1])
    axFS.set_xticks(np.linspace(0.21,0.33,7))
    axFS.spines['top'].set_position(('outward', 30))
    axFS.set_xlabel('Structural Mass Fraction (-)', fontsize=6)    
    labelFS = axFS.get_xticklabels()        
    labelFS[0].set_ha("left")
    labelFS[-1].set_ha("right")
        
    axVC = axis.twiny()
    axVC.set_xlim(regressions['cruiseVelo'][at][0],regressions['cruiseVelo'][at][-1])
    axVC.set_xticks(np.arange(20.,100.01,10.))
    axVC.spines['top'].set_position(('outward', 60))
    axVC.set_xlabel('Cruise Velocity (m/s)', fontsize=6)    
    labelVC = axVC.get_xticklabels()        
    labelVC[0].set_ha("left")
    labelVC[-1].set_ha("right")
        
    return None



#%%============================================================================
#   DATA SETS
#  ============================================================================

# dHybridLIB = loadFueltype('hybridBattery', **{'highEnerSys': dLiIon, 'highPowSys': dLiPo})
# dHybridLIB['ident'] = 'lib'

dHybridLIB = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lib', 'baseline'), 
                                              'highPowSys': loadTech('lpo', 'baseline')})

dHybridLMB = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lmb', 'baseline'), 
                                              'highPowSys': loadTech('lpo', 'baseline')})

dHybridLAB = loadFueltype('hybridBattery', **{'highEnerSys': loadTech('lab', 'baseline'), 
                                              'highPowSys': loadTech('lpo', 'baseline')})

dHybridH2Gaseous = loadFueltype('hybrid', **{'highEnerSys': loadTech('h2gas', 'baseline'), 
                                            'highPowSys': loadTech('lpo', 'baseline')})

dHybridH2Liquid = loadFueltype('hybrid', **{'highEnerSys': loadTech('h2liq', 'baseline'), 
                                            'highPowSys': loadTech('lpo', 'baseline')})

dFuels = {'lib': dHybridLIB,
          'lmb': dHybridLMB,
          'lab': dHybridLAB,
          'h2g': dHybridH2Gaseous,
          'h2l': dHybridH2Liquid}

#%%============================================================================

caseVolo   =  loadCase('volo', 'baseline', dHybridLMB)
caseCora   =  loadCase('cora', 'baseline', dHybridLMB)
caseCity   =  loadCase('city', 'baseline', dHybridLMB)
caseVahana =  loadCase('vahana', 'baseline', dHybridLMB)
caseJoby   =  loadCase('joby', 'baseline', dHybridLMB)
caseLilium =  loadCase('lilium', 'baseline', dHybridLMB)

# caseSkai =  loadCase('skai', 'baseline', dHybridFuelCell)
# #caseSkai =  loadCase('hylevio', 'baseline', dHybridFuelCell)

listCases = [copy.deepcopy(caseVolo), 
             copy.deepcopy(caseCity), 
             copy.deepcopy(caseVahana), 
             copy.deepcopy(caseCora), 
             copy.deepcopy(caseJoby), 
             copy.deepcopy(caseLilium)]

dCaseNames = {'volo':   'Volocopter',
              'city':   'CityAirbus',
              'vahana': 'Vahana',
              'cora':   'Cora',
              'lilium': 'Lilium',
              'joby':   'Joby',
              }

xId = []
# xStr = 'ltod'
# yStr = 'pl'
# xVal = []
# yVal = []
# xMin, xMax = [], []
# yMin, yMax = [], []

# veloBase = []
# veloMin, veloMax = [], []

# ltodBase = []
# ltodMin, ltodMax = [], []
# ltodUpper, ltodLower = [], []

# fstrBase = []
# fstrMax, fstrMin = [], []
# fstrUpper, fstrLower = [], []
    
keys = ['pl', 'cruiseVelo', 'fStruct']
bounds = ['min','baseline','max']

for i, case in enumerate(listCases):
    
    xId.append(case['design']['ident'])
    
    case['name'] = dCaseNames[xId[-1]]
    case['info'] = {}
    case['info']['funcUnit'] = 'PKT'
    
    # xVal.append(case['design'][xStr])
    # yVal.append(case['design'][yStr])
        
    # xMin.append(gdv(xId[i],'min',xStr))
    # xMax.append(gdv(xId[i],'max',xStr))
    
    # yMin.append(gdv(xId[i],'min',yStr))
    # yMax.append(gdv(xId[i],'max',yStr))  
    
    # veloBase.append(case['mission']['cruiseVelo'])
    # veloMin.append(gmv(xId[i],'min','cruiseVelo'))
    # veloMax.append(gmv(xId[i],'max','cruiseVelo'))
    
    # fstrBase.append(case['design']['fStruct'])
    # fstrMax.append(gdv(xId[i],'max','fStruct'))
    # fstrMin.append(gdv(xId[i],'min','fStruct'))
    # fstrUpper.append(gdv(xId[i],'max','fStruct')-case['design']['fStruct'])
    # fstrLower.append(case['design']['fStruct']-gdv(xId[i],'min','fStruct'))
    
    # ltodBase.append(case['design']['ltod'])
    # ltodMin.append(gdv(xId[i],'min','ltod'))
    # ltodMax.append(gdv(xId[i],'max','ltod'))
    # ltodUpper.append(gdv(xId[i],'max','ltod')-case['design']['ltod'])
    # ltodLower.append(case['design']['ltod']-gdv(xId[i],'min','ltod'))
    # xLow.append(gdv(xId[i],'baseline',xStr) - gdv(xId[i],'min',xStr))
    # xUp.append(gdv(xId[i],'max',xStr) - gdv(xId[i],'baseline',xStr))
    
    # yLow.append(gdv(xId[i],'baseline',yStr) - gdv(xId[i],'min',yStr))
    # yUp.append(gdv(xId[i],'max',yStr) - gdv(xId[i],'baseline',yStr))    

model = {}
regX, regY = {}, {}
xlow, xup = {}, {}
ylow, yup = {}, {}
popt, pcov = {}, {}
intercept, slope, r, p, se = {}, {}, {}, {}, {}

for key in keys:
    regY[key] = {}
    popt[key] = {}
    pcov[key] = {}
    intercept[key] = {}
    slope[key] = {}
    r[key] = {}
    p[key] = {}
    se[key] = {}

    
    for at in bounds:
        regX[at] = []
        regY[key][at] = []
        
        for i, case in enumerate(listCases):
            
            ID = case['design']['ident']
            regX[at].append(gdv(ID,at,'ltod'))

            if key == 'cruiseVelo':
                regY[key][at].append(gmv(ID,at,key))
            else:
                regY[key][at].append(gdv(ID,at,key))     
        if key == 'pl':
            popt[key][at], pcov[key][at] = curve_fit(func, regX[at], regY[key][at], bounds=(0, [0.4, 0.4, 0.1]))
            #model[at] = np.poly1d(np.polyfit(regX[at], regY[key][at], 2))    # quadratic regression
        else:
            slope[key][at], intercept[key][at], r[key][at], p[key][at], se[key][at] = linregress(regX[at], regY[key][at])

    ylow[key] = []
    yup[key] = []
    xlow[key] = []
    xup[key] = []
    for i, case in enumerate(listCases):
        
        ID = case['design']['ident']

        xlow[key].append(gdv(ID,'baseline','ltod') - gdv(ID,'min','ltod'))
        xup[key].append(gdv(ID,'max','ltod') - gdv(ID,'baseline','ltod'))
            
        if key == 'cruiseVelo':
            ylow[key].append(gmv(ID,'baseline',key) - gmv(ID,'min',key))
            yup[key].append(gmv(ID,'min',key) - gmv(ID,'baseline',key))
        else:
            if key == 'fStruct' and ID == 'volo':
                ylow[key].append(0.0)
                yup[key].append(0.0) 
            else:   
                ylow[key].append(gdv(ID,'baseline',key) - gdv(ID,'min',key))
                yup[key].append(gdv(ID,'max',key) - gdv(ID,'baseline',key))   

# import sys
# sys.exit()
#%%============================================================================

# mBase, cBase, ldR, ldP, ldSE = linregress(ltodBase, veloBase)
# mMin, cMin, r, p, se = linregress(ltodMin, veloBase)
# mMax, cMax, r, p, se = linregress(ltodMax, veloBase)

ltod_min = min(regX['baseline']) - 2.5
ltod_max = max(regX['baseline']) + 2.5
ltodReg = np.linspace(ltod_min, ltod_max, 100)

veloReg = []
veloRegMin = []
veloRegMax = []
veloSample = []

fstrReg = []
fstrLin = []
fstrLinMin = []
fstrLinMax = []

regressions = {}
    
for key in keys:
    
    regressions[key] = {}
    for at in bounds:
        
        regressions[key][at] = []
        for ld in ltodReg:
            
            if key == 'pl':
                regressions[key][at].append(func(ld, *popt['pl'][at]))
                # quadratic[at].append(model[])
            elif key == 'fStruct' or key =='cruiseVelo':
                regressions[key][at].append(intercept[key][at] + slope[key][at] * ld)
            else:
                print('Nothing happens')


# print(slope['fStruct']['baseline'], intercept['fStruct']['baseline'])
# print(slope['cruiseVelo']['baseline'], intercept['cruiseVelo']['baseline'])


if boolPrintRegression:
    printRegressionGraph(regX, regY)

#%%



# import sys
# sys.exit()


# pl = []
# dgm = []
# ltod = np.arange(3.5,15.0,0.25)

# case = caseJoby

# case['mission']['hoverTime'] = 2.*60.

#%%===========================================================================
# S C E N A R I O S

# BASE CASE TODAY
# dOpImpact = {'elecProduction':        0.475,  # pure wind; ref; world 2030 
#              'batteryProduction':     212.,
#              'hydrProdTotal':         11893,
#              'hydrTankAndEquip':      58.2,
#              'fuelStackProduction':   90.785,
#             }

# BASE CASE 2030
# dOpImpact = {'elecProduction':        0.237,  # pure wind; ref; world 2030 
#              'batteryProduction':    112.,
#              'hydrProdTotal':        2412,
#              'hydrTankAndEquip':     58.2,
#              'fuelStackProduction':  90.785,
#             }

# GREEN CASE 2030
# dOpImpact = {'elecProduction':        0.020,  # pure wind; ref; world 2030 
#               'batteryProduction':    62.,
#               'hydrProdTotal':        970,
#               'hydrTankAndEquip':     58.2,
#               'fuelStackProduction':  90.785,
#             }


#%% ============================================================================
# DEFINITION OF SPECIFIC CASE PARAMETERS
# ==============================================================================

dDesign = {'ident':     'base', 
           'ltod':      None,
           'pl':        None,
           'fStruct':   None,
           'fOth':      0.6}

dMission = {'nPax':         4, 
            'cruiseRange':  None,
            'cruiseVelo':   None,
            'massPayload':  88 * 4, 
            'massCrew':     80., 
            'timeReserve':  0., 
            'hoverTime':    1.5*60, 
            'fUtil':        0.75,
            'fCirc':        1.0}


caseLIB = {'design':        copy.deepcopy(dDesign), 
           'mission':       copy.deepcopy(dMission), 
           'operations':    copy.deepcopy(dMission), 
           'fuel':          copy.deepcopy(dHybridLIB),
           }

caseLMB = {'design':        copy.deepcopy(dDesign), 
           'mission':       copy.deepcopy(dMission), 
           'operations':    copy.deepcopy(dMission), 
           'fuel':          copy.deepcopy(dHybridLMB),
           }

caseLAB = {'design':        copy.deepcopy(dDesign), 
           'mission':       copy.deepcopy(dMission), 
           'operations':    copy.deepcopy(dMission), 
           'fuel':          copy.deepcopy(dHybridLAB),
           }

caseH2G = {'design':        copy.deepcopy(dDesign), 
           'mission':       copy.deepcopy(dMission), 
           'operations':    copy.deepcopy(dMission), 
           'fuel':          copy.deepcopy(dHybridH2Gaseous),
           }

caseH2L = {'design':        copy.deepcopy(dDesign), 
           'mission':       copy.deepcopy(dMission), 
           'operations':    copy.deepcopy(dMission), 
           'fuel':          copy.deepcopy(dHybridH2Liquid),
           }

cases = {'lmb': caseLMB,
         'lab': caseLAB,
         'h2g': caseH2G,
         'h2l': caseH2L}


#%% ============================================================================
# SET SAMPLES FOR CONTOUR ANALYSES
# ==============================================================================

iSamples = 12*10
# iSamples = 6*5

tupX = ('mission', 'cruiseRange', getSample('linspace', [10001, 400001], iSamples)) 
tupY = ('design', 'ltod', getSample('linspace', [1.0, 19.0], iSamples)) 
  
            
# import copy as cp 
# #from math import isnan
# caseA = loadCase('vahana', 'baseline', dHybridLMB)

# minimumX = []
# minimumY = []

# xMin = min(tupX[2])
# xMax = max(tupX[2])
# yMin = min(tupY[2])
# yMax = max(tupY[2])

# samples = 100

# from source.modSizingTier0 import getPowers

# caseA['design']['ltod'] = 3.5
# caseA['info'] = {}
# caseA['info']['popt'] = popt
# caseA['info']['intercept'] = intercept
# pHov, pCrs = [], []
# for x, rng in enumerate(np.linspace(xMin,xMax,samples)):
#     ld = caseA['design']['ltod']
#     popt = caseA['info']['popt']
#     intercept = caseA['info']['intercept']

#     caseA['design']['pl'] = regrPowerloading(ld, 'baseline', popt)
#     caseA['design']['fStruct'] = regrStructRatio(ld, 'baseline', slope, intercept)
#     caseA['mission']['cruiseVelo'] = regrCruiseVelo(ld, 'baseline', slope, intercept)
    
#     caseA['mission']['cruiseRange'] = rng
#     pH, pC = getPowers(caseA)
#     pHov.append(pH/1000)
#     pCrs.append(pC/1000)

# plt.plot(np.linspace(xMin,xMax,samples), pHov, color='gray', linestyle='-', label='pHover')
# plt.plot(np.linspace(xMin,xMax,samples), pCrs, color=blues(0.8) ,linestyle='--',  label='pCruise')
# plt.xlabel('Range (km)')
# plt.ylabel('Powers (kW)')
# plt.ylim(0.,250.)
# plt.xlim(0.,300000.)
# plt.grid()
# plt.legend()
# plt.show()

# caseA['mission']['cruiseRange'] = 50000.
# pHov, pCrs = [], []        
# for y, ld in enumerate(np.linspace(yMin,yMax,samples)):
#     ld = caseA['design']['ltod']
#     popt = caseA['info']['popt']
#     intercept = caseA['info']['intercept']

#     caseA['design']['pl'] = regrPowerloading(ld, 'baseline', popt)
#     caseA['design']['fStruct'] = regrStructRatio(ld, 'baseline', slope, intercept)
#     caseA['mission']['cruiseVelo'] = regrCruiseVelo(ld, 'baseline', slope, intercept)

#     # caseA['design']['ltod'] = ld
#     pH, pC = getPowers(caseA)
#     pHov.append(pH/1000)
#     pCrs.append(pC/1000)                

# plt.plot(np.linspace(yMin,yMax,samples), pHov, 'k-', label='pHover')
# plt.plot(np.linspace(yMin,yMax,samples), pCrs, color=greens(0.8) ,linestyle='--',  label='pCruise')
# plt.legend()
# plt.xlabel('L/D (-)')
# plt.ylabel('Powers (kW)')
# plt.ylim(0.,250.)
# plt.grid()
# plt.show()  

# sys.exit()  

#%% ============================================================================
# STATE OF THE ART DGM AND CSE ANALYSIS
# ==============================================================================

# print('{:4s} {:4s} {:4s} {:4s} {:4s} {:4s}'.format('R', 'L/D', 'hHES', 'hHPS', 'cHES', 'cHPS'))
if doStateOfTheArtPlot:

    fig2, ax = plt.subplots(figsize=(textwidth,0.5*textwidth),
                            nrows=1, ncols=2, sharex=True, sharey=True,
                            gridspec_kw={'wspace': 0.05, 'width_ratios': [1,1]})
    
    ctrs = {}
    keys = ['dgm', 'energy']
    for i, key in enumerate(keys):
       
        caseLIB['info'] = {}
        caseLIB['info']['wrapKey'] = key
        caseLIB['info']['funcUnit'] = 'PKT'
        caseLIB['info']['popt'] = popt
        caseLIB['info']['intercept'] = intercept
        caseLIB['info']['slope'] = slope
        caseLIB['info']['at'] = at
        
        if key == 'dgm':
            axis = ax[0]
            axis.set_ylabel('Maximum Design Range (km)', fontsize=7)
        elif key == 'energy':
            axis = ax[1]
        else:
            pass
    
        setUpperTicks(axis, regressions, 'baseline')   
       
        axis.set_xticks(np.linspace(1.0,19.0,10))
        axis.set_xlabel('Lift-to-Drag Ratio (-)', fontsize=7)        
        axis.set_yticks([])
    
        hatches = []
    
        if key == 'dgm':
            levels = np.arange(1300.,2501.,200)
            for l, lev in enumerate(levels):
                hatches.append(None)
                if l == 1:
                    hatches[-1] = '//'
                elif l == 5:
                    hatches[-1] = '////'            
            sMap = 'Greys'
    
        elif key == 'energy':
            #levels = np.arange(0.1,0.501,0.08)
            levels = np.linspace(0.1,0.8,15)
            for l, lev in enumerate(levels):
                hatches.append(None)
                if l == 1:
                    hatches[-1] = '\\\\'
                elif l == 5:
                    hatches[-1] = '\\\\\\'
            sMap = 'Blues'
          


        # sys.exit()
        X, Y, Z = contourAnalysis2D(wrapRegressions, caseLIB, tupX, tupY)
        #purgeArtifacts(key, X, Y, Z)
        
        
        #ctrs[key] = axis.contourf(Y, X, Z, levels=10, linewidths=1.0, hatches=hatches, cmap='Greys')
        #ctrs[key] = axis.contourf(Y, X, Z, levels=levels, hatches=hatches, edgecolor='r', linewidth=0.8, cmap=sMap)
        ctrs[key] = axis.contourf(Y, X, Z, levels=levels, hatches=hatches, cmap=sMap)
        axis.contour(Y, X, Z, levels=levels, linewidths=0.3, colors='k')
    
                          
    
    
        if key == 'dgm':
            cs = axis.contour(Y, X, Z, levels=[3175.], linewidths=0.6, linestyles='-.', colors='k')
            axis.annotate('VTOL Limit\n3175kg', (12.5, 310000),
                          xytext=(12.0, 330000), textcoords='data',
                          arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
                          fontsize=6, horizontalalignment='center', verticalalignment='bottom')
            # plt.clabel(cs, inline=1, fontsize=6, fmt='%.0f')
            cax = fig2.add_axes([0.14, 0.45, 0.025, 0.40])
            cbar = fig2.colorbar(ctrs['dgm'], cax=cax, drawedges=True)
            cbar.ax.tick_params(labelsize=6,  length=2, pad=3)
            axis.set_title('Design Gross Mass (kg)', fontsize=7)
            
            minX, minY = getContourMin(caseLIB, tupX, tupY, 500)
            # print(minX, minY)
            axis.plot(minX, minY, color=greys(0.8), linewidth=0.8, linestyle='--') 
            axis.annotate(' ', (13.0, 160000),
                          xytext=(11.7, 175000), textcoords='data',
                          arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
                          fontsize=6, horizontalalignment='center', verticalalignment='bottom')
            axis.text(11.2, 182000, 'Min. DGM\nat given\nrange', color='k', fontsize=6,
                      bbox=dict(facecolor='w', alpha=0.6, boxstyle='round', linewidth=0.8),
                      horizontalalignment='center', verticalalignment='bottom')
            
        elif key == 'energy':
            cax = fig2.add_axes([0.54, 0.45, 0.025, 0.40])
            cbar = fig2.colorbar(ctrs['energy'], cax=cax, drawedges=True)
            cbar.ax.tick_params(labelsize=6,  length=2, pad=3)
            axis.set_title('Consumed Specific Energy (kWh/PKT)', fontsize=7)
            
            minX, minY = getContourMin(caseLIB, tupX, tupY, 100)
            # print(minX, minY)
            axis.plot(minX, minY, color=blues(0.8), linewidth=0.8, linestyle='--') 
            axis.annotate(' ', (16.5, 155000),
                          xytext=(15.0, 175000), textcoords='data',
                          arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
                          fontsize=6, horizontalalignment='center', verticalalignment='bottom')
            axis.text(14.0, 180000, 'Min. CSE\nat given\nrange', color='k', fontsize=6,
                      bbox=dict(facecolor='w', alpha=0.6, boxstyle='round', linewidth=0.8),
                      horizontalalignment='center', verticalalignment='bottom')
            # axis.annotate('abc', (16.6, 1550000),
            #               xytext=(12.0, 330000), textcoords='data',
            #               arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
            #               fontsize=6, horizontalalignment='center', verticalalignment='bottom')
            
        axis.grid(axis='both', linestyle='-', linewidth=0.1, color='gray')
            
        del X, Y, Z
    
    
    yticks = np.arange(50000., max(tupX[2]), 50000)
    axis.set_yticks(yticks)
    #ax[0].set_yticklabels([str(round(tick/1000.,0)) for tick in yticks])
    axis.set_yticklabels(['50','100','150','200','250','300','350', '400'])
    
    
    
    #axis.text(17.8,420000.,'DGM (kg)')
    
    # elif case['info']['wrapKey'] == 'energy':
    #     cax = fig.add_axes([0.92, 0.125, 0.03, 0.7555])
    #     fig.colorbar(ctrs['dgm'], cax=cax)
    #     axis.text(17.8,815000.,'Specific\nEnergy\n(kWh/PKT)')
    
    boolPrint = True 
    plt.tight_layout()  
    if boolPrint:
        strTime = getTime()
        #SAVETO = 'output/dissContoursOpsImpact2030Base_' + strTime + '.pdf'
        SAVETO = 'output/dissContoursStateOfTheArt_' + strTime + '.pdf'
        fig2.savefig(SAVETO, bbox_inches='tight', dpi=250.)
    
    plt.show()
    
    import sys


#%% ===========================================================================
# FOUR SUBOPLOT DEPICTION OF CONTOUR ANALYSIS
# =============================================================================
# I. Set keys for simplified execution of code


def fourDimensionsPlot(ctrKey):
    funcUnit = 'PKT'
    
    
    for key, case in cases.items():
        #
        # case['opsImpact'] = loadOpsImpact(case['fuel'], '2030base')
        case['opsImpact'] = loadOpsImpact(case['fuel'], '2030green')
    
    # =============================================================================
    # II. Define Figue environment
    
    fig, ax = plt.subplots(figsize=(textwidth,textwidth),
                            nrows=2, ncols=2, sharex=True, sharey=True,
                            gridspec_kw={'wspace': 0.05,  'hspace': 0.05, 
                                        'width_ratios': [1,1]})
    
    # =============================================================================
    # III. Loop through cases
    
    ctrs = {}
    
    for i, (key, case) in enumerate(cases.items()):
        
        case['info'] = {}
        case['info']['wrapKey'] = ctrKey
        case['info']['funcUnit'] = funcUnit
        case['info']['popt'] = popt
        case['info']['intercept'] = intercept
        case['info']['slope'] = slope
        case['info']['at'] = at
        
        if key == 'lmb':
            axis = ax[0][0]
            setUpperTicks(axis, regressions, 'baseline')
            axis.set_ylabel('Maximum Design Range (km)')
            axis.xaxis.set_tick_params(which='both', labelbottom=False)
            #axis.text(1.2,370000,'Lithium Metal Battery',ha='left',va='center',
            axis.text(2,370000,'LMB',ha='left',va='center',
                      bbox={'facecolor':'w', 'alpha':0.7, 'pad':3})
            if ctrKey == 'dgm':
                axis.annotate('Max. DGM\n3175kg', (8., 275000.),
                              xytext=(-20, 10), textcoords='offset points',
                              arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
                              fontsize=6, horizontalalignment='center', verticalalignment='bottom')
            elif ctrKey == 'energy':
              axis.annotate(' ', (14.7, 165000),
                            xytext=(13.5, 180000), textcoords='data',
                            arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
                            fontsize=6, horizontalalignment='center', verticalalignment='bottom')
              
              axis.text(13.0, 185000, 'Min. CSE\nat given\nrange', color='k', fontsize=6,
                        bbox=dict(facecolor='w', alpha=0.6, boxstyle='round', linewidth=0.8),
                        horizontalalignment='center', verticalalignment='bottom')
            #axis.axvline(0.5)
        elif key == 'lab':
            axis = ax[1][0]
            axis.set_xlabel('Lift-to-Drag Ratio (-)')
            axis.set_ylabel('Maximum Design Range (km)')
            #axis.text(1.2,370000,'Lithium Air Battery',ha='left',va='center',
            axis.text(2,370000,'LAB',ha='left',va='center',
                      bbox={'facecolor':'w', 'alpha':0.7, 'pad':3})
            #axis.axhline(0.2)
        elif key == 'h2g':
            axis = ax[0][1]
            setUpperTicks(axis, regressions, 'baseline')
            axis.xaxis.set_tick_params(which='both', labelbottom=False)
    
            axis.text(2,370000,'GH2',ha='left',va='center',
            #axis.text(1.2,370000,'Gaseous Hydrogen',ha='left',va='center',
                      bbox={'facecolor':'w', 'alpha':0.7, 'pad':3})
        elif key == 'h2l':
            axis = ax[1][1]
            axis.set_xlabel('Lift-to-Drag Ratio (-)')
            #axis.text(1.2,370000,'Liquid Hydrogen',ha='left',va='center',
            axis.text(2,370000,'LH2',ha='left',va='center',
                      bbox={'facecolor':'w', 'alpha':0.7, 'pad':3})
        else:
            pass

        samp = 200
    
        X, Y, Z = contourAnalysis2D(wrapRegressions, case, tupX, tupY)
        if ctrKey == 'dgm':
            # purgeArtifacts(key, X, Y, Z)
            minX, minY = getContourMin(case, tupX, tupY, samp)
            # print(minX, minY)
            axis.plot(minX, minY, color=greys(0.8), linewidth=0.8, linestyle='--') 
            if key == 'lmb':
                axis.annotate(' ', (12.0, 175000),
                              xytext=(10.9, 190000), textcoords='data',
                              arrowprops=dict(facecolor='black', width=0.1, headwidth=2,headlength=2, shrink=0.1),
                              fontsize=6, horizontalalignment='center', verticalalignment='bottom')
                
                axis.text(10.2, 197000, 'Min. DGM\nat given\nrange', color='k', fontsize=6,
                          bbox=dict(facecolor='w', alpha=0.6, boxstyle='round', linewidth=0.8),
                          horizontalalignment='center', verticalalignment='bottom')
            
        
        if ctrKey == 'energy':
            minX, minY = getContourMin(case, tupX, tupY, samp)
            axis.plot(minX, minY, color=blues(0.8), linewidth=0.8, linestyle='--')
        elif ctrKey == 'impact':
            minX, minY = getContourMin(case, tupX, tupY, samp)
            axis.plot(minX, minY, color=greens(0.8), linewidth=0.8, linestyle='--')    
        
        hatches = []
    
        if case['info']['wrapKey'] == 'dgm':
            cs = axis.contour(Y, X, Z, levels=[3175.], linewidths=0.6, linestyles='-.', colors='k')
            # plt.clabel(cs, inline=1, fontsize=6, fmt='%.0f')        
            levels = np.linspace(1100.,2500.,15)
            for l, lev in enumerate(levels):
                hatches.append(None)
                if l == 1:
                    hatches[-1] = '//'
                elif l == 5:
                    hatches[-1] = '///'
                elif l == 9:
                    hatches[-1] = '/////'
            sMap = 'Greys'
    
        elif case['info']['wrapKey'] == 'energy':
            levels = np.linspace(0.0,0.5,11)
            for l, lev in enumerate(levels):
                hatches.append(None)
                if l == 1:
                    hatches[-1] = '\\\\'
                elif l == 5:
                    hatches[-1] = '\\\\\\'
                elif l == 9:
                    hatches[-1] = '\\\\\\\\'
            sMap = 'Blues'
    
        elif case['info']['wrapKey'] == 'impact':
            levels = np.linspace(0.0,100.0,11)
            for l, lev in enumerate(levels):
                hatches.append(None)
                if l == 1:
                    hatches[-1] = '\\\\'
                elif l == 5:
                    hatches[-1] = '\\\\\\'
                elif l == 9:
                    hatches[-1] = '\\\\\\\\'
            sMap = 'Greens'
    
        mpl.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
    
        #ctrs[key] = axis.contourf(Y, X, Z, levels=10, linewidths=1.0, hatches=hatches, cmap='Greys')
        ctrs[key] = axis.contourf(Y, X, Z, levels=levels, linewidths=1.0, hatches=hatches, cmap=sMap)
        #ctrs[key] = axis.contourf(Y, X, Z, levels=levels, linewidths=1.0, cmap=sMap)
        axis.contour(Y, X, Z, levels=levels, linewidths=0.3, colors='k')
        axis.grid(axis='both', linestyle='-', linewidth=0.1, color='gray')
    
        
        del X, Y, Z
    
    
    axis.set_xticks(np.linspace(1.0,19.0,10))       
    yticks = np.arange(50000., max(tupX[2]), 50000)
    axis.set_yticks(yticks)
    axis.set_yticklabels(['50','100','150','200','250','300','350', '400'])
    
    cax = fig.add_axes([0.92, 0.125, 0.03, 0.7555])
    fig.colorbar(ctrs['lmb'], cax=cax, drawedges=True)
    
    if case['info']['wrapKey'] == 'dgm':
        outStr = 'output/dissContoursMasses_'
        axis.text(19.8,825000.,'DGM (kg)', fontsize=7, va='bottom')
    
    elif case['info']['wrapKey'] == 'energy':
        outStr = 'output/dissContoursEnergies_'
        axis.text(19.8,825000.,'CSE\n(kWh/PKT)', fontsize=7, va='bottom')
    
    elif case['info']['wrapKey'] == 'impact':
        outStr = 'output/dissContoursImpacts2030Green_'
        axis.text(19.8,825000.,'Specific\nImpact\n(gCO2/PKT)', fontsize=7, va='bottom')        
    
    boolPrint = True 
    plt.tight_layout()  
    if boolPrint:
        strTime = getTime()
        #SAVETO = 'output/dissContoursOpsImpact2030Base_' + strTime + '.pdf'
        SAVETO = outStr + strTime + '.pdf'
        fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)
    
    plt.show()
    return None



if doFourDimsPlot:
    
    fourDimensionsPlot('dgm')
    fourDimensionsPlot('energy')
    
    # ctrKey = 'dgm'
    # # ctrKey = 'energy'
    # #ctrKey = 'impact'
    
    
 
    

#%% ===========================================================================
# FOUR SUBOPLOT DEPICTION OF SENSITIVTIY ANALYSIS
# =============================================================================


import copy as cp
from matplotlib.lines import Line2D

if doFourSensitivityPlot:   
 
    # ctrKey = 'dgm'
    # ctrKey = 'energy'
    evalKey = 'impact'
    
    funcUnit = 'PKT'
 
    for key, case in cases.items():
        #
        # case['opsImpact'] = loadOpsImpact(case['fuel'], '2030base')
        case['opsImpact'] = loadOpsImpact(case['fuel'], '2030green')
     

    figure4, ax = plt.subplots(figsize=(textwidth,textwidth),
                               nrows=3, ncols=4, sharex=True, sharey=True,
                               gridspec_kw={'wspace': 0.05,  'hspace': 0.05, 
                                            'width_ratios': [1,1,1,1]})
    
    
    baseCase = {'design':        copy.deepcopy(dDesign), 
                'mission':       copy.deepcopy(dMission), 
                'operations':    copy.deepcopy(dMission), 
                'fuel':          copy.deepcopy(dHybridLMB),
                'opsImpact':     loadOpsImpact(copy.deepcopy(dHybridLMB), '2030base')
                }
    
    baseCase['info'] = {}
    # baseCase['info']['wrapKey'] = ctrKey
    baseCase['info']['funcUnit'] = funcUnit
    baseCase['info']['popt'] = popt
    baseCase['info']['intercept'] = intercept
    baseCase['info']['at'] = 'baseline'
    baseCase['info']['slope'] = slope
    
    samples = 50
    
    dicMissions = {'short': 50000.,
                   'medium': 100000.,
                   'long': 200000.}
    
    dicVariates = {'regression': ['min', 'baseline', 'max'],
                   'operations': ['low','base', 'high'],
                   '2030base': ['lib', 'lmb', 'lab', 'h2g', 'h2l'],
                   '2030green': ['lib', 'lmb', 'lab', 'h2g', 'h2l']}
                                
    
    results = {}
    dicLegend = {}

    abscissa = np.linspace(min(tupY[2]), max(tupY[2]), samples)
    for m, (mKey, mRange) in enumerate(dicMissions.items()):
        
        baseCase['mission']['cruiseRange'] = mRange
        baseCase['operations']['cruiseRange'] = mRange
        results[mKey] = {}

        for v, (vKey, listVariate) in enumerate(dicVariates.items()):
            results[mKey][vKey] = {}

            for var in listVariate:

                evalCase = cp.deepcopy(baseCase)
                
                ## -----------------------------------------------------------
                if vKey == 'regression':
                    evalCase['info']['at'] = var
                
                ## -----------------------------------------------------------
                elif vKey == 'operations':
                    if var == 'low':
                        evalCase['operations']['hoverTime'] = 0.5 * 60.
                        evalCase['operations']['fUtil'] = 1.0
                        #evalCase['operations']['cruiseRange'] = evalCase['mission']['cruiseRange']
                        evalCase['operations']['fCirc'] = 1.46
                    elif var == 'base':
                        evalCase['operations']['hoverTime'] = 1.5 * 60.
                        evalCase['operations']['fUtil'] = 0.75
                        #evalCase['operations']['cruiseRange'] = evalCase['mission']['cruiseRange']
                        evalCase['operations']['fCirc'] = 1.0

                    else:
                        evalCase['operations']['hoverTime'] = 1.5 * 60.
                        evalCase['operations']['fUtil'] = 0.5
                        evalCase['operations']['cruiseRange'] = evalCase['mission']['cruiseRange'] / 4.
                        evalCase['operations']['fCirc'] = 1.0
                    
                ## -----------------------------------------------------------
                elif vKey == '2030base':

                    evalCase['fuel'] = dFuels[var]
                    evalCase['opsImpact'] = loadOpsImpact(evalCase['fuel'], '2030base')
                ## -----------------------------------------------------------
                elif vKey == '2030green':
                    evalCase['fuel'] = dFuels[var]
                    evalCase['opsImpact'] = loadOpsImpact(evalCase['fuel'], '2030green')
            
                val = []
                for ld in abscissa:
                    
                    #ld = evalCase['design']['ltod']
                    popt = evalCase['info']['popt']
                    intercept = evalCase['info']['intercept']
                    at = evalCase['info']['at']
                
                    evalCase['design']['ltod'] = ld
                    evalCase['design']['pl'] = regrPowerloading(ld, at, popt)
                    evalCase['design']['fStruct'] = regrStructRatio(ld, at, slope, intercept)
                    evalCase['mission']['cruiseVelo'] = regrCruiseVelo(ld, at, slope, intercept)
                    evalCase['operations']['cruiseVelo'] = regrCruiseVelo(ld, at, slope, intercept)
            
                    # for k0,v0 in evalCase.items():
                    #     for k1,v1 in v0.items():
                    #         print(k0,k1,v1)
                    # sys.exit()
                    if evalKey == 'impact':
                        val.append(getTotalOperationsImpact(evalCase))
                    else:
                        pass
                    # print(mKey, vKey, var, ld)
                    results[mKey][vKey][var] = val
                    # print(evalCase)

    dicMarkers = {'regression':  {'min': 'o',
                                  'baseline': '.',
                                  'max': 'p'},
                  'operations': {'low' : 'p',
                                 'base': '.',
                                 'high': 'o'},
                  '2030base':   {'lib': 'o',
                                 'lmb': '.',
                                 'lab': 'p',
                                 'h2g': 'v',
                                 'h2l': '^'},
                  '2030green':  {'lib': 'o',
                                 'lmb': '.',
                                 'lab': 'p',
                                 'h2g': 'v',
                                 'h2l': '^'},}
    
    # 'min': 'A=$7.9e^{-2}$, B=0.122, C=0.008\n$m_{1}$=4.1, c_{1}=22',
    
    dicLabels = {'regression':  {'min': 'Lower Bound\n(min. values)',
                                 'baseline': 'Baseline Regression\n(mode values)', 
                                 'max': 'Upper Bound\n(max. values)'},
                  'operations': {'low' : '$t_{hov}=30s$, $n_{Leg}=1$,\n$f_{util}=100%$, $f_{Circ}=1.46$',
                                 'base': '$t_{hov}=90s$, $n_{Leg}=1$,\n$f_{util}=75%$, $f_{Circ}=1.0$',
                                 'high': '$t_{hov}=90s$, $n_{Leg}=4$,\n$f_{util}=50%$, $f_{Circ}=1.0$'},
                  '2030base':   {'lib': 'Lithium-Ion Battery',
                                 'lmb': 'Lithium-Metal Battery',
                                 'lab': 'Lithium-Air Battery',
                                 'h2g': 'Gaseous Hydrogen',
                                 'h2l': 'Liquid Hydrogen'},
                  '2030green':  {'lib': 'Lithium-Ion Battery',
                                 'lmb': 'Lithium-Metal Battery',
                                 'lab': 'Lithium-Air Battery',
                                 'h2g': 'Gaseous Hydrogen',
                                 'h2l': 'Liquid Hydrogen'}}
    
    dicColors = {'regression':  {'min': oranges(0.8),
                                 'baseline': 'k',
                                 'max': greys(0.7)},
                  'operations': {'low' : greys(0.7),
                                 'base': 'k',
                                 'high': oranges(0.8)},
                  '2030base':   {'lib': oranges(0.8),
                                 'lmb': 'k',
                                 'lab': greys(0.7),
                                 'h2g': blues(0.8),
                                 'h2l': blues(0.5)},
                  '2030green':  {'lib': oranges(0.8),
                                 'lmb': 'k',
                                 'lab': greys(0.7),
                                 'h2g': blues(0.8),
                                 'h2l': blues(0.5)},}

    for m, (mKey, mRange) in enumerate(dicMissions.items()):
        for v, (vKey, listVariate) in enumerate(dicVariates.items()):
            dicLegend[vKey] = []
            axis = ax[m][v]
            for var in listVariate:
                
                # print(abscissa, results[mKey][vKey][var])
                axis.plot(abscissa, results[mKey][vKey][var], 
                          color=dicColors[vKey][var],linewidth=0.7, marker=dicMarkers[vKey][var], 
                          # color='k', linewidth=0.7, marker=dicMarkers[vKey][var], 
                          markerfacecolor='w', markeredgewidth=0.6, markersize=3, markevery=4)
                
                if m == 2:
                    dicLegend[vKey].append(Line2D([], [], color=dicColors[vKey][var], linewidth=0.7, marker=dicMarkers[vKey][var], 
                                                  markerfacecolor='w', markeredgewidth=0.6, markersize=3, label=dicLabels[vKey][var]))
                # axis.plot(np.linspace(1.,10.,10), np.linspace(2.,4.,10))
                
            axis.grid(axis='both', linestyle='-', linewidth=0.1, color='gray')
            
            # Y Axis --------------------------------------------------------
            axis.set_ylim(0.,100)
            axis.set_yticks(np.linspace(10,90,5))
            axis.yaxis.set_tick_params(which='both', labelleft=False, length=2, pad=2)
            
            # X Axis --------------------------------------------------------
            axis.set_xlim(1.0,19.0)
            axis.set_xticks(np.linspace(2,18,5))
            axis.xaxis.set_tick_params(which='both', labelbottom=True, length=2, pad=2)
            
            if mKey == 'short' or mKey == 'medium':
                axis.xaxis.set_tick_params(which='both', labelbottom=False, length=0, pad=2)
                
            # elif mKey == 'medium':
            #     axis.xaxis.set_tick_params(which='both', labelbottom=False, length=0, pad=2)
                # axis.set_xlabel('Lift-to-Drag Ratio (-)')
    
            if vKey == 'regression' and mKey == 'medium':
                axis.set_ylabel('Specific Carbon Impact (gCO2/PKT)', fontsize = 7)
    
            if vKey == 'regression':
                axis.yaxis.set_tick_params(which='both', labelleft=True, length=2, pad=2)
    
    # figure4.supxlabel('Lift-to-Drag Ratio (-)', fontsize=7)
    # figure4.supylabel('Specific Carbon Impact (gCO2/PKT)', fontsize=7)

    ax[2][1].text(20.,-13.,'Lift-to-Drag Ratio (-)', fontsize=7, ha='center', va='top')
    
    ax[0][0].text(1.8,3.,'Design Range = 50km', fontsize=6, ha='left', va='bottom',
                  bbox=dict(facecolor='w', boxstyle='round', linewidth=0.6))
    ax[1][0].text(1.8,3.,'Design Range = 100km', fontsize=6, ha='left', va='bottom',
                  bbox=dict(facecolor='w', boxstyle='round', linewidth=0.6))
    ax[2][0].text(1.8,3.,'Design Range = 200km', fontsize=6, ha='left', va='bottom',
                  bbox=dict(facecolor='w', boxstyle='round', linewidth=0.6))

    ax[0][0].text(10.,160.,'Regression Variation', fontsize=7, ha='center', va='bottom')
    ax[0][1].text(10.,160.,'Operations Variation', fontsize=7, ha='center', va='bottom')
    ax[0][2].text(10.,160.,'2030 Basic Scenario', fontsize=7, ha='center', va='bottom')
    ax[0][3].text(10.,160.,'2030 Green Scenario', fontsize=7, ha='center', va='bottom')

    ax[0][0].legend(handles=dicLegend['regression'], columnspacing = 0.6, handletextpad = 0.5, handlelength=1.2,
                   loc='upper left', bbox_to_anchor= (0.0, 1.58), ncol=1, borderaxespad=0, frameon=False, fontsize=6) 
    ax[0][1].legend(handles=dicLegend['operations'], columnspacing = 0.6, handletextpad = 0.5, handlelength=1.2,
                   loc='upper left', bbox_to_anchor= (0.0, 1.58), ncol=1, borderaxespad=0, frameon=False, fontsize=6) 
    ax[0][2].legend(handles=dicLegend['2030base'], columnspacing = 0.6, handletextpad = 0.5, handlelength=1.2,
                   loc='upper left', bbox_to_anchor= (0.0, 1.58), ncol=1, borderaxespad=0, frameon=False, fontsize=6) 
    ax[0][3].legend(handles=dicLegend['2030green'], columnspacing = 0.6, handletextpad = 0.5, handlelength=1.2,
                   loc='upper left', bbox_to_anchor= (0.0, 1.58), ncol=1, borderaxespad=0, frameon=False, fontsize=6) 
    
    
    
    boolPrint = True 
    plt.tight_layout()  
    if boolPrint:
        strTime = getTime()
        outStr = 'output/dissGeneralizedSensitivity_'        
        SAVETO = outStr + strTime + '.pdf'
        figure4.savefig(SAVETO, bbox_inches='tight', dpi=250.)

    print(popt)
    print()
    print(intercept)
    print()
    print(slope)

    plt.show()

        
    
    
    
    

