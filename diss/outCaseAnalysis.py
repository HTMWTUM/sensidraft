#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 15:54:50 2020

@author: nicolas
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

from source.modStatistics import getSample
from source.modMonteCarlo import scalarFunMonteCarlo

from source.util import loadTUMcolors, loadPlotFonts, getTime

from source.modAssessmentTier0 import getTotalOperationsImpact
from source.modSizingTier0 import sizeAircraft
    
from scipy.stats import scoreatpercentile  
     
from data.inputVTOL import gdv, gmv, gtv
from data.inputImpact import loadOpsImpact

def normDict(dic):
    normDic = {}
    for k,v in dic.items():
        normDic[k] = float(v) / sum(dic.values()) * 100
    return normDic

def setAlphaComponents(key):
    alpha = {}
    if key == 'struct': 
        alpha[key] = 0.7
    elif key == 'ptrain':
        alpha[key] = 0.1
    elif key == 'useful':
        alpha[key] = 0.9
    elif key == 'other':
        alpha[key] = 0.5        
    elif key == 'fuel':
        alpha[key] = 0.3    
    else:
        print('Dunno this key')
    return alpha  

def setAlphaEnerg(key):
    alpha = {}
    if key == 'hover': 
        alpha[key] = 0.8
    elif key == 'cruise':
        alpha[key] = 0.5  
    else:
        print('Dunno this key')
    return alpha             


# BASE CASE 2030
# dOpImpactBase = {'elecProduction':        0.237,  # pure wind; ref; world 2030 
#                  'batteryProduction':     112.,
#                  'hydrProdTotal':         2412,
#                  'hydrTankAndEquip':      58.2,
#                  'fuelStackProduction':   90.785,
#                 }

# # GREEN CASE 2030
# dOpImpactGreen = {'elecProduction':        0.020,  # pure wind; ref; world 2030 
#                   'batteryProduction':     62.,
#                   'hydrProdTotal':         970,
#                   'hydrTankAndEquip':      58.2,
#                   'fuelStackProduction':   90.785,
#                   }

def dissCaseStudyLCA(sortedCases, boolPrint):

    missions = {'urban':    {'title': 'Urban Commute (25km)',       'range':  25000., 'ylim': 200.},
                'airport':  {'title': 'Airport Transport (60km)',   'range':  60000., 'ylim': 150.},
                'city':     {'title': 'Intercity Travel (150km)',   'range': 150000., 'ylim': 100.}}

    iS = 1000

    for mID, mission in missions.items():
    
        mission['fullOpsImpBase'] = {}
        mission['fullOpsImpGreen'] = {}
        
        # mission['fullOpsMedianBase'] = {}
        # mission['fullOpsLowerBase'] = {}
        # mission['fullOpsUpperBase'] = {}
        
        mission['base'] = {}
        mission['green'] = {}
        
        mission['sizingRanges'] = {}
        mission['CALC'] = {}
        
        #labels = []
        #ranges = []  
            
        for i, case in enumerate(sortedCases):

            ID = case['design']['ident']  
            fuelID = case['fuel']['highEnerSys']['ident']
            mission['sizingRanges'][ID] = case['mission']['cruiseRange']            

            mission['base'][ID] = {}
            mission['green'][ID] = {}
            
            if mission['sizingRanges'][ID] < mission['range']:
                #print('>> Go to next configuration bec.', case['design']['ident'], 'range too short')
                mission['CALC'][ID] = False
            else:
                #print('>> Start with case', case['design']['ident'])
                mission['CALC'][ID] = True

            if mission['CALC'][ID] == True:
                
                case = sizeAircraft(case)
                case['operations']['cruiseRange'] = mission['range']
                                
                # DETERMINISTIC 2030 BASE
                case = sizeAircraft(case)
                #case['opsImpact'] = dOpImpactBase
                case['opsImpact'] = loadOpsImpact(case['fuel'], '2030base')
                mission['fullOpsImpBase'][ID] = getTotalOperationsImpact(case)

                if case['fuel']['fueltype'] == 'hybridBattery':
                    sampleHES = {'esp':      getSample('triang',[gtv(fuelID,'min','esp'),gtv(fuelID,'baseline','esp'),gtv(fuelID,'max','esp')], iS),
                                  'fDoD':     getSample('triang',[gtv(fuelID,'min','fDoD'),gtv(fuelID,'baseline','fDoD'),gtv(fuelID,'max','fDoD')], iS),
                                  'eta_mtr':  getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
                                  }            
                elif case['fuel']['fueltype'] == 'hybrid':
                    sampleHES = {'pSpec':       getSample('triang',[gtv(fuelID,'min','pSpec'),gtv(fuelID,'baseline','pSpec'),gtv(fuelID,'max','pSpec')], iS),
                                  'fractionH2T': getSample('triang',[gtv(fuelID,'min','fractionH2T'),gtv(fuelID,'baseline','fractionH2T'),gtv(fuelID,'max','fractionH2T')], iS),
                                  'eta_mtr':     getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
                                  'eta_fc':      getSample('triang',[gtv(fuelID,'min','eta_fc'),gtv(fuelID,'baseline','eta_fc'),gtv(fuelID,'max','eta_fc')], iS),
                                }

                # PROBABILISTIC 2030 BASE
                sample = {#===============================================
                          'design':     {'ltod':    getSample('triang', [gdv(ID,'min','ltod'), gdv(ID,'baseline','ltod'), gdv(ID,'max','ltod')], iS),
                                          'pl'  :    getSample('triang', [gdv(ID,'min','pl'), gdv(ID,'baseline','pl'), gdv(ID,'max','pl')], iS),
                                          'fStruct': getSample('triang', [gdv(ID,'min','fStruct'), gdv(ID,'baseline','fStruct'), gdv(ID,'max','fStruct')], iS),},
                          #===============================================
                          #'mission':    {'hoverTime': getSample('triang', [gmv(ID,'min','hoverTime'), gmv(ID,'baseline','hoverTime'), gmv(ID,'max','hoverTime')], iS)},
                          #===============================================
                          'fuel':       {'highEnerSys': sampleHES},
                          #===============================================
                          #'opsImpact':    {'elecProduction': getSample('triang', [0.237*0.8,0.237*1.0,0.237*1.2], iS)},
                          #===============================================
                          'operations':  {'fUtil': getSample('triang', [0.65,0.75,0.85], iS),
                                          'fCirc': getSample('triang', [1.0,1.2,1.46], iS)},
                          }

                opsMC, mcDesigns = None, None
                opsMC, mcDesigns = scalarFunMonteCarlo(getTotalOperationsImpact, case, sample, iS)
                # mission['fullOpsMedianBase'][ID]['base'] = scoreatpercentile(opsMC, 50.0)
                # mission['fullOpsLowerBase'][ID]['base'] = mission['fullOpsMedianBase'][ID]['base'] - scoreatpercentile(opsMC, 2.5)
                # mission['fullOpsUpperBase'][ID]['base'] = scoreatpercentile(opsMC, 97.5) - mission['fullOpsMedianBase'][ID]['base']
                mission['base'][ID]['fullOpsMedian'] = scoreatpercentile(opsMC, 50.0)
                mission['base'][ID]['fullOpsLower'] = mission['base'][ID]['fullOpsMedian'] - scoreatpercentile(opsMC, 2.5)
                mission['base'][ID]['fullOpsUpper'] = scoreatpercentile(opsMC, 97.5) - mission['base'][ID]['fullOpsMedian']

                # DETERMINISTIC 2030 GREEN
                case = sizeAircraft(case)
                case['opsImpact'] = loadOpsImpact(case['fuel'], '2030green')
                mission['fullOpsImpGreen'][ID] = getTotalOperationsImpact(case)
                
                # PROBABILISTIC 2030 BASE
                opsMC, mcDesigns = None, None
                opsMC, mcDesigns = scalarFunMonteCarlo(getTotalOperationsImpact, case, sample, iS)
                # mission['fullOpsMedianBase'][ID]['green'] = scoreatpercentile(opsMC, 50.0)
                # mission['fullOpsLowerBase'][ID]['green'] = mission['fullOpsMedianBase'][ID]['green'] - scoreatpercentile(opsMC, 2.5)
                # mission['fullOpsUpperBase'][ID]['green'] = scoreatpercentile(opsMC, 97.5) - mission['fullOpsMedianBase'][ID]['green']  
                mission['green'][ID]['fullOpsMedian'] = scoreatpercentile(opsMC, 50.0)
                mission['green'][ID]['fullOpsLower'] = mission['green'][ID]['fullOpsMedian'] - scoreatpercentile(opsMC, 2.5)
                mission['green'][ID]['fullOpsUpper'] = scoreatpercentile(opsMC, 97.5) - mission['green'][ID]['fullOpsMedian']    
            else:
                pass

    #%%===========================================================================

    textwidth = 5.809    
    textheight = 8.275

    fig, ax = plt.subplots(figsize=(textwidth,0.9*textheight),
                           nrows=3, ncols=1, sharex=True, sharey=False,
                           gridspec_kw={'hspace': 0.15, 
                                        'height_ratios': [1, 1, 1]})

    grays = plt.get_cmap('Greys')
    reds = plt.get_cmap('Blues')
    greens = plt.get_cmap('Greens')
    barwidth = 0.4
    
    caseNames = ['Volocopter', 
                 'City\nAirbus',
                 'Vahana',
                 'Cora', 
                 'Joby',
                 'Lilium',
                 'Skai']
    
    
    pars = []
    import numpy as np
    plt.minorticks_on()
    for i, (mID, mission) in enumerate(missions.items()):

        ax[i].set_title(mission['title'], loc='right', fontsize=7)
        pars.append(ax[i].twinx())         
        ax[i].set_zorder(5)  # default zorder is 0 for ax1 and ax2
        ax[i].patch.set_visible(False)  # prevents ax1 from hiding ax2
        #mission['ylim']

        xPos = []
        xRng = []
        for j, case in enumerate(sortedCases):
            
            ID = case['design']['ident']   
            
            pars[-1].set_ylim(0.,400000.)
            pars[-1].set_yticks(np.linspace(0.,400000.,5))
            pars[-1].set_ylabel('Maximum Design Range (km)')
            pars[-1].set_yticklabels(['0', '100', '200', '300', '400'])
                        
            if mission['CALC'][ID] == True:
                
                pars[-1].bar(j, mission['sizingRanges'][ID], width=2*barwidth, color=grays(0.5), alpha=0.2)
                pars[-1].bar(j, mission['range'], width=2*barwidth, color=grays(0.6), alpha=0.4)
                
                #ax[i].plot(j-barwidth/2, mission['fullOpsImpBase'][ID], marker='s', markersize=7., color=greens(0.8), 
                #           linewidth=0.0, linestyle='-', markerfacecolor=greens(0.2))
                # ax[i].plot(j+barwidth/2, mission['fullOpsImpGreen'][ID], marker='o', markersize=7., color=greens(0.8), 
                #            linewidth=0.0, linestyle='-', markerfacecolor=greens(0.4))
                
                errorbar1 = ax[i].errorbar(j-barwidth/2, mission['base'][ID]['fullOpsMedian'], 
                               #yerr=([mission['fullOpsLowerBase'][ID]], [mission['fullOpsUpperBase'][ID]]),
                               yerr=([mission['base'][ID]['fullOpsLower']], [mission['base'][ID]['fullOpsUpper']]),
                               marker='s', markersize=5., color=greens(0.8), elinewidth=0.8, capsize=3., 
                               linewidth=0.0, linestyle='-', markerfacecolor='w', label='eVTOL "2030 Basic"')

                errorbar2 = ax[i].errorbar(j+barwidth/2, mission['green'][ID]['fullOpsMedian'], 
                               #yerr=([mission['fullOpsLowerBase'][ID]], [mission['fullOpsUpperBase'][ID]]),
                               yerr=([mission['green'][ID]['fullOpsLower']], [mission['green'][ID]['fullOpsUpper']]),
                               marker='o', markersize=5., color=greens(0.8), elinewidth=0.8, capsize=3., 
                               linewidth=0.0, linestyle='-', markerfacecolor=greens(0.4), label='eVTOL "2030 Green"')

                ax[i].set_ylim(0.,mission['ylim'])
                ax[i].set_yticks(np.arange(0.0,mission['ylim']+0.01,50.))
                ax[i].grid(axis='y', linestyle='-', linewidth=0.1, color='gray')  
                
                if i == 2:
                    ax[i].grid(which='minor', axis='y', linestyle='-', linewidth=0.1, color='gray', alpha=0.5)
                #     ax[i].set_yticks([np.arange(0.0,mission['ylim']+0.01,50.)])
                #     #ax[i].set_yticklabels(['0','50','100','150','200'])
                # elif i == 1:
                #     ax[i].set_yticks([np.arange(0.0,mission['ylim']+0.01,25.)])
                #     #ax[i].set_yticklabels(['0','25','50','75','100','125','150'])
                # elif i == 2:
                #     ax[i].set_yticks([np.arange(0.0,mission['ylim']+0.01,25.)])
                #     #ax[i].set_yticklabels(['0','25','50','75','100'])                    # ax[i]

                ax[1].set_ylabel('Specific Carbon Impact (kgCO2e/PKT)')
                ax[2].text(j,-7.5,str(case['name']), ha='center',va='top',fontsize=7)
                ax[i].xaxis.set_tick_params(which='both', labelbottom=False, length=0.0)

            xRng.append(mission['sizingRanges'][ID]) 
       
        ax[i].axvline(j+0.625, color=grays(0.5), linewidth=0.8) 
        pars[-1].bar(j+1.25, 401000., width=2*barwidth, color=grays(0.5), alpha=0.3)
        pars[-1].bar(j+1.25, mission['range'], width=2*barwidth, color=grays(0.6), alpha=0.3)
               
        #ax[i].axvline(32., color=reds(0.8))      
       
        ax[i].tick_params(axis='x', length=0.0)  
        # if i == 2: 
        #     ax[i].set_xticks(range(len(sortedCases)))
        #     ax[i].set_xticklabels(caseNames, fontsize=7, va='center', pad=3)     


        # ax[i].plot(len(sortedCases)+0.25-barwidth/2, 27., marker='s', markersize=7., color=reds(0.8), 
        #                    linewidth=0.0, linestyle='-', markerfacecolor=reds(0.2))
        errorbar3 = ax[i].errorbar(len(sortedCases)+0.25-barwidth/2, 21.4, yerr=([4.6], [10.3]), #yerr=([16.8], [31.7]),
                       marker='^', markersize=5., color=reds(0.8), elinewidth=0.8, capsize=3., 
                       linewidth=0.0, linestyle='-', markerfacecolor=reds(0.2), label='BEV "2030 Basic"')
     
        # ax[i].plot(len(sortedCases)+0.25+barwidth/2, 22., marker='o', markersize=7., color=reds(0.8), 
        #                    linewidth=0.0, linestyle='-', markerfacecolor=reds(0.2))
        errorbar4 = ax[i].errorbar(len(sortedCases)+0.25+barwidth/2, 15.4, yerr=([3.3], [5.5]), #yerr=([12.1], [20.1]),
                       marker='.', markersize=5., color=reds(0.8), elinewidth=0.8, capsize=3., 
                       linewidth=0.0, linestyle='-', markerfacecolor=reds(0.2), label='BEV "2030 Green"')

        ax[2].text(len(sortedCases)+0.25, -7.5, 'Electric\nCars', va='top', ha='center', fontsize=7)

        

    #from matplotlib.patches import Patch

    # legend = []
    
    # legend.append()
   # legend = [Line2D(,, color=dicColors[vKey][var], linewidth=0.7, marker='''', 
    #                                               markerfacecolor='w', markeredgewidth=0.6, markersize=3, label=dicLabels[vKey][var])),
    legend =  [
              Patch(facecolor=grays(0.5), alpha=0.2, label='Design Range'),
              Patch(facecolor=grays(0.6), alpha=0.4, label='Off-Design Range'),
              errorbar1,errorbar2,errorbar3,errorbar4
              ]

    # legendPkt = [Patch(facecolor='k', alpha=0.80, hatch='', label='Structure'),
    #              Patch(facecolor='k', alpha=0.65, hatch='', label='Others'),
    #              Patch(facecolor='k', alpha=0.2, hatch='', label='Powertrain'),
    #              ###
               
    #             ]  
    
    ax[0].legend(handles=legend, columnspacing = 0.6, handletextpad = 0.4, handlelength=1.5,
              loc='lower center', bbox_to_anchor= (0.5, 1.13), ncol=3, frameon=False) 


    #plt.tight_layout()  
    boolPrint = True
    if boolPrint:
        strTime = getTime()
        SAVETO = 'output/dissOpsCaseAssessment_' + strTime + '.pdf'
        fig.savefig(SAVETO, bbox_inches='tight', dpi=250.)
    
    #ax[0].axvline(32. color=reds(0.8))

    
            ### PROBABILISTIC SIZING + ANALYSIS
            # COMPONENTS - DESIGN GROSS MASS
            
            # if case['fuel']['fueltype'] == 'hybridBattery':
            #     sampleHES = {'esp':      getSample('triang',[gtv(fuelID,'min','esp'),gtv(fuelID,'baseline','esp'),gtv(fuelID,'max','esp')], iS),
            #                   'fDoD':     getSample('triang',[gtv(fuelID,'min','fDoD'),gtv(fuelID,'baseline','fDoD'),gtv(fuelID,'max','fDoD')], iS),
            #                   'eta_mtr':  getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
            #                   }            
            # elif case['fuel']['fueltype'] == 'hybrid':
            #     sampleHES = {'pSpec':       getSample('triang',[gtv(fuelID,'min','pSpec'),gtv(fuelID,'baseline','pSpec'),gtv(fuelID,'max','pSpec')], iS),
            #                   'fractionH2T': getSample('triang',[gtv(fuelID,'min','fractionH2T'),gtv(fuelID,'baseline','fractionH2T'),gtv(fuelID,'max','fractionH2T')], iS),
            #                   'eta_mtr':     getSample('triang',[gtv(fuelID,'min','eta_mtr'),gtv(fuelID,'baseline','eta_mtr'),gtv(fuelID,'max','eta_mtr')], iS),
            #                   'eta_fc':      getSample('triang',[gtv(fuelID,'min','eta_fc'),gtv(fuelID,'baseline','eta_fc'),gtv(fuelID,'max','eta_fc')], iS),
            #                 }
                
            # sample = {'design':     {'ltod':    getSample('triang', [gdv(ID,'min','ltod'), gdv(ID,'baseline','ltod'), gdv(ID,'max','ltod')], iS),
            #                           'pl'  :    getSample('triang', [gdv(ID,'min','pl'), gdv(ID,'baseline','pl'), gdv(ID,'max','pl')], iS),
            #                           'fStruct': getSample('triang', [gdv(ID,'min','fStruct'), gdv(ID,'baseline','fStruct'), gdv(ID,'max','fStruct')], iS),
            #                         },
            #           'mission':    {'hoverTime': getSample('triang', [gmv(ID,'min','hoverTime'), gmv(ID,'baseline','hoverTime'), gmv(ID,'max','hoverTime')], iS)},
            #           'fuel':       {'highEnerSys': sampleHES}
            #           }
    
            # massRes, mcDesigns = scalarFunMonteCarlo(getAircraftMass, case, sampleMASS, iS)
            # massMedian.append(scoreatpercentile(massRes, 50.0))
            # massLower.append(massMedian[-1] - scoreatpercentile(massRes, 2.5))
            # massUpper.append(scoreatpercentile(massRes, 97.5) - massMedian[-1])




    plt.show()
