#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 14:53:21 2020

@author: nicolas
"""

# European Comission / EUROSTAT
# ELECTRICITY PRICES FOR INDUSTRY IN EU COUNTRIES
# https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Electricity_price_statistics
#electricityEU28_2019_S2_avg_exTax = 0.0794   # € / kWh # exlcuding taxes and levies (but including energy generation, supply and network)
#electricityEU28_2019_S2_avg_exVAT = 0.0836   # € / kWh # excluding VAT and recoverable taxes and levies
#electricityEU28_2019_S2_avg_incTax = 0.1473  # € / kWh # including all taxes and levies  
  
# ELECTRICITY GRID FEE
#electricityEU28_2015_gridFee_min = 0.005    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
#electricityEU28_2015_gridFee_p10 = 0.016    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
#electricityEU28_2015_gridFee_avg = 0.022    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
#electricityEU28_2015_gridFee_p90 = 0.026    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
#electricityEU28_2015_gridFee_max = 0.037    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf

# ELECTRICITY TAXES AND LEVIES
#electricityEU28_2019_taxes_avg = electricityEU28_2019_S2_avg_incTax - electricityEU28_2019_S2_avg_exTax
#electricityEU28_2019_taxes_min = 0.0083
#electricityEU28_2019_taxes_max = 0.1707
#electricityEU28_2019_taxes_p10 = 0.0162
#electricityEU28_2019_taxes_p90 = 0.0797

#https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Electricity_price_statistics#Electricity_prices_for_non-household_consumers
#elecTotalEU27_2020_NonHousehold_avg = 0.125  # EU27
#elecTotalEU27_2020_NonHousehold_min = 0.06   # DEN
#elecTotalEU27_2020_NonHousehold_max = 0.18   # GER 

# HYDROGEN PRODUCTION COST
#hydrogen_naturalgas_min_2018 = 0.9 * meanUSDtoEUR # IEA Hydrogen production costs by production source, 2018, IEA, Paris https://www.iea.org/data-and-statistics/charts/hydrogen-production-costs-by-production-source-2018
#hydrogen_naturalgas_max_2018 = 3.2 * meanUSDtoEUR
#hydrogen_naturalgas_ccus_min_2018 = 1.5 * meanUSDtoEUR
#hydrogen_naturalgas_ccus_max_2018 = 2.9 * meanUSDtoEUR
#hydrogen_coal_min_2018 = 1.1 * meanUSDtoEUR
#hydrogen_coal_max_2018 = 2.2 * meanUSDtoEUR
#hydrogen_renewable_min_2018 = 3.0 * meanUSDtoEUR
#hydrogen_renewable_max_2018 = 7.5 * meanUSDtoEUR

# HYDROGEN WHOLESALE PRICE
#hydrogen_prod_IRENA2019_wind_best = 1.6 * meanUSDtoEUR # USD / kg * EUR / USD
#hydrogen_prod_IRENA2019_wind_low = 2.7 * meanUSDtoEUR # USD / kg * EUR / USD
#hydrogen_prod_IRENA2019_solar_low = 3.3 * meanUSDtoEUR # USD / kg * EUR /  USD
#hydrogen_prod_IRENA2019_wind_avg = 4.3 * meanUSDtoEUR # USD / kg * EUR / USD
#hydrogen_prod_IRENA2019_solar_avg = 6.8 * meanUSDtoEUR # USD / kg * EUR / USD

# HYDROGEN LOGISTICS
#hydrogen_deliv_UCDavis_2008_min = 0.1 * meanUSDtoEUR # EUR / kg
#hydrogen_deliv_UCDavis_2008_p10 = 1.0 * meanUSDtoEUR # EUR / kg
#hydrogen_deliv_UCDavis_2008_avg = 1.5 * meanUSDtoEUR # EUR / kg
#hydrogen_deliv_UCDavis_2008_p90 = 2.0 * meanUSDtoEUR # EUR / kg
#hydrogen_deliv_UCDavis_2008_max = 4.0 * meanUSDtoEUR # EUR / kg
    
# HYDROGEN PRICE AT GAS STATION
#hydrogen_dispense_GER2020 = 9.5

# FUEL CELL STACK COST
#pemfc_systemcost_2035_experts_min =  35. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
#pemfc_systemcost_2020_experts_min =  45. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
#pemfc_systemcost_2020_experts_avg =  62. * meanUSDtoEUR  # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
#pemfc_systemcost_2035_experts_max =  63. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
#pemfc_systemcost_2020_experts_max = 100. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
#costs['fuelCellStack'] = (pemfc_systemcost_2020_experts_avg,'€ per kW')
    
meanUSDtoEUR = 0.8925 # https://www.poundsterlinglive.com/best-exchange-rates/best-us-dollar-to-euro-history-2019

costData = {#----------------------------------------------------------------------------------------------------------
            # 'hybridBattery':    {'baseline': {'elecWholesale': 0.0794-0.022, 'elecGridfee': 0.022, 'elecTaxLevies': 0.1473-0.0794, 'batteryProd': 176. * meanUSDtoEUR},
            #                      'min':      {'elecWholesale': 0.0794-0.037, 'elecGridfee': 0.005, 'elecTaxLevies': 0.0083, 'batteryProd': 176. * meanUSDtoEUR},
            #                      'max':      {'elecWholesale': 0.0794-0.005, 'elecGridfee': 0.037, 'elecTaxLevies': 0.1707, 'batteryProd': 176. * meanUSDtoEUR}},
            'hybridBattery':    {'baseline': {'elecTotalCost': 0.125, 'batteryProdCost': 200. * meanUSDtoEUR},
                                 'min':      {'elecTotalCost': 0.06,  'batteryProdCost': 100. * meanUSDtoEUR},
                                 'max':      {'elecTotalCost': 0.18,  'batteryProdCost': 400. * meanUSDtoEUR}},
            #----------------------------------------------------------------------------------------------------------
            'hybrid':           {'baseline': {'hydrTotalCost': 4.3 * meanUSDtoEUR, 'fuelcellProdCost': 100. * meanUSDtoEUR, 'elecTotalCost': 0.125, 'batteryProdCost': 176. * meanUSDtoEUR},
                                 'min':      {'hydrTotalCost': 1.6 * meanUSDtoEUR, 'fuelcellProdCost': 50. * meanUSDtoEUR, 'elecTotalCost': 0.06,  'batteryProdCost': 100. * meanUSDtoEUR},
                                 'max':      {'hydrTotalCost': 9.5, 'fuelcellProdCost': 500. * meanUSDtoEUR, 'elecTotalCost': 0.18,  'batteryProdCost': 200. * meanUSDtoEUR}},
            }

 

def gocv(FUEL, TYPE, KEY):
    
    IDENT = FUEL['fueltype']
    
    if IDENT == 'hybridBattery':
        if 'elec' in KEY:
            val = costData[IDENT][TYPE][KEY] / FUEL['highEnerSys']['eta_chg'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr'] 
        elif 'batt' in KEY:
            val = costData[IDENT][TYPE][KEY] / FUEL['highEnerSys']['fDoD'] / FUEL['highEnerSys']['nCycles'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr']  
        else:
            print('The KEY (should be elec or batt), passed to gocv (get operational cost value) is not not known')
    
    elif IDENT == 'hybrid':
        if 'elec' in KEY:
            val = costData[IDENT][TYPE][KEY] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] 
        elif 'batt' in KEY:
            val = costData[IDENT][TYPE][KEY] / FUEL['highPowSys']['fDoD'] / FUEL['highPowSys']['nCycles'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr']  
        elif 'hydr' in KEY:
            val = costData[IDENT][TYPE][KEY] / 33.33 / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr']
        elif 'fuelcell' in KEY:
            val = costData[IDENT][TYPE][KEY] / FUEL['highEnerSys']['opHours'] / FUEL['highEnerSys']['eta_mtr']
        else:
            print('The KEY (should be elec or batt), passed to gocv (get operational cost value) is not known')
            val = None
    
    return val

def loadOpsCost(FUEL, TYPE):
    
    IDENT = FUEL['fueltype']
    
    dCost = {}
    if IDENT == 'hybridBattery':
        dCost['elecTotalCost'] =      costData[IDENT][TYPE]['elecTotalCost'] / FUEL['highEnerSys']['eta_chg'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr']
        dCost['batteryProdCost'] =    costData[IDENT][TYPE]['batteryProdCost'] / FUEL['highEnerSys']['fDoD'] / FUEL['highEnerSys']['nCycles'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr']
    elif IDENT == 'hybrid':
        dCost['elecTotalCost'] =      costData[IDENT][TYPE]['elecTotalCost'] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] 
        dCost['batteryProdCost'] =    costData[IDENT][TYPE]['batteryProdCost'] / FUEL['highPowSys']['fDoD'] / FUEL['highPowSys']['nCycles'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr']  
        dCost['hydrTotalCost'] =      costData[IDENT][TYPE]['hydrTotalCost'] / 33.33 / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr']
        dCost['fuelcellProdCost'] =   costData[IDENT][TYPE]['fuelcellProdCost'] / FUEL['highEnerSys']['opHours'] / FUEL['highEnerSys']['eta_mtr']    
    else:
        print('The IDENT (should be hybridBattery or hybrid) passed to loadOpsCost is not known')
    
    return dCost

#%%===========================================================================             
