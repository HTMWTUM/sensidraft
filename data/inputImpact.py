#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 14:53:21 2020

@author: nicolas
"""

#%%===========================================================================

# TODO 2 : research values
materialImpactData = {#-----------------------------------------------------------
                      'worstCase': {'aluminium':  27.69, # kg CO2e per kg Material
                                    'steel':       2.96, # kg CO2e per kg Material
                                    'titan':      49.00, # kg CO2e per kg Material
                                    'copp':        2.32, # kg CO2e per kg Material
                                    'gfrp':        8.26, # kg CO2e per kg Material
                                    'cfrp':       35.15, # kg CO2e per kg Material
                                    'plastic':     8.30, # kg CO2e per kg Material
                                    'battery':   212.00, # kg CO2e per kWh Capacity !!!!!!,
                                    },
                      #-----------------------------------------------------------
                      'baseline': {'aluminium':   13.70, # kg CO2e per kg Material
                                    'steel':       2.96, # kg CO2e per kg Material
                                    'titan':      40.51, # kg CO2e per kg Material
                                    'copp':        2.32, # kg CO2e per kg Material
                                    'gfrp':        8.26, # kg CO2e per kg Material
                                    'cfrp':       32.73, # kg CO2e per kg Material
                                    'plastic':     7.21, # kg CO2e per kg Material
                                    'battery':   112.00, # kg CO2e per kWh Capacity !!!!!!,
                                   },
                      #-----------------------------------------------------------
                      'cleanFuture': {'aluminium':   8.27, # kg CO2e per kg Material
                                      'steel':       1.92, # kg CO2e per kg Material
                                      'titan':      31.28, # kg CO2e per kg Material
                                      'copp':        1.82, # kg CO2e per kg Material
                                      'gfrp':        7.47, # kg CO2e per kg Material
                                      'cfrp':       28.05, # kg CO2e per kg Material
                                      'plastic':     2.60, # kg CO2e per kg Material
                                      'battery':    62.00, # kg CO2e per kWh Capacity !!!!!!,
                                      },
                      }

def gmiv(SCENARIO, MAT):
    return materialImpactData[SCENARIO][MAT]

def gmid(SCENARIO):
    return materialImpactData[SCENARIO]

#%%===========================================================================             

componentImpactData = {#-----------------------------------------------------------
                      'struct':  {'aluminium': 0.10, 'steel': 0.05, 'titan': 0.05, 'copp': 0.00, 'cfrp': 0.60, 'gfrp': 0.10, 'plastic': 0.10, 'battery': 0.00},
                      'other':   {'aluminium': 0.20, 'steel': 0.00, 'titan': 0.00, 'copp': 0.00, 'cfrp': 0.30, 'gfrp': 0.10, 'plastic': 0.40, 'battery': 0.00},
                      'battery': {'aluminium': 0.00, 'steel': 0.00, 'titan': 0.00, 'copp': 0.00, 'cfrp': 0.00, 'gfrp': 0.00, 'plastic': 0.00, 'battery': 1.00},
                      'tank':    {'aluminium': 0.00, 'steel': 0.00, 'titan': 0.00, 'copp': 0.00, 'cfrp': 0.80, 'gfrp': 0.00, 'plastic': 0.20, 'battery': 0.00},
                      'stack':   {'aluminium': 0.10, 'steel': 0.75, 'titan': 0.00, 'copp': 0.10, 'cfrp': 0.00, 'gfrp': 0.00, 'plastic': 0.05, 'battery': 0.00},
                      'ptrain':  {'aluminium': 0.00, 'steel': 0.50, 'titan': 0.00, 'copp': 0.50, 'cfrp': 0.00, 'gfrp': 0.00, 'plastic': 0.00, 'battery': 0.00},
                       }



def gciv(COMP, MAT):
    return componentImpactData[COMP][MAT]

def gcic(COMP):
    return componentImpactData[COMP]

def gcid():
    return componentImpactData

#%%===========================================================================             


# EMISSIONS FROM RENEWABLE ELECTRICITY PRODUCTION
#electricity_production_wind_Bonou_4MW_onshore = 0.006                  # kg CO2 / kWhe # Bonou et al
#electricity_production_wind_Bonou_4MW_offshore = 0.011                 # kg CO2 / kWhe # Bonou et al
#electricity_production_wind_Guezuraga_2MW_bestRecycling = 0.01         # kg CO2 / kWhe # Guezuraga et al.
#electricity_production_wind_Guezuraga_2MW_worstRecycling = 0.017       # kg CO2 / kWhe # Guezuraga et al.
#electricity_production_wind_Guezuraga_2MW_worstRecycAndOps = 0.030      # kg CO2 / kWhe # Guezuraga et al.
#electricity_production_wind_Ardente_2MW_worstRecycling = 0.015         # kg CO2 / kWhe # Ardente et al.
#electricity_production_wind_Ardente_average = 0.015         # kg CO2 / kWhe # Ardente et al.
#electricity_production_wind_Ardente_best = 0.09             # kg CO2 / kWhe # Ardente et al.
#electricity_production_wind_Ardente_worst = 0.019           # kg CO2 / kWhe # Ardente et al.

#electricity_production_solarRooftop_XYZ_worst = 0.041                 # Schlömer et al Technology-specific cost and performance parameters.

#https://www.iea.org/data-and-statistics/charts/carbon-intensity-of-electricity-generation-in-selected-regions-in-the-sustainable-development-scenario-2000-2040
# ref: IEA, Carbon intensity of electricity generation in selected regions in the Sustainable Development Scenario, 2000-2040, IEA, Paris https://www.iea.org/data-and-statistics/charts/carbon-intensity-of-electricity-generation-in-selected-regions-in-the-sustainable-development-scenario-2000-2040
#electricity_production_scenarioIEA_2030_EU = 0.072
#electricity_production_scenarioIEA_2030_US = 0.163
#electricity_production_scenarioIEA_2030_WORLD = 0.237
#electricity_production_scenarioIEA_2030_CN = 0.340

# EMISSIONS FROM LIB BATTERY PACK PRODUCTION
#battery_LIB_NickelManganeseCobalt_GraphiteAnode_LeVarlet = 201.               # kg CO2e / kWh usable storage
#battery_LIB_IronPhosphate_GraphiteAnode_LeVarlet = 217.                       # kg CO2e / kWh usable storage
#battery_LIB_ManganeseOxide_GraphiteAnode_LeVarlet = 220.                      # kg CO2e / kWh usable storage
#battery_LIB_NickelCobaltAluminiumOxide_GraphiteAnode_LeVarlet = 225.          # kg CO2e / kWh usable storage
#battery_LIB_NickelCobaltOxide_LiTitaniteOxideAnode_LeVarlet = 407.            # kg CO2e / kWh usable storage

#battery_LIB_Emilsson2019_low = 61.       # kg CO2e / kWh usable storage
#battery_LIB_Emilsson2019_avg = 84.       # kg CO2e / kWh usable storage
# battery_LIB_Emilsson2019_high = 146.     # kg CO2e / kWh usable storage

# https://www.carbonbrief.org/factcheck-how-electric-vehicles-help-to-tackle-climate-change

# Regett et al
# https://www.ffe.de/attachments/article/856/Carbon_footprint_EV_FfE.pdf
#battery_LIB_Regett_low = 62.
#battery_LIB_Regett_avg = 112.
#battery_LIB_Regett_low = 212.
    
# HYDROGEN PROCUCTION EMISSIONS FROM VARIOUS RENEWBALE SOURCES
#hydrogen_prod_Spath_WindWindTurbines = 757.89     # g CO2 / kg H2
#hydrogen_prod_Spath_WindElectrolysis = 42.05      # g CO2 / kg H2
#hydrogen_prod_Spath_WindStorage = 169.23          # g CO2 / kg H2
#hydrogen_prod_Spath_WindTotal = 969.17
#hydrogen_prod_Cetinkaya_SolarTotal = 2412         # g CO2 / kg H2
#hydrogen_prod_Burkhardt_Total = 1919.0            # g CO2 / kg H2
#hydrogen_prod_Cetinkaya_NatGasReforming = 11893.0

#emissions['hydrProdTotal'] = (hydrogen_prod_Spath_WindTotal,'gCO2e per kgH2')
#emissions['hydrProdTotal'] = hydrogen_prod_Spath_WindTotal

# - to be checked again with literature
# HYDROGEN FUEL CELL EMISSIONS
#fuelStack_production_SimonsBauer2012_exTank = 25.0           # kg CO2e / kWp  # Simons, A.; Bauer, C. A life-cycle perspective on automotive fuel cells. Applied Energy 2015
#fuelStack_production_SimonsBauer2020_exTank = 17.0           # kg CO2e / kWp 
#fuelStack_production_Notter_incMaintEoL = 28.0               # kg CO2e / kWp 

#fuelStack_production_Tagliaferri_best = 25.0                 # kg CO2e / kWp 
#fuelTank_production_Tagliaferri_best = 15.8                  # kg CO2e / kWp 
#fuelMgmtSys_production_Tagliaferri_best = 7.6                # kg CO2e / kWp 

#fuelStack_production_Tagliaferri_baseline = 43.0             # kg CO2e / kWp 
#fuelTank_production_Tagliaferri_baseline = 50.6               # kg CO2e / kWp  # not printed in the paper. I took measurements of bar lengths
#fuelMgmtSys_production_Tagliaferri_baseline = 7.6             # kg CO2e / kWp  # not printed in the paper. I took measurements of bar lengths

#fuelStack_StackProduction_2kW_Life_EcoInvent_GWP100 = 181.57 / 2.    # kg / kWp
#fuelStack_StackProduction_2kW_Life_EcoInvent_GWP20 = 195.05 / 2.    # kg / kWp

energyImpactData = {#----------------------------------------------------------------------------------------------------------
                    'hybridBattery':    {'baseline':  {'batteryProduction': 212., 'elecProduction': 0.237},
                                         '2030base':  {'batteryProduction': 112., 'elecProduction': 0.072},
                                         '2030green': {'batteryProduction':  62., 'elecProduction': 0.020},
                                         },
                    #----------------------------------------------------------------------------------------------------------
                    'hybrid':           {'baseline':        {'hydrTankAndEquip': 58.2, 'fuelStackProduction': 43.0, 'hydrProdTotal': 10.893, 'liqCompLoss': 4.0, 'batteryProduction': 212., 'elecProduction': 0.237}, # 5.040 from 5% wind, 5% solar, 90% gas
                                         '2030base':        {'hydrTankAndEquip': 23.4, 'fuelStackProduction': 25.0, 'hydrProdTotal':  5.040, 'liqCompLoss': 2.6, 'batteryProduction': 112., 'elecProduction': 0.072}, # 5.040 from 33% wind, 33% solar, 33% gas
                                         '2030green':       {'hydrTankAndEquip': 23.4, 'fuelStackProduction': 17.0, 'hydrProdTotal':  0.970, 'liqCompLoss': 2.6, 'batteryProduction': 62., 'elecProduction': 0.020}},
                    }
    

def loadOpsImpact(FUEL, TYPE):
    
    IDENT = FUEL['fueltype']
    
    dImpact = {}
    if IDENT == 'hybridBattery':
        dImpact['batteryProdImpactHPS'] =   energyImpactData[IDENT][TYPE]['batteryProduction'] / FUEL['highPowSys']['fDoD'] / FUEL['highPowSys']['nCycles'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000.
        dImpact['batteryProdImpactHES'] =   energyImpactData[IDENT][TYPE]['batteryProduction'] / FUEL['highEnerSys']['fDoD'] / FUEL['highEnerSys']['nCycles'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        dImpact['elecImpactHPS'] =          energyImpactData[IDENT][TYPE]['elecProduction'] / FUEL['highPowSys']['eta_grd'] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000.
        dImpact['elecImpactHES'] =          energyImpactData[IDENT][TYPE]['elecProduction'] / FUEL['highEnerSys']['eta_grd'] / FUEL['highEnerSys']['eta_chg'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
    elif IDENT == 'hybrid':
        #dCost['elecTotalCost'] =      costData[IDENT][TYPE]['elecTotalCost'] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] 
        dImpact['hydrTankEquipImpact'] =     energyImpactData[IDENT][TYPE]['hydrTankAndEquip'] / FUEL['highEnerSys']['opHours'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        dImpact['fuelcellProdImpact'] =      energyImpactData[IDENT][TYPE]['fuelStackProduction'] / FUEL['highEnerSys']['opHours'] / FUEL['highEnerSys']['eta_mtr'] * 1000. 
        # dImpact['hydrProdImpact'] =          energyImpactData[IDENT][TYPE]['hydrProdTotal'] / FUEL['highEnerSys']['lhv'] / (1 - FUEL['highEnerSys']['compLoss']/FUEL['highEnerSys']['lhv']) / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        if FUEL['highEnerSys']['ident'] == 'h2liq':
            dImpact['hydrProdImpact'] =          energyImpactData[IDENT][TYPE]['hydrProdTotal'] * ( 1 + 0.12*(energyImpactData[IDENT][TYPE]['liqCompLoss']-1) ) / FUEL['highEnerSys']['lhv'] / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        elif FUEL['highEnerSys']['ident'] == 'h2gas':
            dImpact['hydrProdImpact'] =          energyImpactData[IDENT][TYPE]['hydrProdTotal'] / FUEL['highEnerSys']['lhv'] / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        dImpact['batteryProdImpactHPS'] =    energyImpactData[IDENT][TYPE]['batteryProduction'] / FUEL['highPowSys']['fDoD'] / FUEL['highPowSys']['nCycles'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000. 
        dImpact['elecImpactHPS'] =           energyImpactData[IDENT][TYPE]['elecProduction'] / FUEL['highPowSys']['eta_grd'] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000. 
    else:
        print('The IDENT (should be hybridBattery or hybrid) passed to loadOpsCost is not known')
    
    return dImpact

# / (1 - FUEL['highEnerSys']['compLoss']/FUEL['highEnerSys']['lhv'])

def goiv(FUEL, TYPE, KEY):
    
    IDENT = FUEL['fueltype']
    
    if IDENT == 'hybridBattery':
        if KEY == 'batteryProdImpactHPS' :
            val = energyImpactData[IDENT][TYPE]['batteryProduction'] / FUEL['highPowSys']['fDoD'] / FUEL['highPowSys']['nCycles'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000.
        elif KEY == 'batteryProdImpactHES' :
            val = energyImpactData[IDENT][TYPE]['batteryProduction'] / FUEL['highEnerSys']['fDoD'] / FUEL['highEnerSys']['nCycles'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        elif KEY == 'elecImpactHPS' :
            val = energyImpactData[IDENT][TYPE]['elecProduction'] / FUEL['highPowSys']['eta_grd'] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000.
        elif KEY == 'elecImpactHES' :
            val = energyImpactData[IDENT][TYPE]['elecProduction'] / FUEL['highEnerSys']['eta_grd'] / FUEL['highEnerSys']['eta_chg'] / FUEL['highEnerSys']['eta_dis'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        else:
            print('The KEY (should be elec or batt), passed to gocv (get operational cost value) is not not known')
    
    elif IDENT == 'hybrid':
        if KEY == 'hydrTankEquipImpact' :
            val = energyImpactData[IDENT][TYPE]['hydrTankAndEquip'] / FUEL['highEnerSys']['opHours'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        elif KEY == 'fuelcellProdImpact' :
            val = energyImpactData[IDENT][TYPE]['fuelStackProduction'] / FUEL['highEnerSys']['opHours'] / FUEL['highEnerSys']['eta_mtr'] * 1000. 
        elif KEY == 'hydrProdImpact' :
            # val = energyImpactData[IDENT][TYPE]['hydrProdTotal'] / FUEL['highEnerSys']['lhv'] / (1 - FUEL['highEnerSys']['compLoss']/FUEL['highEnerSys']['lhv']) / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
            if FUEL['highEnerSys']['ident'] == 'h2liq':
                val =  energyImpactData[IDENT][TYPE]['hydrProdTotal'] * ( 1 + 0.12*(energyImpactData[IDENT][TYPE]['liqCompLoss']-1) ) / FUEL['highEnerSys']['lhv'] / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
            elif FUEL['highEnerSys']['ident'] == 'h2gas':
                val = energyImpactData[IDENT][TYPE]['hydrProdTotal'] / FUEL['highEnerSys']['lhv'] / FUEL['highEnerSys']['eta_fc'] / FUEL['highEnerSys']['eta_mtr'] * 1000.
        elif KEY == 'batteryProdImpactHPS' :
            val = energyImpactData[IDENT][TYPE]['batteryProduction'] / FUEL['highPowSys']['fDoD'] / FUEL['highPowSys']['nCycles'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000. 
        elif KEY == 'elecImpactHPS' :
            val = energyImpactData[IDENT][TYPE]['elecProduction'] / FUEL['highPowSys']['eta_grd'] / FUEL['highPowSys']['eta_chg'] / FUEL['highPowSys']['eta_dis'] / FUEL['highPowSys']['eta_mtr'] * 1000. 
        else:
            print('The KEY (should be elec or batt), passed to gocv (get operational cost value) is not known')
    
    return val




