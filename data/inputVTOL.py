#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 14:53:21 2020

@author: nicolas
"""

#%%===========================================================================

# designData = {#-------------------------------------------------------------------------------------------------------------------
#               'volo':   {'baseline': {'ident': 'volo', 'ltod': 3.0, 'pl': 0.100, 'fStruct': 0.26, 'fOth': 0.6},
#                          'min':      {'ident': 'volo', 'ltod': 2.5, 'pl': 0.068, 'fStruct': 0.24, 'fOth': 0.6},
#                          'max':      {'ident': 'volo', 'ltod': 3.5, 'pl': 0.115, 'fStruct': 0.28, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               'city':   {'baseline': {'ident': 'city', 'ltod': 3.0, 'pl': 0.056, 'fStruct': 0.26, 'fOth': 0.6},
#                          'min':      {'ident': 'city', 'ltod': 2.5, 'pl': 0.052, 'fStruct': 0.24, 'fOth': 0.6},
#                          'max':      {'ident': 'city', 'ltod': 3.5, 'pl': 0.060, 'fStruct': 0.28, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               'vahana': {'baseline': {'ident': 'vahana', 'ltod': 6.5, 'pl': 0.056, 'fStruct': 0.27, 'fOth': 0.6},
#                          'min':      {'ident': 'vahana', 'ltod': 5.5, 'pl': 0.049, 'fStruct': 0.25, 'fOth': 0.6},
#                          'max':      {'ident': 'vahana', 'ltod': 7.5, 'pl': 0.063, 'fStruct': 0.29, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               'cora':   {'baseline': {'ident': 'cora', 'ltod': 5.5, 'pl': 0.040, 'fStruct': 0.27, 'fOth': 0.6},
#                          'min':      {'ident': 'cora', 'ltod': 4.5, 'pl': 0.032, 'fStruct': 0.25, 'fOth': 0.6},
#                          'max':      {'ident': 'cora', 'ltod': 6.5, 'pl': 0.049, 'fStruct': 0.29, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               #'hyundai': {'baseline': {'ident': 'hyundai', 'ltod': 7.0, 'pl': 0.05, 'fStruct': 0.22, 'fOth': 0.6},
#               #            'min':      {'ident': 'hyundai',      'ltod': 6.5, 'pl': 0.040, 'fStruct': 0.21, 'fOth': 0.6},
#               #           'max':      {'ident': 'hyundai',      'ltod': 8.0, 'pl': 0.055, 'fStruct': 0.26, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               'lilium': {'baseline': {'ident': 'lilium', 'ltod': 17.0, 'pl': 0.019, 'fStruct': 0.27, 'fOth': 0.6},
#                          'min':      {'ident': 'lilium', 'ltod': 16.0, 'pl': 0.017, 'fStruct': 0.24, 'fOth': 0.6},
#                          'max':      {'ident': 'lilium', 'ltod': 18.0, 'pl': 0.020, 'fStruct': 0.30, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               'joby':   {'baseline': {'ident': 'joby',   'ltod': 13.0, 'pl': 0.036, 'fStruct': 0.27, 'fOth': 0.6},
#                          'min':      {'ident': 'joby',   'ltod': 12.5, 'pl': 0.032, 'fStruct': 0.24, 'fOth': 0.6},
#                          'max':      {'ident': 'joby',   'ltod': 14.5, 'pl': 0.041, 'fStruct': 0.30, 'fOth': 0.6}},
#               #-------------------------------------------------------------------------------------------------------------------
#               'skai':   {'baseline': {'ident': 'skai', 'ltod': 3.0, 'pl': 0.043, 'fStruct': 0.26, 'fOth': 0.6},
#                          'min':      {'ident': 'skai', 'ltod': 2.5, 'pl': 0.033, 'fStruct': 0.24, 'fOth': 0.6},
#                          'max':      {'ident': 'skai', 'ltod': 3.5, 'pl': 0.054, 'fStruct': 0.28, 'fOth': 0.6}},
#               # #- JOBY LIKE ----------------------------------------------------------------------------------------------------------
#                'hylevio':   {'baseline': {'ident': 'hylevio', 'ltod': 13.0, 'pl': 0.043, 'fStruct': 0.28, 'fOth': 0.6},
#                              'min':      {'ident': 'hylevio', 'ltod': 12.5, 'pl': 0.038, 'fStruct': 0.24, 'fOth': 0.6},
#                              'max':      {'ident': 'hylevio', 'ltod': 14.5, 'pl': 0.048, 'fStruct': 0.30, 'fOth': 0.6}},
#               #- VAHANA LIKE ------------------------------------------------------------------------------------------------------------------
#               # 'hylevio': {'baseline': {'ident': 'hylevio', 'ltod': 6.5, 'pl': 0.043, 'fStruct': 0.26, 'fOth': 0.6},
#               #             'min':      {'ident': 'hylevio', 'ltod': 5.5, 'pl': 0.038, 'fStruct': 0.24, 'fOth': 0.6},
#               #             'max':      {'ident': 'hylevio', 'ltod': 7.5, 'pl': 0.048, 'fStruct': 0.28, 'fOth': 0.6}},
#               }

designData = {#-------------------------------------------------------------------------------------------------------------------
              'volo':   {'baseline': {'ident': 'volo', 'ltod': 2.9, 'pl': 0.108, 'fStruct': 0.25, 'fOth': 0.6},
                         'min':      {'ident': 'volo', 'ltod': 1.8, 'pl': 0.094, 'fStruct': 0.24, 'fOth': 0.6},
                         'max':      {'ident': 'volo', 'ltod': 5.3, 'pl': 0.122, 'fStruct': 0.27, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
              'city':   {'baseline': {'ident': 'city', 'ltod': 3.0, 'pl': 0.056, 'fStruct': 0.25, 'fOth': 0.6},
                         'min':      {'ident': 'city', 'ltod': 1.8, 'pl': 0.049, 'fStruct': 0.24, 'fOth': 0.6},
                         'max':      {'ident': 'city', 'ltod': 5.3, 'pl': 0.064, 'fStruct': 0.27, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
              'vahana': {'baseline': {'ident': 'vahana', 'ltod': 7.2, 'pl': 0.052, 'fStruct': 0.26, 'fOth': 0.6},
                         'min':      {'ident': 'vahana', 'ltod': 7.0, 'pl': 0.042, 'fStruct': 0.25, 'fOth': 0.6},
                         'max':      {'ident': 'vahana', 'ltod': 7.4, 'pl': 0.063, 'fStruct': 0.30, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
              'cora':   {'baseline': {'ident': 'cora', 'ltod': 9.65, 'pl': 0.042, 'fStruct': 0.26, 'fOth': 0.6},
                         'min':      {'ident': 'cora', 'ltod': 8.5, 'pl': 0.032, 'fStruct': 0.25, 'fOth': 0.6},
                         'max':      {'ident': 'cora', 'ltod': 10.8, 'pl': 0.051, 'fStruct': 0.30, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
              #'hyundai': {'baseline': {'ident': 'hyundai', 'ltod': 7.0, 'pl': 0.05, 'fStruct': 0.22, 'fOth': 0.6},
              #            'min':      {'ident': 'hyundai',      'ltod': 6.5, 'pl': 0.040, 'fStruct': 0.21, 'fOth': 0.6},
              #           'max':      {'ident': 'hyundai',      'ltod': 8.0, 'pl': 0.055, 'fStruct': 0.26, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
               'lilium': {'baseline': {'ident': 'lilium', 'ltod': 17.3, 'pl': 0.016, 'fStruct': 0.26, 'fOth': 0.6},
                          'min':      {'ident': 'lilium', 'ltod': 16.3, 'pl': 0.013, 'fStruct': 0.25, 'fOth': 0.6},
                          'max':      {'ident': 'lilium', 'ltod': 18.3, 'pl': 0.021, 'fStruct': 0.30, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
               # 'lilium': {'baseline': {'ident': 'lilium', 'ltod': 17.3, 'pl': 0.018, 'fStruct': 0.26, 'fOth': 0.6},
               #            'min':      {'ident': 'lilium', 'ltod': 16.3, 'pl': 0.015, 'fStruct': 0.25, 'fOth': 0.6},
               #            'max':      {'ident': 'lilium', 'ltod': 18.3, 'pl': 0.023, 'fStruct': 0.30, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
              'joby':   {'baseline': {'ident': 'joby',   'ltod': 14.0, 'pl': 0.037, 'fStruct': 0.26, 'fOth': 0.6},
                         'min':      {'ident': 'joby',   'ltod': 13.0, 'pl': 0.030, 'fStruct': 0.25, 'fOth': 0.6},
                         'max':      {'ident': 'joby',   'ltod': 17.0, 'pl': 0.044, 'fStruct': 0.30, 'fOth': 0.6}},
              #-------------------------------------------------------------------------------------------------------------------
              'skai':   {'baseline': {'ident': 'skai', 'ltod': 2.9, 'pl': 0.046, 'fStruct': 0.25, 'fOth': 0.6},
                         'min':      {'ident': 'skai', 'ltod': 1.8, 'pl': 0.033, 'fStruct': 0.24, 'fOth': 0.6},
                         'max':      {'ident': 'skai', 'ltod': 5.3, 'pl': 0.062, 'fStruct': 0.27, 'fOth': 0.6}},
              # #- JOBY LIKE ----------------------------------------------------------------------------------------------------------
               'hylevio':   {'baseline': {'ident': 'hylevio', 'ltod': 13.0, 'pl': 0.043, 'fStruct': 0.28, 'fOth': 0.6},
                             'min':      {'ident': 'hylevio', 'ltod': 12.5, 'pl': 0.038, 'fStruct': 0.24, 'fOth': 0.6},
                             'max':      {'ident': 'hylevio', 'ltod': 14.5, 'pl': 0.048, 'fStruct': 0.30, 'fOth': 0.6}},
              #- VAHANA LIKE ------------------------------------------------------------------------------------------------------------------
              # 'hylevio': {'baseline': {'ident': 'hylevio', 'ltod': 6.5, 'pl': 0.043, 'fStruct': 0.26, 'fOth': 0.6},
              #             'min':      {'ident': 'hylevio', 'ltod': 5.5, 'pl': 0.038, 'fStruct': 0.24, 'fOth': 0.6},
              #             'max':      {'ident': 'hylevio', 'ltod': 7.5, 'pl': 0.048, 'fStruct': 0.28, 'fOth': 0.6}},
              }



#%%===========================================================================

techData = {#------------------------------------------------------------------------------------------------------------------
            'lmb': {'baseline': {'fueltype': 'battery', 'ident': 'lmb', 'esp': 325/1.2, 'eta_mtr': 0.95, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.85, 'nCycles': 800.},
                    'min':      {'fueltype': 'battery', 'ident': 'lmb', 'esp': 300/1.2, 'eta_mtr': 0.93, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.80, 'nCycles': 600.},
                    'max':      {'fueltype': 'battery', 'ident': 'lmb', 'esp': 350/1.2, 'eta_mtr': 0.97, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.90, 'nCycles': 1000.}},
            #------------------------------------------------------------------------------------------------------------------
            'lsb': {'baseline': {'fueltype': 'battery', 'ident': 'lmb', 'esp': 450/1.2, 'eta_mtr': 0.95, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.85, 'nCycles': 800.},
                    'min':      {'fueltype': 'battery', 'ident': 'lmb', 'esp': 400/1.2, 'eta_mtr': 0.93, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.80, 'nCycles': 600.},
                    'max':      {'fueltype': 'battery', 'ident': 'lmb', 'esp': 500/1.2, 'eta_mtr': 0.97, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.90, 'nCycles': 1000.}},
            #------------------------------------------------------------------------------------------------------------------
            'lab': {'baseline': {'fueltype': 'battery', 'ident': 'lmb', 'esp': 700/1.2, 'eta_mtr': 0.95, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.85, 'nCycles': 800.},
                    'min':      {'fueltype': 'battery', 'ident': 'lmb', 'esp': 600/1.2, 'eta_mtr': 0.93, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.80, 'nCycles': 600.},
                    'max':      {'fueltype': 'battery', 'ident': 'lmb', 'esp': 800/1.2, 'eta_mtr': 0.97, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.90, 'nCycles': 1000.}},
            #------------------------------------------------------------------------------------------------------------------
            'lib': {'baseline': {'fueltype': 'battery', 'ident': 'lib', 'esp': 270/1.2, 'eta_mtr': 0.95, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.85, 'nCycles': 800.},
                    'min':      {'fueltype': 'battery', 'ident': 'lib', 'esp': 260/1.2, 'eta_mtr': 0.93, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.80, 'nCycles': 600.},
                    'max':      {'fueltype': 'battery', 'ident': 'lib', 'esp': 280/1.2, 'eta_mtr': 0.97, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 2.5,'fDoD': 0.90, 'nCycles': 1000.}},
            #------------------------------------------------------------------------------------------------------------------
            'lpo': {'baseline':  {'fueltype': 'battery', 'ident': 'lpo', 'esp': 160/1.2, 'eta_mtr': 0.95, 'eta_dis': 0.95, 'eta_chg': 0.98, 'eta_grd': 0.97, 'dcp': 1.03, 'cRate': 25,'fDoD': 0.80, 'nCycles': 800.},
                    'min':       None,
                    'max':       None},
            #------------------------------------------------------------------------------------------------------------------,
            'h2liq': {'baseline': {'fueltype': 'fuelcell', 'ident': 'h2liq', 'lhv': 33.322, 'fractionH2T': 0.30, 'eta_mtr': 0.95, 'eta_fc': 0.62, 'pSpec': 1000., 'opHours': 7500., 'addLossCompr': 8.0},
                      'min':      {'fueltype': 'fuelcell', 'ident': 'h2liq', 'lhv': 33.322, 'fractionH2T': 0.27, 'eta_mtr': 0.93, 'eta_fc': 0.59, 'pSpec': 750.,  'opHours': 5000., 'addLossCompr': 2.5},
                      'max':      {'fueltype': 'fuelcell', 'ident': 'h2liq', 'lhv': 33.322, 'fractionH2T': 0.33, 'eta_mtr': 0.97, 'eta_fc': 0.65, 'pSpec': 2000., 'opHours': 10000., 'addLossCompr': 13.0}},
            #------------------------------------------------------------------------------------------------------------------,
            'h2gas': {'baseline': {'fueltype': 'fuelcell', 'ident': 'h2gas', 'lhv': 33.322, 'fractionH2T': 0.08, 'eta_mtr': 0.95, 'eta_fc': 0.62, 'pSpec': 1000.,  'opHours': 7500., 'addLossCompr': 1.0},
                      'min':      {'fueltype': 'fuelcell', 'ident': 'h2gas', 'lhv': 33.322, 'fractionH2T': 0.08, 'eta_mtr': 0.93, 'eta_fc': 0.59, 'pSpec': 750.,  'opHours': 5000., 'addLossCompr': 1.0},
                      'max':      {'fueltype': 'fuelcell', 'ident': 'h2gas', 'lhv': 33.322, 'fractionH2T': 0.10, 'eta_mtr': 0.97, 'eta_fc': 0.65, 'pSpec': 2000., 'opHours': 10000., 'addLossCompr': 1.0}},
            }

#%%===========================================================================

missionData = {#------------------------------------------------------------------------------------------------------------------
              'volo':   {'baseline': {'nPax': 1, 'massPayload': 100., 'massCrew': 100., 'timeReserve': 60 *  0., 'cruiseRange': 36000, 'hoverTime': 1.5*60, 'cruiseVelo': 27.8},
                         'min':      {'nPax': 1, 'massPayload': 100., 'massCrew': 100., 'timeReserve': 60 *  0., 'cruiseRange': 32400, 'hoverTime': 1.0*60, 'cruiseVelo': 27.8},
                         'max':      {'nPax': 1, 'massPayload': 100., 'massCrew': 100., 'timeReserve': 60 *  0., 'cruiseRange': 36000, 'hoverTime': 2.5*60, 'cruiseVelo': 27.8}},
              #------------------------------------------------------------------------------------------------------------------
              'city':   {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 0., 'timeReserve': 60 *  0., 'cruiseRange': 50000, 'hoverTime': 1.5*60, 'cruiseVelo': 33.3},
                         'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 0., 'timeReserve': 60 *  0., 'cruiseRange': 45000, 'hoverTime': 1.0*60, 'cruiseVelo': 33.3},
                         'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 0., 'timeReserve': 60 *  0., 'cruiseRange': 50000, 'hoverTime': 2.5*60, 'cruiseVelo': 33.3}},
              #------------------------------------------------------------------------------------------------------------------
              'vahana':   {'baseline': {'nPax': 1, 'massPayload': 100, 'massCrew': 100., 'timeReserve': 60 *  0., 'cruiseRange': 100000, 'hoverTime': 1.5*60, 'cruiseVelo': 52.8},
                           'min':      {'nPax': 1, 'massPayload': 100, 'massCrew': 100., 'timeReserve': 60 *  0., 'cruiseRange': 90000, 'hoverTime': 1.0*60, 'cruiseVelo': 52.8},
                           'max':      {'nPax': 1, 'massPayload': 100, 'massCrew': 100., 'timeReserve': 60 *  0., 'cruiseRange': 100000, 'hoverTime': 2.5*60, 'cruiseVelo': 52.8}},
              #------------------------------------------------------------------------------------------------------------------
              'cora':   {'baseline': {'nPax': 1, 'massPayload': 88 * 1, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 100000, 'hoverTime': 1.5*60, 'cruiseVelo': 44.},
                         'min':      {'nPax': 1, 'massPayload': 88 * 1, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 90000, 'hoverTime': 1.0*60, 'cruiseVelo': 44.},
                         'max':      {'nPax': 1, 'massPayload': 88 * 1, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 100000, 'hoverTime': 2.5*60, 'cruiseVelo': 44.}},
              #------------------------------------------------------------------------------------------------------------------
              'hyundai':   {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange':  97000, 'hoverTime': 1.5*60, 'cruiseVelo': 80.},
                            'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange':  90000, 'hoverTime': 1.0*60, 'cruiseVelo': 80.},
                            'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 105000, 'hoverTime': 2.5*60, 'cruiseVelo': 80.}},              
              #------------------------------------------------------------------------------------------------------------------
              'lilium':   {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 300000, 'hoverTime': 1.5*60, 'cruiseVelo': 83.},
                           'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 270000, 'hoverTime': 1.0*60, 'cruiseVelo': 83.},
                           'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 300000, 'hoverTime': 2.5*60, 'cruiseVelo': 83.}},              
              #------------------------------------------------------------------------------------------------------------------
              'joby':     {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 240000, 'hoverTime': 1.5*60, 'cruiseVelo': 89.},
                           'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 216000, 'hoverTime': 1.0*60, 'cruiseVelo': 89.},
                           'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 240000, 'hoverTime': 2.5*60, 'cruiseVelo': 89.}},  
              #------------------------------------------------------------------------------------------------------------------
              'skai':   {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 644000, 'hoverTime': 1.5*60, 'cruiseVelo': 37.9},
                         'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 576000, 'hoverTime': 1.0*60, 'cruiseVelo': 37.9},
                         'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 644000, 'hoverTime': 2.5*60, 'cruiseVelo': 37.9}},
              # #--- JOBY LIKE ------------------------------------------------------------------------------------------------------
               'hylevio':   {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 600000, 'hoverTime': 1.5*60, 'cruiseVelo': 83.},
                             'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 600000, 'hoverTime': 1.0*60, 'cruiseVelo': 83.},
                             'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 600000, 'hoverTime': 2.0*60, 'cruiseVelo': 83.}},
              # --- VAHANA LIKE ---------------------------------------------------------------------------------------------------------
              # 'hylevio':   {'baseline': {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 550000, 'hoverTime': 1.5*60, 'cruiseVelo': 52.8},
              #              'min':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 500000, 'hoverTime': 1.0*60, 'cruiseVelo': 52.8},
              #              'max':      {'nPax': 4, 'massPayload': 88 * 4, 'massCrew': 80., 'timeReserve': 60 *  0., 'cruiseRange': 644000, 'hoverTime': 2.5*60, 'cruiseVelo': 52.8}},
              }     
 

#%%===========================================================================             

import copy

def gdv(IDENT, TYPE, KEY):
    return designData[IDENT][TYPE][KEY]

def gmv(IDENT, TYPE, KEY):
    return missionData[IDENT][TYPE][KEY]

def gtv(IDENT, TYPE, KEY):
    return techData[IDENT][TYPE][KEY]

def loadDesign(IDENT, TYPE):
    return copy.deepcopy(designData[IDENT][TYPE])
    
def loadMission(IDENT, TYPE):
    return copy.deepcopy(missionData[IDENT][TYPE])
    
def loadTech(IDENT, TYPE):
    return copy.deepcopy(techData[IDENT][TYPE])

def loadFueltype(fueltype, **kwargs):

    if fueltype == 'hybrid':
        fuel = {'fueltype':   fueltype,
                'highPowSys':     None,
                'highEnerSys':    None,
                'fHybrid':        None,
                'calcOptHybr':    True}

    elif fueltype == 'hybridBattery':
        fuel = {'fueltype':          fueltype,
                'highEnerSys':      None,
                'highPowSys':       None,
                'fHybrid':          None,
                'calcOptHybr':      True}
    
    else:
        print('No fueltype specified!')
    
    for key, arg in kwargs.items():
        fuel[key] = arg       

    return fuel


def loadCase(IDENT, TYPE, dFuel):
    dDesign = loadDesign(IDENT, TYPE)
    dMission = loadMission(IDENT, TYPE)
    dCase = {'design': dDesign, 'mission': dMission, 'fuel': copy.deepcopy(dFuel)}
    return dCase