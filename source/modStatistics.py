#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 07:42:38 2020

@author: Nicolas
"""


import numpy as np
from scipy.stats import norm, randint, triang, uniform

def getSample(dist, bounds, size):
    
    # normal distribution: mean = bound[0], stddev = bound[1]
    if dist == 'normal':
        sample = norm.rvs(loc=bounds[0], scale=bounds[1], size=size)
    
    # randint distribution: note that randint returns an open interval, thus the +1 in high bound
    elif dist == 'randint':
        sample = randint.rvs(low=bounds[0], high=1+bounds[1], loc=0, size=size)
    
    # uniform distribution: lower limit = bound[0], upper limit = bound[1]
    elif dist == 'uniform':
        sample = uniform.rvs(loc=bounds[0], scale=bounds[1]-bounds[0], size=size)
    
    # triangular distribution: lower limit = bounds[0], peak location = bounds[1], upper limit = bounds[2])
    elif dist == 'triang':
        c = (bounds[1] - bounds[0]) / (bounds[2] - bounds[0])
        #print(bounds)
        sample = triang.rvs(c, loc=bounds[0], scale=(bounds[2] - bounds[0]), size=size)
    
    # linear spaced values: lower multiplier = bounds[0], upper multiplier = bounds[1]
    elif dist == 'linspace':
        sample = np.linspace(bounds[0], bounds[1], size)
    
    # linear spaced values: lower multiplier = bounds[0], baseline value = bounds[1], upper multiplier = bounds[2]
    elif dist == 'linvariation':
        sample = np.linspace(bounds[1] * bounds[0], bounds[1] * bounds[2], size)
    
    else:
        print('I dont know this distribution!')
    
    return sample.tolist()

def getPDF(dist, bounds, size):
    
    if dist == 'triang':
        c = (bounds[1] - bounds[0]) / (bounds[2] - bounds[0])
        sample = triang.rvs(c, loc=bounds[0], scale=(bounds[2] - bounds[0]), size=size)
        pdf = triang.pdf(np.sort(sample),c,loc=bounds[0],scale=[(bounds[2] - bounds[0])])
        
    else:
        pass
    
    return pdf

def getCDF(dist, bounds, size):
    
    if dist == 'triang':
        c = (bounds[1] - bounds[0]) / (bounds[2] - bounds[0])
        sample = triang.rvs(c, loc=bounds[0], scale=(bounds[2] - bounds[0]), size=size)
        cdf = triang.cdf(np.sort(sample),c,loc=bounds[0],scale=[(bounds[2] - bounds[0])])
        
    else:
        pass
    
    return cdf