#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 13:42:12 2020

@author: Nicolas
"""

import matplotlib.pyplot as plt 
import numpy as np

def quickBarplotAxis(dResult, sVar, dVar, size, annotate, **kwargs):

    # DATA PREPARATION   s
    y = np.arange(len(sVar))    

    outMin = np.zeros(( len(sVar) ))
    outMax = np.zeros(( len(sVar) ))
    
    for i, base in enumerate(dResult['base']):
        outMin[i] = dResult['min'][i] - base
        outMax[i] = dResult['max'][i] - base
    
    # THE FIGURE
    fig = plt.figure(figsize=size)
    
    axes = plt.subplot()
    
    axes.invert_yaxis()
    axes.set_yticks(y)
    axes.set_yticklabels(sVar)
    axes.set_xlabel('gCO2e / PKT')
    axes.xaxis.grid(zorder=0, which='both')
    axes.tick_params(axis='y', which='both', length=0)
    
    ###########################################################################
    axes.barh(y, outMin, left=dResult['base'], **kwargs)
    axes.barh(y, outMax, left=dResult['base'], **kwargs)
        
    if annotate:
        for key, val in dVar.items():
            for i, v in enumerate(val):
                axes.text(dResult['base'][i]+0.015*dResult['base'][i], i+0.02, str(dVar['base'][i][2]), color='k', fontsize=10, va='center')

    axes.spines['right'].set_visible(False)
    axes.spines['left'].set_visible(False)
    
    return fig, axes