#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 12:29:20 2020

@author: Nicolas
"""
import copy

from .modSizingTier0 import sizeAircraft

#%%============================================================================
#    I N T E R F A C E   F U N C T I O N S 
#%=============================================================================

def getTotalMaterialImpact(dData):
    dMatImp = calcMaterialImpact(dData)
    totalImpact = sum(dMatImp.values())
    return totalImpact

#%=============================================================================

def getTotalComponentsMaterialImpact(dData):
    dCompMatImp = calcComponentsMaterialImpact(dData)
    dTotal = {k0: sum(v0.values()) for k0,v0 in dCompMatImp.items()}
    return dTotal

def getOtherMaterialImpact(dData):
    dCompMatImp = calcComponentsMaterialImpact(dData)
    structImpact = sum(dCompMatImp['other'].values())
    return structImpact

def getStructMaterialImpact(dData):
    dCompMatImp = calcComponentsMaterialImpact(dData)
    structImpact = sum(dCompMatImp['struct'].values())
    return structImpact

def getPtrainMaterialImpact(dData):
    dCompMatImp = calcComponentsMaterialImpact(dData)
    structImpact = sum(dCompMatImp['ptrain'].values())
    return structImpact

def getFuelMaterialImpact(dData):
    dCompMatImp = calcComponentsMaterialImpact(dData)
    structImpact = sum(dCompMatImp['fuel'].values())
    return structImpact

#%=============================================================================

def getTotalOperationsImpact(case):
    case = sizeAircraft(case)
    specImpact = case['opsImpact']    
    impact = specificOperationsUnit(specImpact, case)  
    return sum(impact.values())
    
def getTotalOperationsCost(case):
    case = sizeAircraft(case)
    specCost = case['opsCost']
    cost = specificOperationsUnit(specCost, case)  
    return sum(cost.values())

#%=============================================================================


#%=============================================================================

def getTotalProductionImpact(case):
    case = sizeAircraft(case)
    impactsComponents = calcImpactPerComponent(case)
    return sum(impactsComponents.values())

def calcProductionImpacts(case):
    case = sizeAircraft(case)
    impactsComponents = calcImpactPerComponent(case)
    return impactsComponents   

def getTotalLifeCycleImpact(case):
    impacts = calcLifeCycleImpact(case)
    return sum(impacts.values())

def calcLifeCycleImpact(case):
    case = sizeAircraft(case)
    impactComponents = calcImpactPerComponent(case)
    # delete material impacts that were calculated on kg bases and without depreciation
    if 'battery' in impactComponents.keys(): del impactComponents['battery']
    if 'stack' in impactComponents.keys(): del impactComponents['stack']
    if 'tank' in impactComponents.keys(): del impactComponents['tank']    
    specImpComps = specificComponentsUnit(impactComponents, case)
    
    impOps = case['opsImpact']
    specImpOps = specificOperationsUnit(impOps, case) 
    # stack to output dictionary
    impacts = {**specImpComps, **specImpOps}   
    return impacts

#%%============================================================================
#    E N E R G I E S   A S S E S S M E N T 
#%=============================================================================

def calcEnergies(inputCase):
    case = copy.deepcopy(inputCase)
    case = sizeAircraft(case)
    ops = case['operations']
    pwr = case['analysis']
    
    energy = {}
    energy['hover'] = pwr['pHov'] * ops['hoverTime'] / 1000. / 3600. # to return in kWh
    energy['cruise'] = pwr['pCrs'] * ops['cruiseRange'] / ops['cruiseVelo'] / 1000. / 3600. # to return in kWh

    #print(pwr['pCrs'] / 1000. , pwr['pHov'] / 1000., ops['hoverTime'], ops['cruiseRange']/1000., ops['cruiseVelo'], energy['hover']/1000.,energy['cruise']/1000.)
    # this is not used because it would calc energy based on the sizing mission, not on the actual mission
    # for key, state in case['flightstates'].items():
    #     dEnergy[key] = state['t'] * state['pReq'] / 1000. / 3600.

    case['energies'] = energy

    return energy

def getSpecEnergies(case):
    energy = calcEnergies(case)
    funcUnit = case['info']['funcUnit']
    ops = case['operations']
    specEnergy = specFunctionalUnit(funcUnit, energy, ops)
    return specEnergy

def getTotalSpecEnergy(case):
    energy = calcEnergies(case)
    funcUnit = case['info']['funcUnit']
    ops = case['operations']
    #print()
    #print(case['mission']['cruiseRange'], ops['cruiseRange'], case['masses']['dgm'], energy)

    specEnergy = specFunctionalUnit(funcUnit, energy, ops)
    #print(case['mission']['cruiseRange'], ops['cruiseRange'], case['masses']['dgm'], specEnergy)
    #print()
    return sum(specEnergy.values())


#%%============================================================================
#    C O N V E R I O N   T O   F U N C I O N A L   U N I T S
#%=============================================================================

def specFunctionalUnit(funcUnit, dic, dOps):
    out = copy.deepcopy(dic)

    # import (m) and convert to unit (km)
    crsRange = dOps['cruiseRange'] / 1000.
    
    # import (sec) and convert to unit (hr)
    tHov = dOps['hoverTime'] / 3600.
    tCrs = dOps['cruiseRange'] / dOps['cruiseVelo'] / 3600. 
    
    # --- Returns Impact per operational Mission
    if funcUnit == None:
        pass
    # --- Vehicle Kilometer Traveled
    elif funcUnit == 'VKT':
        #print(crsRange)
        for k,v in out.items():
            out[k] = v / crsRange 
    # --- Flight Hour
    elif funcUnit == 'FH':
        for k,v in out.items():
            out[k] = v / (tHov + tCrs)
    # --- Passenger Kilometer Traveled
    elif funcUnit == 'PKT':
        nPax = dOps['nPax']
        fUtil = dOps['fUtil']
        fCirc = dOps['fCirc']
        #print(crsRange, nPax, fUtil)
        for k,v in out.items():
            #print(out[k], v / crsRange / nPax / fUtil, crsRange , nPax , fUtil)
            out[k] = v / crsRange / nPax / fUtil / fCirc

    # --- Payload Kilometer Traveled
    elif funcUnit == 'LKT':
        payload = dOps['massPayload']
        #print(crsRange, payload)
        for k,v in out.items():
            out[k] = v / crsRange / payload
    else:
        print('I dont know this functional Unit!')  
    
    return out

#%%===========================================================================

#def specificImpact(funcUnit, dImpactKWh, dCase):
def specificOperationsUnit(dInp, dCase):

    funcUnit = dCase['info']['funcUnit']

    # import (W) and convert to unit (kW)
    #pHov = dCase['analysis']['pHov'] / 1000. 
    #pCrs = dCase['analysis']['pCrs'] / 1000.

    # import delivered power and time in flightstates and convert to unit (kW)
    pHovHES = dCase['flightstates']['hover']['pHES'] / 1000.
    pHovHPS = dCase['flightstates']['hover']['pHPS'] / 1000.
    pCrsHES = dCase['flightstates']['cruise']['pHES'] / 1000.
    pCrsHPS = dCase['flightstates']['cruise']['pHPS'] / 1000.

    dOps = dCase['operations']

    # import (sec) and convert to unit (hr)
    tHov = dOps['hoverTime'] / 3600.
    tCrs = dOps['cruiseRange'] / dOps['cruiseVelo'] / 3600.  
    
    # make deepcopy to not change the calculated impacts per kWh 
    dOut = copy.deepcopy(dInp)  
    
    # STEP 1: Bring to an operational level, i.e. by norming the energy-specific impact
    # with the amount of kWh per operational unit (or say, the standard operational mission)
    # ========================================================================

    if dCase['fuel']['fueltype'] == 'hybrid':
        #fHybrid = dSizing['fuel']['fHybrid']
        #print(dCase['fuel']['fueltype'], list(dOpsImpact))
        for key in list(dOut):
            
            # IMPACT ASSESSMENT
            if key == 'elecImpactHPS' or key == 'batteryProdImpactHPS':
                # multiply high power system IMPACT PER kWh with the ENERGY provided by the high power system (Power Battery)
                dOut[key] = dOut[key] * (pHovHPS * tHov + pCrsHPS * tCrs)
            elif key == 'hydrProdImpact' or key == 'hydrTankEquipImpact' or key == 'fuelcellProdImpact':
                # multiply high energy system IMPACT PER kWh with the ENERGY provided by the high energy system (Fuel Cell System)
                dOut[key] = dOut[key] * (pHovHES * tHov + pCrsHES * tCrs)
                
            # COST ASSESSMENT
            elif key == 'elecTotalCost' or key == 'batteryProdCost':
                # multiply high power system COST PER kWh with the ENERGY provided by the high power system (Power Battery)
                dOut[key] = dOut[key] * (pHovHPS * tHov + pCrsHPS * tCrs)
            elif key == 'hydrTotalCost' or 'fuelcellProdCost':
                # multiply high energy system COST PER kWh with the ENERGY provided by the high energy system (Fuel Cell System)
                dOut[key] = dOut[key] * (pHovHES * tHov + pCrsHES * tCrs)
                
            # CATCH EXCEPTIONS
            else:
                print('EXCEPTION: Something with your assessment key went wrong!!!')
                
            # if 'hydr' in key or 'Stack' in key: 
            #     dOut[key] = dOut[key] * (pHovHES * tHov + pCrsHES * tCrs)
            #     print(dOut[key])
            # elif 'elec' in key or 'battery' in key:
            #     dOut[key] = dOut[key] * (pHovHPS * tHov + pCrsHPS * tCrs)
            #     print(dOut[key])

    
    elif dCase['fuel']['fueltype'] == 'hybridBattery':
        #print(dCase['fuel']['fueltype'], list(dOut))
        for key in list(dOut):
            
            # IMPACT ASSESSMENT
            if key == 'elecImpactHPS' or key == 'batteryProdImpactHPS':
                dOut[key] = dOut[key] * (pHovHPS * tHov + pCrsHPS * tCrs)
            elif key == 'elecImpactHES' or key == 'batteryProdImpactHES':   
                dOut[key] = dOut[key] * (pHovHES * tHov + pCrsHES * tCrs)
                
            # COST ASSESSMENT
            elif key == 'elecTotalCost' or key == 'batteryProdCost':
                dOut[key] = dOut[key] * ( (pHovHPS + pHovHES) * tHov + (pCrsHPS + pCrsHES) * tCrs )

            
            # if 'HPS' in key:
            #     dOut[key] = dOut[key] * (pHovHPS * tHov + pCrsHPS * tCrs)
            #     print(dOut[key])
            # elif 'HES' in key:
            #     dOut[key] = dOut[key] * (pHovHES * tHov + pCrsHES * tCrs)
            #     print(dOut[key])
            else:
                print('EXCEPTION: Something with your assessment key went wrong!!!')
    else:
        print('fueltype not implemented')
    
    # STEP 2: Bring to the requested functional unit, assuming the operational scheme is as defined
    # ========================================================================
    dSpecImpact = specFunctionalUnit(funcUnit, dOut, dOps)
    
    return dSpecImpact


#def specImpactPerComponent(funcUnit, dImpact, dCase):
def specificComponentsUnit(dImpact, dCase):

    funcUnit = dCase['info']['funcUnit']    

    dSpecImpact = copy.deepcopy(dImpact)    

    vehLife = dCase['ecosys']['vehicleLife']
    crsRange = dCase['operations']['cruiseRange'] / 1000.
    fUtil = dCase['operations']['fUtil'] 
    nPax = dCase['operations']['nPax'] 

    # import (sec) and convert to unit (hr)
    tHov = dCase['operations']['hoverTime'] / 3600.
    tCrs = dCase['operations']['cruiseRange'] / dCase['operations']['cruiseVelo'] / 3600. 

    if funcUnit == 'FH':    
        for key in dSpecImpact.keys():
            dSpecImpact[key] = dSpecImpact[key] / vehLife * 1000.  # output in gCO2e/Vehicle
    elif funcUnit == 'PKT':
        for key in dSpecImpact.keys():
            dSpecImpact[key] = dSpecImpact[key] / vehLife * (tHov + tCrs) / crsRange / nPax / fUtil * 1000.
    else:
        print('I dont know this functional Unit!')  

    return dSpecImpact

#%%============================================================================
#    N E W   A S   O F   A U G - 0 5  
#%=============================================================================

def calcImpactPerComponent(case):
   
    dImpact = {}    
    dCompMatImp = calcComponentsMaterialImpact(case)
    
    dImpact['struct'] = case['masses']['struct'] * sum(dCompMatImp['struct'].values())
    dImpact['other'] = case['masses']['other'] * sum(dCompMatImp['other'].values())
    
    if case['fuel']['fueltype'] == 'hybridBattery':
        dImpact['battery'] = ( (case['fuel']['highEnerSys']['eStored'] * sum(dCompMatImp['battery'].values()) + 
                                case['fuel']['highPowSys']['eStored'] * sum(dCompMatImp['battery'].values())) / 3600. / 1000. )
        dImpact['ptrain'] = case['masses']['ptrain'] * sum(dCompMatImp['ptrain'].values())
    elif case['fuel']['fueltype'] == 'hybrid':
        dImpact['battery'] = case['fuel']['highPowSys']['eStored'] * sum(dCompMatImp['battery'].values()) / 3600. / 1000.
        dImpact['tank'] = case['fuel']['highEnerSys']['massTank'] * sum(dCompMatImp['tank'].values())
        dImpact['stack'] = case['fuel']['highEnerSys']['stack'] * sum(dCompMatImp['stack'].values())
        dImpact['ptrain'] = case['fuel']['elecMotor'] * sum(dCompMatImp['ptrain'].values())
    else:
        print('This fueltype is not defined to be evaluated in function calcImpactPerComponent')
    
    #case['impact'] = dImpact    
    
    return dImpact

#%%============================================================================
#    I M P A C T   A S S E S S M E N T   M A T E R I A L S
#%=============================================================================

def calcMaterialImpact(dData):
    dMaterials = dData['materials']
    dImpacts = dData['matImpact']
    dMatImp = {}
    for k in dMaterials.keys():
       dMatImp[k] = dImpacts[k] * dMaterials[k]
    return dMatImp

#%=============================================================================

def calcComponentsMaterialImpact(dData):
    dComponents = dData['components']
    dImpacts = dData['matImpact']
    
    dCompMatImp = {}
    for comp, dMaterials in dComponents.items():
        dCompMatImp[comp] = {}
        #print('---')
        for mat, fraction in dMaterials.items():
            #print('{:8s} {:>10s} {:>5.2f}'.format(comp, mat, dImpacts[mat] * fraction))
            dCompMatImp[comp][mat] = dImpacts[mat] * fraction
    return dCompMatImp


#%%============================================================================
#    N E W   A S   O F   A U G - 0 5  
#%=============================================================================

# def specificCostKWh(dInput, dFuel):
    
#     return None

    # fueltype = dFuel['fueltype']
    
    # dCost = {}
    
    # # cost per kWh stored

    # if fueltype == 'battery':
    #     dBattery = dFuel
    #     dCost['elecWholesale'] = dInput['elecWholesale'][0] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #     dCost['elecGridfee']   = dInput['elecGridfee'][0]   / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #     dCost['elecTaxLevies'] = dInput['elecTaxLevies'][0] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #     dCost['batteryProd']   = dInput['batteryProd'][0]   / dBattery['fDoD'] / dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr']        
    
    # elif fueltype == 'hybrid':
    #     dBattery = dFuel['battery']
    #     dFuelCell = dFuel['fuelcell']
        
    #     dCost['hydrWholesale'] = dInput['hydrWholesale'][0] / 33.33 / dFuelCell['eta_fc'] / dFuelCell['eta_mtr']
    #     #dUnit['hydrWholesale'] = dInput['hydrWholesale'][1] + '/kWh*kg' # could be done but has not value now
    #     dCost['hydrLogistics'] = dInput['hydrLogistics'][0] / 33.33 / dFuelCell['eta_fc'] / dFuelCell['eta_mtr']
    #     #dCost['hydrDispenser'] = dInput['hydrDispenser'][0] / 33.33 / dFuelCell['eta_fc'] / dFuelCell['eta_mtr']
    #     dCost['fuelCellStack'] = dInput['fuelCellStack'][0] / dFuelCell['opHours']
    
    #     dCost['elecWholesale'] = dInput['elecWholesale'][0] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #     dCost['elecGridfee']   = dInput['elecGridfee'][0]   / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #     dCost['elecTaxLevies'] = dInput['elecTaxLevies'][0] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] 
    #     dCost['batteryProd']   = dInput['batteryProd'][0]   / dBattery['fDoD'] / dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr']           
    # else:
    #     print('NOT IMPLEMENTED YET')
    
    # return dCost

# def specificImpactKWh(dInput, dFuel, dEcoSys):
    
#     return None
    
    # dImpact = {}
    # #dUnit = {}
    # fueltype = dFuel['fueltype']
    
    # # cost per kWh stored
    # if fueltype == 'hybridBattery':
    #     dPowerBatt = dFuel['highPowSys']
    #     dEnergyBatt = dFuel['highEnerSys']
    #     dImpact['batteryProductionHPS'] = dInput['batteryProduction'] / dPowerBatt['fDoD'] / dPowerBatt['nCycles'] / dPowerBatt['eta_dis'] / dPowerBatt['eta_mtr'] * 1000. #* dFuel['fHybrid']    
    #     dImpact['batteryProductionHES'] = dInput['batteryProduction'] / dEnergyBatt['fDoD'] / dEnergyBatt['nCycles'] / dEnergyBatt['eta_dis'] / dEnergyBatt['eta_mtr'] * 1000. # * (1 - dFuel['fHybrid'])   
    #     dImpact['elecProductionHPS']    = dInput['elecProduction'] / dEcoSys['eta_grd'] / dPowerBatt['eta_chg'] / dPowerBatt['eta_dis'] / dPowerBatt['eta_mtr'] * 1000. #* dFuel['fHybrid'] 
    #     dImpact['elecProductionHES']    = dInput['elecProduction'] / dEcoSys['eta_grd'] / dEnergyBatt['eta_chg'] / dEnergyBatt['eta_dis'] / dEnergyBatt['eta_mtr'] * 1000. # * (1 - dFuel['fHybrid'])
    
    # elif fueltype == 'hybrid':
    #     dBattery = dFuel['highPowSys']
    #     dFuelCell = dFuel['highEnerSys']
    #     dImpact['hydrTankAndEquip']    = dInput['hydrTankAndEquip'] / dFuelCell['opHours'] / dFuelCell['eta_mtr'] * 1000. # * (1 - dFuel['fHybrid'])        
    #     dImpact['fuelStackProduction'] = dInput['fuelStackProduction'] / dFuelCell['opHours'] / dFuelCell['eta_mtr'] * 1000. # * (1 - dFuel['fHybrid'])
    #     dImpact['hydrProdTotal']       = dInput['hydrProdTotal'] / dFuelCell['lhv'] / dFuelCell['eta_fc'] / dFuelCell['eta_mtr'] #* (1 - dFuel['fHybrid'])
    #     dImpact['batteryProduction']   = dInput['batteryProduction'] / dBattery['fDoD'] / dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr'] * 1000. #* dFuel['fHybrid'] 
    #     dImpact['elecProduction']    = dInput['elecProduction'] / dEcoSys['eta_grd'] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] * 1000. # * dFuel['fHybrid'] 

            
    
    # SOUTH OF HERE IS DEPRECATED
    
    # elif fueltype == 'battery':
    #     dBattery = dFuel
    #     dImpact['elecProduction']    = dInput['elecProduction'] / dBattery['eta_grd'] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] * 1000.
    #     dImpact['batteryProduction'] = dInput['batteryProduction'] / dBattery['fDoD'] / dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr'] * 1000.
        
    # elif fueltype == 'fuelcell':
    #     dFuelCell = dFuel
    #     dImpact['hydrProdTotal']       = dInput['hydrProdTotal'] / 33.33 / dFuelCell['eta_fc'] / dFuelCell['eta_mtr']
    #     dImpact['fuelStackProduction'] = dInput['fuelStackProduction'] / dFuelCell['opHours'] * 1000.
    #     dImpact['hydrTankAndEquip']    = dInput['hydrTankAndEquip'] / dFuelCell['opHours'] * 1000.

    # # elif fueltype = 'hybridBattery':
    # #     dPowerBat = dFuel
    # #     dImpact['elecProduction']    = dInput['elecProduction'] / dPowerBat['eta_grd'] / dPowerBat['eta_chg'] / dPowerBat['eta_dis'] / dPowerBat['eta_mtr'] * 1000.
    # #     dImpact['batteryProduction']   = dInput['batteryProduction'] / dPowerBat['fDoD'] / dPowerBat['nCycles'] / dPowerBat['eta_dis'] / dPowerBat['eta_mtr'] * 1000.

    # #     dEnergyBat = dFuel
    # #     dImpact['elecProduction']    = dInput['elecProduction'] / dEnergyBat['eta_grd'] / dEnergyBat['eta_chg'] / dEnergyBat['eta_dis'] / dEnergyBat['eta_mtr'] * 1000.
    # #     dImpact['batteryProduction']   = dInput['batteryProduction'] / dEnergyBat['fDoD'] / dEnergyBat['nCycles'] / dEnergyBat['eta_dis'] / dEnergyBat['eta_mtr'] * 1000.

    # elif fueltype == 'kerosene':
    #     dKerosene = dFuel
    #     dImpact['keroseneCombustion'] = dInput['keroseneCombustion'] * dKerosene['sfc'] * 1000.    
    #     dImpact['keroseneProduction'] = dInput['keroseneProduction'] * dKerosene['sfc'] * 1000.
    #     dImpact['keroseneTransport'] = dInput['keroseneTransport'] * dKerosene['sfc'] * 1000.
    
    # elif fueltype == 'hybridTurboshaft':
    #     dKerosene = dFuel['kerosene']
    #     dBattery = dFuel['battery']
    #     # print(dFuel['fHybrid'] )
    #     dImpact['keroseneCombustion'] = dInput['keroseneCombustion'] * dKerosene['sfc'] * (1 - dKerosene['fSyn']) * (1 - dFuel['fHybrid']) * 1000.    
    #     dImpact['keroseneProduction'] = dInput['keroseneProduction'] * dKerosene['sfc'] * (1 - dFuel['fHybrid']) * 1000.
    #     dImpact['keroseneTransport'] = dInput['keroseneTransport'] * dKerosene['sfc'] * (1 - dFuel['fHybrid']) * 1000.    
    #     dImpact['elecProduction']    = dInput['elecProduction'] / dBattery['eta_grd'] / dBattery['eta_chg'] / dBattery['eta_dis'] / dBattery['eta_mtr'] * dFuel['fHybrid'] * 1000.
    # #     dImpact['batteryProduction']   = dInput['batteryProduction'] / dBattery['fDoD'] / dBattery['nCycles'] / dBattery['eta_dis'] / dBattery['eta_mtr'] * dFuel['fHybrid'] * 1000.        
    # else:
        
    #     print('ERROR. I dont know this fuel type')
        
    # return dImpact


# def specificMissionAircraft(dic, dSizing):

#     tHov = dSizing['mission']['hoverTime'] / 3600.
#     tCrs = dSizing['mission']['cruiseRange'] / dSizing['mission']['cruiseVelo'] / 3600.  
    
#     #mStruct = dSizing['masses']['struct']
#     #mStruct = dAircraft['mStruct']
#     opHours = dSizing['mission']['opHours']
#     #print(tCrs + tHov)

#     out = copy.deepcopy(dic)  
    
#     # print('Impact per kg     ', out['structureProduction'])
#     # print('Impact per Vehicle', out['structureProduction'] * float(mStruct))
#     # print('Impact per FH     ', out['structureProduction'] * float(mStruct)  / opHours)
    
#     #out['structureProduction'] = out['structureProduction'] * float(mStruct) / opHours * (tCrs + tHov) * 1000.

#     for key in out.keys():
#         if 'struct' in key:
#             out[key] = out[key] * float(dSizing['masses']['struct']) / opHours * (tCrs + tHov) * 1000.
#         elif 'other' in key:
#             out[key] = out[key] * float(dSizing['masses']['other']) / opHours * (tCrs + tHov) * 1000.
#         #elif 'tbd' in key:
#         #    out[key] = out[key] * float(dSizing['masses']['other']) / opHours * (tCrs + tHov) * 1000.
#     return out

#%%===========================================================================

# def specificMissionFuel(dic, dSizing):
    
#     return None
    
    # fueltype = dSizing['fuel']['fueltype']
    # tHov = dSizing['mission']['hoverTime'] / 3600.
    # tCrs = dSizing['mission']['cruiseRange'] / dSizing['mission']['cruiseVelo'] / 3600.  
    
    # hovPower = dSizing['analysis']['pHov'] / 1000. 
    # crsPower = dSizing['analysis']['pCrs'] / 1000.     

    # # I think this is important to be able to look at other fUnits in one calculation run
    # out = copy.deepcopy(dic)   

    # if fueltype == 'battery':
    #     for key in list(out):
    #         #print(key, out)
    #         if 'hydr' in key or 'Stack' in key or 'kerosene' in key: 
    #             out[key] = 0.0
    #             #del out[key]
    #         else:
    #             #print(out[key], crsPower,tCrs,hovPower,tHov)
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)
      
    # elif fueltype == 'fuelcell':
    #     for key in list(out):
    #         if 'elec' in key or 'battery' in key or 'kerosene' in key:
    #             out[key] = 0.0
    #             #del out[key]
    #         else:
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)
    
    # elif fueltype == 'hybrid':
    #     #fHybrid = dSizing['fuel']['fHybrid']
    #     for key in list(out):
    #         if 'hydr' in key or 'Stack' in key: 
    #             #out[key] = out[key] * (crsPower * tCrs + hovPower * tHov * (1 - fHybrid))
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)
    #             #out[key] = out[key] * dSizing['fuel']['highEnerSys']['eStored']
    #         elif 'elec' in key or 'battery' in key:
    #             #out[key] = out[key] * (hovPower * tHov * fHybrid)  
    #             out[key] = out[key] * (hovPower * tHov)
    #             #out[key] = out[key] * dSizing['fuel']['highPowSys']['eStored']
    
    # # this goes with the assumption that distro based on fHybrid has already been done before
    # # like in the function specImpactKWh
    # elif fueltype == 'hybridBattery':
    #     for key in list(out):
    #         if 'HPS' in key:
    #             out[key] = out[key] * (hovPower * tHov)
    #         elif 'HES' in key:
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)
    
    # # this does right now apply the assumption that both battery types are 
    # # identical in their environmental footprint and thus to be treatet equally
    # elif fueltype == 'hybridBattery':
    #     for key in list(out):
    #         if 'hydr' in key or 'Stack' in key or 'kerosene' in key: 
    #             out[key] = 0.0
    #         elif 'elec' in key or 'battery' in key:
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)       
    #             #out[key] = out[key] * dSizing['fuel']['highEnerSys']['eStored'] + out[key] * dSizing['fuel']['highPowSys']['eStored']
                
    # elif fueltype == 'kerosene':
    #     for key in list(out):
    #         if not 'kerosene' in key:
    #             out[key] = 0.0
    #         else:
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)

    # elif fueltype == 'hybridTurboshaft':
    #     #fHybrid = dSizing['fuel']['fHybrid']
    #     for key in list(out):
    #         if 'kerosene' in key:
    #             #out[key] = out[key] * (crsPower * tCrs + hovPower * tHov * (1 - fHybrid))   # now in the specificImpactKWh()
    #             out[key] = out[key] * (crsPower * tCrs + hovPower * tHov)  
    #         else: 
    #             #out[key] = out[key] * (hovPower * tHov * fHybrid)   # now in the specificImpactKWh()
    #             out[key] = out[key] * (hovPower * tHov)  
    
    #print(out)
    #print()
    #[print('{:20s} {:7.5f} per Mission'.format(k,v)) for k,v in out.items()]

    # return out



# def specificFunctionalUnit(funcUnit, dic, dOps):
    
#     return None




