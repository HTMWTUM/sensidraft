#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 11:00:38 2019

@author: gu95pug
"""

from .util import grav


#%%============================================================================
#    F U N C T I O N S
#%============================================================================= 

# def calcCruiseHybridization(pHov, pCrs):
#     # simples hydridization is 1 - pCrs/pHov
#     return (pHov - pCrs) / pHov

# def calcPowersNew(dData):
#     dPowers = getPowers(dData)
#     dData['analysis'] = {}
#     dData['analysis'] = dPowers
#     return dData


def calcPowers(dData):

    dData['analysis'] = {}
    
    dgm = dData['masses']['dgm']      
    
    pl = dData['design']['pl']                        # N/W
    vCrs = dData['mission']['cruiseVelo']             # m/s
    ltod = dData['design']['ltod']                    # -
    rCrs = dData['mission']['cruiseRange']            # m
    tHov = dData['mission']['hoverTime']              # t
    
    fHov = shareHover(rCrs, vCrs, tHov)

    pHov = powerHover(dgm, pl)
    if dData['design']['ident'] == 'lilium':
        import math 
        aw = 1.3
        pHov = pHov / math.sqrt(2*aw)
    
    pCrs = powerCruise(dgm, ltod, vCrs)
    
    #print(dData['mission']['cruiseRange'], pl, ltod, pHov, pCrs)
    
    dData['analysis']['pHov'] = float(pHov) 
    dData['analysis']['pCrs'] = float(pCrs)
    dData['analysis']['pAvg'] = float(pHov * fHov + pCrs * (1 - fHov))
    
    return dData 

def powerCruise(m, ltod, vel):
    return m * grav * vel / ltod                 # W

def powerHover(m, pl):                           
    return m * grav / pl                         # W

def timeHover(rCrs, vCrs, fHov):
    """ Calculate the hover time in seconds

    Parameters
    ----------
    rCrs : float
        The mission cruise range requirement (in m)
    vCrs : float
        The cruise velocity (in m/s)
    fHov : float
        The time share spent in hover flight (in -)

    Returns
    -------
    float
        Hover time (in sec)

    """
    return rCrs / vCrs * fHov / (1 - fHov)       # s

def shareHover(rCrs, vCrs, tHov):
    """ Calculate the hover share 

    Parameters
    ----------
    rCrs : float
        The mission cruise range requirement (in m)
    vCrs : float
        The cruise velocity (in m/s)
    tHov : float
        The time spent in hover flight (in sec)

    Returns
    -------
    float
        Hover share (in -)

    """
    return tHov / (tHov + (rCrs / vCrs))         # -


def getAvgOpsPower(dArgs):
    
    dgm = dArgs['masses']['dgm']      
    pl = dArgs['design']['pl']                        # N/W
    vCrs = dArgs['operations']['cruiseVelo']             # m/s
    ltod = dArgs['design']['ltod']                    # -
    rCrs = dArgs['operations']['cruiseRange']            # m
    tHov = dArgs['operations']['hoverTime']              # t    

    fHov = shareHover(rCrs, vCrs, tHov)

    pHov = powerHover(dgm, pl)
    pCrs = powerCruise(dgm, ltod, vCrs)
        
    pAvg =  float(pHov * fHov + pCrs * (1 - fHov))
    return pAvg


# def getPowers(dArgs):
#     """Takes data dictionary to calc and return hover power, 
#     cruise power and average power, e.g. for further use in eco analyses. 
    
#     Parameters
#     ----------
#     dArgs : dict
#         Dictionary containing dictionaries for keys 'design', 'mission', and 'masses'
#         i. e. Sizing must be completed

#     Returns
#     -------
#     dData : dict
#         Dictionary with the calculated powers in W

#     """    
#     dPowers = {}
    
#     dgm = dArgs['masses']['dgm']      
    
#     pl = dArgs['design']['pl']                        # N/W
#     vCrs = dArgs['mission']['cruiseVelo']             # m/s
#     ltod = dArgs['design']['ltod']                    # -
#     #rCrs = dArgs['mission']['cruiseRange']            # m
#     #tHov = dArgs['mission']['hoverTime']              # t
    
#     #fHov = shareHover(rCrs, vCrs, tHov)

#     pHov = powerHover(dgm, pl)
#     pCrs = powerCruise(dgm, ltod, vCrs)

#     dPowers['pHov'] = pHov
#     dPowers['pCrs'] = pCrs
#     #dPowers['pAvg'] = pHov * fHov + pCrs * (1 - fHov)
    
#     return dPowers

#%%=============================================================================
#   I N T R O 
#===============================================================================

# Interface shall only containt functions that wrap multiple other functions from
# various domains, i.e. sizing & assessment


# def getAircraftRange(dData):
#     dData = sizeAircraftForRange(dData)
#     return dData['mission']['range']

# #%=============================================================================


# def getBatteryCruisePowerDraw(dData):
#     dData = sizeAircraft(dData)
#     eta_dis = dData['fuel']['eta_dis']
#     eta_mtr = dData['fuel']['eta_mtr']    
#     return dData['flightstates']['cruise']['pReq'] / eta_dis / eta_mtr / 1000.

# def getBatteryHoverPowerDraw(dData):
#     eta_dis = dData['fuel']['eta_dis']
#     eta_mtr = dData['fuel']['eta_mtr']
#     dData = sizeAircraft(dData)
#     return dData['flightstates']['hover']['pReq'] / eta_dis / eta_mtr / 1000.


# def getCruiseEnergy(dData):
#     dData = sizeAircraft(dData)
#     #return dData['flightstates']['cruise']['t'] * dData['flightstates']['cruise']['pReq'] / 3600. / 1000.
#     return dData['flightstates']['cruise']['eReq'] / 3600. / 1000.

# def getReserveEnergy(dData):
#     dData = sizeAircraft(dData)
#     #return dData['flightstates']['reserve']['t'] * dData['flightstates']['reserve']['pReq'] / 3600. / 1000.
#     return dData['flightstates']['reserve']['eReq'] * 0.9 / 3600. / 1000.

# def getHoverEnergy(dData):
#     dData = sizeAircraft(dData)
#     eta_dis = dData['fuel']['eta_dis']
#     eta_mtr = dData['fuel']['eta_mtr']
#     return dData['flightstates']['hover']['t'] * dData['flightstates']['hover']['pReq'] / eta_dis / eta_mtr / 3600. / 1000.
#     #return dData['flightstates']['hover']['eReq'] / 3600. / 1000.

#%=============================================================================
