#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 16:26:07 2019

@author: gu95pug
"""

import copy
from scipy import optimize
from .modMassesTier0 import getMassComponents

from .modAnalysisTier0 import calcPowers


#%%============================================================================
#    I N T E R F A C E   F U N C T I O N S 
#%=============================================================================

# potential improvement: integrate "info" dict to case and include variable to
# define the case type - i.e. - sizing for gross mass, range, payload?

# def getAircraftPayload(dData):
#     dData = sizeAircraftForPayload(dData)
#     return dData['masses']['useful'] - dData['mission']['massCrew']

# def getHoverPowerDistribution(dData):
#     dData = sizeAircraft(dData)
    
#     return dData['flightstate']['dgm']


# from source.modSizingTier0 import getAirframeMass, getFuelMass, getUsefulLoad
# from source.modSizingTier0 import getCruiseEnergy, getReserveEnergy, getHoverEnergy
# from source.modSizingTier0 import getBatteryCruisePowerDraw, getBatteryHoverPowerDraw


def getPowers(dData):
    dData = sizeAircraft(dData)
    return dData['analysis']['pHov'], dData['analysis']['pCrs']

def getCruisePower(dData):
    dData = sizeAircraft(dData)
    return dData['analysis']['pCrs'] / 1000.

def getHoverPower(dData):
    dData = sizeAircraft(dData)
    return dData['analysis']['pHov'] / 1000.

def getCruiseEnergy(dData):
    dData = sizeAircraft(dData)
    return dData['flightstates']['cruise']['pReq'] * dData['flightstates']['cruise']['t']

def getHoverEnergy(dData):
    dData = sizeAircraft(dData)
    return dData['flightstates']['hover']['pReq'] * dData['flightstates']['hover']['t']

def getReserveEnergy(dData):
    dData = sizeAircraft(dData)
    return dData['flightstates']['reserve']['pReq'] * dData['flightstates']['reserve']['t']

#%=============================================================================

def getAircraftMass(dData):
    dData = sizeAircraft(dData)
    return dData['masses']['dgm']

def getAirframeMass(dData):
    dData = sizeAircraft(dData)
    return dData['masses']['struct'] + dData['masses']['other'] + dData['masses']['ptrain']

def getFuelMass(dData):
    dData = sizeAircraft(dData)
    return dData['masses']['fuel']

def getUsefulLoad(dData):
    dData = sizeAircraft(dData)
    return dData['masses']['useful']

#%=============================================================================

def getAircraftComponents(dCase):
    dComponents = {}
    dCase = sizeAircraft(dCase)
    for k,v in dCase['masses'].items():
        if not k == 'dgm':
            dComponents[k] = float(v)
    return dComponents

def getPowertrainComponents(dCase):
    dPtr = {}
    dCase = sizeAircraft(dCase)
    for k,v in dCase['ptr'].items():
        dPtr[k] = float(v)
    return dPtr
  
    
#%%============================================================================
#    F L I G H T S T A T E   I N I T 
#%=============================================================================

def initFlightStates(dArgs):
    
    dArgs['analysis'] = {}
    dArgs = calcPowers(dArgs)

    dArgs['flightstates'] = {'hover':{'pReq': dArgs['analysis']['pHov'], 't': dArgs['mission']['hoverTime']},
                             'cruise':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['cruiseRange'] / dArgs['mission']['cruiseVelo']},
                             'reserve':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['timeReserve']},
                             }
    
    return dArgs

#%%============================================================================
#    S I Z I N G
#%=============================================================================

def precheckCase(initMass, dData):

    dCheck = copy.deepcopy(dData)
    dCheck['masses'] = {}
    dCheck['masses']['dgm'] = initMass
    dCheck = getMassComponents(dCheck)   
    fuelFraction = ( dCheck['masses']['fuel'] + dCheck['masses']['ptrain'] ) / initMass
    del dCheck

    sizingPossible = False
    if fuelFraction > 0.55 and fuelFraction <= 0.6:
        print('! WARNING: convergence might fail due to fuel weight requirement')
        # print('  -> Type: {} | Fuel: {} | PL: {:.3f}kN/kW  L/D: {:.2f}'.format(
        #     dData['design']['ident'], 
        #     dData['fuel']['fueltype'],
        #     dData['design']['pl'], 
        #     dData['design']['ltod'])) 
        sizingPossible = True
    elif fuelFraction > 0.6:
        print('! ERROR: I will not continue the sizing, it will fail due to fuel weight requirement')
        # print('  -> Type: {} | Fuel: {} | PL: {:.3f}kN/kW  L/D: {:.2f}'.format(
        #     dData['design']['ident'], 
        #     dData['fuel']['fueltype'],
        #     dData['design']['pl'], 
        #     dData['design']['ltod']))        
        sizingPossible = False
    else:
        sizingPossible = True
 
    return sizingPossible

#%%============================================================================

def objFuncSolveRange(rng, dArgs):
    
    dArgs['mission']['cruiseRange'] = rng
    
    # CALCULATE THE REQUIRED POWERS IN DISTINCT FLIGHTSTATES
    # dArgs['analysis'] = {}
    # dArgs = calcPowersNew(dArgs)

    # INITIALIZE FLIGHTSTATES
    # dArgs['flightstates'] = {'hover':{'pReq': dArgs['analysis']['pHov'], 't': dArgs['mission']['hoverTime']},
    #                           'cruise':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['cruiseRange'] / dArgs['mission']['cruiseVelo']},
    #                           #'reserve':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['timeReserve']},
    #                           }

    dArgs = initFlightStates(dArgs)
    dArgs = getMassComponents(dArgs)    

    struct = dArgs['masses']['struct']
    ptrain = dArgs['masses']['ptrain']
    fuel = dArgs['masses']['fuel']
    other = dArgs['masses']['other']   
    dgm = dArgs['masses']['dgm'] 
    pay = dArgs['masses']['useful'] 

    residual =  dgm - struct - ptrain - fuel - pay - other

    return residual

def findRange(dData):
    
    #print('#################################### at mass', dData['masses']['dgm'])
    startRange = 100000.
    res = 0.
    res, infodict, flagSolver, mesg = optimize.fsolve(objFuncSolveRange, startRange, args=dData, 
                                                      full_output=True, xtol=0.1, maxfev=100)     
         
    dData['mission']['cruiseRange'] = res[0]
    if flagSolver == 0: print(mesg)
    return dData

#%%============================================================================

def objFuncSolvePayload(pay, dArgs):
    
    dArgs['masses']['useful'] = pay
    
    # CALCULATE THE REQUIRED POWERS IN DISTINCT FLIGHTSTATES
    # dArgs['analysis'] = {}
    # dArgs = calcPowersNew(dArgs)

    # INITIALIZE FLIGHTSTATES
    # dArgs['flightstates'] = {'hover':{'pReq': dArgs['analysis']['pHov'], 't': dArgs['mission']['hoverTime']},
    #                           'cruise':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['cruiseRange'] / dArgs['mission']['cruiseVelo']},
    #                           #'reserve':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['timeReserve']},
    #                           }

    dArgs = initFlightStates(dArgs)
    dArgs = getMassComponents(dArgs)    

    struct = dArgs['masses']['struct']
    ptrain = dArgs['masses']['ptrain']
    fuel = dArgs['masses']['fuel']
    other = dArgs['masses']['other']   
    dgm = dArgs['masses']['dgm'] 
    #pay = dArgs['masses']['useful'] 

    residual =  dgm - struct - ptrain - fuel - pay - other

    return residual

def findPayload(dData):
    
    #print('#################################### at mass', dData['masses']['dgm'])
    startPayload = 4*88. + 80.
    res = 0.
    res, infodict, flagSolver, mesg = optimize.fsolve(objFuncSolvePayload, startPayload, args=dData, 
                                                      full_output=True, xtol=0.1, maxfev=100)     
         
    dData['masses']['useful'] = res[0]
    if flagSolver == 0: print(mesg)
    return dData

#%%============================================================================

def objFuncSolveMass(m, dArgs):
        
    dArgs['masses'] = {}
    dArgs['masses']['dgm'] = m

    dArgs = initFlightStates(dArgs)
    dArgs = getMassComponents(dArgs)    
    
    struct = dArgs['masses']['struct']
    ptrain = dArgs['masses']['ptrain']
    fuel   = dArgs['masses']['fuel']
    useful = dArgs['masses']['useful']
    other  = dArgs['masses']['other']
        
    residual =  m - struct - ptrain - fuel - useful - other
    return residual

def sizeAircraftCaller(dData):
    
    startDGW = (dData['mission']['massCrew'] + dData['mission']['massPayload']) / 0.2    
    
    res = 0.
    res, infodict, flagSolver, mesg = optimize.fsolve(objFuncSolveMass, startDGW, args=dData, full_output=True, xtol=0.1, maxfev=100)     
         
    #if dData['design']['ltod'] < 3.:
    #    print(startDGW)
     #   print('{:4.0f} {:3.1f} {:6.0f} {:2} {}'.format(dData['mission']['cruiseRange']/1000, dData['design']['ltod'], res[0], infodict['nfev'], infodict['r']))
    # print('=========')
    # print(dData['design']['ltod'], res[0])
    if res[0] == startDGW:
        if infodict['nfev'] > 3:
            dData['masses']['dgm'] = 9999.
    else:
        dData['masses']['dgm'] = res[0]
        # print(dData['analysis'])
        # print(dData['flightstates']['hover'])
        # print()
        # print(dData['flightstates']['cruise'])
        # print()
        # # print(dData['flightstates']['reserve'])
        # print()
        # sys.exit()
        
        # if dData['mission']['cruiseRange']/1000 < 100 and 7. < dData['design']['ltod'] < 11.:
        #     print('{:4.0f} {:4.1f} {:4.1f} {:4.1f} {:4.1f} {:4.1f}'.format(dData['mission']['cruiseRange']/1000, dData['design']['ltod'], 
        #                                                                     dData['flightstates']['hover']['pHES']/1000.,dData['flightstates']['hover']['pHPS']/1000.,
        #                                                                     dData['flightstates']['cruise']['pHES']/1000.,dData['flightstates']['cruise']['pHPS']/1000.))
                                                       
    if flagSolver == 0: print(mesg)
    return dData

def sizeAircraft(dData):
    
    #startDGW = (dData['mission']['massCrew'] + dData['mission']['massPayload']) / 0.2    
    #doSizing = precheckCase(startDGW, dData)
    doSizing = True
    
    if doSizing:
        dData = sizeAircraftCaller(dData)
        # res = 0.
        # res, infodict, flagSolver, mesg = optimize.fsolve(objFuncSolveMass, startDGW, args=dData, 
        #                                               full_output=True, xtol=0.1, maxfev=100)     
         
        # dData['masses']['dgm'] = res[0]
        # if flagSolver == 0: print(mesg)

    else:
        dData['masses'] = {}
        dData['masses']['dgm'] = None
    

    return dData


#%%============================================================================

# def objFuncSolvePayload(pay, dArgs):

#     dArgs['analysis'] = {}
#     dArgs = calcPowers(dArgs)

#     dArgs['flightstates'] = {'hover':{'pReq': dArgs['analysis']['pHov'], 't': dArgs['mission']['hoverTime']},
#                              'cruise':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['cruiseRange'] / dArgs['mission']['cruiseVelo']},
#                               #'reserve':{'pReq': dArgs['analysis']['pCrs'], 't': dArgs['mission']['timeReserve']},
#                               }
    
#     #dArgs['masses'] = {}
#     dArgs['masses']['useful'] = pay
#     dArgs = getMassComponents(dArgs)    

#     struct = dArgs['masses']['struct']
#     ptrain = dArgs['masses']['ptrain']
#     fuel = dArgs['masses']['fuel']
#     other = dArgs['masses']['other']   
#     dgm = dArgs['masses']['dgm'] 

#     residual =  dgm - struct - ptrain - fuel - pay - other
#     #print(dgm, struct, ptrain, fuel, pay, other, residual)

#     return residual

# def sizeAircraftForPayload(dData):
#     startPay = dData['mission']['massPayload'] 
#     res = 0.
#     res, infodict, flagSolver, mesg = optimize.fsolve(objFuncSolvePayload, startPay, args=dData, 
#                                                       full_output=True, xtol=0.1, maxfev=100)     
         
#     dData['masses']['useful'] = res[0]
#     if flagSolver == 0: print(mesg)
#     return dData
#####

# def getAircraftMassAndPower(dData):
#     dAircraft = sizeAircraft(dData)
#     dPower = getPowers(dAircraft)
#     return dAircraft['masses']['dgm'], dPower['pAvg']
 
# def sizeAndReturnPAvg(dData):
#     dAircraft = sizeAircraft(dData)
#     dPower = getPowers(dAircraft)
#     return dPower['pAvg'] / 1000.    
