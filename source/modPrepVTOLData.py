#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 11:45:06 2020

@author: nicolas
"""
import math 

grav = 9.80665
dens = 1.225

# collection of data (where available)

# VOLO | Volocopter.com # MTOM = 900 kg # Pay = 200 kg # OEW = 700 kg # Range = 35 km # Max. Airspeed = 110 km/0 = 30.6 m/s
# VOLO | evtol.news # Cruisespeed = 100 km/0 = 27.8 m/s # nProp = 18 # rProp = 2.3 m # 

# CITY | airbus.com # Cruisespeed = 120 km/H

# VAHANA | airbus.com # Cruisespeed = 190 km/H = 52.8 m/s

# CORA | airbus.com # Cruisespeed = 160 km/H = 44.4 m/s
# CORA | Bacchini # MTOM = 1124 kg

# JOBY S4 | WEIGHT ACCORDING TO ARTICLE ON EVTOL.COM

dVTOL = {'volo':    {'baseline':   {'mtom': 900., 'rProp': 2.3/2, 'nProp': 18., 'fm': 0.70},
                     'min':        {'mtom': 900., 'rProp': 2.3/2, 'nProp': 18., 'fm': 0.60},
                     'max':        {'mtom': 900., 'rProp': 2.3/2, 'nProp': 18., 'fm': 0.80}},
         'lilium':  {'baseline':   {'mtom': 2000., 'rProp': 0.33/2, 'nProp': 36., 'fm': 0.80}, # 0.9
                     'min':        {'mtom': 2200., 'rProp': 0.28/2, 'nProp': 36., 'fm': 0.85}, # 0.925
                     'max':        {'mtom': 1800., 'rProp': 0.38/2, 'nProp': 36., 'fm': 0.90}}, # 0.95
         'joby':    {'baseline':   {'mtom': 2177., 'rProp': 1.13, 'nProp': 6., 'fm': 0.70},    # 1.23
                     'min':        {'mtom': 2177., 'rProp': 1.08, 'nProp': 6., 'fm': 0.60},    # 1.18
                     'max':        {'mtom': 2177., 'rProp': 1.18, 'nProp': 6., 'fm': 0.80}},   # 1.28
         'city':    {'baseline':   {'mtom': 2200., 'rProp': 2.8/2, 'nProp': 8., 'fm': 0.70},
                     'min':        {'mtom': 2200., 'rProp': 2.8/2, 'nProp': 8., 'fm': 0.60},
                     'max':        {'mtom': 2200., 'rProp': 2.8/2, 'nProp': 8., 'fm': 0.80}},
         'vahana':  {'baseline':   {'mtom': 815., 'rProp': 0.85, 'nProp': 8., 'fm': 0.70},
                     'min':        {'mtom': 815., 'rProp': 0.80, 'nProp': 8., 'fm': 0.60},
                     'max':        {'mtom': 815., 'rProp': 0.90, 'nProp': 8., 'fm': 0.80}},         
         'cora':   {'baseline':   {'mtom': 979., 'rProp': 0.61,  'nProp': 12., 'fm': 0.70},
                    'min':        {'mtom': 1012., 'rProp': 0.56,  'nProp': 12., 'fm': 0.60},
                    'max':        {'mtom': 946., 'rProp': 0.65,  'nProp': 12., 'fm': 0.80}},
         'skai':   {'baseline':   {'mtom': 1500., 'rProp': 1.10,  'nProp': 6., 'fm': 0.70},
                    'min':        {'mtom': 1600., 'rProp': 0.95,  'nProp': 6., 'fm': 0.60},
                    'max':        {'mtom': 1400., 'rProp': 1.25,  'nProp': 6., 'fm': 0.80}},         
         }

     
# %FM city 0.8, joby 0.6, ehang 0.8, vahana 0,66, cora 0.7
   
def calcPL(DL, FM, RHO):
    return math.sqrt(2*RHO) / math.sqrt(DL) * FM

def calcDL(MTOM, R, N):
    return MTOM * grav / (math.pi * R**2 * N)

def getDL(IDENT, TYPE):
    return calcDL(dVTOL[IDENT][TYPE]['mtom'], dVTOL[IDENT][TYPE]['rProp'], dVTOL[IDENT][TYPE]['nProp'])

def getPL(IDENT, TYPE):
    DL = calcDL(dVTOL[IDENT][TYPE]['mtom'], dVTOL[IDENT][TYPE]['rProp'], dVTOL[IDENT][TYPE]['nProp'])
    return calcPL(DL, dVTOL[IDENT][TYPE]['fm'], dens)

#print(getDL('volo', 'baseline'))
print('\nVolocopter')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('volo', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('volo', 'min'), getPL('volo', 'baseline'), getPL('volo', 'max')))

print('\nLilium')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('lilium', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('lilium', 'min'), getPL('lilium', 'baseline'), getPL('lilium', 'max')))

print('\nJoby')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('joby', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('joby', 'min'), getPL('joby', 'baseline'), getPL('joby', 'max')))

print('\nCity Airbus')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('city', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('city', 'min'), getPL('city', 'baseline'), getPL('city', 'max')))

print('\nVahana')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('vahana', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('vahana', 'min'), getPL('vahana', 'baseline'), getPL('vahana', 'max')))

print('\nWisk Cora')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('cora', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('cora', 'min'), getPL('cora', 'baseline'), getPL('cora', 'max')))

print('\nSkai')
print('DL {:6.1f} N/m2 (baseline)'.format(getDL('skai', 'baseline')))
print('PL ({:3f}, ..., {:3f}, ..., {:3f}) N/W'.format(getPL('skai', 'min'), getPL('skai', 'baseline'), getPL('skai', 'max')))

#print(calcPL(718, 0.9, dens))




