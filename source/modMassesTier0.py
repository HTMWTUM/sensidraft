#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 10:45:56 2020

@author: Nicolas
"""


import math
import numpy as np
from .util import grav


#%%============================================================================

def weightAFDDengine(pHov, fEng):
    # One Engine, currently implemented following the Code of DW
    # AFDD Helo
    K0_eng = 0.0     # set from NDARC reference helicopter
    K1_eng = 0.252   # modified to match NDARC reference helicopter. Simplication necessary due to rudimentary fidelity of
                     # Daedalus engine model which overly simplifies derivation of Sea Level Standard equivalent power from
                     # power demand in sizing points at higher density altitudes.
    K2_eng = 0.0     # set from NDARC reference helicopter
    xhi_eng = 0.0    # set from NDARC reference helicopter
    P_mot = pHov / fEng / 745.7 # in shp, installed takeoff power (sea level static)        
    mEngine = (K0_eng + K1_eng * P_mot + K2_eng * math.pow(P_mot,xhi_eng) ) * 0.4536    
    return mEngine # in kg

#%%============================================================================

def getMassComponents(dData):
   
    dData['ptr'] = {}
   
    dFuel = dData['fuel'] 
    dPtr = dData['ptr']     
    
    dData['masses']['useful'] = dData['mission']['massPayload'] + dData['mission']['massCrew']    
    dData['masses']['struct'] = dData['design']['fStruct'] * float(dData['masses']['dgm'])
    dData['masses']['other'] = dData['design']['fOth'] * dData['masses']['useful']
    
    dData = sizeElecMotor(dData)
    #==============================================================================
    if dFuel['fueltype'] == 'battery':
        # >> PERFORM SIZING        
        dData = sizeBatterySystem(dData)
        # >> FILL DATA STRUCTURE    
        dPtr['elecMotor']   = dFuel['elecMotor']
        dPtr['battery']   = dFuel['massBatt']
        # >> FILL OUTPUT DATA VARIABLES           
        fuelmass    = dPtr['battery']
        powertrain  = 0.0
        #powertrain  = dPtr['elecMotor']
    #==============================================================================        
    elif dFuel['fueltype'] == 'fuelcell':
        pass
    #==============================================================================
    elif dFuel['fueltype'] == 'kerosene':

        sfc = dData['fuel']['sfc'] / 3600. / 1000.        # kg/Ws (from kg/kWh)
        # ---------------------------------------------------------------------
        m = dData['masses']['dgm']
        rCrs = dData['mission']['cruiseRange']
        tHov = dData['mission']['hoverTime']
        ltod = dData['design']['ltod']
        # pCrs = dData['analysis']['pCrs']
        pHov = dData['analysis']['pHov']
        rRes = dData['mission']['timeReserve'] * dData['mission']['cruiseVelo']
        
        fuelCrs = fuelmassCruiseRange(m, rCrs, ltod, sfc)
        fuelHov = fuelmassHoverTime(pHov, tHov, sfc)
        fuelRes = fuelmassCruiseRange(m, rRes, ltod, sfc) 
        # ---------------------------------------------------------------------
        fuelmass = fuelCrs + fuelHov  + fuelRes   
        
        # Unkown source
        # ptrain = 1.8 * ( powerHover(m, pl) / 745.7 )**0.9 * 0.4536 # with 745.7 W/hp and 0.4536 lb/kg
        # Hajek
        # transmission = 0.00000166 * (0.9 * m)**2 + 0.087780096 * m - 113.81241656
        # engines = 1.83 * ( 133.8 + 0.1156 * powerHover(m, pl) / 1000 )

        # AFDD Helo
        powertrain = 0.0
        #powertrain = weightAFDDengine(pHov, dData['fuel']['fEng']) * dData['fuel']['nEng']
        
#==============================================================================
    elif dFuel['fueltype'] == 'hybrid':
        # >> PERFORM SIZING        
        dData = sizeHybridFuelSystem(dData)
        # >> FILL DATA STRUCTURE    
        dPtr['elecMotor']   = dFuel['elecMotor']
        dPtr['powerBatt']   = dFuel['highPowSys']['massBatt']
        dPtr['tank']        = dFuel['highEnerSys']['massTank']
        dPtr['hydrogen']    = dFuel['highEnerSys']['massH2']
        dPtr['stackCell']   = dFuel['highEnerSys']['stack']        
        # >> FILL OUTPUT DATA VARIABLES           
        fuelmass    = dPtr['hydrogen'] + dPtr['tank'] + dPtr['powerBatt']
        powertrain  = dPtr['elecMotor'] + dPtr['stackCell']
    #==============================================================================
    elif dFuel['fueltype'] == 'hybridBattery':
        # >> PERFORM SIZING
        dData = sizeHybridBatterySytem(dData)
        # >> FILL DATA STRUCTURE    
        dPtr['elecMotor']   = dFuel['elecMotor']
        dPtr['powerBatt']   = dData['fuel']['highPowSys']['massBatt']
        dPtr['energyBatt']  = dData['fuel']['highEnerSys']['massBatt']        
        # >> FILL OUTPUT DATA VARIABLES           
        fuelmass    = dPtr['powerBatt'] + dPtr['energyBatt']
        powertrain  = dPtr['elecMotor']
                 
    dData['masses']['fuel'] = float(fuelmass)
    dData['masses']['ptrain'] = float(powertrain)
    dData['ptr'] = dPtr


    if 'info' in dData.keys():
        if 'debug' in dData['info'].keys():
            if dData['info']['debug'] == True:
                if dFuel['fueltype'] == 'hybrid':
                    print('==> HYDROGEN HYBRID: mH2 = {:.1f} | mTk = {:.1f} | mSt = {:.1f} | mBp = {:.1f} ] mMt = {:.1f} (kg)'.format(dPtr['hydrogen'], dPtr['tank'], dPtr['stackCell'], dPtr['powerBatt'], dPtr['elecMotor']))
                elif dFuel['fueltype'] == 'hybridBattery':
                    print('==> BATTERY HYBRID: mBe = {:.1f} | mBp = {:.1f} ] mMt = {:.1f} (kg)'.format(dPtr['energyBatt'], dPtr['powerBatt'], dPtr['elecMotor']))
                else:
                    print('No other debugging included for this fueltype | or you messed up the instantiation')
    
    return dData

#%%===========================================================================
def prepareHybridSystem(dData):
    """ Prepare hybrid fuel systems based on the flight states prescribed before.
    Currently flight state are defined in the solver routine, this could be done more advanced
    in the future...
    
    The Idea: based on the degree of hybridization, the ideal power distribution between
    elements of the hybrid powertrain is determined. This routine iterates through all 
    given flight states and determines for each, how much power is to be drawn from which
    power source (i. e. from the High Energy System (HES) and the High Power System (HPS))
    
    Parameters
    ----------
    dData : dictionary
        The data container.

    Returns
    -------
    dData : dictionary
        The data container.

    """    
    
    if dData['fuel']['calcOptHybr'] == True:
        dData['fuel']['fHybrid'] = 1. - dData['mission']['cruiseVelo'] * dData['design']['pl'] / dData['design']['ltod']
    else:
        print('Should have a prescribed fOpt then.... Do you? --> Check!!!')
        pass

    # determine the aircrafts hover and cruise power requirement
    pCrs = dData['analysis']['pCrs']
    pHov = dData['analysis']['pHov']
    
    # calc the maximum power to be delivered by the high energy system
    # think this is mostly (always? --> check) equal to the cruise power requirement
    pMaxHES = max(pCrs, pHov) * (1 - dData['fuel']['fHybrid'])  
    # pMaxHES = pCrs 

    for kState, dState in dData['flightstates'].items():
        
        # high energy system is enough to power the state
        if pMaxHES >= dState['pReq']:
            dState['pHES'] = dState['pReq']
            dState['pHPS'] = 0.0
        # high power system and high energy system must be used
        else:
            dState['pHES'] = pMaxHES
            dState['pHPS'] = dState['pReq'] - pMaxHES    
    
    return dData

  
#%%===========================================================================
def sizeHybridFuelSystem(dData):

    dData = prepareHybridSystem(dData)
    
    dBattery = dData['fuel']['highPowSys']
    dFuelCell = dData['fuel']['highEnerSys']
    
    for kState, dState in dData['flightstates'].items():

        # HES | THE AMOUNT OF ENERGY STORED IN HYDROGEN FOR THE FLIGHT STATE
        dState['eHydrogen'] = getFuelEnergy(dState['pHES'], dState['t'], dFuelCell)
        #print('___ in State', kState, 'eHydrogen is', dState['eHydrogen'] / 3600. / 1000. )
        
        # HES | THE MASS OF THE FUEL STACK TO DELIVER THE AMOUNT OF POWER IN EACH FLIGHT STATE
        dState['mFuelStack'] = dState['pHES'] / dFuelCell['pSpec'] / dFuelCell['eta_mtr']
        #print(dState['mFuelStack'], dState['pHES'], dFuelCell['pSpec'] )

        #print('{:8} HES Req: {:5.1f} kWh (based on P,t) {:5.1f} kg (Stack)'.format(kState,dState['eHydrogen']/1000./3600.,dState['mFuelStack']))        
        #print('### after State the hydrogen energy is', dFuelCell['energy'])
        
        # =====================================================================
        
        # HPS | THE POWER TO BE DELIVERED BY THE POWER BATTERY IN EACH STATE, EITHER BY POWER OR ENERGY REQUIREMENT --> deprecated because i think its actually wrong
        #dState['ePower'] = max(getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dBattery),
        #                       getHighPowerBatteryEnergy(dState['pHPS'], dBattery))
        
        # HPS | THE POWER TO BE DELIVERED BY THE POWER BATTERY IN EACH STATE BY ENERGY REQUIREMENT
        dState['HPScapaForE'] = getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dBattery)
        # HPS | THE POWER TO BE DELIVERED BY THE POWER BATTERY IN EACH STATE BY POWER REQUIREMENT
        dState['HPScapaForP'] = getHighPowerBatteryEnergy(dState['pHPS'], dBattery)   
        
        #dState['ePower'] = getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dBattery)
        #print('{:8} HPS Req: {:5.1f} kWh (based on P,t) {:5.1f} kWh (based on P,C)'.format(kState, 
        #                                                           getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dBattery)/1000./3600.,
        #                                                           getHighPowerBatteryEnergy(dState['pHPS'], dBattery)/1000./3600.))

    # HPS | THE POWER REQUIREMENT AS A SUM OF ALL FLIGHTSTATES
    HPSallStatesCapaForE = float(sum(d['HPScapaForE'] for d in dData['flightstates'].values() if d))
    # HPS | THE POWER REQUIREMENT AS THE MAXIMUM OF THE FLIGHTSTATES
    HPSallStatesCapaForP = float(max(d['HPScapaForP'] for d in dData['flightstates'].values() if d))
    # HPS | WHATEVER IS BIGGER 
    dBattery['eStored'] = max(HPSallStatesCapaForE, HPSallStatesCapaForP)     
    dBattery['massBatt'] = float(dBattery['eStored'] / dBattery['esp'] / 3600.)  
    #dBattery['eStored'] = float(sum(d['ePower'] for d in dData['flightstates'].values() if d))  
    
    # HES | THE AMOUNT OF ENERGY STORED IN HYDROGEN FOR ALL FLIGHT STATES 
    dFuelCell['energy'] = sum(dState['eHydrogen'] for dState in dData['flightstates'].values() if dState)    
    dFuelCell['massH2'] = getFuelMass(dFuelCell)
    dFuelCell['massTank'] = dFuelCell['massH2'] * (1 / dFuelCell['fractionH2T'] - 1) 
   
    # HES | THE MASS OF THE FUEL STACK TO DELIVER THE AMOUNT OF POWER IN EACH FLIGHT STATE
    dFuelCell['stack'] = float(max(d['mFuelStack'] for d in dData['flightstates'].values() if d))


    return dData

#%%===========================================================================

# DO THIS WITH THE NEW APPROACH

# def sizeBatterySystem(dData):

#     dData['energy'] = {}
#     dBattery = dData['fuel']

#     for kState, dState in dData['flightstates'].items():
        
#     # Ansatz falsch, glaube ich. nochmal anschauen.
#          dState['eReq'] = max(getHighEnerBatteryEnergy(dState['pReq'], dState['t'], dBattery),
#                               getHighPowerBatteryEnergy(dState['pReq'], dBattery))
#          #dState['eReq'] = getHighEnerBatteryEnergy(dState['pReq'], dState['t'], dBattery)
#          #print('vCrs', dData['mission']['cruiseVelo'], 'Preq:', dState['pReq']/1000., 'tState', dState['t'])
#          #print(kState, dState['eReq'], '  of ', getHighEnerBatteryEnergy(dState['pReq'], dState['t'], dBattery)/1000., getHighPowerBatteryEnergy(dState['pReq'], dBattery)/1000.)
    
#     dBattery['eStored'] = float(sum(d['eReq'] for d in dData['flightstates'].values() if d))
#     dBattery['massBatt'] = float(dBattery['eStored'] / dBattery['esp'] / 3600.)

#     return dData

#%%===========================================================================
def sizeHybridBatterySytem(dData):
    
    prepareHybridSystem(dData)

    dPowerBat = dData['fuel']['highPowSys']
    dEnergyBat = dData['fuel']['highEnerSys']
    
    for kState, dState in dData['flightstates'].items():

        if not dState['t'] == 0.:
            
            # HIGH ENERGY SYSTEM
            #dState['eEnergy'] = max(getHighEnerBatteryEnergy(dState['pHES'], dState['t'], dEnergyBat),
            #                        getHighPowerBatteryEnergy(dState['pHES'], dEnergyBat))
            
            dState['HEScapaForE'] = getHighEnerBatteryEnergy(dState['pHES'], dState['t'], dEnergyBat)
            dState['HEScapaForP'] = getHighPowerBatteryEnergy(dState['pHES'], dEnergyBat)
            # print('{:8} HES Req: {:5.1f} kWh (based on P,t) {:5.1f} kWh (based on P,C)'.format(kState, 
            #                                                           getHighEnerBatteryEnergy(dState['pHES'], dState['t'], dEnergyBat)/1000./3600.,
            #                                                           getHighPowerBatteryEnergy(dState['pHES'], dEnergyBat)/1000./3600.))
            
            # HIGH POWER SYSTEM
            #dState['ePower'] = max(getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dPowerBat),
            #                       getHighPowerBatteryEnergy(dState['pHPS'], dPowerBat))
            
            dState['HPScapaForE'] = getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dPowerBat)
            dState['HPScapaForP'] = getHighPowerBatteryEnergy(dState['pHPS'], dPowerBat)
            # print('{:8} HPS Req: {:5.1f} kWh (based on P,t) {:5.1f} kWh (based on P,C)'.format(kState, 
            #                                                           getHighEnerBatteryEnergy(dState['pHPS'], dState['t'], dPowerBat)/1000./3600.,
            #                                                           getHighPowerBatteryEnergy(dState['pHPS'], dPowerBat)/1000./3600.))
               
        else:
            #dState['eEnergy'] = 0.0
            #dState['ePower'] = 0.0
            dState['HEScapaForE'] = 0.0
            dState['HEScapaForP'] = 0.0
            dState['HPScapaForE'] = 0.0
            dState['HPScapaForP'] = 0.0
            
    # HPS | THE POWER REQUIREMENT AS A SUM OF ALL FLIGHTSTATES
    HESallStatesCapaForE = float(sum(d['HEScapaForE'] for d in dData['flightstates'].values() if d))
    HPSallStatesCapaForE = float(sum(d['HPScapaForE'] for d in dData['flightstates'].values() if d))
    # HPS | THE POWER REQUIREMENT AS THE MAXIMUM OF THE FLIGHTSTATES
    HESallStatesCapaForP = float(max(d['HEScapaForP'] for d in dData['flightstates'].values() if d))
    HPSallStatesCapaForP = float(max(d['HPScapaForP'] for d in dData['flightstates'].values() if d))
    # HPS | WHATEVER IS BIGGER 
    dEnergyBat['eStored'] = max(HESallStatesCapaForE, HESallStatesCapaForP)
    dPowerBat['eStored'] = max(HPSallStatesCapaForE, HPSallStatesCapaForP)
    
    dPowerBat['massBatt'] = float(dPowerBat['eStored'] / dPowerBat['esp'] / 3600.)
    dEnergyBat['massBatt'] = float(dEnergyBat['eStored'] / dEnergyBat['esp'] / 3600.)
        
    return dData

#%%============================================================================
def sizeBatterySystem(dData):

    # NOT VERIFIED SO FAR
    
    pCrs = dData['analysis']['pCrs']
    pHov = dData['analysis']['pHov']
    # pMax = max(pCrs, pHov)
    
    dBat = dData['fuel']
    
    for kState, dState in dData['flightstates'].items():

        if not dState['t'] == 0.:
            
            # HIGH ENERGY SYSTEM
            #dState['eEnergy'] = max(getHighEnerBatteryEnergy(dState['pHES'], dState['t'], dEnergyBat),
            #                        getHighPowerBatteryEnergy(dState['pHES'], dEnergyBat))
            
            # dState['capaForE'] = getHighEnerBatteryEnergy(pCrs, dState['t'], dBat)
            dState['capaForE'] = getHighEnerBatteryEnergy(dState['pReq'], dState['t'], dBat)
            #print('   >> {} {}'.format(kState, dState['capaForE']))
            #print('   ' , kState, pCrs/1000., pHov/1000., dState['pReq']/1000.)
            #dState['capaForP'] = getHighPowerBatteryEnergy(pHov, dBat)
            # print('{:8} HES Req: {:5.1f} kWh (based on P,t) {:5.1f} kWh (based on P,C)'.format(kState, 
            #                                                           getHighEnerBatteryEnergy(dState['pHES'], dState['t'], dEnergyBat)/1000./3600.,
            #                                                           getHighPowerBatteryEnergy(dState['pHES'], dEnergyBat)/1000./3600.))
            
        else:
            #dState['eEnergy'] = 0.0
            #dState['ePower'] = 0.0
            dState['capaForE'] = 0.0
            #dState['capaForP'] = 0.0
    
    capaForE = float(sum(d['capaForE'] for d in dData['flightstates'].values() if d))
    #capaForP = float(max(d['capaForP'] for d in dData['flightstates'].values() if d))



    #dBat['eStored'] = max(capaForE, capaForP)
    dBat['eStored'] = capaForE
    dBat['massBatt'] = float(dBat['eStored'] / dBat['esp'] / 3600.)
    
    #print('{:.4f} {:5.0f} {}'.format(dData['design']['pl'], dData['analysis']['pHov']/1000., dBat['massBatt']))

    
    return dData

#%%============================================================================

def getPeukertTime(tReq, dBattery):
    rBat = 3600.
    nBat = dBattery['dcp']    
    return rBat * ( tReq / rBat ) ** (1/nBat)

def getHighEnerBatteryEnergy(pReq, tReq, dBattery): # supposed to return Ws
    eta_dis = dBattery['eta_dis']
    eta_mtr = dBattery['eta_mtr']
    fDoD = dBattery['fDoD']
    time = getPeukertTime(tReq, dBattery)
    #print(time/3600, tReq/3600)
    #print(pReq/1000 , eta_dis , eta_mtr , fDoD , tReq, time, (pReq / eta_dis / eta_mtr / fDoD * time/3600/1000))
    return pReq / eta_dis / eta_mtr / fDoD * time
    
def getHighEnerBatteryTime(pReq, eEner, dBattery):
    eta_dis = dBattery['eta_dis']
    eta_mtr = dBattery['eta_mtr']
    rBat = 3600.
    nBat = dBattery['dcp']    
    return rBat * (eEner / pReq * eta_dis * eta_mtr / rBat) ** nBat 

def getHighPowerBatteryEnergy(pReq, dBattery): #supposed to return in Ws
    #print('in:', pReq, 'W')
    eta_dis = dBattery['eta_dis']
    eta_mtr = dBattery['eta_mtr']
    cRate = dBattery['cRate']
    #print('out', pReq / eta_dis / eta_mtr / cRate, 'Wh')
    return pReq / eta_dis / eta_mtr / cRate * 3600.
    
#%%===========================================================================
def getFuelEnergy(pReq, tReq, dFuel):
    if 'sfc' in dFuel.keys():
        dFuel['energy'] = pReq * tReq
    elif 'eta_fc' in dFuel.keys():
        eta_fc = dFuel['eta_fc']
        eta_mtr = dFuel['eta_mtr']
        dFuel['energy'] = pReq / eta_fc / eta_mtr * tReq 
        #print('___', pReq, eta_fc, eta_mtr, tReq, dFuel['energy'] )
    return dFuel['energy']

def getFuelMass(dFuel):
    if dFuel['fueltype'] == 'kerosene':
        dFuel['mass'] = dFuel['sfc'] * dFuel['energy']
    elif dFuel['fueltype'] == 'fuelcell':
        #print('FUELCALC', dFuel)
        dFuel['mass'] = dFuel['energy'] / dFuel['lhv'] / 3600. / 1000.
    else:
        print(dFuel['fueltype'])
    return dFuel['mass']

#%%===========================================================================

def sizeElecMotor(dData):
    pCrs = dData['analysis']['pCrs']
    pHov = dData['analysis']['pHov']
    dData['fuel']['elecMotor'] = float(motorModelNg(max(pHov, pCrs)))
    return dData

def motorModelNg(pReq): # based on state of the art electric motors for aviation
    return np.exp(-0.89 + 0.89 * np.log(pReq / 1000.))

def motorModelSinsay(pReq):
    return 1.96 * ( pReq /1000. )**0.8997 * 0.4536 

def motorModelPatterson(pReq):
    # from Patterson, 2012: 3hp/lb (most simplistic and quite optimistic assumption)
    return 1000 * pReq / 4931.97 # 4931.97 W/kg

#%%===========================================================================

# return C (As) required to cruise the maximum range at specified Velocity
# based on formulas (18) and (12) by Lance W. Traub, 2011
def capacityCruiseRange(mass, power, tCrs, voltage, nBat, rBat, eta): 
    return power / eta / voltage * rBat * ( tCrs / rBat ) ** (1/nBat)

def getBatteryMassSegment(pReq, tReq, dBattery):
    eta_dis = dBattery['eta_dis']
    eta_mtr = dBattery['eta_mtr']
    eSpec = dBattery['esp']
    rBat = 3600.
    nBat = dBattery['dcp']
    #capaBattery = pReq / (eta_dis + eta_mtr) / voltage * rBat * ( tReq / rBat ) ** (1/nBat) 
    #massBattery = capaBattery * voltage / eSpec
    #massBatterySeg = pReq / (eta_dis + eta_mtr) / eSpec * rBat * ( tReq / rBat ) ** (1/nBat) / 3600.
    massBatterySeg = pReq / eta_dis / eta_mtr / eSpec * rBat * ( tReq / rBat ) ** (1/nBat) / 3600.
    return massBatterySeg

def getBatteryMassFlightstate(pReq, dBattery):
    eta_dis = dBattery['eta_dis']
    eta_mtr = dBattery['eta_mtr']
    cRate = dBattery['cRate']
    eSpec = dBattery['esp']
    #capaBattery = pReq / (eta_dis + eta_mtr) / voltage / cRate
    #massBattery = capaBattery * voltage / eSpec
    massBatteryState = pReq / eta_dis / eta_mtr / cRate / eSpec 
    return massBatteryState

def getFuelMassTime(pReq, tReq, dFuel):
    if 'sfc' in dFuel.keys():
        sfc = dFuel['sfc']
        massFuelTime = pReq * sfc * tReq
    elif 'eta_fc' in dFuel.keys():
        lhv = dFuel['lhv']
        eta_fc = dFuel['eta_fc']
        eta_mtr = dFuel['eta_mtr']
        massFuelTime = pReq / lhv / eta_fc / eta_mtr * tReq / 3600. / 1000.
    return massFuelTime

# return C (As) required to hover the hovertime out of ground effect
# based on formula (7) by Lance W. Traub, 2011
def capacityHoverTime(mass, power, tHov, voltage, nBat, rBat, eta, cMax): 
    capEnergy = power / eta / voltage * rBat * (tHov / rBat) ** (1/nBat)
    return capEnergy

def capacityCRate(power, voltage, cRate):
    return power / voltage / cRate

# return fuel mass m (kg) to cruise the maximum range
# comment: as this is for power shaft engine, the velocity doesn't appear here 
def fuelmassCruiseRange(mass, rCrs, ltod, sfc):
    exponent = np.exp( rCrs * grav * sfc / ltod)
    if exponent > (10**5):
        print('Warning! Breguet Exponent exceeds 10⁵ at m = ', mass)
    fuelCruise = mass * (1. - 1./exponent) 
    return fuelCruise

# return fuel mass m (kg) to hover the hovertime out of ground effect
# comment: as this is for power shaft engine, the velocity doesn't appear here 
def fuelmassHoverTime(power, tHov, sfc):
    return power * sfc * tHov

def massStructure(mass, fStruct, useful):
    structure = fStruct * mass 
    return structure
    
def massOther(mass, fOth, useful):
    # --- approach to size other mass by payload + crew
    other = fOth * useful
    # --- model proposed by Ng & Datta following Harris: 30% of empty weight    
    #other = 0.3 * (mass - useful)
    return other

# return fuel mass m (kg) to cruise the maximum range
# comment: as this is for power shaft engine, the velocity doesn't appear here 
def fuelmassCruiseRange(mass, rCrs, ltod, sfc):
    # -------------------------------------------------------------------------
    exponent = np.exp( rCrs * grav * sfc / ltod)
    if exponent > (10**5):
        print('Warning! Breguet Exponent exceeds 10⁵ at m = ', mass)
    fuelCruise = mass * (1. - 1./exponent) 
    return fuelCruise

# return fuel mass m (kg) to hover the hovertime out of ground effect
# comment: as this is for power shaft engine, the velocity doesn't appear here 
def fuelmassHoverTime(power, tHov, sfc):
    return power * sfc * tHov  

#def fuelmassHoverTime(mass, rCrs, vCrs, fHov, pl, sfc):
#    # -------------------------------------------------------------------------
#    power = powerHover(mass, pl)
#    tHov = timeHover(rCrs, vCrs, fHov)
#    return power * sfc * tHov
     
