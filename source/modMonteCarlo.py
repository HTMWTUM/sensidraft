#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 10:41:58 2020

@author: Nicolas
"""

import copy

def scalarFunMonteCarlo(fun, dData, dSamples, iS):
    """ Monte Carlo Analysis of a scalar function.
    
    Parameters
    ----------
    fun : scalar function
        The function to be evaluated in the Monte Carlo Analyis
    dData : dictionary
        The data container.
    dSamples : dictionary
        Sample container. Keys must be identical to dData.
    iS : integer
        Number of samples in Monte Carlo Analysis

    Returns
    -------
    mcResults : list
        List of scalar return values
    mcFactDesign : list of dictionaries
        List of dictionaries, representing the experimental design (factorial)

    """     
    # important: make a deepcopy of dData as variation is not reverted
    dMutable = copy.deepcopy(dData)
    
    mcResults = []
    mcFactDesign = []
    
    for i in range(iS):
        experiments = []
        for k0, s0 in dSamples.items():
            if type(s0) == dict:
                for k1, s1 in s0.items():
                
                    if type(s1) is dict:
                        for k2, s2 in s1.items():
                            #print('here we go for:', i, k0, k1, k2, s2[i])
                            dMutable[k0][k1][k2] = s2[i]
                            #tSample = (k0,k1,k2,s2[i])
                            experiments.append((k0,k1,k2,s2[i]))
                            
                            #dFactorial = (i, k0, k1, k2, s2[i])
                            #mcFactDesign.append(dFactorial) 
                        #mcResults.append(fun(dMutable))
                    else:
                        #print('no thrid layer dict:', i, k0, k1, s1[i])
                        dMutable[k0][k1] = s1[i]                  
                        #tSample = (k0,k1,s1[i])
                        experiments.append((k0,k1,s1[i]))
    
                    #experiments.append(tSample)
                    #del tSample
            else:
                dMutable[k0] = s0[i]
                experiments.append((k0,s0[i]))
        mcFactDesign.append((i, experiments))
        #print() # handy way to compare what is original (dData) and factoral (dMutable)
        #[print(i, k,v) for k,v in dData.items()]
        #[print(i, k,v) for k,v in dMutable.items()]
        #print()
        mcResults.append(fun(dMutable))
    
    return mcResults, mcFactDesign

def vectFunMonteCarlo(funcs, dData, dSamples, iS):
    """ Monte Carlo Analysis of a vector function. (Simple adaption of scalar as of now)
    
    """     
    # important: make a deepcopy of dData as variation is not reverted
    dMutable = copy.deepcopy(dData)
    mcFactDesign = []
    vecResults = []
    
    for fun in funcs:
        
        mcResults = []
        
        for i in range(iS):
            experiments = []
            for k0, s0 in dSamples.items():
                for k1, s1 in s0.items():
                
                    if type(s1) is dict:
                        for k2, s2 in s1.items():
                            #print('here we go for:', i, k0, k1, k2, s2[i])
                            dMutable[k0][k1][k2] = s2[i]
                            #tSample = (k0,k1,k2,s2[i])
                            experiments.append((k0,k1,k2,s2[i]))
                            
                            #dFactorial = (i, k0, k1, k2, s2[i])
                            #mcFactDesign.append(dFactorial) 
                        #mcResults.append(fun(dMutable))
                    else:
                        #print('no thrid layer dict:', i, k0, k1, s1[i])
                        dMutable[k0][k1] = s1[i]                  
                        #tSample = (k0,k1,s1[i])
                        experiments.append((k0,k1,s1[i]))
    
                    #experiments.append(tSample)
                    #del tSample
            mcFactDesign.append((i, experiments))
            #print() # handy way to compare what is original (dData) and factoral (dMutable)
            #[print(i, k,v) for k,v in dData.items()]
            #[print(i, k,v) for k,v in dMutable.items()]
            #print()
            mcResults.append(fun(dMutable))
        
        vecResults.append(mcResults)
    
    return vecResults, mcFactDesign


            # if type(v1) is dict:
            #     #print(k0, k1, 'holds a dict')
            #     dResult[k0][k1] = {}
            #     for k2, v2 in v1.items():
            #         #print('\n',k2,v2)
            #         #print('values are', v2)
            #         listRes = []
            #         for i, value in enumerate(v2):
            #             baseline = dCase[k0][k1][k2]
            #             dCase[k0][k1][k2] = value
            #             listRes.append(fun(dCase))
            #             dCase[k0][k1][k2] = baseline
            #             #if dCase['fuel']['fueltype'] == 'battery': print(listRes[-1])
            #         #print(k0,k1,k2, listRes)
            #         dResult[k0][k1][k2] = listRes                        
            
            # else:
            #     #print(k0, k1, 'doesnt hold a dict')
            #     listRes = []
            #     for i, value in enumerate(v1):
            #         baseline = dCase[k0][k1]
            #         dCase[k0][k1] = value
            #         listRes.append(fun(dCase))
            #         dCase[k0][k1] = baseline
            #         #if dCase['fuel']['fueltype'] == 'battery': print(listRes[-1])
            #     #print(k0,k1,listRes)
            #     dResult[k0][k1] = listRes 
            
# # important: make a deepcopy of dData as variation is not reverted
# dMutable = copy.deepcopy(dData)

# mcResults = []
# mcFactDesign = []

# for i in range(iS):
    
#     for k0, s0 in dSamples.items():
#         for k1, s1 in s0.items():
#             dMutable[k0][k1] = s1[i]
#             dFactorial = (i, k0, k1, s1[i])
#             mcFactDesign.append(dFactorial)  
            
#     mcResults.append(fun(dMutable))
    
#     #print(mcResults[i], mcFactDesign[i])

# return mcResults, mcFactDesign