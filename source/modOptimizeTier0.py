#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 12:19:38 2020

@author: Nicolas
"""

from .modSizingTier0 import getAircraftMass, sizeAircraftCaller
from .modAssessmentTier0 import getAircraftImpactTotal

def calc_ltod(pl):
    
    LtoD_min = 4.         # y2
    LtoD_max = 16.0      # y1
    
    PL_min = 0.015         # x1
    PL_max = 0.050         # x2

    slope = (LtoD_min - LtoD_max) / (PL_max - PL_min)

    offset = (LtoD_max * PL_max - LtoD_min * PL_min) / (PL_max - PL_min)   
    
    return slope * pl + offset

def optAircraftMass(plInit, dData):
    
    dData['design']['pl'] = plInit
    dData['design']['ltod'] = calc_ltod(dData['design']['pl'])    
    #print(dData['design']['ltod'], type(dData['design']['ltod']))
    
    #dgm = getAircraftMass(dData)
    dgm = sizeAircraftCaller(dData)['masses']['dgm']
    return dgm


def optAircraftImpact(plInit, dData):
      
    dData['design']['pl'] = plInit
    dData['design']['ltod'] = calc_ltod(dData['design']['pl'])    
    
    #print('--- opt ---', dData['design']['ltod'], dData['design']['pl'])
    #impact = getAircraftImpactTotal(dData['optFunction'], dData)
    impact = getAircraftImpactTotal(dData, True)

    return impact

