#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 17:35:33 2020

@author: Nicolas
"""

grav = 9.80665

def loadPlotFonts():

    import matplotlib.pyplot as plt
    
    XSMALL = 6
    SMALL_SIZE = 8
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 12
    
    plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')  


def getTime():
    """Return the current time as a string for output creation
    
    Returns
    -------
    strtime : str
        The current timestamp in YY_MM_DD_HR_MI
    """
    
    import time
    YEAR, MONTH, DAY, HOUR, MIN = map(int, time.strftime("%Y %m %d %H %M").split())
    strTime = str(YEAR)[2:] + '_' + str(MONTH) + '_' + str(DAY) + '_' + str(HOUR) + str(MIN)
    return strTime

def printCase(dic):
    print()
    for k0, v0 in dic.items():
        if not type(v0) == dict:
            print('>>> {} {}'.format(k0,v0))
        else:
            print('### {}'.format(k0))
            for k1, v1 in v0.items():
                if not type(v1) == dict:
                    #print(' >>> {:15} {:>10}'.format(k1,v1))
                    print(k1,v1)
                else:
                    for k2,v2 in v1.items():
                        print(' >>> {} {}'.format(k2,v2))
    

def LoD_to_DoL(lod):
    """Sort a list of dictionaries to a dictionary of lists
    
    Parameters
    ----------
    lod : str
        The list of dictionaries. 
        Dictionaries must have identical keys

    Returns
    -------
    dol : list
        a list of strings used that are the header columns
    """
    
    dol = {}
    for k,v in {k: [dic[k] for dic in lod] for k in lod[0]}.items():
        dol[k] = v    
    return dol

def lengthRecursiveDict(dic):
    lenDic = 0
    for v0 in dic.values():
        lenDic += len(v0)   
    return lenDic


def loadTUMcolors():
    """Allow user to use colors according to TUM-CI scheme
    
    Usable colors
    -------------
    tuple
        TUMdarkest100 = (0/255, 51/255, 89/255)
        TUMdarkest80 = (51/255, 92/255, 122/255)
        TUMdarkest50 = (128/255, 153/255, 172/255)
        TUMdarkest20 = (204/255, 214/255, 222/255)
        
        TUMdarker100 = (0/255, 82/255, 147/255)
        TUMdarker80 = (51/255, 117/255, 169/255)
        TUMdarker50 = (128/255, 169/255, 201/255)
        TUMdarker20 = (204/255, 220/255, 233/255)
        
        TUMlighter100 = (100/255, 160/255, 200/255)
        TUMlighter80 = (131/255, 179/255, 211/255)
        TUMlighter50 = (178/255, 207/255, 228/255)
        TUMlighter20 = (224/255, 236/255, 244/255)
        
        TUMlightest100 = (152/255, 198/255, 234/255)
        TUMlightest80 = (173/255, 209/255, 238/255)
        TUMlightest50 = (203/255, 227/255, 244/255)
        TUMlightest20 = (234/255, 244/255, 251/255)
        
        TUMpptGreen = (162/255, 173/255, 0/255)
        TUMpptOrange = (227/255, 114/255, 34/255)
        TUMpptBlue = (0/255, 115/255, 207/255)
        TUMpptGray = (51/255, 51/255, 51/255)
        

    """
    
    TUMcolors = {}
    
    TUMcolors['TUMdarkest100'] = (0/255, 51/255, 89/255)
    TUMcolors['TUMdarkest80'] = (51/255, 92/255, 122/255)
    TUMcolors['TUMdarkest50'] = (128/255, 153/255, 172/255)
    TUMcolors['TUMdarkest20'] = (204/255, 214/255, 222/255)
    
    TUMcolors['TUMdarker100'] = (0/255, 82/255, 147/255)
    TUMcolors['TUMdarker80'] = (51/255, 117/255, 169/255)
    TUMcolors['TUMdarker50'] = (128/255, 169/255, 201/255)
    TUMcolors['TUMdarker20'] = (204/255, 220/255, 233/255)
    
    TUMcolors['TUMlighter100'] = (100/255, 160/255, 200/255)
    TUMcolors['TUMlighter80'] = (131/255, 179/255, 211/255)
    TUMcolors['TUMlighter50'] = (178/255, 207/255, 228/255)
    TUMcolors['TUMlighter20'] = (224/255, 236/255, 244/255)
    
    TUMcolors['TUMlightest100'] = (152/255, 198/255, 234/255)
    TUMcolors['TUMlightest80'] = (173/255, 209/255, 238/255)
    TUMcolors['TUMlightest50'] = (203/255, 227/255, 244/255)
    TUMcolors['TUMlightest20'] = (234/255, 244/255, 251/255)
    
    TUMcolors['TUMpptGreen'] = (162/255, 173/255, 0/255)
    TUMcolors['TUMpptOrange'] = (227/255, 114/255, 34/255)
    TUMcolors['TUMpptBlue'] = (0/255, 115/255, 207/255)
    TUMcolors['TUMpptGray'] = (51/255, 51/255, 51/255)
    
    TUMcolors['NewGreenDarker'] = (108/255, 133/255, 70/255)
    TUMcolors['NewGreenLighter'] = (152/255, 182/255, 110/255)

    TUMcolors['HyLevio_Blue'] =    (33/255, 84/255, 105/255)
    TUMcolors['HyLevio_Dark1'] =   (32/255, 32/255, 32/255)
    TUMcolors['HyLevio_Light2'] =  (255/255, 251/255, 251/255)
    TUMcolors['HyLevio_Accent1'] = (17/255, 141/255, 146/255)
    TUMcolors['HyLevio_Accent2'] = (0/255, 73/255, 84/255)
    TUMcolors['HyLevio_Accent3'] = (193/255, 195/255, 203/255)
    TUMcolors['HyLevio_Accent4'] = (94/255, 92/255, 109/255)
    TUMcolors['HyLevio_Accent5'] = (209/255, 199/255, 178/255)
    TUMcolors['HyLevio_Accent6'] = (219/255, 182/255, 141/255)
    return TUMcolors
