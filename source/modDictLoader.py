#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 18:29:50 2020

@author: Nicolas
"""

def loadCostData():
    
    costs = {}
   
    meanUSDtoEUR = 0.8925 # https://www.poundsterlinglive.com/best-exchange-rates/best-us-dollar-to-euro-history-2019
   
    ########################################################################## 
   
    # European Comission / EUROSTAT
    # ELECTRICITY PRICES FOR INDUSTRY IN EU COUNTRIES
    # https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Electricity_price_statistics
    electricityEU28_2019_S2_avg_exTax = 0.0794   # € / kWh # exlcuding taxes and levies (but including energy generation, supply and network)
    #electricityEU28_2019_S2_avg_exVAT = 0.0836   # € / kWh # excluding VAT and recoverable taxes and levies
    electricityEU28_2019_S2_avg_incTax = 0.1473  # € / kWh # including all taxes and levies  
  
    # ELECTRICITY GRID FEE
    #electricityEU28_2015_gridFee_min = 0.005    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
    #electricityEU28_2015_gridFee_p10 = 0.016    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
    #electricityEU28_2015_gridFee_avg = 0.022    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
    electricityEU28_2015_gridFee_p90 = 0.026    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf
    #electricityEU28_2015_gridFee_max = 0.037    # https://ec.europa.eu/energy/sites/ener/files/documents/report_ecofys2016.pdf

    # ELECTRICITY TAXES AND LEVIES
    electricityEU28_2019_taxes_avg = electricityEU28_2019_S2_avg_incTax - electricityEU28_2019_S2_avg_exTax
    #electricityEU28_2019_taxes_min = 0.0083
    #electricityEU28_2019_taxes_max = 0.1707
    #electricityEU28_2019_taxes_p10 = 0.0162
    #electricityEU28_2019_taxes_p90 = 0.0797
    
    costs['elecGridfee'] = (electricityEU28_2015_gridFee_p90,'€ per kg')
    costs['elecWholesale'] = ((electricityEU28_2019_S2_avg_exTax - electricityEU28_2015_gridFee_p90),'€ per kg')
    costs['elecTaxLevies'] = (electricityEU28_2019_taxes_avg,'€ per kg')
    
    battery_PackPrice_BloombergNEF_2018 = 176. * meanUSDtoEUR  # https://about.bnef.com/blog/behind-scenes-take-lithium-ion-battery-prices/
    
    # BATTERY PRODUCTION COST
    costs['batteryProd'] = (battery_PackPrice_BloombergNEF_2018,'€ per kWhc')    
    
    ##########################################################################
       
    # HYDROGEN PRODUCTION COST
    #hydrogen_naturalgas_min_2018 = 0.9 * meanUSDtoEUR # IEA Hydrogen production costs by production source, 2018, IEA, Paris https://www.iea.org/data-and-statistics/charts/hydrogen-production-costs-by-production-source-2018
    #hydrogen_naturalgas_max_2018 = 3.2 * meanUSDtoEUR
    #hydrogen_naturalgas_ccus_min_2018 = 1.5 * meanUSDtoEUR
    #hydrogen_naturalgas_ccus_max_2018 = 2.9 * meanUSDtoEUR
    #hydrogen_coal_min_2018 = 1.1 * meanUSDtoEUR
    #hydrogen_coal_max_2018 = 2.2 * meanUSDtoEUR
    #hydrogen_renewable_min_2018 = 3.0 * meanUSDtoEUR
    #hydrogen_renewable_max_2018 = 7.5 * meanUSDtoEUR
    
    # HYDROGEN WHOLESALE PRICE
    #hydrogen_prod_IRENA2019_wind_best = 1.6 * meanUSDtoEUR # USD / kg * EUR / USD
    hydrogen_prod_IRENA2019_wind_low = 2.7 * meanUSDtoEUR # USD / kg * EUR / USD
    #hydrogen_prod_IRENA2019_solar_low = 3.3 * meanUSDtoEUR # USD / kg * EUR /  USD
    #hydrogen_prod_IRENA2019_wind_avg = 4.3 * meanUSDtoEUR # USD / kg * EUR / USD
    #hydrogen_prod_IRENA2019_solar_avg = 6.8 * meanUSDtoEUR # USD / kg * EUR / USD
    
    # HYDROGEN LOGISTICS
    #hydrogen_deliv_UCDavis_2008_min = 0.1 * meanUSDtoEUR # EUR / kg
    #hydrogen_deliv_UCDavis_2008_p10 = 1.0 * meanUSDtoEUR # EUR / kg
    hydrogen_deliv_UCDavis_2008_avg = 1.5 * meanUSDtoEUR # EUR / kg
    #hydrogen_deliv_UCDavis_2008_p90 = 2.0 * meanUSDtoEUR # EUR / kg
    #hydrogen_deliv_UCDavis_2008_max = 4.0 * meanUSDtoEUR # EUR / kg
    
    # HYDROGEN PRICE AT GAS STATION
    hydrogen_dispense_GER2020 = 9.5

    costs['hydrWholesale'] = (hydrogen_prod_IRENA2019_wind_low,'€ per kg')
    costs['hydrLogistics'] = (hydrogen_deliv_UCDavis_2008_avg,'€ per kg')
    costs['hydrDispenser'] = (hydrogen_dispense_GER2020,'€ per kg')

    # FUEL CELL STACK COST
    #pemfc_systemcost_2035_experts_min =  35. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
    #pemfc_systemcost_2020_experts_min =  45. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
    pemfc_systemcost_2020_experts_avg =  62. * meanUSDtoEUR  # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
    #pemfc_systemcost_2035_experts_max =  63. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
    #pemfc_systemcost_2020_experts_max = 100. * meanUSDtoEUR # € / kW     fuel cell system cost https://www.pnas.org/content/pnas/116/11/4899.full.pdf
    costs['fuelCellStack'] = (pemfc_systemcost_2020_experts_avg,'€ per kW')
    
    return costs

def loadEmissionData():
    emissions = {}

    emissions['keroseneCombustion'] = 3.16             # kg CO2e / kg Kerosene
    emissions['keroseneProduction'] = 0.45             # kg CO2e / kg Kerosene
    emissions['keroseneTransport'] = 0.35              # kg CO2e / kg Kerosene
    #emissions['keroseneCombustion'] = (3.16,'kg CO2e / kgKerosene')              # kg CO2e / kg Kerosene
    #emissions['keroseneProduction'] = (0.45,'kg CO2e / kgKerosene')               # kg CO2e / kg Kerosene
    #emissions['keroseneTransport'] = (0.35 ,'kg CO2e / kgKerosene')               # kg CO2e / kg Kerosene
    
    # EMISSIONS FROM RENEWABLE ELECTRICITY PRODUCTION
    #electricity_production_wind_Bonou_4MW_onshore = 0.006                  # kg CO2 / kWhe # Bonou et al
    #electricity_production_wind_Bonou_4MW_offshore = 0.011                 # kg CO2 / kWhe # Bonou et al
    #electricity_production_wind_Guezuraga_2MW_bestRecycling = 0.01         # kg CO2 / kWhe # Guezuraga et al.
    #electricity_production_wind_Guezuraga_2MW_worstRecycling = 0.017       # kg CO2 / kWhe # Guezuraga et al.
    electricity_production_wind_Guezuraga_2MW_worstRecycAndOps = 0.030      # kg CO2 / kWhe # Guezuraga et al.
    #electricity_production_wind_Ardente_2MW_worstRecycling = 0.015         # kg CO2 / kWhe # Ardente et al.
    #electricity_production_wind_Ardente_average = 0.015         # kg CO2 / kWhe # Ardente et al.
    #electricity_production_wind_Ardente_best = 0.09             # kg CO2 / kWhe # Ardente et al.
    #electricity_production_wind_Ardente_worst = 0.019           # kg CO2 / kWhe # Ardente et al.

    #electricity_production_solarRooftop_XYZ_worst = 0.041                 # Schlömer et al Technology-specific cost and performance parameters.

    #https://www.iea.org/data-and-statistics/charts/carbon-intensity-of-electricity-generation-in-selected-regions-in-the-sustainable-development-scenario-2000-2040
    # ref: IEA, Carbon intensity of electricity generation in selected regions in the Sustainable Development Scenario, 2000-2040, IEA, Paris https://www.iea.org/data-and-statistics/charts/carbon-intensity-of-electricity-generation-in-selected-regions-in-the-sustainable-development-scenario-2000-2040
    #electricity_production_scenarioIEA_2030_EU = 0.072
    #electricity_production_scenarioIEA_2030_US = 0.163
    #electricity_production_scenarioIEA_2030_WORLD = 0.237
    #electricity_production_scenarioIEA_2030_CN = 0.340


    emissions['elecProduction'] = electricity_production_wind_Guezuraga_2MW_worstRecycAndOps
    #emissions['elecProduction'] = (electricity_production_wind_Guezuraga_2MW_worstRecycAndOps,'kgCO2e per kWhe')



    # EMISSIONS FROM LIB BATTERY PACK PRODUCTION
    #battery_LIB_NickelManganeseCobalt_GraphiteAnode_LeVarlet = 201.               # kg CO2e / kWh usable storage
    #battery_LIB_IronPhosphate_GraphiteAnode_LeVarlet = 217.                       # kg CO2e / kWh usable storage
    #battery_LIB_ManganeseOxide_GraphiteAnode_LeVarlet = 220.                      # kg CO2e / kWh usable storage
    #battery_LIB_NickelCobaltAluminiumOxide_GraphiteAnode_LeVarlet = 225.          # kg CO2e / kWh usable storage
    #battery_LIB_NickelCobaltOxide_LiTitaniteOxideAnode_LeVarlet = 407.            # kg CO2e / kWh usable storage

    #battery_LIB_Emilsson2019_low = 61.       # kg CO2e / kWh usable storage
    battery_LIB_Emilsson2019_avg = 84.       # kg CO2e / kWh usable storage
   # battery_LIB_Emilsson2019_high = 146.     # kg CO2e / kWh usable storage

    # Emilsson & Dahlloef

    # https://www.carbonbrief.org/factcheck-how-electric-vehicles-help-to-tackle-climate-change

    # Regett et al
    # https://www.ffe.de/attachments/article/856/Carbon_footprint_EV_FfE.pdf
    #battery_LIB_Regett_low = 62.
    #battery_LIB_Regett_avg = 112.
    #battery_LIB_Regett_low = 212.
    

    emissions['batteryProduction'] = battery_LIB_Emilsson2019_avg
    #emissions['batteryProduction'] = (battery_LIB_Emilsson2019_low,'kgCO2e per kWhc')
    #emissions['batteryProduction'] = (battery_LIB_Emilsson2019_avg,'kgCO2e per kWhc')

    ##########################################################################
    
    # HYDROGEN PROCUCTION EMISSIONS FROM VARIOUS RENEWBALE SOURCES
    #hydrogen_prod_Spath_WindWindTurbines = 757.89     # g CO2 / kg H2
    #hydrogen_prod_Spath_WindElectrolysis = 42.05      # g CO2 / kg H2
    #hydrogen_prod_Spath_WindStorage = 169.23          # g CO2 / kg H2
    hydrogen_prod_Spath_WindTotal = 969.17
    
    #hydrogen_prod_Cetinkaya_SolarTotal = 2412         # g CO2 / kg H2

    #hydrogen_prod_Burkhardt_Total = 1919.0            # g CO2 / kg H2
    
    #hydrogen_prod_Cetinkaya_NatGasReforming = 11893.0
    
    #emissions['hydrProdTotal'] = (hydrogen_prod_Spath_WindTotal,'gCO2e per kgH2')
    emissions['hydrProdTotal'] = hydrogen_prod_Spath_WindTotal
    
    # - to be checked again with literature
    # HYDROGEN FUEL CELL EMISSIONS
    #fuelStack_production_SimonsBauer2012_exTank = 25.0           # kg CO2e / kWp  # Simons, A.; Bauer, C. A life-cycle perspective on automotive fuel cells. Applied Energy 2015
    #fuelStack_production_SimonsBauer2020_exTank = 17.0           # kg CO2e / kWp 
    #fuelStack_production_Notter_incMaintEoL = 28.0               # kg CO2e / kWp 
    
    #fuelStack_production_Tagliaferri_best = 25.0                 # kg CO2e / kWp 
    #fuelTank_production_Tagliaferri_best = 15.8                  # kg CO2e / kWp 
    #fuelMgmtSys_production_Tagliaferri_best = 7.6                # kg CO2e / kWp 
    
    #fuelStack_production_Tagliaferri_baseline = 43.0             # kg CO2e / kWp 
    fuelTank_production_Tagliaferri_baseline = 50.6               # kg CO2e / kWp  # not printed in the paper. I took measurements of bar lengths
    fuelMgmtSys_production_Tagliaferri_baseline = 7.6             # kg CO2e / kWp  # not printed in the paper. I took measurements of bar lengths
    
    fuelStack_StackProduction_2kW_Life_EcoInvent_GWP100 = 181.57 / 2.    # kg / kWp
    #fuelStack_StackProduction_2kW_Life_EcoInvent_GWP20 = 195.05 / 2.    # kg / kWp
        
    emissions['fuelStackProduction'] = fuelStack_StackProduction_2kW_Life_EcoInvent_GWP100
    emissions['hydrTankAndEquip'] = (fuelTank_production_Tagliaferri_baseline + fuelMgmtSys_production_Tagliaferri_baseline)
    #emissions['fuelStackProduction'] = (fuelStack_StackProduction_2kW_Life_EcoInvent_GWP100,'kgCO2e per kWp')
    #emissions['hydrTankAndEquip'] = ((fuelTank_production_Tagliaferri_baseline + fuelMgmtSys_production_Tagliaferri_baseline),'kgCO2e per kWp')    
    
    return emissions


#%%============================================================================

import copy as cp

def loadPredefCase(ident, dFuel):
    
    designs = {'volo':      {'pl': 0.055, 'ltod':  3.5, 'fStruct': 0.22, 'fOth': 0.4},
               'city':      {'pl': 0.059, 'ltod':  4.5, 'fStruct': 0.22, 'fOth': 0.4},
               'vahana':    {'pl': 0.055, 'ltod':  5.5, 'fStruct': 0.22, 'fOth': 0.4},
               'cora':      {'pl': 0.041, 'ltod':  8.5, 'fStruct': 0.22, 'fOth': 0.4},
               'hyundai':   {'pl': 0.045, 'ltod':  7.0, 'fStruct': 0.22, 'fOth': 0.4},
               'lilium':    {'pl': 0.022, 'ltod': 16.5, 'fStruct': 0.22, 'fOth': 0.4},
               'skai':      {'pl': 0.055, 'ltod':  4.5, 'fStruct': 0.22, 'fOth': 0.4}
               }
    
    missions = {'volo':     {'nPAX': 1, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange':  35000, 'cruiseVelo': 28., 'hoverTime': 1.5*60},
                'city':     {'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange':  50000, 'cruiseVelo': 33., 'hoverTime': 1.5*60},
                'vahana':   {'nPAX': 1, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange':  50000, 'cruiseVelo': 53., 'hoverTime': 1.5*60},
                'cora':     {'nPAX': 1, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange':  40000, 'cruiseVelo': 44., 'hoverTime': 1.5*60},
                'hyundai':  {'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange':  97000, 'cruiseVelo': 80., 'hoverTime': 1.5*60},
                'lilium':   {'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange': 300000, 'cruiseVelo': 83., 'hoverTime': 1.5*60},
                'skai':     {'nPAX': 4, 'nCrew': 1, 'minutesResCrs': 0., 'cruiseRange': 640000, 'cruiseVelo': 53., 'hoverTime': 1.5*60},
                }
    
    dDes = loadAircraftDesign(**{'ident'   : ident,
                                 'pl'      : designs[ident]['pl'],
                                 'ltod'    : designs[ident]['ltod'],
                                 'fStruct' : designs[ident]['fStruct'],
                                 'fOth'    : designs[ident]['fOth']})
    
    dMis = loadMissionReq({'ident'   : ident,
                           'nPAX'          : missions[ident]['nPAX'], 
                           'nCrew'         : missions[ident]['nCrew'],
                           'minutesResCrs' : missions[ident]['minutesResCrs']},
                          **{'cruiseRange'   : missions[ident]['cruiseRange'],
                             'cruiseVelo'    : missions[ident]['cruiseVelo'],
                             'hoverTime'     : missions[ident]['hoverTime']})
    
    dCase = {'design': dDes, 'mission': dMis, 'fuel': cp.deepcopy(dFuel)}
    
    return dCase

#%%============================================================================


def loadMissionReq(dReq, **kwargs):

    # I N P U T
    nPax = dReq['nPAX']
    nCrew = dReq['nCrew']
    minutesReserve = dReq['minutesResCrs']
    
    mPayload = 88 * nPax              # repr. for m/f ratio of 70/30 (94kg/75kg)
    mCrew = 80 * nCrew                # repr. for men (quite 1900s, find sth. better!)
    tReserve = 60 * minutesReserve    # regulation: 20mins in fwd flight
    
    mission = {'ident':       'BASELINE',     
               'cruiseRange': 100 * 1000,     # m
               'cruiseVelo':         45.,     # m/s
               'hoverTime':       3 * 60,     # s
               'massPayload':   mPayload,     # kg
               'massCrew':         mCrew,     # kg
               'nPax':              nPax,     # -
               'timeReserve':   tReserve}     # s   
    
    for key, arg in kwargs.items():
        mission[key] = arg
    
    return mission
    
def loadAircraftDesign(**kwargs):
    
    aircraft = {'ident':   'BASELINE',
                'pl':           0.025,         # N/W
                'ltod':           14.,         # -
#                'fPtr':           1.1, 
                'fStruct':       0.24,         # - (mStruc = fStruc * DGW)
                'fOth':           0.6}         # - (mOth = fOth * Use)

    for key, arg in kwargs.items():
        aircraft[key] = arg       

    return aircraft

def loadConfiguration(acType, **kwargs):
    
    from concepts.acType import getConceptSettings
    configuration = getConceptSettings()

    return configuration

def loadFueltype(fueltype, **kwargs):
    
    # from source.cornyFuelCell import calcFractionH2, calcStackSystemSpecPower
    # from source.cornyCooling import initCooling
    
    if fueltype == 'battery':
    
        fuel = {'fueltype':       fueltype,
                'ident':        'BASELINE',
                'nCycles':             750,          # -
                'esp':             260/1.2,          # Wh/kg (pack specific = cell specific / 1.2) 
                'eta':                None,          # - (motor efficiency)
                'eta_mtr':            0.97,
                'eta_dis':             0.9,
                #'voltage':            550.,          # V 425 - 650 but doesnt have a direct influence on weight if C-rate arbitrary
                'dcp':                1.03,          # - (battery discharge parameter - see L.Traub )
                'motorError':            1,          # - (factor multiplied on the electric motor weight)
                'cRate':               15.,
                'fDoD':               0.78}
        
    #     fuel['eta'] = fuel['eta_dis'] * fuel['eta_mtr']
        
        
    # elif fueltype == 'fuelcell':

    #     fuel = {'fueltype':           fueltype,
    #             'ident':            'BASELINE',
    #             'fractionH2T':            None,
    #             #'specEnergyFC':           1.6,         # kW / kg
    #             'pSpec':                  None,         # kW / kg
    #             'lhv':                  33.322,         # kWh / kg
    #             'eta_fc':                 0.59,         # -
    #             'eta_mtr':                 1.0,         # -
    #             'motorError':                1,         # - (factor multiplied on the electric motor weight)
    #             'opHours':               5000., 
    #             'eta_ct':                  0.8,
    #             'eta_elysis':             0.65, 
    #             'cylinderPressure':        350, 
    #             'cooling type': 'phase change',
    #             'specPowerStack':         2.23,   # kW/kg (according to Ning and He,2017)
    #             'specPowerPDU':           5.33,   # kW/kg (https://docs.oracle.com/cd/E19657-01/html/E23956/z40000081397550.html)
    #             'specPowerVentilation':   None,   # Will be calculated by code}        
    #             }   
        
    #     fuel = initCooling(fuel)
    #     fuel = calcFractionH2(fuel)
    #     fuel = calcStackSystemSpecPower(fuel)
        
    elif fueltype == 'hybrid':
        fuel = {'fueltype':   fueltype,
                'highPowSys':     None,
                'highEnerSys':    None,
                'fHybrid':        None,
                'calcOptHybr':    True}

    elif fueltype == 'hybridBattery':
        fuel = {'fueltype':          fueltype,
                'highEnerSys':      None,
                'highPowSys':       None,
                'fHybrid':               None,
                'calcOptHybr':           True}
    
    elif fueltype == 'kerosene':
        
        fuel = {'fueltype':      'kerosene',
                'ident':         'BASELINE', 
                'sfc':                0.365,
                'nEng':                   1,
                'fEng':                  1.,
                'fSyn':                 1.0}

    # elif fueltype == 'hybridTurboshaft':
        
    #     fuel = {'fueltype':    fueltype,
    #             'battery':         None,
    #             'kerosene':        None,
    #             'fHybrid':         None}
    
    #elif fueltype == 'hybrid':
    
    else:
        print('No fueltype specified!')
    
    for key, arg in kwargs.items():
        fuel[key] = arg       

    return fuel