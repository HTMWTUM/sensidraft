#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 07:41:27 2020

@author: Nicolas
"""

import numpy as np
from scipy.stats import norm


def sensitivityAnalysisOAT(fun, dCase, dSample):
    """ Framework for One-at-a-Time sampling sensitivity analysis
    
    Parameters
    ----------
    fun : scalar function
        The function to be evaluated in the Contour Analysis
    dCase : dictionary
        The data container.
    dSample : dictionary
        Sample container. Keys must be identical to dData.

    Returns
    -------
    dResult : dictionary
        Result values for each sampled key

    """
    
    dResult = {}
    for k0, v0 in dSample.items():
        dResult[k0] = {}
        #print('\n# Variation of', k0, 'parameters')
        for k1, v1 in v0.items():
            #print('\n## Variation of', k1)
            
            # check whether to go even one stage further, i.e. if multiple fuels are defined or mission segments
            if type(v1) is dict:
                #print(k0, k1, 'holds a dict')
                dResult[k0][k1] = {}
                for k2, v2 in v1.items():
                    #print('\n',k2,v2)
                    #print('values are', v2)
                    listRes = []
                    for i, value in enumerate(v2):
                        baseline = dCase[k0][k1][k2]
                        dCase[k0][k1][k2] = value
                        listRes.append(fun(dCase))
                        dCase[k0][k1][k2] = baseline
                        #if dCase['fuel']['fueltype'] == 'battery': print(listRes[-1])
                    #print(k0,k1,k2, listRes)
                    dResult[k0][k1][k2] = listRes                        
            
            else:
                #print(k0, k1, 'doesnt hold a dict')
                listRes = []
                for i, value in enumerate(v1):
                    baseline = dCase[k0][k1]
                    dCase[k0][k1] = value
                    listRes.append(fun(dCase))
                    dCase[k0][k1] = baseline
                    #if dCase['fuel']['fueltype'] == 'battery': print(listRes[-1])
                #print(k0,k1,listRes)
                dResult[k0][k1] = listRes     
        # print('---')
        # print(dResult, v0)
        # print()               
    return dResult


# def sensitivityAnalysisOAT(fun, dCase, dSample):
#     """ Framework for One-at-a-Time sampling sensitivity analysis
    
#     Parameters
#     ----------
#     fun : scalar function
#         The function to be evaluated in the Contour Analysis
#     dCase : dictionary
#         The data container.
#     dSample : dictionary
#         Sample container. Keys must be identical to dData.

#     Returns
#     -------
#     dResult : dictionary
#         Result values for each sampled key

#     """
    
#     dResult = {}
#     for k0, v0 in dSample.items():
#         dResult[k0] = {}
#         #print('# Variation of', k0, 'parameters')
#         for k1, v1 in v0.items():
#             #print('## Variation of', k1)
#             listRes = []
#             for i, value in enumerate(v1):
#                 baseline = dCase[k0][k1]
#                 dCase[k0][k1] = value
#                 listRes.append(fun(dCase))
#                 dCase[k0][k1] = baseline
#                 #if dCase['fuel']['fueltype'] == 'battery': print(listRes[-1])
#             dResult[k0][k1] = listRes                        
#     return dResult


def sensitivityBarplotOAT(fun, dArgs, dVariations, lstKeys):
    """ Framework for One-at-a-Time sampling sensitivity analysis
    
    Parameters
    ----------
    fun : scalar function
        The function to be evaluated in the Contour Analysis
    dCase : dictionary
        The data container.
    dSample : dictionary
        Sample container. Keys must be identical to dData.
    dKeys : dictionary
    
    Returns
    -------
    dResult : dictionary
        Result values along the specified keys
    sVar : list of strings
        The names of the parameters with variability

    """

    dVar = {}
    dResult = {}
    sVar = []
    
    for i, key in enumerate(lstKeys):
        lstVals = []   
        for k0, v0 in dVariations.items(): 
            for k1, v1 in v0.items():
                if type(v1) is dict:
                    for k2,v2 in v1.items():
                        #print(i, key, k0, k1, k2, v2[i])
                        if i == 0: sVar.append(k2)
                        lstVals.append((k0, k1, k2, v2[i]))
                else:
                    #print(i, key, k0, k1, v1[i])
                    if i == 0: sVar.append(k1)
                    lstVals.append((k0, k1, v1[i]))
                #print('\n',k0,k1,v1)
            
            dVar.update(**{key: lstVals})

    for key, value in dVar.items():
        #print(key, value, len(value))
        results = []
        for vals in value:
            #print(vals, len(vals))
            if len(vals) == 4:
                #print('do sth new')
                baseline = dArgs[vals[0]][vals[1]][vals[2]]
                dArgs[vals[0]][vals[1]][vals[2]] = vals[3]
                results.append(fun(dArgs))
                dArgs[vals[0]][vals[1]][vals[2]] = baseline
            if len(vals) == 3:
                #print(vals[0], vals[1], vals[2])
                baseline = dArgs[vals[0]][vals[1]]
                dArgs[vals[0]][vals[1]] = vals[2]
                results.append(fun(dArgs))
                dArgs[vals[0]][vals[1]] = baseline
        dResult[key] = results
    # print(dResult)
    
    #print(dVar, sVar)
    
    return dResult, dVar, sVar

    # dVar = {}
    # dResult = {}
    # sVar = []
    
    # for i, key in enumerate(lstKeys):
    #     lstVals = []   
    #     for k0, v0 in dVariations.items(): 
    #         for k1, v1 in v0.items():
    #             #print(i, key, k0, k1, v1[i])
    #             if i == 0: sVar.append(k1)
                
    #             lstVals.append((k0, k1, v1[i]))
    #             #print('\n',k0,k1,v1)
            
    #         dVar.update(**{key: lstVals})

    # for key, value in dVar.items():
    #     results = []
    #     for vals in value:
    #         #print(vals[0], vals[1], vals[2])
    #         baseline = dArgs[vals[0]][vals[1]]
    #         dArgs[vals[0]][vals[1]] = vals[2]
    #         results.append(fun(dArgs))
    #         dArgs[vals[0]][vals[1]] = baseline
    #     dResult[key] = results
    
    # return dResult, dVar, sVar

def contourAnalysis2D(fun, dCase, tupX, tupY):
    """ Provide data for a 2D contour plot
    
    Parameters
    ----------
    fun : scalar function
        The function to be evaluated in the Contour Analysis
    dCase : dictionary
        The data container.
    tupX : tuple
        Contains the keys (identifiers) values (linspace) of variaion in x 
    tupY : tuple
        Contains the keys (identifiers) values (linspace) of variaion in y 

    Returns
    -------
    matX, matY, matZ : arrays
        Matrices including the data points for plotting the result

    """
    
    kX0 = tupX[0]
    kX1 = tupX[1]
    lspX = tupX[2]
    
    kY0 = tupY[0]
    kY1 = tupY[1]
    lspY = tupY[2]
    
    matX, matY = np.meshgrid(lspX, lspY)
    matZ = np.zeros((len(lspX), len(lspY)))
    
    for i, (lx, ly) in enumerate(zip(matX, matY)):
        for j, (x, y) in enumerate(zip(lx, ly)):
            # set Baseline
            baseX = dCase[kX0][kX1]
            baseY = dCase[kY0][kY1]
            # set and do Case
            dCase[kX0][kX1] = x
            dCase[kY0][kY1] = y
            #rint(dCase['design'])
            matZ[i][j] = fun(dCase)
            #print(x, y,  matZ[i][j])

            # reset Baseline
            dCase[kX0][kX1] = baseX
            dCase[kY0][kY1] = baseY            
    
    return matX, matY, matZ

def singleMaterialSample(dData):
    """ Calculate and return a single material fraction sample so that the overall
    sum of fractions remains to equal 1.0
    
    Parameters
    ----------
    dData : dictionary
        The data container. Needs to contain 'materials' and 'matStDev'

    Returns
    -------
    dResult : dictionary
        The fractions of materials for each key in dData['materials']

    """

    dMaterials = dData['materials']
    dDeviations = dData['matStDev']
    
    #print(dMaterials)
    
    d1, d2 = {}, {}
    # v represents fraction
    for k, v in dMaterials.items():
        d1[k] = norm.rvs(loc=v, scale=v*dDeviations[k])
        d2[k] = norm.rvs(loc=v, scale=v*dDeviations[k])
    
    #print('1',d1, sum(d1.values())) 
    #print('2',d2, sum(d2.values()))
    
    dMaterialSample = {}
    for k, v in dMaterials.items():
        dMaterialSample[k] = d1[k] + d2[k] * (1.0 - sum(d1.values())) / sum(d2.values())
        if dMaterialSample[k] < 0.0: print('WARNING: Sampled material fraction is below 0.0')
     
    #print(dMaterialSample, sum(dMaterialSample.values()))
    
    return dMaterialSample

def getMaterialSamples(dData, iS):
    """ Calculate and return a the full sample of iS material fractions so that 
    the overall sum of fractions remains to equal 1.0
    
    Parameters
    ----------
    dData : dictionary
        The data container. Needs to contain 'materials' and 'matStDev'
    iS : integer
        Number of sample points

    Returns
    -------
    dResult : dictionary
        Holds a list for each key in dData['materials']

    """

    dMaterials = dData['materials']
   
    dMaterialSamples = {}
    
    for i in range(0,iS):
        dOneSample = singleMaterialSample(dData)
        #print(i+1, iS, dOneSample)
        
        for k, v in dMaterials.items():
            if i == 0:
                dMaterialSamples[k] = []
                dMaterialSamples[k].append(dOneSample[k])
            else:
                dMaterialSamples[k].append(dOneSample[k])
    
    #[print(key, ms) for key, ms in dMaterialSamples.items()]
        
    return dMaterialSamples


################################################################################

def getComponentMaterialSamples(dData, iS):

    dComponents = dData['components']
   
    dComponentMaterialSamples = {}
    
    for comp, dMaterials in dComponents.items():
        dComponentMaterialSamples[comp] = {}
        for mat in dMaterials.keys():
            dComponentMaterialSamples[comp][mat] = []
    
    for i in range(0,iS):
        dOneSample = singleComponentMaterialSample(dData)
        #print(i+1, iS, dOneSample)
        
        for comp, dMaterials in dComponents.items():
            for mat in dMaterials.keys():
                #print(comp, mat)
                dComponentMaterialSamples[comp][mat].append(dOneSample[comp][mat])


    #[print(key, ms) for key, ms in dMaterialSamples.items()]
        
    return dComponentMaterialSamples

###############################################################################

def singleComponentMaterialSample(dData):
    
    dComponents = dData['components']
    dDeviations = dData['matStDev']
    
    dComponentMaterialSample = {}
    # v represents fraction
    for comp, dMaterials in dComponents.items():
        d1, d2 = {}, {}
        for mat, fraction in dMaterials.items():
            d1[mat] = norm.rvs(loc=fraction, scale = fraction * dDeviations[comp][mat])
            d2[mat] = norm.rvs(loc=fraction, scale = fraction * dDeviations[comp][mat])
    
    #print('1',d1, sum(d1.values())) 
    #print('2',d2, sum(d2.values()))
    
        dComponentMaterialSample[comp] = {}
        for mat in dMaterials.keys():
            dComponentMaterialSample[comp][mat] = d1[mat] + d2[mat] * (1.0 - sum(d1.values())) / sum(d2.values())
            if dComponentMaterialSample[comp][mat] < 0.0: print('WARNING: Sampled material fraction is below 0.0')   
    
    return dComponentMaterialSample
    